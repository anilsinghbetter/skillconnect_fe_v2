import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';

import { store } from './_helpers';
import { App } from './App';

//import 'bootstrap/dist/css/bootstrap.css';

//Already added  css files in PUBLIC folder.
/*
import './assets/css/betterplace.css';
import './assets/css/betterplace-theme.css';
import './assets/css/custom-dev.css';
*/
// setup fake backend
//import { configureFakeBackend } from './_helpers';
//configureFakeBackend();

render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('app')
);