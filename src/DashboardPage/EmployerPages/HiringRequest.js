import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Header } from '../../_components/Header';
import { Footer } from '../../_components/Footer';
import { Sidebar } from '../../_components/Sidebar';
import { userActions, alertActions } from '../../_actions';
import classnames from 'classnames';
import { history } from '../../_helpers';
import hiringRequestSVG from '../../assets/images/hiring_requests_grey.svg';

import { Input, Form, FormGroup, TabContent, TabPane, Nav, NavItem, NavLink, Card, Button, CardTitle, Row, Col, Breadcrumb, BreadcrumbItem, Badge, CardHeader, ListGroup, ListGroupItem, Modal, ModalBody, ModalHeader } from 'reactstrap';
class EmployerHiringRequest extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isOpen: false,
            modalIndexValue: undefined,
            cancelCardValue: undefined,
            reasonSelected: '',
            activeTab: '1',
            hiringrequest_status: 1,
            checked: false,
            positionChecked: [],
            selectAll: false,
            page: 1,
        };
        this.toggleModal = this.toggleModal.bind(this);
        this.toggle = this.toggle.bind(this);
        this.handleCheckboxSelection = this.handleCheckboxSelection.bind(this);
        this.toggleModalforCancel = this.toggleModalforCancel.bind(this);
        this.handleSelectAll = this.handleSelectAll.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleCancelRequestChange = this.handleCancelRequestChange.bind(this);
        this.saveCancelRequestForm = this.saveCancelRequestForm.bind(this);
    }

    toggleModal = (e) => {
        this.setState({ isOpen: !this.state.isOpen, cancelCardValue: undefined });
        this.setState({ modalIndexValue: e });
    }
    toggleModalforCancel = (e) => {
        this.setState({ isOpen: !this.state.isOpen, modalIndexValue: undefined });
        this.setState({ cancelCardValue: e, reasonSelected: '' });
    }
    toggle(tab) {
        //const { hiringrequest_status } = this.state;
        if (this.state.activeTab !== tab) {
            this.state.hiringrequest_status = tab;

            this.setState({
                activeTab: tab,
                modalIndexValue: undefined,
                page: 1
            });
        }
        history.push({
            pathname: '/Employer-hiring-requests/'+tab,
            state: { value :this.state.hiringrequest_status }
        })

        this.getHiringRequests(1);//page = 1
    }
    saveCancelRequestForm(e) {
        e.preventDefault();

        if (this.state.reasonSelected !== '' && this.state.reasonSelected !== undefined && this.state.cancelCardValue !== undefined && this.state.cancelCardValue !== '') {
            this.props.dispatch(userActions.updateHiringRequestStatus(this.state.cancelCardValue, 5, this.state.hiringrequest_status, this.state.reasonSelected, 1));
            this.setState({ isOpen: !this.state.isOpen });
            this.setState({ cancelCardValue: undefined });
            this.setState({ reasonSelected: '' });
            this.state.page = 1;
        } else {
            this.props.dispatch(alertActions.error("Please select appropriate reason to cancel this request."));
        }
    }
    componentDidMount() {
        this.getHiringRequests(1);//page = 1
    }
    getHiringRequests(pageno) {
        if (localStorage.getItem('user') !== null) {
        if (this.props.user.user_type === 'EMP') var trainingpartnerid = this.props.user.data[0].industrypartnerid
        this.props.dispatch(userActions.getHiringRequests(trainingpartnerid, this.state.hiringrequest_status, 'Employer', pageno));
        }
        else{
            history.push('/login');
        return ('');
        }
}

    handleSubmit(e) {
        e.preventDefault();
        const { user } = this.state;
        var hiringrequestid = this.state.positionChecked.join();
        // console.log('Send data',this.state.positionChecked.join());
        //console.log("Selection part",this.state.user,hiringrequestid);
        var mergedKeys = { hiringrequestid, ...user };
        if (mergedKeys.hiringrequestid !== undefined && mergedKeys.hiringrequestid !== '' && mergedKeys.internalstatus !== undefined && mergedKeys.internalstatus !=='') {
            //Update Status based on the selection..
            this.props.dispatch(userActions.updateHiringStatus(mergedKeys));
            this.setState({ positionChecked: [], page: 1, selectAll: false });
            //console.log('Send data', this.state.positionChecked);
        } else {
            this.props.dispatch(alertActions.error("Please select appropriate status and select candidate to proceed."));
        }
    }
    handleCheckboxSelection(e) {
        // current array of options

        const checkboxArray = this.state.positionChecked
        let index

        // check if the check box is checked or unchecked
        if (e.target.checked) {
            // add the numerical value of the checkbox to options array
            checkboxArray.push(+e.target.value)
        }
        else if (this.state.selectAll === true) {
            this.setState({ selectAll: false })
            index = checkboxArray.indexOf(+e.target.value)
            checkboxArray.splice(index, 1)
        }
        else {
            // or remove the value from the unchecked checkbox from the array
            index = checkboxArray.indexOf(+e.target.value)
            checkboxArray.splice(index, 1)
        }
        // console.log('options checked', checkboxArray);
        // update the state with the new array of options
        this.setState({ positionChecked: checkboxArray })

    }
    handleCancelRequestChange(event) {
        this.setState({ reasonSelected: event.target.value });
    }
    handleChange(event) {
        const { name, value } = event.target;
        const { user } = this.state;

        this.setState({
            user: {
                ...user,
                [name]: value
            }
        });
    }
    handleSelectAll(e) {
        this.setState({ selectAll: e.target.checked },/*  () => console.log('selected all', this.state.selectAll) */);
        var rowState = [];
        if (e.target.checked) {
            if (!!this.props.allhiringrequests && this.props.allhiringrequests.data !== undefined && this.props.allhiringrequests.data.length > 0) {
                for (var i = 0; i < this.props.allhiringrequests.data.length; i++) {
                    rowState.push(this.props.allhiringrequests.data[i].hiringrequestdetailid);
                }
            }
        }
        this.setState({ positionChecked: rowState })
    };
    loadData() {
        const { page } = this.state
        this.getHiringRequests(page);
    }
    loadMore = () => {
        //First update the state and then use callback method to call next page call...
        this.setState(prevState => ({
            page: prevState.page + 1
        }), this.loadData)
    }

    render() {
        var loaderClass = '';
        //Show loader..
        if (this.props.loading === true) {
            loaderClass = 'loading';
        }
        const styles = {
            top: '115px',
            left: '60px'
        };
        var ErrorLable = "";

        if (this.state.activeTab === '1') ErrorLable = "Pending"
        if (this.state.activeTab === '2') ErrorLable = "Accepted"
        if (this.state.activeTab === '3') ErrorLable = "Completed"
        if (this.state.activeTab === '4') ErrorLable = "Declined"
        if (this.state.activeTab === '5') ErrorLable = "Cancelled"
        // console.log('afte selectall what is the checbox val', this.state.testval);
        // console.log("Updated CHECKALL VALUE",this.state.hideSelectAll);
        //console.log("data from Emp Hiring Request", this.props);
        var final_hiringrequests = [];

        var loadMore = "";
        if (!!this.props.allhiringrequests && this.props.allhiringrequests.paginationOptions !== undefined) {

            var pendingCount = this.props.allhiringrequests.paginationOptions.totalPendingHiringRequests;
            var acceptedCount = this.props.allhiringrequests.paginationOptions.totalAcceptedHiringRequests;
            var completedCount = this.props.allhiringrequests.paginationOptions.totalCompletedHiringRequests;
            var declinedCount = this.props.allhiringrequests.paginationOptions.totalDeclinedHiringRequests;
            var cancelledCount = this.props.allhiringrequests.paginationOptions.totalCancelledHiringRequests;

            var totalpages = this.props.allhiringrequests.paginationOptions.totalPages;
            var currentpage = this.props.allhiringrequests.paginationOptions.curPage;
            if (currentpage < totalpages) {
                loadMore = (<div className="text-center"><a title="Load More" onClick={this.loadMore} className="btn btn-outline-secondary card-link">Load More</a></div>);
            }

        }
        if (!!this.props.allhiringrequests && this.props.allhiringrequests.data !== undefined && this.props.allhiringrequests.data.length > 0) {
            var hiringrequests = this.props.allhiringrequests.data;

            //Cancel request Modal Box
            if (this.state.cancelCardValue !== undefined) {
                var cancelmymodaval = this.state.cancelCardValue;
                var CancelRequestModal = (
                    <Modal backdrop={false} centered isOpen={this.state.isOpen} toggle={() => this.toggleModalforCancel(cancelmymodaval)}>
                        <ModalHeader toggle={() => this.toggleModalforCancel(cancelmymodaval)} className="border-bottom-0 modal-header-shadow align-items-center">
                            <div className="section-title">
                                <h5 className="title modal-title">Cancel Request</h5>
                            </div>
                        </ModalHeader>
                        <ModalBody>
                            <Form onSubmit={this.saveCancelRequestForm}>
                                <FormGroup>
                                    <label className="custom-radio-btn">
                                        <input type="radio" name="cancelRequest" value="Training partner late in responding." onChange={this.handleCancelRequestChange} />
                                        <span className="radiocheck">Training partner late in responding.</span>
                                    </label>
                                </FormGroup>
                                <FormGroup>
                                    <label className="custom-radio-btn">
                                        <input type="radio" name="cancelRequest" value="Requested by mistake." onChange={this.handleCancelRequestChange} />
                                        <span className="radiocheck">Requested by mistake.</span>
                                    </label>
                                </FormGroup>
                                <FormGroup>
                                    <label className="custom-radio-btn">
                                        <input type="radio" name="cancelRequest" value="Hired from Somewhere else." onChange={this.handleCancelRequestChange} />
                                        <span className="radiocheck">Hired from Somewhere else.</span>
                                    </label>
                                </FormGroup>
                                <Button color="primary">Submit</Button>
                            </Form>
                        </ModalBody>
                    </Modal>
                );
            }
            //End of Cancel request Modal Box

            if (this.state.modalIndexValue !== undefined) {
                var mymodaval = this.state.modalIndexValue;
                
                var tel = hiringrequests[this.state.modalIndexValue].contact_person_phone ? "tel:" + hiringrequests[this.state.modalIndexValue].contact_person_phone : ''
                var mailTo = hiringrequests[this.state.modalIndexValue].contact_person_email ? "mailto:" + hiringrequests[this.state.modalIndexValue].contact_person_email : ''
                var location = hiringrequests[this.state.modalIndexValue].cityname + ', ' + hiringrequests[this.state.modalIndexValue].statename;
                var interviewdatetime = hiringrequests[this.state.modalIndexValue].interview_date + ", " + hiringrequests[this.state.modalIndexValue].interview_time;
                var other_perks = hiringrequests[this.state.modalIndexValue].other_perks  ? hiringrequests[this.state.modalIndexValue].other_perks : '-';
                var other_information = hiringrequests[this.state.modalIndexValue].other_information !== '' ? hiringrequests[this.state.modalIndexValue].other_information : '-';
                var pf = hiringrequests[this.state.modalIndexValue].is_pf === 'yes' ? 1 : 0;
                var esic = hiringrequests[this.state.modalIndexValue].is_esic === 'yes' ? 1 : 0;
                var food = hiringrequests[this.state.modalIndexValue].is_food === 'yes' ? 1 : 0;
                var transportation = hiringrequests[this.state.modalIndexValue].is_transportation === 'provided' ? 1 : 0;
                var accomodation = hiringrequests[this.state.modalIndexValue].is_accomodation === 'provided' ? 1 : 0;
                var Benefits = '';
                if (pf === 1 || esic === 1 || food === 1 || transportation === 1 || accomodation === 1) {
                    Benefits = (
                        <Col xs="12">
                            <label className="subtitle">Benefits</label>
                            <p className="title">
                                {pf === 1 &&
                                    <Badge color="default" >Pf</Badge>
                                }
                                {esic === 1 &&
                                    <Badge color="default" >Esic</Badge>
                                }
                                {food === 1 &&
                                    <Badge color="default" >Food</Badge>
                                }
                                {transportation === 1 &&
                                    <Badge color="default" >Transportation</Badge>
                                }
                                {accomodation === 1 &&
                                    <Badge color="default" >Accomodation</Badge>
                                }
                            </p>
                        </Col>
                    );
                }



                var hiringRequestsModalDetails = (
                    <Modal backdrop={false} centered size="lg" isOpen={this.state.isOpen} toggle={() => this.toggleModal(mymodaval)}>
                        <ModalHeader toggle={() => this.toggleModal(mymodaval)} className="border-bottom-0 modal-header-shadow align-items-center">
                            <div className="section-title">
                                <h5 className="title modal-title">{hiringrequests[this.state.modalIndexValue].candidate_name}</h5>
                            </div>
                        </ModalHeader>
                        <ModalBody className="details-view">
                            <Row>
                                <Col xs="12">
                                    <h5 className="title modal-view-title">Job Details</h5>
                                </Col>
                                <Col xs="12">
                                    <label className="subtitle">Training Partner</label>
                                    <p className="title">{hiringrequests[this.state.modalIndexValue].training_partner_name}</p>
                                </Col>
                                <Col xs="12">
                                    <label className="subtitle">Jobrole</label>
                                    <p className="title">{hiringrequests[this.state.modalIndexValue].jobrolename}</p>
                                </Col>
                                <Col xs="12" md="6">
                                    <label className="subtitle">Location</label>
                                    <p className="title">{location}</p>
                                </Col>
                                <Col xs="12" md="6">
                                    <label className="subtitle">Salary</label>
                                    <p className="title">{hiringrequests[this.state.modalIndexValue].salary}</p>
                                </Col>
                                <Col xs="12" md="6">
                                    <label className="subtitle">Other Perks</label>
                                    <p className="title">{other_perks}</p>
                                </Col>
                                <Col xs="12" md="6">
                                    <label className="subtitle">Other Information</label>
                                    <p className="title">{other_information}</p>
                                </Col>
                                {Benefits}

                            </Row>
                            <hr className="modal-data-seperator" />
                            <Row>
                                <Col xs="12">
                                    <h5 className="title modal-view-title">Interview Details</h5>
                                </Col>
                                <Col xs="12">
                                    <label className="subtitle">Date &amp; Time</label>
                                    <p className="title">{interviewdatetime}</p>
                                </Col>
                                {/*  <Col xs="12">
                                    <label className="subtitle">Other Information</label>
                                    <p className="title mb-1">{hiringrequests[this.state.modalIndexValue].other_information}</p>
                                </Col> */}
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">Contact Person</label>
                                    <p className="title">{hiringrequests[this.state.modalIndexValue].contact_person_name}</p>
                                </Col>
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">Mobile</label>
                                    <p className="title">
                                        {tel === '' ? 'NA' :
                                            <a href={tel} title={hiringrequests[this.state.modalIndexValue].contact_person_phone}>{hiringrequests[this.state.modalIndexValue].contact_person_phone}</a>
                                        }
                                    </p>
                                </Col>
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">Email</label>
                                    <p className="title">
                                        {mailTo === '' ? 'NA' :
                                            <a href={mailTo} title={hiringrequests[this.state.modalIndexValue].contact_person_email}>{hiringrequests[this.state.modalIndexValue].contact_person_email}</a>
                                        }
                                    </p>
                                </Col>
                            </Row>
                        </ModalBody>
                    </Modal>
                );
            }

            hiringrequests.map((value, index) => {
                /* var name = hiringrequests[index].candidate_name;
                var initials = name.match(/\b\w/g) || [];
                initials = ((initials.shift() || '') + (initials.pop() || '')).toUpperCase(); */
                var interviewDateTime = hiringrequests[index].interview_date + ", " + hiringrequests[index].interview_time;
                var hiringRequestStatusDb = hiringrequests[index].hiring_request_status;
                var internalStatus = hiringrequests[index].internal_status;
                var StatusLable = "";

                if (internalStatus === 1) StatusLable = "Available"
                if (internalStatus === 2) StatusLable = "Not Available"
                if (internalStatus === 3) StatusLable = "Not Reachable"
                if (internalStatus === 4) StatusLable = "Hired"
                if (internalStatus === 5) StatusLable = "Joined"
                if (internalStatus === 6) StatusLable = "Left"
                if (internalStatus === 7) StatusLable = "Interviewed But Not Hired"
                if (internalStatus === 8) StatusLable = "No Show"


                return final_hiringrequests.push(
                    <Col xs="12" md="6" xl="4" key={index}>

                        <Card className="card z-depth-1">
                            <CardHeader className="d-flex justify-content-between bg-transparent align-items-center border-bottom-0 card-header-shadow">
                                <div className="section-title">
                                    <CardTitle className="title card-title">{hiringrequests[index].candidate_name}</CardTitle>
                                    <ul className="list-inline list-inline-separator">
                                        <li className="list-inline-item">{hiringrequests[index].statename}</li>
                                        <li className="list-inline-item"> {hiringrequests[index].cityname}</li>
                                    </ul>
                                </div>
                                {hiringRequestStatusDb === 2 &&
                                    <label className="custom-checkbox">
                                        <input
                                            onChange={this.handleCheckboxSelection}
                                            checked={(this.state.page < 2 && this.state.selectAll) || (this.state.positionChecked.some(i => i === hiringrequests[index].hiringrequestdetailid))}
                                            key={index}
                                            type="checkbox"
                                            value={hiringrequests[index].hiringrequestdetailid}
                                        />
                                        <span className="checkmark"></span>
                                    </label>

                                }
                            </CardHeader>
                            <ListGroup className="no-border">
                                {hiringRequestStatusDb === 2 &&
                                    <ListGroupItem>
                                        <Badge color="secondary">{StatusLable}</Badge>
                                    </ListGroupItem>
                                }
                                <ListGroupItem>
                                    <label className="subtitle">Training Partner</label>
                                    <p className="title mb-0">{hiringrequests[index].training_partner_name}</p>
                                </ListGroupItem>
                                <ListGroupItem>
                                    <label className="subtitle">Sector</label>
                                    <p className="title mb-0">{hiringrequests[index].sectorname}</p>
                                </ListGroupItem>
                                <ListGroupItem>
                                    <label className="subtitle">Job Role</label>
                                    <p className="title mb-0">{hiringrequests[index].jobrolename}</p>
                                </ListGroupItem>
                                <ListGroupItem>
                                    <label className="subtitle">Interview on</label>
                                    <p className="title mb-0">{interviewDateTime}</p>

                                </ListGroupItem>

                                {hiringRequestStatusDb === 5 &&
                                    <ListGroupItem className="info-tile-wrapper">
                                        <p className="m-0 d-inline-block align-middle danger-info"><i className="material-icons align-middle">error</i>{hiringrequests[index].cancel_reason}</p>
                                    </ListGroupItem>
                                }

                                <ListGroupItem className="d-flex">
                                    <Col className="pl-0 align-self-center">
                                        <Button color="primary" className="btn-raised mr-2" onClick={() => this.toggleModal(index)}>View More</Button>
                                        {hiringRequestStatusDb === 1 &&
                                            <Button outline color="danger" onClick={() => this.toggleModalforCancel(hiringrequests[index].hiringrequestdetailid)}>Cancel</Button>
                                        }
                                    </Col>
                                    {hiringRequestStatusDb === 1 &&
                                        <Col xs="auto" className="order-last action-btn px-0 pt-0">
                                            <Link to={`/edit-candidate-detail/#id:${hiringrequests[index].candidatedetailid}`} title="Edit" className="btn-fab btn-fab-default btn btn-sm">
                                                <i className="svg-icons">
                                                    <svg viewBox="0 0 24 24">
                                                        <g id="Bounding_Boxes">
                                                            <g id="ui_x5F_spec_x5F_header_copy_3"></g>
                                                            <path fill="none" d="M0,0h24v24H0V0z" />
                                                        </g>
                                                        <g id="Outline_1_">
                                                            <g id="ui_x5F_spec_x5F_header_copy_4"></g>
                                                            <path id="XMLID_37_" d="M14.06,9.02l0.92,0.92L5.92,19H5v-0.92L14.06,9.02 M17.66,3c-0.25,0-0.51,0.1-0.7,0.29l-1.83,1.83 l3.75,3.75l1.83-1.83c0.39-0.39,0.39-1.02,0-1.41l-2.34-2.34C18.17,3.09,17.92,3,17.66,3L17.66,3z M14.06,6.19L3,17.25V21h3.75 L17.81,9.94L14.06,6.19L14.06,6.19z" />
                                                        </g>
                                                    </svg>
                                                </i>
                                            </Link>
                                        </Col>
                                    }
                                </ListGroupItem>

                            </ListGroup>
                        </Card>

                    </Col>


                );
            })
        }
        return (

            <div className="content-wrapper user-flow TP-flow">
                <Sidebar {...this.props} />

                <div id="content" className="main-content">
                    <Header />

                    <div className="d-block d-md-flex align-items-center section-header">
                        <div>
                            <h1 className="page-title">Hiring Requests</h1>
                            <Breadcrumb>
                                <BreadcrumbItem><a href="/Employer-dashboard">Dashboard</a></BreadcrumbItem>
                                <BreadcrumbItem active>Hiring Requests</BreadcrumbItem>
                            </Breadcrumb>
                        </div>
                    </div>

                    <div>
                        <Nav tabs className="mb-4">
                            <NavItem>
                                <NavLink className={classnames({ active: this.state.activeTab === '1' })} onClick={() => { this.toggle('1'); }} >
                                    Pending <Badge color="default" pill>{pendingCount}</Badge>
                                </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink className={classnames({ active: this.state.activeTab === '2' })} onClick={() => { this.toggle('2'); }} >
                                    Accepted <Badge color="default" pill>{acceptedCount}</Badge>
                                </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink className={classnames({ active: this.state.activeTab === '3' })} onClick={() => { this.toggle('3'); }} >
                                    Completed <Badge color="default" pill>{completedCount}</Badge>
                                </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink className={classnames({ active: this.state.activeTab === '4' })} onClick={() => { this.toggle('4'); }} >
                                    Declined <Badge color="default" pill>{declinedCount}</Badge>
                                </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink className={classnames({ active: this.state.activeTab === '5' })} onClick={() => { this.toggle('5'); }} >
                                    Cancelled <Badge color="default" pill>{cancelledCount}</Badge>
                                </NavLink>
                            </NavItem>
                        </Nav>
                        <TabContent activeTab={this.state.activeTab}>
                            <TabPane tabId={this.state.activeTab}>
                                {this.state.activeTab === '2' && acceptedCount > 0 &&
                                    <div className="mb-3">

                                        <form name="SubmitHandleRequestform" className="w-100" onSubmit={this.handleSubmit}>
                                            <div className="form-inline">
                                                <FormGroup className="mr-2 mr-md-4">
                                                    <label className="custom-checkbox">
                                                        <input
                                                            onChange={this.handleSelectAll}
                                                            checked={this.state.selectAll}
                                                            key={"SelectAll"}
                                                            type="checkbox"
                                                            value={"All"}
                                                        />
                                                        <span className="checkmark">Select All</span>
                                                    </label>
                                                </FormGroup>
                                                <FormGroup className="mr-sm-2 w-sm-160">
                                                    <Input type="select" name="internalstatus" onChange={this.handleChange}>
                                                        <option value="">Select Status</option>
                                                        <option value="7">Mark as Interviewed But Not Hired</option>
                                                        <option value="4" >Mark as Hired </option>
                                                        <option value="5">Mark as Joined</option>
                                                        <option value="6" >Mark as Left</option>
                                                        <option value="8">Mark as No Show</option>
                                                    </Input>
                                                </FormGroup>
                                                <Button color="primary" className="btn-raised">Apply</Button>
                                            </div>
                                        </form>
                                    </div>
                                }
                                <div className={loaderClass} style={styles}></div>
                                {final_hiringrequests.length > 0 ?
                                    <Row>
                                        {final_hiringrequests}
                                    </Row>
                                    :
                                    <div className="d-flex justify-content-center align-items-center error-page">
                                        <div className="text-center">
                                            <div className="img-wrapper mb-4">
                                                <div className="circle-xl user-avatar">
                                                    <img src={hiringRequestSVG} alt="hiringRequest" />
                                                </div>
                                            </div>
                                            <h1 className="h2 fw-regular mb-0">No {ErrorLable} Hiring Request Yet!</h1>
                                        </div>
                                    </div>
                                }
                                {hiringRequestsModalDetails}
                            </TabPane>

                        </TabContent>
                    </div>
                    {CancelRequestModal}
                    {loadMore}
                    <Footer />
                </div>

            </div>
        );

    }
}

function mapStateToProps(state) {
    const { users, authentication } = state;
    const { user } = authentication;
    const { allhiringrequests, loading } = state.hiringrequests; //From Reducer - hiringrequests function ...
    return {
        user,
        users,
        allhiringrequests,
        loading
    };
}
const connectedEmployerHiringRequest = connect(mapStateToProps)(EmployerHiringRequest);
export { connectedEmployerHiringRequest as EmployerHiringRequest };