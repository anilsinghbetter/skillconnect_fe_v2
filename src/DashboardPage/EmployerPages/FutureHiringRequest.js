import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Header } from '../../_components/Header';
import { Footer } from '../../_components/Footer';
import { Sidebar } from '../../_components/Sidebar';
import { userActions } from '../../_actions';
import classnames from 'classnames';
import { TabContent, TabPane, Nav, NavItem, NavLink, Card, Button, CardTitle, CardText, Row, Col, Breadcrumb, BreadcrumbItem, ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem, CardBody, Media, Badge, CardHeader, ListGroup, ListGroupItem, CardSubtitle, Modal, ModalBody, ModalHeader, ModalFooter } from 'reactstrap';
class EmployerFutureHiringRequest extends React.Component {
    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.state = {
            isOpen: false,
            modalIndexValue: undefined,
            activeTab: '1',
            hiringrequest_status:1,
        };
        this.toggleModal = this.toggleModal.bind(this);
        this.toggle = this.toggle.bind(this);
    }
    toggleModal = (e) => {
        this.setState({ isOpen: !this.state.isOpen });
        this.setState({ modalIndexValue: e });
    }

    toggle(tab) {
        const{hiringrequest_status}=this.state;
        if (this.state.activeTab !== tab) {
            this.state.hiringrequest_status=tab;
            this.setState({
                activeTab: tab
            });
        }
       this.getHiringRequests();
    }
    componentDidMount() {
       this.getHiringRequests();
    }
    getHiringRequests()
    {
        if(this.props.user.user_type ==='EMP') var trainingpartnerid = this.props.user.data[0].industrypartnerid
        this.props.dispatch(userActions.getHiringRequests(trainingpartnerid,this.state.hiringrequest_status,'Employer'));    
    }
    render() {
        var loaderClass = '';
        //Show loader..
        if (this.props.loading == true) {
            loaderClass = 'loading';
        }
        const styles = {
            top: '115px',
            left: '60px'
        };
        //console.log("data from Emp Hiring Request", this.props);
        if (!!this.props.allhiringrequests && this.props.allhiringrequests.paginationOptions != undefined) {
            var pendingCount = this.props.allhiringrequests.paginationOptions.totalPendingHiringRequests;
            var acceptedCount = this.props.allhiringrequests.paginationOptions.totalAcceptedHiringRequests;
            var completedCount = this.props.allhiringrequests.paginationOptions.totalCompletedHiringRequests;
            var declinedCount = this.props.allhiringrequests.paginationOptions.totalDeclinedHiringRequests;
            var cancelledCount = this.props.allhiringrequests.paginationOptions.totalCancelledHiringRequests;
        }
        if (!!this.props.allhiringrequests && this.props.allhiringrequests.data != undefined && this.props.allhiringrequests.data.length > 0) {
            var hiringrequests = this.props.allhiringrequests.data;
            var final_hiringrequests = [];

            if (this.state.modalIndexValue != undefined) {
                var mymodaval = this.state.modalIndexValue;
                var tel = hiringrequests[this.state.modalIndexValue].contact_person_phone ? "tel:" + hiringrequests[this.state.modalIndexValue].contact_person_phone : ''
                var mailTo = hiringrequests[this.state.modalIndexValue].contact_person_email ? "mailto:" + hiringrequests[this.state.modalIndexValue].contact_person_email : ''
                var location = hiringrequests[this.state.modalIndexValue].cityname + ', ' + hiringrequests[this.state.modalIndexValue].statename;
                var interviewdatetime = hiringrequests[this.state.modalIndexValue].interview_date + ", " + hiringrequests[this.state.modalIndexValue].interview_time;
                var pf = hiringrequests[this.state.modalIndexValue].is_pf == 'yes' ? 1 : 0;
                var esic = hiringrequests[this.state.modalIndexValue].is_esic == 'yes' ? 1 : 0;
                var food = hiringrequests[this.state.modalIndexValue].is_food == 'yes' ? 1 : 0;
                var transportation = hiringrequests[this.state.modalIndexValue].is_transportation == 'provided' ? 1 : 0;
                var accomodation = hiringrequests[this.state.modalIndexValue].is_accomodation == 'provided' ? 1 : 0;
                var Benefits = '';
                if(pf == 1 || esic == 1 || food == 1 || transportation == 1 || accomodation ==1){
                    Benefits = (
                        <Col xs="12">
                            <label className="subtitle">Benefits</label>
                            <p className="title">
                            {pf == 1 && 
                                <Badge color="default" >Pf</Badge>
                            }
                            {esic == 1 && 
                                <Badge color="default" >Esic</Badge>
                            }
                            {food == 1 && 
                                <Badge color="default" >Food</Badge>
                            }
                            {transportation == 1 && 
                                <Badge color="default" >Transportation</Badge>
                            }
                            {accomodation == 1 && 
                                <Badge color="default" >Accomodation</Badge>
                            }
                            </p>
                        </Col>
                    );
                }

                var hiringRequestsModalDetails = (
                    <Modal backdrop={false} centered size="lg" isOpen={this.state.isOpen} toggle={() => this.toggleModal(mymodaval)}>
                        <ModalHeader toggle={() => this.toggleModal(mymodaval)} className="border-bottom-0 modal-header-shadow align-items-center">
                            <div className="section-title">
                                <h5 className="title modal-title">{hiringrequests[this.state.modalIndexValue].candidate_name}</h5>
                            </div>
                        </ModalHeader>
                        <ModalBody className="details-view">
                            <Row>
                                <Col xs="12">
                                    <h5 className="title modal-view-title">Job Details</h5>
                                </Col>
                                <Col xs="12">
                                    <label className="subtitle">Training Partner</label>
                                    <p className="title">{hiringrequests[this.state.modalIndexValue].training_partner_name}</p>
                                </Col>
                                <Col xs="12">
                                    <label className="subtitle">Jobrole</label>
                                    <p className="title">{hiringrequests[this.state.modalIndexValue].jobrolename}</p>
                                </Col>
                                <Col xs="12" md="6">
                                    <label className="subtitle">Location</label>
                                    <p className="title">{location}</p>
                                </Col>
                                <Col xs="12" md="6">
                                    <label className="subtitle">Salary</label>
                                    <p className="title">{hiringrequests[this.state.modalIndexValue].salary}</p>
                                </Col>
                                {Benefits}

                            </Row>
                            <hr className="modal-data-seperator" />
                            <Row>
                                <Col xs="12">
                                    <h5 className="title modal-view-title">Interview Details</h5>
                                </Col>
                                <Col xs="12">
                                    <label className="subtitle">Date &amp; Time</label>
                                    <p className="title">{interviewdatetime}</p>
                                </Col>
                               {/*  <Col xs="12">
                                    <label className="subtitle">Other Information</label>
                                    <p className="title mb-1">{hiringrequests[this.state.modalIndexValue].other_information}</p>
                                </Col> */}
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">Contact Person</label>
                                    <p className="title">{hiringrequests[this.state.modalIndexValue].contact_person_name}</p>
                                </Col>
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">Mobile</label>
                                    <p className="title">
                                    {tel === '' ? 'NA' :
                                            <a href={tel} title={hiringrequests[this.state.modalIndexValue].contact_person_phone}>{hiringrequests[this.state.modalIndexValue].contact_person_phone}</a>
                                        }
                                    </p>
                                </Col>
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">Email</label>
                                    <p className="title">
                                    {mailTo === '' ? 'NA' :
                                            <a href={mailTo} title={hiringrequests[this.state.modalIndexValue].contact_person_email}>{hiringrequests[this.state.modalIndexValue].contact_person_email}</a>
                                        }
                                    </p>
                                </Col>
                                {/* <Col xs="12">
                                    <Button color="secondary" className="btn-icon btn-raised mr-3">
                                        <i className="material-icons">done</i>Accept
                            </Button>
                                    <Button color="danger" outline className="btn-icon">
                                        <i className="material-icons">close</i>Decline
                            </Button>
                                </Col> */}
                            </Row>
                        </ModalBody>
                    </Modal>
                );
            }

            hiringrequests.map((value, index) => {
                var name = hiringrequests[index].candidate_name;
                var initials = name.match(/\b\w/g) || [];
                initials = ((initials.shift() || '') + (initials.pop() || '')).toUpperCase();
                var interviewDateTime = hiringrequests[index].interview_date + ", " + hiringrequests[index].interview_time;
                var hiringRequestStatusDb = hiringrequests[index].hiring_request_status;


                final_hiringrequests.push(
                    <Col xs="12" md="6" xl="4" key={index}>
                        <Card className="card z-depth-1">
                            <CardHeader className="d-flex justify-content-between bg-transparent align-items-center border-bottom-0 card-header-shadow">
                                <div className="section-title">
                                    <CardTitle className="title card-title">{hiringrequests[index].candidate_name}</CardTitle>
                                    <ul className="list-inline list-inline-separator">
                                        <li className="list-inline-item">{hiringrequests[index].statename}</li>
                                        <li className="list-inline-item"> {hiringrequests[index].cityname}</li>
                                    </ul>
                                </div>
                            </CardHeader>
                            <ListGroup className="no-border">
                                <ListGroupItem>
                                    <label className="subtitle">Training Partner</label>
                                    <p className="title mb-0">{hiringrequests[index].training_partner_name}</p>
                                </ListGroupItem>
                                <ListGroupItem>
                                    <label className="subtitle">Sector</label>
                                    <p className="title mb-0">{hiringrequests[index].sectorname}</p>
                                </ListGroupItem>
                                <ListGroupItem>
                                    <label className="subtitle">Job Role</label>
                                    <p className="title mb-0">{hiringrequests[index].jobrolename}</p>
                                </ListGroupItem>
                                <ListGroupItem>
                                    <label className="subtitle">Interview on</label>
                                    <p className="title mb-0">{interviewDateTime}</p>
                                </ListGroupItem>
                                <ListGroupItem className="d-flex">
                                    <Col className="pl-0 align-self-center">
                                        <Button color="primary" className="btn-raised mr-2" onClick={() => this.toggleModal(index)}>View More</Button>
                                        {hiringRequestStatusDb == 1 &&
                                            <Link to={`/edit-candidate-detail/#id:${hiringrequests[index].candidatedetailid}`} title="Edit" className="btn btn-outline">Edit</Link>
                                        }
                                    </Col>
                                </ListGroupItem>
                                {/* <ListGroupItem className="info-tile-wrapper">
                            <p className="m-0 d-inline-block align-middle danger-info"><i className="material-icons align-middle">error</i>Requested by mistake.</p>
                        </ListGroupItem> */}

                            </ListGroup>
                        </Card>
                    </Col>


                );
            })
        }
        return (

            <div className="content-wrapper user-flow TP-flow">
                <Sidebar {...this.props} />

                <div id="content" className="main-content">
                    <Header />

                    <div className="d-block d-md-flex align-items-center section-header">
                        <div>
                            <h1 className="page-title">Hiring Requests</h1>
                            <Breadcrumb>
                                <BreadcrumbItem><a href="/TrainingPartnerDashboard">Dashboard</a></BreadcrumbItem>
                                <BreadcrumbItem active>Hiring Requests</BreadcrumbItem>
                            </Breadcrumb>
                        </div>
                    </div>

                    <div>
                        <Nav tabs className="mb-4">
                            <NavItem>
                                <NavLink className={classnames({ active: this.state.activeTab === '1' })} onClick={() => { this.toggle('1'); }} >
                                    Pending <Badge color="default" pill>{pendingCount}</Badge>
                      </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink className={classnames({ active: this.state.activeTab === '2' })} onClick={() => { this.toggle('2'); }} >
                                    Accepted <Badge color="default" pill>{acceptedCount}</Badge>
                      </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink className={classnames({ active: this.state.activeTab === '3' })} onClick={() => { this.toggle('3'); }} >
                                    Completed <Badge color="default" pill>{completedCount}</Badge>
                      </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink className={classnames({ active: this.state.activeTab === '4' })} onClick={() => { this.toggle('4'); }} >
                                    Declined <Badge color="default" pill>{declinedCount}</Badge>
                      </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink className={classnames({ active: this.state.activeTab === '5' })} onClick={() => { this.toggle('5'); }} >
                                    Cancelled <Badge color="default" pill>{cancelledCount}</Badge>
                      </NavLink>
                            </NavItem>
                        </Nav>
                        <div className={loaderClass} style={styles}></div>
                        <TabContent activeTab={this.state.activeTab}>
                            <TabPane tabId={this.state.activeTab}>
                                <Row>
                                    {final_hiringrequests}
                                </Row>
                                {hiringRequestsModalDetails}
                            </TabPane>

                            {/* <TabPane tabId="2">
                            <Row>
                                {final_hiringrequests}
                                </Row>
                            </TabPane>

                            <TabPane tabId="3">
                            <Row>
                                {final_hiringrequests}
                                </Row>
                            </TabPane>

                            <TabPane tabId="4">
                            <Row>
                                {final_hiringrequests}
                                </Row>
                            </TabPane>

                            <TabPane tabId="5">
                                <Row>
                                {final_hiringrequests}
                                </Row>
                            </TabPane> */}
                        </TabContent>
                    </div>

                    <Footer />
                </div>

            </div>
        );

    }
}

function mapStateToProps(state) {
    const { users, authentication } = state;
    const { user } = authentication;
    const { allhiringrequests, loading } = state.hiringrequests; //From Reducer - hiringrequests function ...
    return {
        user,
        users,
        allhiringrequests,
        loading
    };
}
const connectedEmployerFutureHiringRequest = connect(mapStateToProps)(EmployerFutureHiringRequest);
export { connectedEmployerFutureHiringRequest as EmployerFutureHiringRequest };