import React from 'react';
import { connect } from 'react-redux';
import { history } from '../_helpers';
import { Header } from '../_components/Header';
import { Footer } from '../_components/Footer';
import { Sidebar } from '../_components/Sidebar';
import { GetCenters } from '../_components/GetCenters';
import { Breadcrumb, BreadcrumbItem, Button, ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';

class TrainingCenters extends React.Component {

    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);

        this.state = {
            isAddCenterClick: false,
            //Intial State..
            dropdownOpen: false,
        };

    }
    handleRedirectionToAddCenter(e) {
        // history.push('/add-center');
        history.push('/add-edit-center');
    }
    toggle() {
        this.setState({
            dropdownOpen: !this.state.dropdownOpen
        });
    }
    render() {
        const { user } = this.props;
        // const { isAddCenterClick } = this.state;
        if (user === undefined) {
            // console.log("Redierect to specific Dashboard page");
            history.push('/login');
            return ('');
        }
        //  console.log("Center Data", this.props.allcenters);
        /* if (!!this.props.allcenters) {
            var getCenterCount = this.props.allcenters.data.length;
        } */
        return (
            <div className="content-wrapper user-flow TP-flow">
                <Sidebar {...this.props} />
                <div id="content" className="main-content">
                    <Header />
                    <div className="d-block d-md-flex align-items-center section-header">
                        <div>
                            <h1 className="page-title">Training Center(s)</h1>
                            <Breadcrumb>
                                <BreadcrumbItem><a href="/TrainingPartner-dashboard">Dashboard</a></BreadcrumbItem>
                                <BreadcrumbItem active>Training Centers</BreadcrumbItem>
                            </Breadcrumb>
                        </div>

                        <div className="ml-auto d-flex">
                            <Button outline color="primary" title="Add Center" onClick={this.handleRedirectionToAddCenter} className="mr-0 mr-md-2 sm-floating-btn d-md-inline-block d-none" >
                                <i className="material-icons">add</i>
                                <span>Add Center</span>
                            </Button>

                            <ButtonDropdown isOpen={this.state.dropdownOpen} toggle={this.toggle} direction="up" className="d-block d-md-none floating-dropdown-btn">
                                <DropdownToggle outline color="primary" className="mr-0 mr-md-2 sm-floating-btn">
                                    <i className="material-icons">add</i>
                                </DropdownToggle>
                                <DropdownMenu className="z-depth-2 border-0">
                                    <DropdownItem onClick={this.handleRedirectionToAddCenter}>Add Centers</DropdownItem>
                                </DropdownMenu>
                            </ButtonDropdown>
                        </div>
                    </div>
                    
                        <div className="d-block d-xl-flex search-bar">
                            {/* <form className="form-inline mb-2 mb-xl-0 d-none d-md-flex">
                                <div className="form-group form-icon">
                                    <i className="material-icons">search</i>
                                    <input name="text" id="search" placeholder="Search Center" type="text" className="form-control" />
                                </div>
                                <div className="form-group">
                                    <select name="select" id="selectSector" className="form-control">
                                        <option>Sector</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                    </select>
                                </div>
                                <div className="form-group">
                                    <select name="select" id="selectJobRole" className="form-control">
                                        <option>Job Role</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                    </select>
                                </div>
                                <div className="form-group">
                                    <select name="select" id="selectTrainingStatus" className="form-control">
                                        <option>Training Status</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                    </select>
                                </div>
                                <button className="btn btn-default">Go</button>
                            </form> */}

                            {/* <form className="form-inline mb-2 d-flex d-md-none media">
                                <div className="media-body">
                                    <div className="form-group form-icon">
                                        <i className="material-icons">search</i>
                                        <input className="text" id="search" placeholder="Search Center" type="text" className="form-control" />
                                    </div>
                                </div>
                                <button className="btn btn-default media-right disabled">Go</button>
                            </form> */}

                        </div>
                    
                    {/* <TrainingCenterSearch /> */}
                    <GetCenters />
                    <Footer />
                </div>

            </div>
        );
    }
}

function mapStateToProps(state) {

    const { allcenters } = state.trainingpartners; //From Reducer - TrainingCenters function ...
    const { users, authentication } = state;
    const { user } = authentication;
    return {
        user,
        users,
        allcenters
    };
}

const connectedTrainingCenters = connect(mapStateToProps)(TrainingCenters);
export { connectedTrainingCenters as TrainingCenters };