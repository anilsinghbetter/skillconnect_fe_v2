import React from 'react';
import { connect } from 'react-redux';
import { GetCandidates } from '../_components/GetCandidates';
import { NotFound } from '../_components/NotFound';

// import { history } from '../_helpers';

class TrainingPartner extends React.Component {

    constructor(props) {
        super(props);
         this.state = {
            //Intial State..           
            user : {},
        };
    }
    componentDidMount(){
        const user_loc = JSON.parse(localStorage.getItem('user'));
        this.setState({ user: user_loc });
        // if(user.user_type !== 'TC'){
        //     history.push('/login');
        // }
        // console.log(user,"User");
    }
    render() {       
        return (
            <div>
                {!!this.state.user && this.state.user.user_type === 'TC' ? <GetCandidates /> : <NotFound />}
                {/* {!!this.state.user && this.state.user !== 'TC' && } */}
                
            </div>
        );
    }
}
function mapStateToProps(state) {
    //const { isnewData } = state;
     return {
    };
}
const connectedTrainingPartner = connect(mapStateToProps)(TrainingPartner);
export { connectedTrainingPartner as TrainingPartner };
