import React from 'react';
import { connect } from 'react-redux';
import { dashboardActions } from '../_actions';
import { history } from '../_helpers';
import { Header } from '../_components/Header';
import { Footer } from '../_components/Footer';
import { Sidebar } from '../_components/Sidebar';
import employer from '../assets/images/ic_employer_sm.svg';
import { Card, CardTitle, Row, Col, Breadcrumb, BreadcrumbItem, CardBody, Media, CardHeader, ListGroup, ListGroupItem, CardSubtitle, } from 'reactstrap';
import { Link } from 'react-router-dom';
import { getColor } from '../utils/colors';
import { constants } from '../constants';
import { Doughnut, defaults } from 'react-chartjs-2';
//Hide chart legend..
defaults.global.legend.display = false;
const allowedLength = constants.allowedLength;
const slicedLength = constants.slicedLength;

class TrainingPartnerDashboard extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isAddCandidateClick: false
            //Intial State..
        };
    }
    componentDidMount() {
        //console.log('#1');
    }
    render() {
        const { user, loading } = this.props;
        var loaderClass;
        if (loading === true) {
            loaderClass = 'loading';
        }
        const styles = {
            top: '115px',
            left: '60px'
        };
        if (user === undefined) {
            // console.log("Redierect to specific Dashboard page");
                history.push('/login');
            return ('');
        }
        // console.log("Dashboard Data", this.props.dashboarddata);

        if (!!this.props.dashboarddata && this.props.dashboarddata.status === true) {

            let totalCandidates = this.props.dashboarddata.data.counts[0].totalcandidates;
            let totalHiringRequests = this.props.dashboarddata.data.counts[0].totalhiringrequests;
            let totalTrainingCenters = this.props.dashboarddata.data.counts[0].totaltrainingcenters;
            var getAllTotalValues = [];

            /***************Fetch Total Counts for - Candidates,Hiring request & Trainigin Centers.**************/
            const getTotalCount = [
                { title: totalCandidates === 0 ? 'No Candidates' : 'Total Candidates', values: totalCandidates.toLocaleString('en-IN'), classnameValue: "z-depth-1 info-tile secondary-tile", Icon: "../assets/images/ic_candidates_colored.svg", alt: "Candidates" },
                { title: totalTrainingCenters === 0 ? 'No Training Centers' : 'Training Centers', classnameValue: "z-depth-1 info-tile primary-tile", values: totalTrainingCenters.toLocaleString('en-IN'), Icon: "../assets/images/ic_training_center_colored.svg", alt: "trainingCenter" },
                { title: totalHiringRequests === 0 ? 'No Hiring Requests' : 'Hiring Requests', classnameValue: "z-depth-1 info-tile danger-tile", values: totalHiringRequests.toLocaleString('en-IN'), Icon: "../assets/images/ic_hiring_requests_colored.svg", alt: "hiringRequest" }
            ];
            getTotalCount.map(({ Icon, title, alt, values, classnameValue }, index) => (
                getAllTotalValues.push(<Col xs="12" md="4" key={index}>
                    <Card className={classnameValue}>
                        <CardBody>
                            <Media className="d-md-block d-xl-flex d-flex">
                                <Media left className="circle-lg user-avatar avatar-outline mr-3">
                                    <img src={Icon} alt={alt} />
                                </Media>
                                <Media body>
                                    {values !== 0 ?
                                        <h2 className="mb-0">{values} <span className="secondary-text">{title}</span></h2>
                                        :
                                        <h2 className="mb-0"><span className="secondary-text">{title}</span></h2>
                                    }
                                </Media>
                            </Media>
                        </CardBody>
                    </Card>
                </Col>)
            ));
            /***************END OF Fetching Total Counts for - Candidates,Hiring request & Trainigin Centers.**************/

            /*************** Fetch Total Recent Hiring Requests.**************/
            var allHiringrequests = this.props.dashboarddata.data.recent_hiring_requests;
            var getAll_recent_hiring_requests = [];
            var blankStateRecentHiringRequest = "";
            if (allHiringrequests.length > 0) {
                allHiringrequests.map((value, index) => {
                    return getAll_recent_hiring_requests.push(
                        <ListGroupItem key={index}>
                            <Row className="d-block d-md-flex">
                                <Col className="pr-md-0 pr-lg-1">
                                    <Media>
                                        <Media body className="d-xl-flex d-block">
                                            <Row className="w-100">
                                                <Col xs="12" xl="6" className="pr-md-0">
                                                    <h5 className="m-0">{value.candidatename}</h5>
                                                    <ul className="list-inline list-inline-separator">
                                                        <li className="list-inline-item" title={value.jobrolename}> {value.jobrolename.length > allowedLength ? (value.jobrolename.slice(0,slicedLength)+'...') : value.jobrolename} </li>
                                                        <li className="list-inline-item">{value.statename}</li>
                                                    </ul>
                                                </Col>
                                                <Col xs="12" xl="6" className="media-sub-detail pr-md-1">
                                                    <img src={employer} alt="employer" />
                                                    <span>{value.employername}</span>
                                                </Col>
                                            </Row>
                                        </Media>
                                    </Media>
                                </Col>
                                <Col xs="auto" className="action-btn order-last">
                                    <Link title="Accept" to='/TrainingPartner-hiring-requests' className="btn-fab btn-fab-secondary btn-sm btn"><i className="material-icons">done</i></Link>
                                    <Link title="Decline" to='/TrainingPartner-hiring-requests' className="btn-fab btn-fab-danger btn-sm btn"><i className="material-icons">close</i></Link>
                                </Col>
                            </Row>
                        </ListGroupItem>
                    );
                })
            } else {
                blankStateRecentHiringRequest = (
                    <p className="lead text-muted-2">No request yet!</p>
                );
            }

            /*************** End of Fetching Total Recent Hiring Requests.**************/

            /*************** Fetch Total Recruitment History.***************************/
            // console.log("Get Recruitement Histoy", this.props.dashboarddata.data.recruitment_history);

            var allRecruitmentHistories = this.props.dashboarddata.data.recruitment_history;
            var getAll_recent_recruitments = [];
            var blankStateRecruitmentHistories = "";
            if (allRecruitmentHistories.length > 0) {
                allRecruitmentHistories.map((value, index) => {
                    return getAll_recent_recruitments.push(

                        <ListGroupItem key={index}>
                            <Row className="d-block d-lg-flex">
                                <Col>
                                    <Media>
                                        <div className="circle-sm img-icon">
                                            <img src={employer} alt="employer" />
                                        </div>
                                        <Media body>
                                            <h5 className="m-0">{value.employername}</h5>
                                            <ul className="list-inline list-inline-separator">
                                                <li className="list-inline-item">{value.date}</li>
                                            </ul>
                                        </Media>
                                    </Media>
                                </Col>
                                <Col xs="auto" className="right-lg-block order-last">
                                    <em className="info-text">{value.count} Candidates</em>
                                </Col>
                            </Row>
                        </ListGroupItem>
                    );
                })
            } else {
                blankStateRecruitmentHistories = (
                    <p className="lead text-muted-2">No recruitment history found!</p>
                );
            }
            /*************** End of Fetching Recruitment History.**************/

            /*************** Fetch All Recent Activities.***************************/
            //console.log("Get Recent ACtivities", this.props.dashboarddata.data.recent_activities);

            var allActivities = this.props.dashboarddata.data.recent_activities;
            var getAll_recent_activities = [];
            var blankStateActivities = "";
            if (allActivities.length > 0) {
                allActivities.map((value, index) => {
                    return getAll_recent_activities.push(
                        <ListGroupItem key={index}>
                            <Row className="d-block d-lg-flex">
                                <Col>
                                    <Media>
                                        <div className="circle-sm img-icon">
                                            <img src={employer} alt="employer" />
                                        </div>
                                        <Media body>
                                            <h5 className="m-0">{value.activityname}</h5>
                                            <ul className="list-inline list-inline-separator">
                                                <li className="list-inline-item">{value.activitytytime}</li>
                                            </ul>
                                        </Media>
                                    </Media>
                                </Col>
                            </Row>
                        </ListGroupItem>
                    );
                })
            } else {
                blankStateActivities = (
                    <p className="lead text-muted-2">No recent activities!</p>
                );
            }
            /*************** End of Fetching Recent activities.**************/

            //console.log("Get Chart Data", this.props.dashboarddata.data.candidate_statistics[0].availablecandidates);

            var availablecandidates = this.props.dashboarddata.data.candidate_statistics[0].availablecandidates;
            var hiredcandidates = this.props.dashboarddata.data.candidate_statistics[0].hiredcandidates;
            var totalcandidates = availablecandidates + hiredcandidates;
            
            var getAll_Statistics = [];
            var getAll_Statistics_Legends = [];
            var blankStateStatistics = "";
            if (availablecandidates > 0 || hiredcandidates > 0) {
                const genCandidateChartData = () => {
                    return {
                        datasets: [
                            {
                                data: [availablecandidates, hiredcandidates],
                                backgroundColor: [
                                    getColor('primary'),
                                    getColor('success')
                                ],
                                label: 'Candidates',
                            },
                        ],
                        labels: ['Available Candidates', 'Hired Candidates']
                    }
                };
                getAll_Statistics.push(<Doughnut key="1" data={genCandidateChartData()} />)

                getAll_Statistics_Legends.push(

                    <Col key="1" xs="12" xl="5" className="d-flex align-items-center">
                        <ul className="chart-listing list-group">
                            <li className="list-group-item primary-bullet border-0">
                                <p className="m-0">Available Candidates</p>
                                <div className="answer-text">
                                    <h2>{availablecandidates.toLocaleString('en-IN')} <span>/ {totalcandidates.toLocaleString('en-IN')}</span></h2>
                                </div>
                            </li>
                            <li className="list-group-item secondary-bullet border-0">
                                <p className="m-0">Hired Candidates</p>
                                <div className="answer-text">
                                    <h2>{hiredcandidates.toLocaleString('en-IN')} <span>/ {totalcandidates.toLocaleString('en-IN')}</span></h2>
                                </div>
                            </li>
                        </ul>
                    </Col>
                )
            } else {
                blankStateStatistics = (
                    <p className="lead text-muted-2">No candidates added yet! <Link to="/add-candidates" title="Add candidate">Add candidate</Link> to display data.</p>
                );
            }
        }
        return (
            <div className="content-wrapper user-flow TP-flow">
                <Sidebar {...this.props} />
                <div id="content" className="main-content">
                    <Header />

                    <div className="d-block d-md-flex align-items-center section-header">
                        <div>
                            <h1 className="page-title">Dashboard</h1>
                            <Breadcrumb>
                                <BreadcrumbItem><a href="/TrainingPartner-dashboard">Home</a></BreadcrumbItem>
                                <BreadcrumbItem active>Dashboard</BreadcrumbItem>
                            </Breadcrumb>
                        </div>
                    </div>

                    <Row>
                        {getAllTotalValues}
                    </Row>
                    <div className={loaderClass} style={styles}></div>
                    <Row>
                        <Col xs="12" md="6" lg="7">
                            <Card className="z-depth mb-4">
                                <CardHeader className="d-flex justify-content-between bg-transparent align-items-center border-bottom-0 card-header-shadow">
                                    <div className="section-title">
                                        <CardTitle className="title">Hiring Requests</CardTitle>
                                        <CardSubtitle className="subtitle">Recently posted hiring requests by the employers</CardSubtitle>
                                    </div>
                                    {/* <CardLink title="View All" className="btn btn-primary btn-sm">View All</CardLink> */}
                                    {blankStateRecentHiringRequest === '' &&
                                        <Link title="View All" to='/TrainingPartner-hiring-requests' className="btn btn-primary btn-sme">View All</Link>
                                    }
                                </CardHeader>

                                <CardBody className={blankStateRecentHiringRequest !== "" ? "card-scrollable no-data" : "card-scrollable"}>
                                    <ListGroup className="media-list">
                                        {blankStateRecentHiringRequest === '' ?
                                            getAll_recent_hiring_requests/*get hiring request*/
                                            :
                                            blankStateRecentHiringRequest
                                        }
                                    </ListGroup>
                                </CardBody>
                            </Card>
                        </Col>

                        <Col xs="12" md="6" lg="5">
                            <Card className="z-depth mb-4">
                                <CardHeader className="d-flex justify-content-between bg-transparent align-items-center border-bottom-0 card-header-shadow">
                                    <div className="section-title">
                                        <CardTitle className="title">Recruitment History</CardTitle>
                                        <CardSubtitle className="subtitle">Employers who recently onboarded the candidates</CardSubtitle>
                                    </div>
                                </CardHeader>

                                <CardBody className={blankStateRecruitmentHistories !== "" ? "card-scrollable no-data" : "card-scrollable"}>
                                    <ListGroup className="media-list">
                                        {blankStateRecruitmentHistories === '' ?
                                            getAll_recent_recruitments/*get recruitment history*/
                                            :
                                            blankStateRecruitmentHistories
                                        }
                                    </ListGroup>
                                </CardBody>
                            </Card>
                        </Col>
                    </Row>

                    <Row>
                        <Col xs="12" md="6" lg="5">
                            <Card className="z-depth mb-4">
                                <CardHeader className="d-flex justify-content-between bg-transparent align-items-center border-bottom-0 card-header-shadow">
                                    <div className="section-title">
                                        <CardTitle className="title">Recent Activities</CardTitle>
                                        <CardSubtitle className="subtitle">Recently performed activities</CardSubtitle>
                                    </div>
                                </CardHeader>

                                <CardBody className={blankStateActivities !== "" ? "card-scrollable no-data" : "card-scrollable"}>
                                    <ListGroup className="media-list">
                                        {blankStateActivities === '' ?
                                            getAll_recent_activities/*Get recent activities*/
                                            :
                                            blankStateActivities
                                        }
                                    </ListGroup>
                                </CardBody>
                            </Card>
                        </Col>

                        <Col xs="12" md="6" lg="7">
                            <Card className="z-depth mb-4">
                                <CardHeader className="d-flex justify-content-between bg-transparent align-items-center border-bottom-0 card-header-shadow">
                                    <div className="section-title">
                                        <CardTitle className="title">Candidate Statistics</CardTitle>
                                        <CardSubtitle className="subtitle">Recent Candidate Statistics</CardSubtitle>
                                    </div>
                                </CardHeader>

                                {/*  Graph Card */}
                                <CardBody className={blankStateStatistics !== "" ? "card-scrollable no-data" : "card-scrollable pt-xl-4"}>
                                    {blankStateStatistics}
                                    <Row>

                                        <Col xs="12" xl="7" className="text-center">
                                            {getAll_Statistics/*genCandidateChartData()} */}
                                        </Col>
                                        {getAll_Statistics_Legends}
                                    </Row>
                                </CardBody>
                            </Card>
                        </Col>
                    </Row>

                    <Footer />
                </div>

            </div>
        );
    }
}

function mapStateToProps(state) {
    const { user } = state.authentication;
    const { dashboarddata, loading } = state.dashboardData;

    return {
        user, dashboarddata, loading
    };
}

function mapDispatchToProps(dispatch, ownProps) {
    var getDashboardData = "";
    if (localStorage.getItem('user') !== null) {
        var userdata = JSON.parse(localStorage.getItem('user'));
        getDashboardData = dispatch(dashboardActions.getDashboardData(userdata.data[0].trainingpartnerid, 'TP'));
        return { getDashboardData }
    } else {
        history.push('/login');
        return ('');
    }
    // console.log('#TPID',userdata);
}

const connectedTrainingPartnerDashboard = connect(mapStateToProps, mapDispatchToProps)(TrainingPartnerDashboard);
export { connectedTrainingPartnerDashboard as TrainingPartnerDashboard };