import React from 'react';
import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import { Header } from '../_components/Header';
import { Footer } from '../_components/Footer';
import { userActions } from '../_actions';
import { Sidebar } from '../_components/Sidebar';
import { CardSubtitle, CardLink, ListGroup, ListGroupItem, Button, Form, Row, Col, Card, CardHeader, CardTitle, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { EmployerCandidateSearch } from '../_components/EmployerCandidateSearch';
import { history } from '../_helpers';
import candidatesSVG from '../assets/images/candidates_grey.svg';


// const renderField = ({ input, label, type, placeholder, textarea, meta: { touched, error, warning, invalid } }) => {
//     const textareaType = <textarea {...input} placeholder={label} type={type} className={`form-control ${touched && invalid ? 'has-danger' : ''}`} />;

//     return <div className={((label === "Gender") ? 'col-md-8 col-12' : 'col-md-4 col-12')}>
//         <div className={'form-group' + (error && touched ? ' has-error' : '')}>
//             <label>{label}</label>
//             <div> {textarea ? textareaType : <input {...input} type={type} placeholder={placeholder} className="form-control" />}
//                 {touched && ((error && <div className="help-block">{error}</div>) || (warning && <span>{warning}</span>))}
//             </div>

//         </div>
//     </div>
// };

// const renderColorSelector = ({ input, label, dynamic_values, meta: { touched, error } }) => (
//     <div key='1' className='form-group'>
//         <label>{label}</label>
//         <select  {...input} className="form-control">
//             <option value="">Select</option>
//             {dynamic_values}
//             ))}
//      </select>
//     </div>
// )

class Employer extends React.Component {

    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.state = {
            isOpen: false,
            collapse: true,
            CardValue: undefined,
            checkboxArray: [],
            page: 1,
            pagePrev: 0,
        };
        this.viewMoreAPICall = this.viewMoreAPICall.bind(this);
        this.toggleModal = this.toggleModal.bind(this);
        this.handleCheckboxChange = this.handleCheckboxChange.bind(this);
        this.handleRedirectionToAddCandidateDetail = this.handleRedirectionToAddCandidateDetail.bind(this);
    }
    toggleModal = (e) => {
        this.setState({ isOpen: !this.state.isOpen });
        this.setState({ CardValue: e });
    }
    /* componentDidUpdate () {
        window.scrollBy(0, 250); 
    } */
    componentDidMount() {
        //  this.props.dispatch(userActions.getAll());
    }
    viewMoreAPICall = (e, candidateid) => {
        this.props.dispatch(userActions.getCandidateViewMoreDetails(candidateid));
        this.toggleModal(e);
    }
    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }
    addDetailChange(candidateid) {
        const checkboxArray = [];
        // check if the check box is checked or unchecked
        if (candidateid > 0) {
            // add the numerical value of the checkbox to options array
            checkboxArray.push(candidateid)
            history.push({
                pathname: '/add-candidate-detail',
                state: { addcandiatedetailids: JSON.stringify(checkboxArray) }
            })
        }
    }
    handleRedirectionToAddCandidateDetail() {
        const checkboxArray = this.state.checkboxArray
        history.push({
            pathname: '/add-candidate-detail',
            state: { addcandiatedetailids: JSON.stringify(checkboxArray) }
        })
    }
    handleCheckboxChange(e) {
        // current array of options
        const checkboxArray = this.state.checkboxArray
        let index

        // check if the check box is checked or unchecked
        if (e.target.checked) {
            // add the numerical value of the checkbox to options array
            checkboxArray.push(+e.target.value)
        } else {
            // or remove the value from the unchecked checkbox from the array
            index = checkboxArray.indexOf(+e.target.value)
            checkboxArray.splice(index, 1)
        }
        //console.log('options checked', checkboxArray);
        // update the state with the new array of options
        this.setState({ checkboxArray: checkboxArray })
    }
    handleChild = (langValue) => {
        //this.setState({ mydata: langValue });
        this.state.page = 1;
        this.setState({ searchparams: langValue, isOpen: false, CardValue: undefined });
    }
    loadData() {
        const { page } = this.state

        //console.log("NEXT PAGE NO", page);
        if (this.state.searchparams !== undefined) {

            var userFinal = this.state.searchparams;
            //console.log("NEXT PAGE NO", userFinal);
            let has_joined = 0;
            userFinal = { has_joined, ...userFinal };

            let defaultSectors = 0;
            if (this.props.user.data[0].sector !== undefined) {
                defaultSectors = this.props.user.data[0].sector;
            }
            if (userFinal.sectorid !== undefined && userFinal.sectorid > 0) {
                defaultSectors = 0;
            }
            if (defaultSectors !== 0) {
                userFinal = { defaultSectors, ...userFinal };
            }

            this.props.dispatch(userActions.getSearchCandidate(userFinal, page, 1));
        }
    }
    loadMore = () => {
        //First update the state and then use callback method to call next page call...
        this.setState(prevState => ({
            page: prevState.page + 1
        }), this.loadData)
    }
    render() {
        var loaderClass = '';
        //Show loader..
        //console.log('#this.props.loading',this.props.loading);
        if (this.props.loading === true || this.props.loadingviewmore === true) {
            loaderClass = 'loading';
        }
        const styles = {
            top: '115px',
            left: '60px'
        };
        var recordFound = 0;
        const { user } = this.props;
        // var final_sectors = [];
        // var final_allJobs = [];
        // var final_trainingstatus = [];
        // var final_allStates = [];
        // var final_allCities = [];
        // var final_placementstatus = [];
        // console.log("Employer Data", user.user_type);



        if (user === undefined) {
            // console.log("Redierect to specific Dashboard page");
            history.push('/login');
            return ('');
        }

        if (!!this.props.allsearchcandidates) {

            //console.log("data from Emp Search Candidate", this.props.allsearchcandidates);
            var loadMore = "";
            if (this.props.allsearchcandidates.paginationOptions !== undefined) {
                var totalpages = this.props.allsearchcandidates.paginationOptions.totalPages;
                var currentpage = this.props.allsearchcandidates.paginationOptions.curPage;
                if (currentpage < totalpages) {
                    loadMore = (<div className="text-center"><a title="Load More" onClick={this.loadMore} className="btn btn-outline-secondary card-link">Load More</a></div>);
                }
            }

            if (!!this.props.allsearchcandidates) {// this.props.allsearchcandidates.status == true && this.props.allsearchcandidates.data != undefined
                var final_allCandiates = [];
                var allcandidates = [];
                if (this.props.allsearchcandidates.data.length > 0) recordFound = 1;
                allcandidates = this.props.allsearchcandidates.data;
                if (!!this.props.candidate_updated_details && this.state.CardValue !== undefined) {
                    var candidatedetaildata = this.props.candidate_updated_details.data[0];
                    var mymodaval = this.state.CardValue;
                    var tel = candidatedetaildata.phone ? "tel:" + candidatedetaildata.phone : ''
                    var mailTo = candidatedetaildata.email_address ? "mailto:" + candidatedetaildata.email_address : ''
                    var allcandidatesModalDetails = (

                        <Modal backdrop={false} centered size="lg" isOpen={this.state.isOpen} toggle={() => this.toggleModal(mymodaval)}>
                            <ModalHeader toggle={() => this.toggleModal(mymodaval)} className="border-bottom-0 modal-header-shadow align-items-center">
                                <div className="section-title">
                                    <h5 className="title modal-title">{candidatedetaildata.candidate_name}'s Details</h5>
                                </div>
                            </ModalHeader>
                            <ModalBody className="details-view">
                                <Row>
                                    <Col xs="12">
                                        <h5 className="title modal-view-title">Personal Details</h5>
                                    </Col>
                                    <Col xs="12" md="6" xl="4">
                                        <label className="subtitle">Name</label>
                                        <p className="title">{candidatedetaildata.candidate_name ? candidatedetaildata.candidate_name : 'NA'}</p>
                                    </Col>
                                    {/* <Col xs="12" md="6" xl="4">
                                        <label className="subtitle">Aadhaar No.</label>
                                        <p className="title">{allcandidates[this.state.CardValue].aadhaar_number ? allcandidates[this.state.CardValue].aadhaar_number : 'NA'}</p>
                                    </Col>
                                    <Col xs="12" md="6" xl="4">
                                        <label className="subtitle">Alternate ID Type</label>
                                        <p className="title">{allcandidates[this.state.CardValue].alternateidtype ? allcandidates[this.state.CardValue].alternateidtype : 'NA'}</p>
                                    </Col>
                                    <Col xs="12" md="6" xl="4">
                                        <label className="subtitle">Alternate ID No.</label>
                                        <p className="title">{allcandidates[this.state.CardValue].alternate_id_number ? allcandidates[this.state.CardValue].alternate_id_number : 'NA'}</p>
                                    </Col>
                                    <Col xs="12" md="6" xl="4">
                                        <label className="subtitle">Aadhaar Enrollment ID</label>
                                        <p className="title">{allcandidates[this.state.CardValue].aadhaar_enrollment_id ? allcandidates[this.state.CardValue].aadhaar_enrollment_id : 'NA'}</p>
                                    </Col> */}
                                    <Col xs="12" md="6" xl="4">
                                        <label className="subtitle">Mobile No.</label>
                                        <p className="title">
                                            {tel === '' ? 'NA' :
                                                <a href={tel} title={candidatedetaildata.phone}>{candidatedetaildata.phone}</a>
                                            }
                                        </p>
                                    </Col>
                                    <Col xs="12" md="6" xl="4">
                                        <label className="subtitle">Email Address</label>
                                        <p className="title">
                                            {mailTo === '' ? 'NA' :
                                                <a href={mailTo} title={candidatedetaildata.email_address}>{candidatedetaildata.email_address}</a>
                                            }
                                        </p>
                                    </Col>
                                    <Col xs="12" md="6" xl="4">
                                        <label className="subtitle">Date Of Birth</label>
                                        <p className="title">{candidatedetaildata.dob ? candidatedetaildata.dob : 'NA'}</p>
                                    </Col>
                                    <Col xs="12" md="6" xl="4">
                                        <label className="subtitle">Gender</label>
                                        <p className="title">{candidatedetaildata.gender ? candidatedetaildata.gender : 'NA'}</p>
                                    </Col>
                                    <Col xs="12" md="6" xl="4">
                                        <label className="subtitle">State</label>
                                        <p className="title">{candidatedetaildata.stateName ? candidatedetaildata.stateName : 'NA'}</p>
                                    </Col>
                                    <Col xs="12" md="6" xl="4">
                                        <label className="subtitle">District</label>
                                        <p className="title">{candidatedetaildata.cityName ? candidatedetaildata.cityName : 'NA'}</p>
                                    </Col>
                                    <Col xs="12" md="6" xl="4">
                                        <label className="subtitle">Pincode</label>
                                        <p className="title">{candidatedetaildata.pincode ? candidatedetaildata.pincode : 'NA'}</p>
                                    </Col>
                                    <Col xs="12" md="6" xl="4">
                                        <label className="subtitle">Address</label>
                                        <p className="title">{candidatedetaildata.address ? candidatedetaildata.address : 'NA'}</p>
                                    </Col>
                                    <Col xs="12" md="6" xl="4">
                                        <label className="subtitle">Age</label>
                                        <p className="title">{candidatedetaildata.age ? candidatedetaildata.age : 'NA'}</p>
                                    </Col>
                                    <Col xs="12" md="6" xl="4">
                                        <label className="subtitle">Candidate Id</label>
                                        <p className="title">{candidatedetaildata.candidate_id ? candidatedetaildata.candidate_id : 'NA'}</p>
                                    </Col>
                                </Row>
                                <hr className="modal-data-seperator" />
                                <Row>

                                    <Col xs="12">
                                        <h5 className="title modal-view-title">Professional Details</h5>
                                    </Col>
                                    <Col xs="12" md="6" xl="4">
                                        <label className="subtitle">Experience in Years</label>
                                        <p className="title">{candidatedetaildata.experience_in_years ? candidatedetaildata.experience_in_years : 'NA'}</p>
                                    </Col>
                                    <Col xs="12" md="6" xl="4">
                                        <label className="subtitle">Training Type</label>
                                        <p className="title">{candidatedetaildata.training_type ? candidatedetaildata.training_type : 'NA'}</p>
                                    </Col>
                                    {/* <Col xs="12" md="6" xl="4">
                                        <label className="subtitle">SDMS/NSDC Enrollment No</label>
                                        <p className="title">{allcandidates[this.state.CardValue].sdmsnsdc_enrollment_number ? allcandidates[this.state.CardValue].sdmsnsdc_enrollment_number : 'NA'}</p>
                                    </Col> */}
                                    <Col xs="12" md="6" xl="4">
                                        <label className="subtitle">Training Status</label>
                                        <p className="title">{candidatedetaildata.training_status ? candidatedetaildata.training_status : 'NA'}</p>
                                    </Col>
                                    <Col xs="12" md="6" xl="4">
                                        <label className="subtitle">Attendance Percent</label>
                                        <p className="title">{candidatedetaildata.attendence_percentage ? candidatedetaildata.attendence_percentage : 'NA'}</p>
                                    </Col>
                                    {/* <Col xs="12" md="6" xl="4">
                                        <label className="subtitle">Placement Status</label>
                                        <p className="title">{allcandidates[this.state.CardValue].placement_status ? allcandidates[this.state.CardValue].placement_status : 'NA'}</p>
                                    </Col> */}
                                    <Col xs="12" md="6" xl="4">
                                        <label className="subtitle">Employment Type</label>
                                        <p className="title">{candidatedetaildata.employment_type ? candidatedetaildata.employment_type : 'NA'}</p>
                                    </Col>
                                    <Col xs="12" md="6" xl="4">
                                        <label className="subtitle">Assessment Score</label>
                                        <p className="title">{candidatedetaildata.assessment_score ? candidatedetaildata.assessment_score : 'NA'}</p>
                                    </Col>
                                    <Col xs="12" md="6" xl="4">
                                        <label className="subtitle">Max Assessment Score</label>
                                        <p className="title">{candidatedetaildata.max_assessment_score ? candidatedetaildata.max_assessment_score : 'NA'}</p>
                                    </Col>
                                    <Col xs="12" md="6" xl="4">
                                        <label className="subtitle">Assessment Date</label>
                                        <p className="title">{candidatedetaildata.assessment_date ? candidatedetaildata.assessment_date : 'NA'}</p>
                                    </Col>
                                    <Col xs="12" md="6" xl="4">
                                        <label className="subtitle">Sector</label>
                                        <p className="title">{candidatedetaildata.sectorName ? candidatedetaildata.sectorName : 'NA'}</p>
                                    </Col>
                                    <Col xs="12" md="6" xl="4">
                                        <label className="subtitle">Job Role</label>
                                        <p className="title">{candidatedetaildata.jobroleName ? candidatedetaildata.jobroleName : 'NA'}</p>
                                    </Col>
                                    {/* <Col xs="12" md="6" xl="4">
                                        <label className="subtitle">Batch ID</label>
                                        <p className="title">{allcandidates[this.state.CardValue].batch_id ? allcandidates[this.state.CardValue].batch_id : 'NA'}</p>
                                    </Col>
                                    <Col xs="12" md="6" xl="4">
                                        <label className="subtitle">Batch Start Date</label>
                                        <p className="title">{allcandidates[this.state.CardValue].batch_start_date ? allcandidates[this.state.CardValue].batch_start_date : 'NA'}</p>
                                    </Col>
                                    <Col xs="12" md="6" xl="4">
                                        <label className="subtitle">Batch End Date</label>
                                        <p className="title">{allcandidates[this.state.CardValue].batch_end_date ? allcandidates[this.state.CardValue].batch_end_date : 'NA'}</p>
                                    </Col>
                                    <Col xs="12" md="6" xl="4">
                                        <label className="subtitle">Scheme</label>
                                        <p className="title">{allcandidates[this.state.CardValue].schemeType ? allcandidates[this.state.CardValue].schemeType : 'NA'}</p>
                                    </Col>
                                    {allcandidates[this.state.CardValue].schemeType === 'State Skill Development Mission Scheme'
                                        ?
                                        <Col xs="12" md="6" xl="4">
                                            <label className="subtitle">State Ministry</label>
                                            <p className="title">{allcandidates[this.state.CardValue].stateministry ? allcandidates[this.state.CardValue].stateministry : 'NA'}</p>
                                        </Col>
                                        :
                                        <Col xs="12" md="6" xl="4">
                                            <label className="subtitle">Central Ministry</label>
                                            <p className="title">{allcandidates[this.state.CardValue].centralministry ? allcandidates[this.state.CardValue].centralministry : 'NA'}</p>
                                        </Col>
                                    } */}


                                </Row>
                            </ModalBody>
                            <ModalFooter>
                                <Button onClick={() => this.toggleModal(mymodaval)}>Close</Button>
                            </ModalFooter>
                        </Modal>
                    );
                }

                //Card Details
                allcandidates.map((value, index) => {
                    var TrainingStatus = allcandidates[index].training_status ? allcandidates[index].training_status.charAt(0).toUpperCase() + allcandidates[index].training_status.substr(1) : '-';
                    switch (TrainingStatus) {
                        case 'Duplicateenrollment':
                            TrainingStatus = 'Duplicate Enrollment';
                            break;
                        default:
                    }
                    return final_allCandiates.push(

                        <Col xs="12" md="6" xl="4" key={index}>
                            <Card className="card z-depth-1">
                                <CardHeader className="d-flex justify-content-between bg-transparent align-items-center border-bottom-0 card-header-shadow">
                                    <div className="section-title">
                                        <CardTitle className="title card-title">{allcandidates[index].candidate_name}</CardTitle>
                                        <CardSubtitle className="subtitle">{allcandidates[index].batch_id}</CardSubtitle>
                                    </div>
                                    <Form>
                                        <label className="custom-checkbox">
                                            <input type="checkbox" value={allcandidates[index].candidateid} onChange={this.handleCheckboxChange.bind(this)} />
                                            <span className="checkmark"></span>
                                        </label>
                                    </Form>
                                </CardHeader>
                                <ListGroup className="no-border">
                                    <ListGroupItem>
                                        <label className="subtitle">Job Role</label>
                                        <p className="title mb-0">{allcandidates[index].jobroleName}</p>
                                    </ListGroupItem>
                                    <ListGroupItem>
                                        <label className="subtitle">Mobile No.</label>
                                        <p className="title mb-0"><a href={"tel:" + allcandidates[index].phone}>{allcandidates[index].phone}</a></p>
                                    </ListGroupItem>

                                    {allcandidates[index].training_center_name &&
                                        <ListGroupItem>
                                            <label className="subtitle">Training Center</label>
                                            <p className="title mb-0">{allcandidates[index].phone}</p>
                                        </ListGroupItem>}
                                    <ListGroupItem>
                                        <label className="subtitle">Sector</label>
                                        <p className="title mb-0">{allcandidates[index].sectorName}</p>
                                    </ListGroupItem>
                                    <ListGroupItem>
                                        <label className="subtitle">Training Status</label>
                                        <p className="title mb-0">{TrainingStatus}</p>
                                    </ListGroupItem>

                                    <ListGroupItem className="d-flex">
                                        <Col className="px-0 align-self-center">
                                            <CardLink title="Add Interview Details" onClick={() => this.addDetailChange(allcandidates[index].candidateid)} className="btn btn-secondary btn-raised" >Add Interview Details</CardLink>
                                        </Col>
                                        <Col xs="auto" order="last" className="action-btn pr-0 pt-0">
                                            <CardLink title="View More" onClick={() => this.viewMoreAPICall(index, allcandidates[index].candidateid)} className="btn-fab btn-fab-primary btn btn-link btn-sm">
                                                <i className="svg-icons">
                                                    <svg viewBox="0 0 24 24">
                                                        <g id="Bounding_Boxes">
                                                            <g id="ui_x5F_spec_x5F_header_copy_3"></g>
                                                            <path fill="none" d="M0,0h24v24H0V0z" />
                                                        </g>
                                                        <g id="Outline">
                                                            <g id="ui_x5F_spec_x5F_header"></g>
                                                            <g>
                                                                <path d="M12,6c3.79,0,7.17,2.13,8.82,5.5C19.17,14.87,15.79,17,12,17s-7.17-2.13-8.82-5.5C4.83,8.13,8.21,6,12,6 M12,4 C7,4,2.73,7.11,1,11.5C2.73,15.89,7,19,12,19s9.27-3.11,11-7.5C21.27,7.11,17,4,12,4L12,4z" />
                                                                <path d="M12,9c1.38,0,2.5,1.12,2.5,2.5S13.38,14,12,14s-2.5-1.12-2.5-2.5S10.62,9,12,9 M12,7c-2.48,0-4.5,2.02-4.5,4.5 S9.52,16,12,16s4.5-2.02,4.5-4.5S14.48,7,12,7L12,7z" />
                                                            </g>
                                                        </g>
                                                    </svg>
                                                </i>
                                            </CardLink>
                                        </Col>
                                    </ListGroupItem>
                                    {/* <ListGroupItem className="d-flex"> */}
                                    {/* <CardLink title="View More" onClick={() => this.viewMoreAPICall(index, allcandidates[index].candidateid)} className="btn btn-primary btn-raised mr-3" >View More</CardLink> */}

                                    {/* <CardLink title="Add Interview Details" href="javascript:void(0)" onClick={() => this.addDetailChange(allcandidates[index].candidateid)} className="btn btn-outline">Add Interview Details</CardLink> */}


                                    {/* <CardLink order="last"  class="action-btn pr-0 pt-0 col-auto"> */}
                                    {/* <a onClick={() => this.viewMoreAPICall(index, allcandidates[index].candidateid)} title="View More" class="btn-fab btn-fab-primary btn btn-link btn-sm card-link">
                                                <i class="svg-icons"><svg viewBox="0 0 24 24"><g id="Bounding_Boxes"><g id="ui_x5F_spec_x5F_header_copy_3"></g><path fill="none" d="M0,0h24v24H0V0z"></path></g><g id="Outline"><g id="ui_x5F_spec_x5F_header"></g><g><path d="M12,6c3.79,0,7.17,2.13,8.82,5.5C19.17,14.87,15.79,17,12,17s-7.17-2.13-8.82-5.5C4.83,8.13,8.21,6,12,6 M12,4 C7,4,2.73,7.11,1,11.5C2.73,15.89,7,19,12,19s9.27-3.11,11-7.5C21.27,7.11,17,4,12,4L12,4z"></path><path d="M12,9c1.38,0,2.5,1.12,2.5,2.5S13.38,14,12,14s-2.5-1.12-2.5-2.5S10.62,9,12,9 M12,7c-2.48,0-4.5,2.02-4.5,4.5 S9.52,16,12,16s4.5-2.02,4.5-4.5S14.48,7,12,7L12,7z"></path></g></g></svg></i>
                                            </a>
									    </CardLink>
                                    </ListGroupItem> */}
                                </ListGroup>
                            </Card>
                        </Col>

                    );

                })
            }
        } else {
            recordFound = -1;
        }

        return (
            <div className="content-wrapper user-flow TP-flow">
                <Sidebar {...this.props} />


                <div id="content" className="main-content">
                    <Header />
                    <EmployerCandidateSearch onSelectedChange={this.handleChild} />
                     {/* <EmployerCandidateSearch_old onSelectedChange={this.handleChild} /> */}

                    {recordFound === 1 &&
                        <Row className="d-flex align-items-center section-header search-result" >
                            <Col>
                                <h1 className="page-title">
                                    {(!!this.props.allsearchcandidates && !!this.props.allsearchcandidates.paginationOptions) ? this.props.allsearchcandidates.paginationOptions.totalcount.toLocaleString('en-IN') + ' Candidate(s) Found ' : ''}
                                </h1>

                                <span className="subtitle mb-0">Select candidates and click on <b>ADD INTERVIEW DETAILS</b> for adding multiple interview detail.</span>
                            </Col>
                            <Col xs="12" xl="auto" className="ml-auto align-items-center d-flex justify-content-xl-end">
                                <div className="button-block w-100">
                                    <Button outline color="primary" title="Add Interview Details" onClick={this.handleRedirectionToAddCandidateDetail} className="mr-0 mr-md-2 sm-floating-btn d-md-inline-block d-none" >
                                        <i className="material-icons">add</i>
                                        <span>Add Interview Details</span>
                                    </Button>
                                </div>
                            </Col>
                        </Row>
                    }

                    {recordFound === 0 &&
                        <div className="d-flex justify-content-center align-items-center error-page search-result">
                            <div className="text-center">
                                <div className="img-wrapper mb-4">
                                    <div className="circle-xl user-avatar">
                                        <img src={candidatesSVG} alt="hiringRequest" />
                                    </div>
                                </div>
                                <h1 className="h2 fw-regular mb-0">No Candidate(s) Found!</h1>
                            </div>
                        </div>
                    }

                    <div className={loaderClass} style={styles}></div>
                    {!!this.props.immidiate_hiring_request &&
                        <Row className="d-flex align-items-center section-header search-result" >
                            <Col>
                                <h1 className="page-title">
                                    {this.props.immidiate_hiring_request.data} Candidate(s) Found
                                    {/* {(!!this.props.allsearchcandidates && !!this.props.allsearchcandidates.paginationOptions) ? this.props.allsearchcandidates.paginationOptions.totalcount.toLocaleString('en-IN') + ' Candidate(s) Found ' : ''} */}
                                </h1>

                            </Col>
                        </Row>
                    }
                    {recordFound === 1 &&
                        <Row className="details-view">
                            {final_allCandiates}
                        </Row>

                    }
                    {allcandidatesModalDetails}
                    {loadMore}
                    <Footer />
                </div>
            </div>
        );
    }
}
Employer = reduxForm({
    form: 'CandidateSearch',
    renableReinitialize: true,
    asyncBlurFields: [], //to remove validation, we must need to remove it from Blur fields...
})(Employer);
function mapStateToProps(state) {
    const { users, authentication } = state;
    const { user } = authentication;
    const { immidiate_hiring_request } = state.trainingpartners; //From Reducer - trainingpartners function ...
    const { allsearchcandidates, loading } = state.allsearchcandidates; //From Reducer - allsearchcandidates function ...
    const { candidate_updated_details, loadingviewmore } = state.getcandidatedetails; //From Reducer - candidatedetails function ...
    // console.log(immidiate_hiring_request, " immidiate_hiring_request employer js...");
    return {
        user,
        users,
        allsearchcandidates,
        loading,
        candidate_updated_details,
        loadingviewmore,
        immidiate_hiring_request
    };
}

const connectedEmployer = connect(mapStateToProps)(Employer);
export { connectedEmployer as Employer };
