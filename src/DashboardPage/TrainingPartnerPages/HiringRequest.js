import React from 'react';
import { connect } from 'react-redux';
import { Header } from '../../_components/Header';
import { Footer } from '../../_components/Footer';
import { Sidebar } from '../../_components/Sidebar';
import { userActions, alertActions } from '../../_actions';
import { history } from '../../_helpers';
import hiringRequestSVG from '../../assets/images/hiring_requests_grey.svg';

import classnames from 'classnames';
import { Input, FormGroup, TabContent, TabPane, Nav, NavItem, NavLink, Card, Button, CardTitle, Row, Col, Breadcrumb, BreadcrumbItem, Badge, CardHeader, ListGroup, ListGroupItem, Modal, ModalBody, ModalHeader } from 'reactstrap';

class TrainingPartnerHiringRequest extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            activeTab: '1',
            isOpen: false,
            modalIndexValue: undefined,
            delCardValue: undefined,
            acceptCardValue: undefined,
            hiringrequest_status: 1,
            checked: false,
            positionChecked: [],
            selectAll: false,
            page: 1,
        };
        this.toggle = this.toggle.bind(this);
        this.toggleModal = this.toggleModal.bind(this);
        this.toggleModalforDelete = this.toggleModalforDelete.bind(this);
        this.toggleModalforAccept = this.toggleModalforAccept.bind(this);
        this.closeModalBox = this.closeModalBox.bind(this);
        this.changeMainHiringRequestStatus = this.changeMainHiringRequestStatus.bind(this);
        this.handleCheckboxSelection = this.handleCheckboxSelection.bind(this);
        this.handleSelectAll = this.handleSelectAll.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }
    handleSubmit(e) {
        e.preventDefault();
        const { user } = this.state;
        var hiringrequestid = this.state.positionChecked.join();
        // console.log('Send data',this.state.positionChecked.join());
        //console.log("Selection part",this.state.user,hiringrequestid);
        var mergedKeys = { hiringrequestid, ...user };
        if (mergedKeys.hiringrequestid !== undefined && mergedKeys.hiringrequestid !== '' && mergedKeys.internalstatus !== undefined && mergedKeys.internalstatus !== '') {
            if (mergedKeys.internalstatus === '-1') {
                this.changeMainHiringRequestStatus(hiringrequestid, 3);//Mark as completed will change the main hiring request status
            } else {
                //Update Status based on the selection..
                this.props.dispatch(userActions.updateHiringStatus(mergedKeys));
                this.state.page = 1;
            }
            this.setState({ positionChecked: [] });
            //console.log('Send data', this.state.positionChecked);
        } else {
            this.props.dispatch(alertActions.error("Please select appropriate status and select candidate to proceed."));
        }
    }
    handleCheckboxSelection(e) {
        // current array of options

        const checkboxArray = this.state.positionChecked
        let index

        // check if the check box is checked or unchecked
        if (e.target.checked) {
            // add the numerical value of the checkbox to options array
            checkboxArray.push(+e.target.value)
        }
        else if (this.state.selectAll === true) {
            this.setState({ selectAll: false })
            index = checkboxArray.indexOf(+e.target.value)
            checkboxArray.splice(index, 1)
        }
        else {
            // or remove the value from the unchecked checkbox from the array
            index = checkboxArray.indexOf(+e.target.value)
            checkboxArray.splice(index, 1)
        }
        //console.log('options checked', checkboxArray);
        // update the state with the new array of options
        this.setState({ positionChecked: checkboxArray })

    }
    handleChange(event) {
        const { name, value } = event.target;
        const { user } = this.state;

        this.setState({
            user: {
                ...user,
                [name]: value
            }
        });
    }
    handleSelectAll(e) {
        this.setState({ selectAll: e.target.checked }, () => console.log('selected all', this.state.selectAll));
        var rowState = [];
        if (e.target.checked) {
            if (!!this.props.allhiringrequests && this.props.allhiringrequests.data !== undefined && this.props.allhiringrequests.data.length > 0) {
                for (var i = 0; i < this.props.allhiringrequests.data.length; i++) {
                    rowState.push(this.props.allhiringrequests.data[i].hiringrequestdetailid);
                }
            }
        }
        this.setState({ positionChecked: rowState })
    };
    closeModalBox() {
        this.setState({ isOpen: !this.state.isOpen });
    }
    toggleModal = (e) => {

        this.setState({ isOpen: !this.state.isOpen, delCardValue: undefined, acceptCardValue: undefined });
        this.setState({ modalIndexValue: e });
        console.log("Modal INdex Value", e);
    }
    toggleModalforDelete = (e) => {
        this.setState({ isOpen: !this.state.isOpen, modalIndexValue: undefined, acceptCardValue: undefined });
        this.setState({ delCardValue: e });
    }
    toggleModalforAccept = (e) => {
        this.setState({ isOpen: !this.state.isOpen, modalIndexValue: undefined, delCardValue: undefined });
        this.setState({ acceptCardValue: e });


    }
    toggle(tab) {
        // const { hiringrequest_status } = this.state;        
        /* console.log("Active tab is : ", typeof this.state.activeTab);
        console.log("Active tab is : ", typeof tab); */

        if (this.state.activeTab !== tab) {
            this.state.hiringrequest_status = tab;
            this.setState({
                activeTab: tab,
                modalIndexValue: undefined,
                acceptCardValue: undefined,
                delCardValue: undefined,
                page: 1
            });
        }
        history.push({
            pathname: '/TrainingPartner-hiring-requests/' + tab,
            state: { value: this.state.hiringrequest_status }
        })

        this.getHiringRequests(1); // page = 1
    }
    componentDidMount() {
        this.getHiringRequests(1); // page = 1
    }
    getHiringRequests(pageno) {
        if (localStorage.getItem('user') !== null) {
            if (this.props.user.user_type === 'TP') var trainingpartnerid = this.props.user.data[0].trainingpartnerid
            this.props.dispatch(userActions.getHiringRequests(trainingpartnerid, this.state.hiringrequest_status, 'TrainingPartner', pageno));
        }
        else {
            history.push('/login');
            return ('');
        }
    }
    changeMainHiringRequestStatus(e, status) {
        //accept = 2
        //decline = 4
        //complete = 3
        this.props.dispatch(userActions.updateHiringRequestStatus(e, status, this.state.hiringrequest_status,undefined,1));
        //console.log('##sta',status);
        //console.log('##all values',typeof status);
        if (status === 2 || status === 4) this.setState({ isOpen: !this.state.isOpen });
        this.state.page = 1;
    }
    loadData() {
        const { page } = this.state
        this.getHiringRequests(page);
    }
    loadMore = () => {
        //First update the state and then use callback method to call next page call...
        this.setState(prevState => ({
            page: prevState.page + 1
        }), this.loadData)
    }
    render() {
        var loaderClass = '';
        //Show loader..
        //console.log('#this.props.loading',this.props.loading);
        if (this.props.loading === true) {
            loaderClass = 'loading';
        }
        const styles = {
            top: '115px',
            left: '60px'
        };

        var ErrorLable = "";

        if (this.state.activeTab === '1') ErrorLable = "Pending"
        if (this.state.activeTab === '2') ErrorLable = "Accepted"
        if (this.state.activeTab === '3') ErrorLable = "Completed"
        if (this.state.activeTab === '4') ErrorLable = "Declined"
        if (this.state.activeTab === '5') ErrorLable = "Cancelled"

        var final_hiringrequests = [];
        var loadMore = "";
        //console.log("data from Emp Hiring Request", this.props);
        if (!!this.props.allhiringrequests && this.props.allhiringrequests.paginationOptions !== undefined) {
            var pendingCount = this.props.allhiringrequests.paginationOptions.totalPendingHiringRequests;
            var acceptedCount = this.props.allhiringrequests.paginationOptions.totalAcceptedHiringRequests;
            var completedCount = this.props.allhiringrequests.paginationOptions.totalCompletedHiringRequests;
            var declinedCount = this.props.allhiringrequests.paginationOptions.totalDeclinedHiringRequests;
            var cancelledCount = this.props.allhiringrequests.paginationOptions.totalCancelledHiringRequests;

            var totalpages = this.props.allhiringrequests.paginationOptions.totalPages;
            var currentpage = this.props.allhiringrequests.paginationOptions.curPage;
            if (currentpage < totalpages) {
                loadMore = (<div className="text-center"><a title="Load More" onClick={this.loadMore} className="btn btn-outline-secondary card-link">Load More</a></div>);
            }
        }
        if (!!this.props.allhiringrequests && this.props.allhiringrequests.data !== undefined && this.props.allhiringrequests.data.length > 0) {

            //Decline request Modal Box
            if (this.state.delCardValue !== undefined) {
                var delmymodaval = this.state.delCardValue;

                var reajectRequestModal = (
                    <Modal backdrop={false} centered isOpen={this.state.isOpen} className="alert-modal" >
                        <ModalHeader toggle={() => this.toggleModalforDelete(delmymodaval)} className="d-block section-title text-center border-bottom-0 no-border">
                            Are you sure you want to decline this request?
                        </ModalHeader>
                        <ModalBody>
                            <div className="d-flex justify-content-center">
                                <Button color="primary" className="btn-raised mr-3" onClick={() => this.changeMainHiringRequestStatus(delmymodaval, 4)}>Yes</Button>
                                <Button ouline='true' color="default" onClick={this.closeModalBox}>No</Button>
                            </div>
                        </ModalBody>

                    </Modal>
                );
            }
            //End of Decline request Modal Box

            //Accept request Modal Box
            if (this.state.acceptCardValue !== undefined) {
                var acceptmymodaval = this.state.acceptCardValue;

                var acceptRequestModal = (
                    <Modal backdrop={false} centered isOpen={this.state.isOpen} className="alert-modal">
                        <ModalHeader toggle={() => this.toggleModalforAccept(acceptmymodaval)} className="d-block section-title text-center border-bottom-0 no-border">
                            Are you sure you want to accept this request?
                        </ModalHeader>
                        <ModalBody>
                            <div className="d-flex justify-content-center">
                                <Button color="primary" className="btn-raised mr-3" onClick={() => this.changeMainHiringRequestStatus(acceptmymodaval, 2)}>Yes</Button>
                                <Button ouline="true" color="default" onClick={this.closeModalBox}>No</Button>
                            </div>
                        </ModalBody>
                    </Modal>
                );
            }
            //End of Accept request Modal Box


            var hiringrequests = this.props.allhiringrequests.data;
            if (this.state.modalIndexValue !== undefined) {
                var mymodaval = this.state.modalIndexValue;

                var tel = hiringrequests[this.state.modalIndexValue].contact_person_phone ? "tel:" + hiringrequests[this.state.modalIndexValue].contact_person_phone : ''
                var mailTo = hiringrequests[this.state.modalIndexValue].contact_person_email ? "mailto:" + hiringrequests[this.state.modalIndexValue].contact_person_email : ''
                var location = hiringrequests[this.state.modalIndexValue].cityname + ', ' + hiringrequests[this.state.modalIndexValue].statename;
                var interviewdatetime = hiringrequests[this.state.modalIndexValue].interview_date + ", " + hiringrequests[this.state.modalIndexValue].interview_time;
                var other_perks = hiringrequests[this.state.modalIndexValue].other_perks ? hiringrequests[this.state.modalIndexValue].other_perks : '-';
                var other_information = hiringrequests[this.state.modalIndexValue].other_information !== '' ? hiringrequests[this.state.modalIndexValue].other_information : '-';
                var pf = hiringrequests[this.state.modalIndexValue].is_pf === 'yes' ? 1 : 0;
                var esic = hiringrequests[this.state.modalIndexValue].is_esic === 'yes' ? 1 : 0;
                var food = hiringrequests[this.state.modalIndexValue].is_food === 'yes' ? 1 : 0;
                var transportation = hiringrequests[this.state.modalIndexValue].is_transportation === 'provided' ? 1 : 0;
                var accomodation = hiringrequests[this.state.modalIndexValue].is_accomodation === 'provided' ? 1 : 0;
                var Benefits = '';
                if (pf === 1 || esic === 1 || food === 1 || transportation === 1 || accomodation === 1) {
                    Benefits = (
                        <Col xs="12">
                            <label className="subtitle">Benefits</label>
                            <p className="title">
                                {pf === 1 &&
                                    <Badge color="default" >Pf</Badge>
                                }
                                {esic === 1 &&
                                    <Badge color="default" >Esic</Badge>
                                }
                                {food === 1 &&
                                    <Badge color="default" >Food</Badge>
                                }
                                {transportation === 1 &&
                                    <Badge color="default" >Transportation</Badge>
                                }
                                {accomodation === 1 &&
                                    <Badge color="default" >Accomodation</Badge>
                                }
                            </p>
                        </Col>
                    );
                }

                var hiringRequestsModalDetails = (
                    <Modal  backdrop={false}centered size="lg" isOpen={this.state.isOpen} toggle={() => this.toggleModal(mymodaval)}>
                        <ModalHeader toggle={() => this.toggleModal(mymodaval)} className="border-bottom-0 modal-header-shadow align-items-center">
                            <div className="section-title">
                                <h5 className="title modal-title">{hiringrequests[this.state.modalIndexValue].candidate_name}</h5>
                            </div>
                        </ModalHeader>
                        <ModalBody className="details-view">
                            <Row>
                                <Col xs="12">
                                    <h5 className="title modal-view-title">Job Details</h5>
                                </Col>
                                <Col xs="12">
                                    <label className="subtitle">Employer</label>
                                    <p className="title">{hiringrequests[this.state.modalIndexValue].industry_partner_name}</p>
                                </Col>
                                <Col xs="12">
                                    <label className="subtitle">Jobrole</label>
                                    <p className="title">{hiringrequests[this.state.modalIndexValue].jobrolename}</p>
                                </Col>
                                <Col xs="12" md="6">
                                    <label className="subtitle">Location</label>
                                    <p className="title">{location}</p>
                                </Col>
                                <Col xs="12" md="6">
                                    <label className="subtitle">Salary</label>
                                    <p className="title">{hiringrequests[this.state.modalIndexValue].salary}</p>
                                </Col>
                                <Col xs="12" md="6">
                                    <label className="subtitle">Other Perks</label>
                                    <p className="title">{other_perks}</p>
                                </Col>
                                <Col xs="12" md="6">
                                    <label className="subtitle">Other Information</label>
                                    <p className="title">{other_information}</p>
                                </Col>
                                {Benefits}

                            </Row>
                            <hr className="modal-data-seperator" />
                            <Row>
                                <Col xs="12">
                                    <h5 className="title modal-view-title">Interview Details</h5>
                                </Col>
                                <Col xs="12">
                                    <label className="subtitle">Date &amp; Time</label>
                                    <p className="title">{interviewdatetime}</p>
                                </Col>
                                {/*  <Col xs="12">
                                    <label className="subtitle">Other Information</label>
                                    <p className="title mb-1">{hiringrequests[this.state.modalIndexValue].other_information}</p>
                                </Col> */}
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">Contact Person</label>
                                    <p className="title">{hiringrequests[this.state.modalIndexValue].contact_person_name}</p>
                                </Col>
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">Mobile</label>
                                    <p className="title">
                                        {tel === '' ? 'NA' :
                                            <a href={tel} title={hiringrequests[this.state.modalIndexValue].contact_person_phone}>{hiringrequests[this.state.modalIndexValue].contact_person_phone}</a>
                                        }
                                    </p>
                                </Col>
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">Email</label>
                                    <p className="title">
                                        {mailTo === '' ? 'NA' :
                                            <a href={mailTo} title={hiringrequests[this.state.modalIndexValue].contact_person_email}>{hiringrequests[this.state.modalIndexValue].contact_person_email}</a>
                                        }
                                    </p>
                                </Col>
                            </Row >
                        </ModalBody >
                    </Modal >
                );
            }

            hiringrequests.map((value, index) => {
                /* var name = hiringrequests[index].candidate_name;
                var initials = name.match(/\b\w/g) || [];
                initials = ((initials.shift() || '') + (initials.pop() || '')).toUpperCase(); */
                var interviewDateTime = hiringrequests[index].interview_date + ", " + hiringrequests[index].interview_time;
                var hiringRequestStatusDb = hiringrequests[index].hiring_request_status;
                
                var internalStatus = hiringrequests[index].internal_status;
                var StatusLable = "";
                
                if (internalStatus === 1) StatusLable = "Available"
                if (internalStatus === 2) StatusLable = "Not Available"
                if (internalStatus === 3) StatusLable = "Not Reachable"
                if (internalStatus === 4) StatusLable = "Hired"
                if (internalStatus === 5) StatusLable = "Joined"
                if (internalStatus === 6) StatusLable = "Left"
                if (internalStatus === 7) StatusLable = "Interviewed But Not Hired"
                if (internalStatus === 8) StatusLable = "No Show"

                return final_hiringrequests.push(
                    <Col xs="12" md="6" xl="4" key={index}>
                        <Card className="card z-depth-1">
                            <CardHeader className="d-flex justify-content-between bg-transparent align-items-center border-bottom-0 card-header-shadow">
                                <div className="section-title">
                                    <CardTitle className="title card-title">{hiringrequests[index].candidate_name}</CardTitle>
                                    <ul className="list-inline list-inline-separator">
                                        <li className="list-inline-item">{hiringrequests[index].statename}</li>
                                        <li className="list-inline-item"> {hiringrequests[index].cityname}</li>
                                    </ul>
                                </div>
                                {hiringRequestStatusDb === 2 &&
                                    <label className="custom-checkbox">
                                        <input
                                            onChange={this.handleCheckboxSelection}
                                            checked={(this.state.page < 2 && this.state.selectAll) || (this.state.positionChecked.some(i => i === hiringrequests[index].hiringrequestdetailid))}
                                            key={index}
                                            type="checkbox"
                                            value={hiringrequests[index].hiringrequestdetailid}
                                        />
                                        <span className="checkmark"></span>
                                    </label>

                                }
                            </CardHeader>
                            <ListGroup className="no-border">
                                {hiringRequestStatusDb === 2 &&
                                    <ListGroupItem>
                                        <Badge color="secondary">{StatusLable}</Badge>
                                    </ListGroupItem>
                                }
                                <ListGroupItem>
                                    <label className="subtitle">Employer</label>
                                    <p className="title mb-0">{hiringrequests[index].industry_partner_name}</p>
                                </ListGroupItem>
                                <ListGroupItem>
                                    <label className="subtitle">Interview on</label>
                                    <p className="title mb-0">{interviewDateTime}</p>
                                </ListGroupItem>
                                <ListGroupItem>
                                    <label className="subtitle">Sector</label>
                                    <p className="title mb-0">{hiringrequests[index].sectorname}</p>
                                </ListGroupItem>
                                <ListGroupItem>
                                    <label className="subtitle">Job Role</label>
                                    <p className="title mb-0">{hiringrequests[index].jobrolename}</p>
                                </ListGroupItem>
                                {hiringRequestStatusDb === 5 &&
                                    <ListGroupItem className="info-tile-wrapper">
                                        <p className="m-0 d-inline-block align-middle danger-info"><i className="material-icons align-middle">error</i>{hiringrequests[index].cancel_reason}</p>
                                    </ListGroupItem>
                                }
                                <ListGroupItem className="d-flex">
                                    <Col className="pl-0 align-self-center">
                                        <Button color="primary" className="btn-raised mr-2" onClick={() => this.toggleModal(index)}>View More</Button>
                                    </Col>
                                    {hiringRequestStatusDb === 1 &&
                                        <Col xs="auto" className="order-last action-btn pr-0 pt-0">
                                            <Button color="link" size="sm" className="btn-fab btn-fab-secondary" onClick={() => this.toggleModalforAccept(hiringrequests[index].hiringrequestdetailid)}>
                                                <i className="material-icons">done</i>
                                            </Button>
                                            <Button color="link" size="sm" className="btn-fab btn-fab-danger " onClick={() => this.toggleModalforDelete(hiringrequests[index].hiringrequestdetailid)}>
                                                <i className="material-icons">close</i>
                                            </Button>
                                        </Col>
                                    }
                                </ListGroupItem>
                            </ListGroup>
                        </Card>
                    </Col>


                );
            })
        }
        return (

            <div className="content-wrapper user-flow TP-flow">
                <Sidebar {...this.props} />

                <div id="content" className="main-content">
                    <Header />

                    <div className="d-block d-md-flex align-items-center section-header">
                        <div>
                            <h1 className="page-title">Hiring Requests</h1>
                            <Breadcrumb>
                                <BreadcrumbItem><a href="/TrainingPartnerDashboard">Dashboard</a></BreadcrumbItem>
                                <BreadcrumbItem active>Hiring Requests</BreadcrumbItem>
                            </Breadcrumb>
                        </div>
                    </div>

                    <div>
                        <Nav tabs className="mb-4">
                            <NavItem>
                                <NavLink className={classnames({ active: this.state.activeTab === '1' })} onClick={() => { this.toggle('1'); }} >
                                    Pending <Badge color="default" pill>{pendingCount}</Badge>
                                </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink className={classnames({ active: this.state.activeTab === '2' })} onClick={() => { this.toggle('2'); }} >
                                    Accepted <Badge color="default" pill>{acceptedCount}</Badge>
                                </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink className={classnames({ active: this.state.activeTab === '3' })} onClick={() => { this.toggle('3'); }} >
                                    Completed <Badge color="default" pill>{completedCount}</Badge>
                                </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink className={classnames({ active: this.state.activeTab === '4' })} onClick={() => { this.toggle('4'); }} >
                                    Declined <Badge color="default" pill>{declinedCount}</Badge>
                                </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink className={classnames({ active: this.state.activeTab === '5' })} onClick={() => { this.toggle('5'); }} >
                                    Cancelled <Badge color="default" pill>{cancelledCount}</Badge>
                                </NavLink>
                            </NavItem>
                        </Nav>
                        <TabContent activeTab={this.state.activeTab}>
                            <TabPane tabId={this.state.activeTab}>
                                {this.state.activeTab === '2' && acceptedCount > 0 &&

                                    <div className="mb-3">

                                        <form name="SubmitHandleRequestform" className="w-100" onSubmit={this.handleSubmit}>
                                            <div className="form-inline">
                                                <FormGroup className="mr-2 mr-md-4">
                                                    <label className="custom-checkbox">
                                                        <input
                                                            onChange={this.handleSelectAll}
                                                            checked={this.state.selectAll}
                                                            key={"SelectAll"}
                                                            type="checkbox"
                                                            value={"All"}
                                                        />
                                                        <span className="checkmark">Select All</span>
                                                    </label>
                                                </FormGroup>
                                                <FormGroup className="mr-sm-2 w-sm-160">
                                                    <Input type="select" name="internalstatus" onChange={this.handleChange}>
                                                        <option value="">Select Status</option>
                                                        <option value="1">Mark as Available</option>
                                                        <option value="2" >Mark as Not Available</option>
                                                        <option value="3" >Mark as Not Reachable</option>
                                                        <option value="5">Mark as Joined</option>
                                                        <option value="6" >Mark as Left</option>
                                                        <option value="-1">Mark as Completed</option>
                                                    </Input>
                                                </FormGroup>
                                                <Button color="primary" className="btn-raised">Apply</Button>
                                            </div>
                                        </form>
                                    </div>
                                }
                                <div className={loaderClass} style={styles}></div>
                                {final_hiringrequests.length > 0 ?
                                    <Row>
                                        {final_hiringrequests}
                                    </Row>
                                    :
                                    <div className="d-flex justify-content-center align-items-center error-page">
                                        <div className="text-center">
                                            <div className="img-wrapper mb-4">
                                                <div className="circle-xl user-avatar">
                                                    <img src={hiringRequestSVG} alt="hiringRequest" />
                                                </div>
                                            </div>
                                            <h1 className="h2 fw-regular mb-0">No {ErrorLable} Hiring Request Yet!</h1>
                                        </div>
                                    </div>
                                }
                                {hiringRequestsModalDetails}
                            </TabPane>
                        </TabContent>
                    </div>
                    {acceptRequestModal}
                    {reajectRequestModal}
                    {loadMore}
                    <Footer />
                </div>

            </div>
        );

    }
}

function mapStateToProps(state) {
    const { users, authentication } = state;
    const { user } = authentication;
    const { allhiringrequests, loading } = state.hiringrequests; //From Reducer - hiringrequests function ...
    return {
        user,
        users,
        allhiringrequests,
        loading
    };
}
const connectedTrainingPartnerHiringRequest = connect(mapStateToProps)(TrainingPartnerHiringRequest);
export { connectedTrainingPartnerHiringRequest as TrainingPartnerHiringRequest };