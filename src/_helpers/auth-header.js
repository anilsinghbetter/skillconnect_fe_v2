export function authHeader() {
    // return authorization header with jwt token
    let user = JSON.parse(localStorage.getItem('user'));

    //console.log("Auth Header", user);

    if (user && user.status) {
        return { 'Authorization': 'Bearer ' + user.status };
    } else {
        return {};
    }
}