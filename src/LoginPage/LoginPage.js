import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Col, Media, Row, FormGroup,Button } from 'reactstrap';
import candidate from '../assets/images/ic_candidates_white.svg';
import CandidateDemand from '../assets/images/ic_candidate_demand.svg';
import EMP from '../assets/images/ic_EMP_white.svg';
import hiringRequest from '../assets/images/ic_hiring_requests_white.svg';
import TP from '../assets/images/ic_TP_white.svg';
import { alertActions, userActions } from '../_actions';
import { Footer } from '../_components/Footer';
import { Header } from '../_components/Header';
import { reduxForm, Field } from 'redux-form';

const renderField = ({ input, label, type, placeholder,autoFocus, textarea, meta: { touched, error, warning, invalid } }) => {
	const textareaType = <textarea {...input} placeholder={label} type={type} className={`form-control ${touched && invalid ? 'has-danger' : ''}`} />;
	return <div >
		<div className={(error && touched ? ' has-error' : '')}>
			{type !== 'password' ? <label>{label}</label> : ''}
			<div> {textarea ? textareaType : <input {...input} autoFocus={autoFocus} type={type} placeholder={placeholder} className="form-control" />}
				{touched && ((error && <div className="help-block">{error}</div>) || (warning && <span>{warning}</span>))}
			</div>

		</div>
	</div>
};
const validate = values => {

	const errors = {}

	if (!values.tpid) {
		errors.tpid = 'User ID is required'
	}
	if (!values.password) {
		errors.password = 'Password is required'
	}
	return errors;
}

class LoginPage extends React.Component {
	constructor(props) {
		super(props);

		// reset login status
		this.props.dispatch(userActions.logout());

		this.state = {
			tpid: '',
			password: '',
			candidate_name: '',//for search only....
			submitted: false,
			applyWidthClass: '',
			searchApply: false,


		};
		this.handleChange = this.handleChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
		this.handleSearch = this.handleSearch.bind(this);
	}

	handleChange(e) {
		const { name, value } = e.target;
		this.setState({ [name]: value });
	}

	handleSubmit(e) {
		e.preventDefault();

		this.setState({ submitted: true });
		const { tpid, password } = this.state;
		const { dispatch } = this.props;

		if (tpid && password) {
			dispatch(userActions.login(tpid, password));
		}
		else {
			//  if tpid & pwd are blank
			dispatch(alertActions.clear(''));
		}
	}
	toggleRememberMe() {
		this.setState({
			rememberMe: !this.state.rememberMe
		});
	}
	handleSearch(e) {
		e.preventDefault();
		const { candidate_name } = this.state;
		const { dispatch } = this.props;
		//console.log("In search button", this.state.candidate_name);

		if (candidate_name) {
			dispatch(userActions.getSearchCandidate(this.state));
		}
		else {
			dispatch(userActions.searchClear(''));
		}
	}

	updateDimensions() {
		const winWidth = window.innerWidth
		const maxWidth = 767
		const winHeight = window.innerHeight
		//console.log("updating state",winWidth,maxWidth);
		//console.log("max width", maxWidth);
		//console.log("window width", winWidth);
		//console.log("window height", winHeight);
		if (true || winWidth < maxWidth) {
			this.setState({
				applyWidthClass: winHeight + "px",
			});
		}
	}
	componentWillMount() {
		this.updateDimensions();
	}
	componentDidMount() {

		//setTimeout(() => this.setState({ loading: false }), 1500);
		//this._input.focus();
		this.updateDimensions();
		window.addEventListener("resize", this.updateDimensions.bind(this));

		this.props.dispatch(userActions.getHomePageStatistics());
	}
	componentWillUnmount() {
		window.removeEventListener("resize", this.updateDimensions.bind(this));
	}

	render() {
		//const { loggingIn, user, alert, candidate_name, users, height,searchApply } = this.props;
		const { applyWidthClass } = this.state;
		const { loading } = this.props;
		var loaderClass;
		if (loading === true) {
			loaderClass = 'loading';
		}
		const styles_loader = {
			top: '20px',
			left: '500px'
		};

		const styles = {
			minHeight: applyWidthClass//'497px'
		};

		var totalFutureRequests = 0;
		var totalCandidates = 0;
		var totalTrainingPartners = 0;
		var totalEmployers = 0;
		var totalHiringRequests = 0;
		if (!!this.props.homepagetatistics) {
			//console.log('# homepagetatistics',this.props.homepagetatistics.data.counts[0]);
			totalFutureRequests = this.props.homepagetatistics.data.counts[0].totalFutureRequests > 0 ? this.props.homepagetatistics.data.counts[0].totalFutureRequests : 0;
			totalCandidates = this.props.homepagetatistics.data.counts[0].totalCandidates > 0 ? this.props.homepagetatistics.data.counts[0].totalCandidates : 0;
			totalTrainingPartners = this.props.homepagetatistics.data.counts[0].totalTrainingPartners > 0 ? this.props.homepagetatistics.data.counts[0].totalTrainingPartners : 0;
			totalEmployers = this.props.homepagetatistics.data.counts[0].totalEmployers > 0 ? this.props.homepagetatistics.data.counts[0].totalEmployers : 0;
			totalHiringRequests = this.props.homepagetatistics.data.counts[0].totalHiringRequests > 0 ? this.props.homepagetatistics.data.counts[0].totalHiringRequests : 0;
		}
		return (

			<div className="content-wrapper user-onboarding landing-page login-page half-bg">
				<Header />
				<div className="container">
					<Row>
						<Col xs="12" md="6">

							<div className="left-block half-bg-custom-height" style={styles}>
								<div className="d-md-none d-block tag-line onboarding-text-content">
									<ul className="list-inline">
										<li className="list-inline-item"><h2 className="h1 fw-bold">Build</h2></li>
										<li className="list-inline-item"><h2 className="h1 fw-bold">Connect</h2></li>
										<li className="list-inline-item"><h2 className="h1 fw-bold">Hire</h2></li>
									</ul>
									<p>
										A Platform for Blue Collar Skilling to Meet Demands of the Industry.
								</p>
								</div>

								<div className="form-content-card">
									<div className="section-title">
										<h2 className="title">Welcome Back</h2>
										<span className="subtitle">Login to your SkillConnect Account</span> </div>
									<form id="frm-login" className="form frm-login" onSubmit={this.handleSubmit}>
										<FormGroup>
											<Field name="tpid" autoFocus={true}  type="text" component={renderField} placeholder="Enter your registered user ID" label="User ID" onChange={this.handleChange} />
										</FormGroup>

										<FormGroup>
											<div className="d-flex"><label htmlFor="password">Password</label>
												<Link to="/forgot-password" title="Forgot Password?" className="text-underline ml-auto forgot-link">
													Forgot Password?
					                               </Link>
											</div>
											<Field name="password" autoFocus={false} type="password" component={renderField} placeholder="Enter your account's Password" onChange={this.handleChange} />
										</FormGroup>

										<FormGroup>
											<Button color="primary" className="btn btn-primary btn-lg btn-raised btn-block">
												Login
                            				</Button>
										</FormGroup>
									</form>
									<div className="terms-block pmd-display-0 text-muted">
										<p className="mb-0">By logging in, you agree to SkillConnect's</p>
										<p className="mb-0"><a href="/terms-and-conditions">Terms &amp; Conditions </a> &amp; <a href="/privacy-policy">Privacy Policy.</a></p>
									</div>
									<div className="or-divider text-center"><span>Or</span></div>
									<div className="row form-interlinks">
										<div className="col-lg-6 pr-0">
											<h3 className="mb-0">New to <strong>SkillConnect</strong>?</h3>
											{/* <span className="subtitle mb-lg-0">Start your free account</span>  */}
										</div>
										<div className="col-lg-6 text-lg-right text-left">
											<Link to="/SelectUserType" className="btn btn-outline-primary btn-lg" title="Register Now">
												Register Now
			                               </Link>
										</div>
									</div>
								</div>
							</div>
						</Col>
						<div className="col-md-6 col-12 right-block">
							<div className="right-content-block theme-inverse half-bg-custom-height onboarding-text-content" style={styles} >

								{/* <div className="d-md-block d-none">
							<h2 className="h1 fw-bold">
								Our vision is to become the largest technology platform for in-formal &amp; semi-formal workforce and impact a Billion Indians
							</h2>
							<p>
								Only digital platform in India with NSDC approved Job roles in 40+ Sectors. One stop platform for training partners and employers to connect with each other.
							</p>
							<p>
								Credible data of present and future supply of trained and certified candidates based on job role and location.
							</p>
						</div> */}

								<div className="d-md-block d-none">
									<ul className="list-inline">
										<li className="list-inline-item"><h2 className="h1 fw-bold">Build</h2></li>
										<li className="list-inline-item"><h2 className="h1 fw-bold">Connect</h2></li>
										<li className="list-inline-item"><h2 className="h1 fw-bold">Hire</h2></li>
									</ul>
									<p>
										A Platform for Blue Collar Skilling to Meet Demands of the Industry.
								</p>
									<div className={loaderClass} style={styles_loader}></div>
									{this.props.loading === false &&
										<div className="info-counter info-counter-lg no-border mb-0">
											<Media className="d-flex align-items-center mb-3 mb-lg-4">
												<Media left className="circle-lg user-avatar avatar-filled mr-3">
													<img src={CandidateDemand} alt="CandidateDemand" />
												</Media>
												<Media body>
													<h3 className="ff-roboto h1">{totalFutureRequests.toLocaleString('en-IN')}</h3>
													<p className="mb-0">Demand (Next 12-months)</p>
												</Media>
											</Media>
										</div>
									}
									{this.props.loading === false &&
										<div className="info-counter">

											<Row>
												<Col xl="6">
													<Media className="d-flex align-items-center mb-3 mb-lg-4">
														<Media left className="circle-lg user-avatar avatar-outline mr-3">
															<img src={candidate} alt="candidate" />
														</Media>
														<Media body>
															<h3>{totalCandidates.toLocaleString('en-IN')}</h3>
															<p className="subtitle">Candidates</p>
														</Media>
													</Media>
												</Col>

												<Col xl="6">
													<Media className="d-flex align-items-center mb-3 mb-lg-4">
														<Media left className="circle-lg user-avatar avatar-outline mr-3">
															<img src={hiringRequest} alt="Hiring Request" />
														</Media>
														<Media body>
															<h3>{totalHiringRequests.toLocaleString('en-IN')}</h3>
															<p className="subtitle">Hiring Requests</p>
														</Media>
													</Media>
												</Col>

												<Col xl="6">
													<Media className="d-flex align-items-center mb-3 mb-lg-4 mb-xl-0">
														<Media left className="circle-lg user-avatar avatar-outline mr-3">
															<img src={TP} alt="Training Partners" />
														</Media>
														<Media body>
															<h3>{totalTrainingPartners.toLocaleString('en-IN')}</h3>
															<p className="subtitle">Training Partners</p>
														</Media>
													</Media>
												</Col>

												<Col xl="6">
													<Media className="d-flex align-items-center">
														<Media left className="circle-lg user-avatar avatar-outline mr-3">
															<img src={EMP} alt="Employers" />
														</Media>
														<Media body>
															<h3>{totalEmployers.toLocaleString('en-IN')}</h3>
															<p className="subtitle">Employers</p>
														</Media>
													</Media>
												</Col>
											</Row>
										</div>
									}
									<ul className="list-inline small">
										<li className="list-inline-item"><h3>Demand Aggregation</h3></li>
										<li className="list-inline-item"><h3>Training</h3></li>
										<li className="list-inline-item"><h3>Certification</h3></li>
										<li className="list-inline-item"><h3>Placement</h3></li>
									</ul>
								</div>
							</div>
							<Footer />
						</div>

					</Row>
				</div>
			</div>
		);
	}
}

LoginPage = reduxForm({
	form: 'LoginPage',
	validate,
	enableReinitialize: true,
})(LoginPage);

function mapStateToProps(state) {
	const { alert, users } = state;
	const { loggingIn, user } = state.authentication;
	const { homepagetatistics, loading } = state.generaldata;
	return {
		loggingIn, user, alert, users, homepagetatistics, loading
	};
}

const connectedLoginPage = connect(mapStateToProps)(LoginPage);
export { connectedLoginPage as LoginPage };

