import React from 'react';
// import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

class CardIcon extends React.Component {
constructor(props) {
super(props);
this.state = {
user_flow:true,//not using now, but it can be use later.
userType:false,
};
}
render() {
return(<div className="card-icon text-center"><span className="ic-indication ic-success">
<i className="material-icons"><strong>done</strong></i>
</span></div>
);
}
}
function mapStateToProps(state) {
const { registering } = state.registration;
return {
registering
};
}
const connectedCardIcon = connect(mapStateToProps)(CardIcon);
export { connectedCardIcon as CardIcon };