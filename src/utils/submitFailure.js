export function submitFailure(fieldList) {
    return (errors) => {
        if(!errors){
            return false;
        }
        // Field Error(s). We need to step through fieldList to guarantee ordering (since redux-form returns an errors object, not array or map)
        fieldList.find((field) => {
            if (errors[field]) {
                const elem = document.querySelector(`input[name=${field}], select[name=${field}]`);
                if (elem) {
                    elem.focus();
                    return true;
                }
            }
            // continue stepping through fieldList
            return false;
        });
    }
}

//Alternate Solution

// # Focus on first error
/* const flattenObject = (c, d = '.') => {
    const r = {};
    (function f(o, p) {
        Object.keys(o).forEach(k => (o[k] && typeof o[k] === 'object' ? f(o[k], p ? `${p}${d}${k}` : k) : (r[p ? `${p}${d}${k}` : k] = o[k])));
    }(c));
    return r;
  };
  
// When Immutable:
// The errors are _not_ in `REGISTER_FIELD` order so we cant just "use" the first one..
// (possibly requires an ordered list reviver when getting errors?)

// so we try to get the first error element in another way, using a DOM query:

// Note: You can't query a possible error class in `onSubmitFail` as the `render` may not have yet happened.

// We do this using a DOM selector of all the fields that have error'ed and use [`querySelector`](https://developer.mozilla.org/en-US/docs/Web/API/Document/querySelector) which
// Returns the first Element within the document that matches the specified selector, or group of selectors.
const errorEl = document.querySelector(
  // flattenObject: https://github.com/hughsk/flat/issues/52
  Object.keys(flattenObject(errors)).map(fieldName => `[name="${fieldName}"]`).join(',')
);
if (errorEl && errorEl.focus) { // npe
  // if (errorEl.scrollIntoView) {
  //   errorEl.scrollIntoView(); // fails as the site has a fixed/floating header
  // }
  errorEl.focus(); // this scrolls without visible scroll
}
} */