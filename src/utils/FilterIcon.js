import React from 'react';
import { connect } from 'react-redux';
class FilterIcon extends React.Component {
    constructor(props) {
        super(props);
        this.state = {        
        //Initial State..
        };       
    }
    render() {
  return (
     <svg>
      <g><g>
          <path d="M9.072,21.162v-9.81L2.214,3.41C1.847,2.961,1.889,2.037,2.837,2.037h18.292
                  c0.892,0,1.098,0.816,0.624,1.373l-6.693,7.732v6.069c0,0.248-0.083,0.458-0.293,0.624l-4.28,3.95
                  C9.992,22.151,9.072,21.954,9.072,21.162z M4.668,3.703l5.864,6.775c0.124,0.165,0.207,0.334,0.207,0.54v8.272l2.659-2.412v-6.024
                  c0-0.207,0.083-0.375,0.206-0.541l5.694-6.61H4.668L4.668,3.703z"></path>
      </g></g>
  </svg>
  )
}
}
function mapStateToProps(state) {
    //Add state values which we need to integrate with props..
    return {
    //registering
    };
    }
    const connectedFilterIcon = connect(mapStateToProps)(FilterIcon);
    export { connectedFilterIcon as FilterIcon };