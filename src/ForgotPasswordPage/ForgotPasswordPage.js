import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { reduxForm, Field } from 'redux-form';
import { CardHeader,CardFooter,Card,CardBody,CardText,CardTitle } from 'reactstrap';
import { Footer } from '../_components/Footer';
import { Header } from '../_components/Header';
import { userActions } from '../_actions';
const renderField = ({ input, label, type, meta: { touched, error} }) => (

    <div className= ''>
       <div className={'form-group' + (error && touched ? ' has-error' : '')}>
           <label>{label}</label>
           <input {...input} type={type} className="form-control" />
           <div>
               {touched && ((error && <div className="help-block">{error}</div>))}
           </div>
       </div>
   </div>
);

const validate = values => {

    const errors = {}

    if (!values.tpid) {
        errors.tpid = 'Please enter your registered ID'
    }
    if (!values.email_address) {
        errors.email_address = 'Please enter your registered email address'
    }/*
    if (!values.contact_mobile) {
        errors.contact_mobile = 'Please enter Contact Mobile No'
    }*/
    return errors;
}

class ForgotPasswordPage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            tpid: '',
            email_address:'',           
            submitted: false,
            applyWidthClass: ''
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    updateDimensions() {
        const winWidth = window.innerWidth
        const maxWidth = 767
        const winHeight = window.innerHeight

        //console.log("max width", maxWidth);
        //console.log("window width", winWidth);
        //console.log("window height", winHeight);

        if (true || winWidth < maxWidth) {
            this.setState({
                applyWidthClass: winHeight + "px",
            });
        }
        else {
            this.setState({
                applyWidthClass: '',
            });
        }
    }
    componentWillMount() {

        this.updateDimensions();
    }
    componentDidMount() {
        this.updateDimensions();
        window.addEventListener("resize", this.updateDimensions.bind(this));
    }
    componentWillUnmount() {
        window.removeEventListener("resize", this.updateDimensions.bind(this));
    }
    handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });
    }

    handleSubmit(e) {
       // e.preventDefault();

        this.setState({ submitted: true });
        const { tpid,email_address } = this.state;
        const { dispatch } = this.props;
        
        if (tpid && email_address) {
           dispatch(userActions.forgotPassword(tpid,email_address)); 
                     
        }       
    }
    

    render() {
        const { handleSubmit } = this.props;
        const { applyWidthClass } = this.state;       
        // const { tpid,email_address, submitted } = this.state;
        const styles = {
            minHeight: applyWidthClass//'497px'
        };
        return (
            <div className="content-wrapper user-onboarding landing-page">
            <Header/>
		<div className="container">
			<div className="d-flex justify-content-center align-items-center window-height" style={styles}>
                <Card className="card card-md z-depth">
                    <CardHeader className="border-bottom-0 bg-transparent section-title text-center">
                        <CardTitle className="title card-title">Forgot Password?</CardTitle>
                    </CardHeader>
                    <CardBody>
                    <form id="frm-forgot" className="form frm-forgot" onSubmit={handleSubmit(this.handleSubmit)}>
                                   
                        <CardText className="text-center"> Don't worry! Please enter your respective ID and registered Email Address and we'll help you to reset your password.
                        </CardText>
                    <Field name="tpid" type="text" component={renderField} label="Training Partner ID/Employer ID/Training Center ID " onChange={this.handleChange} />
                       {/* <div className="or-divider text-center"><span>Or</span></div>*/}
                        <Field name="email_address" type="text" component={renderField} label="Email Address" onChange={this.handleChange} />

                        <button className="btn btn-primary btn-raised btn-lg btn-block" title="Send Email">Send Email</button>                               
                       
                   </form>                   
                    </CardBody>

                    <CardFooter className="border-top-0 bg-transparent">
                        <Link to="/login" className="btn-link" title="Return to login">Return to login</Link>
                    </CardFooter>

                </Card>
			</div>
		</div>
        <Footer/>
	</div>

        );
    }
}

ForgotPasswordPage = reduxForm({
    form: 'ForgotPasswordPage',
    // fields: ['nsdc_registration_number','legal_entity_name','contact_email_address'],
    validate,
    enableReinitialize: true,
})(ForgotPasswordPage);

function mapStateToProps(state) {
    const {alert} = state;
    const { loggingIn,user } = state.authentication;
    return {
        loggingIn,user,alert
    };
}

const connectedLoginPage = connect(mapStateToProps)(ForgotPasswordPage);
export { connectedLoginPage as ForgotPasswordPage }; 
