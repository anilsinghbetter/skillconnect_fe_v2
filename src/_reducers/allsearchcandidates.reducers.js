import { userConstants } from '../_constants';

export function allsearchcandidates(state = {}, action) {
  switch (action.type) {

    //Get candidate search data for Employers...
    case userConstants.GETALL_SEARCH_CANDIDATES_REQUEST:
      if (!!state.allsearchcandidates) {
        return { loading: true,allsearchcandidates : state.allsearchcandidates };
      } else {
        return { loading: true };
      }
    case userConstants.GETALL_SEARCH_CANDIDATES_FAILURE:
      return { error: action.error };
    case userConstants.GETALL_SEARCH_CANDIDATES_SUCCESS:
      if (!!state.allsearchcandidates && action.isFirstTime === 1) { //after search pagination append data
        const oldstate = state.allsearchcandidates;
        //console.log("Old value ", oldstate);
        const currentstate = action.users;
        //console.log("current value ", currentstate);
        if (oldstate.data !== undefined && currentstate.data.length > 0) {
          var updatedState = [...oldstate.data, ...currentstate.data]
          //console.log("New value here", updatedState);

          if (updatedState !== undefined) {
            var newobjct = {};
            newobjct.staus = action.users.status;
            newobjct.paginationOptions = action.users.paginationOptions;
            newobjct.data = updatedState;
            //console.log("New value ", newobjct);
            return { allsearchcandidates: newobjct, loading: false }
          }
        } else {
          return { allsearchcandidates: action.users, loading: false };
        }
      }
      else {
        return { allsearchcandidates: action.users, loading: false };
      }

      break;

    case userConstants.GETALL_SEARCH_CANDIDATES_CLEAR:
      return { allsearchcandidates: action.users, loading: false };  


    default:
      return state
  }
}