import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';

import { authentication } from './authentication.reducer';
import { registration } from './registration.reducer';
import { users } from './users.reducer';
import { alert } from './alert.reducer';
import { forgotpassword } from './forgotpassword.reducer';
import { resetpassword } from './resetpassword.reducer';
import {trainingpartners} from './trainingpartners.reducers';
import {hiringrequests} from './hiringrequests.reducers';
import {futurehiringrequests} from './futurehiringrequests.reducers';
import {allsearchcandidates} from './allsearchcandidates.reducers';
import {jobdata} from './jobdata.reducers';
import {generaldata} from './common.reducers';
import { presentdistrictdata } from './district_present_address.reducers';
import { allpreffereddistrictdata } from './district_for_preferred.reducers';
import {centralministrydata} from './centralministry.reducers';
import {searchform} from './searchform.reducer';
import {searchformnew} from './searchformnew.reducer';
import {setting} from './setting.reducer';
import {getcandidatedetails} from './candidatedetails.reducer'
import {getcandidateviewmoredetails} from './candidatedetails.reducer'
import {getcenterdetails} from './centerdetails.reducer';
import {allErrorFiles} from './bulkuploadstatus.reducer';
import {employerForTC} from './employerForTc.reducers';
//For Dashboard
import {dashboardData} from './dashboard.reducer';
import {pagecontentData} from './pagecontent.reducer';

const rootReducer = combineReducers({
  authentication,
  registration,
  users,
  alert,
  forgotpassword,
  resetpassword,
  trainingpartners,
  hiringrequests,
  allsearchcandidates,
  generaldata,
  presentdistrictdata,
  allpreffereddistrictdata,
  allErrorFiles,
  jobdata,
  centralministrydata,
  searchform,
  searchformnew,
  setting,
  getcandidatedetails,
  getcandidateviewmoredetails,
  getcenterdetails,
  futurehiringrequests,
  dashboardData,
  pagecontentData,
  employerForTC,
  form: formReducer // ← redux-form
});

export default rootReducer;