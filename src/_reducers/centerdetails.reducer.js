import { userConstants } from '../_constants';

export function getcenterdetails(state = {}, action) {
    switch (action.type) {
        //Get Training centers detail [get data for particular center id]
        case userConstants.GETALL_TRAININGCENTERDETAILS_REQUEST:
            return { loading: true };
        case userConstants.GETALL_TRAININGCENTERDETAILS_CLEAR:
            return {};
        case userConstants.GETALL_TRAININGCENTERDETAILS_FAILURE:
            return { error: action.error };
        case userConstants.GETALL_TRAININGCENTERDETAILS_SUCCESS:
            return { trainingcenterdata: action.users };

        default:
            return state
    }
}