import { userConstants } from '../_constants';

export function forgotpassword(state = {}, action) {
  switch (action.type) {
    case userConstants.FORGOTPWD_REQUEST:
      return { forgotpassword_request: true };
    case userConstants.FORGOTPWD_SUCCESS:
      return {};
    case userConstants.FORGOTPWD_FAILURE:
      return {};
    default:
      return state
  }
}