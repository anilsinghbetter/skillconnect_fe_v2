import { pagecontentConstants } from '../_constants';

export function pagecontentData(state = {}, action) {

    switch (action.type) {

    //Get page content Data...
    case pagecontentConstants.GET_PAGECONTENT_DATA_REQUEST:
      return { loading: true };
    case pagecontentConstants.GET_PAGECONTENT_DATA_FAILURE:
      return { error: action.error };
    case pagecontentConstants.GET_PAGECONTENT_DATA_SUCCESS:
      return { pagecontentdata: action.users };

    default:
      return state
  }
}