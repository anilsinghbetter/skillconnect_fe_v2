import { userConstants } from '../_constants';

export function searchformnew(state = {}, action) {
  switch (action.type) {

    //Get all form data while adding SEARCH form..
    case userConstants.GETALL_SEARCFORMDATA_NEW_FAILURE:
      return { error: action.error };
    case userConstants.GETALL_SEARCHFORMDATA_NEW_SUCCESS:
      return { searchformdatanew: action.users };
    default:
      return state
  }
}