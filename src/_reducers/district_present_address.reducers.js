import { userConstants } from '../_constants';

export function presentdistrictdata(state = {}, action) {
  //console.log("my action",action);
  switch (action.type) {

      //Get all District value while changing the request..
    case userConstants.GETALL_DISTRICT_PRESENT_FAILURE:
      return { error: action.error };
    case userConstants.GETALL_DISTRICT_PRESENT_SUCCESS:
      return { districtlistpresent: action.users };

    default:
      return state
  }
}