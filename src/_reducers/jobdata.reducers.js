import { userConstants } from '../_constants';

export function jobdata(state = {}, action) {
  //console.log("my action",action);
  switch (action.type) {

     //Get all Jobroles value while changing the request..
    case userConstants.GETALL_JOBROLES_REQUEST:
    return { loading: true };
  case userConstants.GETALL_JOBROLES_CLEAR:
    return {};
  case userConstants.GETALL_JOBROLES_FAILURE:
    return { error: action.error };
  case userConstants.GETALL_JOBROLES_SUCCESS:
    return { jobroles: action.users };


    default:
      return state
  }
}