import { userConstants } from '../_constants';

export function centralministrydata(state = {}, action) {
  //console.log("my action",action);
  switch (action.type) {

     //Get all Jobroles value while changing the request..
    case userConstants.GETALL_CENTRALMINISTRIES_REQUEST:
    return { loading: true };
  case userConstants.GETALL_CENTRALMINISTRIES_CLEAR:
    return {};
  case userConstants.GETALL_CENTRALMINISTRIES_FAILURE:
    return { error: action.error };
  case userConstants.GETALL_CENTRALMINISTRIES_SUCCESS:
    return { centralministriesdata: action.users };


    //Get all Jobroles value while changing the request..
    case userConstants.GETALL_STATE_REQUEST:
    return { loading: true };
  case userConstants.GETALL_STATE_CLEAR:
    return {};
  case userConstants.GETALL_STATE_FAILURE:
    return { error: action.error };
  case userConstants.GETALL_STATE_SUCCESS:
    return { statedata: action.users };



    default:
      return state
  }
}