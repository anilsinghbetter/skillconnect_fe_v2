import { userConstants } from '../_constants';

export function generaldata(state = {}, action) {
  //console.log("my action",action);
  switch (action.type) {

    //Get all District value while changing the request..
    case userConstants.GETALL_DISTRICT_REQUEST:
      return { loading: true };
    case userConstants.GETALL_DISTRICT_CLEAR:
      return {};
    case userConstants.GETALL_DISTRICT_FAILURE:
      return { error: action.error };
    case userConstants.GETALL_DISTRICT_SUCCESS:
      return { districtlist: action.users };

    //Get all District value while changing the request..
    case userConstants.GET_HOMEPAGESTATISTICS_REQUEST:
      return { loading: true };
    case userConstants.GET_HOMEPAGESTATISTICS_FAILURE:
      return { error: action.error, loading: false };
    case userConstants.GET_HOMEPAGESTATISTICS_SUCCESS:
      return { homepagetatistics: action.users, loading: false };  

    default:
      return state
  }
}