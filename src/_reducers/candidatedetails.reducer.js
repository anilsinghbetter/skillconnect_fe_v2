import { userConstants } from '../_constants';

export function getcandidatedetails(state = {}, action) {
  //console.log("my action",action);
  switch (action.type) {

    //Get all District value while changing the request..
    case userConstants.GETALL_CANDIDATEDETAILS_REQUEST:
      return { loadingviewmore: true };
    case userConstants.GETALL_CANDIDATEDETAILS_CLEAR:
      return {};
    case userConstants.GETALL_CANDIDATEDETAILS_FAILURE:
      return { error: action.error,loadingviewmore: false };
    case userConstants.GETALL_CANDIDATEDETAILS_SUCCESS:
      return { candidate_updated_details: action.users,loadingviewmore: false };

      
      /*
        //Deleting Candidate..
    case userConstants.DELETECANDIDATE_REQUEST:
    // add 'deleting:true' property to user being deleted
    return {
      ...state,
      items: state.items.map(user =>
        user.id === action.id
          ? { ...user, deleting: true }
          : user
      )
    };
  case userConstants.DELETECANDIDATE_SUCCESS:
    // remove deleted user from state
    console.log("DELETED@@@@@@@",...state);
    console.log("ACTION ID DELETED@@@@@@@",action.id);

    return {
      items: state.items.filter(user => user.id !== action.id)
    };
  case userConstants.DELETECANDIDATE_FAILURE:
    // remove 'deleting:true' property and add 'deleteError:[error]' property to user 
    return {
      ...state,
      items: state.items.map(user => {
        if (user.id === action.id) {
          // make copy of user without 'deleting:true' property
          const { deleting, ...userCopy } = user;
          // return copy of user with 'deleteError:[error]' property
          return { ...userCopy, deleteError: action.error };
        }

        return user;
      })
    };  */   

    default:
      return state
  }
}
export function getcandidateviewmoredetails(state = {}, action) {
  //console.log("my action",action);
  switch (action.type) {

    //Get all District value while changing the request..
    case userConstants.GETALL_CANDIDATE_VIEW_MORE_DETAILS_REQUEST:
      return { loadingviewmore: true };
    case userConstants.GETALL_CANDIDATE_VIEW_MORE_DETAILS_CLEAR:
      return {};
    case userConstants.GETALL_CANDIDATE_VIEW_MORE_DETAILS_FAILURE:
      return { error: action.error,loadingviewmore: false };
    case userConstants.GETALL_CANDIDATE_VIEW_MORE_DETAILS_SUCCESS:
      return { candidate_view_more_details: action.users,loadingviewmore: false };


    default:
      return state
  }
}