import { userConstants } from '../_constants';

export function employerForTC(state = {}, action) {
  //console.log("my action",action);
  switch (action.type) {

    //   case userConstants.GET_EMPLOYERS_REQUEST:
    //   return { loading: true };
    // case userConstants.GET_EMPLOYERS_CLEAR:
    //   return {};
    case userConstants.GET_EMPLOYERS_FAILURE:
      return { error: action.error };
    case userConstants.GET_EMPLOYERS_SUCCESS:
      return { employerName: action.users };

    default:
      return state
  }
}