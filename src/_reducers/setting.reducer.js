import { userConstants } from '../_constants';

export function setting(state = {}, action) {
  // console.log("my action",action);
  switch (action.type) {

    //Get all EMPLOYER value while changing the request..
    case userConstants.GETALL_EMPLOYERLIST_REQUEST:
      return { loading: true };
    case userConstants.GETALL_EMPLOYERLIST_FAILURE:
      return { error: action.error };
    case userConstants.GETALL_EMPLOYERLIST_SUCCESS:
      return { employerDetails: action.users };
    
    //get all TP value  
    case userConstants.GETALL_TRAININGPARTNERLIST_REQUEST:
      return { loading: true };
    case userConstants.GETALL_TRAININGPARTNERLIST_FAILURE:
      return { error: action.error };
    case userConstants.GETALL_TRAININGPARTNERLIST_SUCCESS:
      return { trainingPartnerDetails: action.users };

    default:
      return state
  }
}