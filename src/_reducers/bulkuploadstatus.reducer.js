import { userConstants } from '../_constants';

export function allErrorFiles(state = {}, action) {
    
    switch (action.type) {

        case userConstants.GETALL_BULKUPLOADSTATUS_REQUEST:
            return { loading: true };
        case userConstants.GETALL_BULKUPLOADSTATUS_FAILURE:
            return { error: action.error,loading: false };
        case userConstants.GETALL_BULKUPLOADSTATUS_SUCCESS:
            return { ErrorFileList: action.users,loading: false };
        default:
            return state
    }

}