import { userConstants } from '../_constants';

export function hiringrequests(state = {}, action) {
  switch (action.type) {

    //Get Hiring Requests List...
    case userConstants.GETALL_HIRINGREQUESTLIST_REQUEST:
      if (!!state.allhiringrequests && action.id.pageNo > 1) {
        return { allhiringrequests: state.allhiringrequests, loading: true }
      } else {
        return { loading: true };
      }
    case userConstants.GETALL_HIRINGREQUESTLIST_FAILURE:
      return { error: action.error };
    case userConstants.GETALL_HIRINGREQUESTLIST_SUCCESS:

      var updatedValues = "";
      if (!!state.allhiringrequests) {
        const oldstate = state.allhiringrequests;
        //console.log("Old value ", oldstate);
        const currentstate = action.users;
        //console.log("Old value for hiring ", currentstate.paginationOptions.internalStatus);
        if (oldstate.data !== undefined && currentstate.data.length > 0) {// && currentstate.paginationOptions.internalStatus !== 2
          var updatedState = [...oldstate.data, ...currentstate.data]
          //console.log("New value here", updatedState);

          updatedValues = updatedState.filter(user => user.hiring_request_status === currentstate.data[0].hiring_request_status);
          //console.log("filtered value here", updatedValues);

          if (updatedState !== undefined) {
            var newobjct = {};
            newobjct.staus = action.users.status;
            newobjct.paginationOptions = action.users.paginationOptions;
            newobjct.data = updatedValues;
            return { allhiringrequests: newobjct, loading: false }
          }
        } else {
          //console.log("blank state", action.users);
          return { allhiringrequests: action.users, loading: false };
        }
      }
      else {
        return { allhiringrequests: action.users, loading: false };
      }
      break;

    //Update Hiring Requests List...
    case userConstants.UPDATEHIRINGREQUEST_REQUEST:
      return { loading: true };
    case userConstants.UPDATEHIRINGREQUEST_SUCCESS:
      return { hiringrequests_update_status: action.users }
    case userConstants.UPDATEHIRINGREQUEST_FAILURE:
      return { error: action.error };

    /* Code for cancel request with pagination
  case userConstants.UPDATEHIRINGREQUEST_REQUEST:
    return { allhiringrequests: state.allhiringrequests };
  case userConstants.UPDATEHIRINGREQUEST_SUCCESS:
    var updatedValues = "";
    if (!!state.allhiringrequests) {
      //console.log('# all requests');
      updatedValues = state.allhiringrequests.data.filter(user => user.hiringrequestdetailid !== action.id);
      //console.log('# all requests updates',updatedValues);
      var newobjct = {};
      newobjct.staus = state.allhiringrequests.status;
      newobjct.paginationOptions = state.allhiringrequests.paginationOptions;
      newobjct.data = updatedValues;
      //console.log("requests updates new ", newobjct);
      return { allhiringrequests: newobjct }
    } else {
      return { allhiringrequests: state.allhiringrequests }
    }
  case userConstants.UPDATEHIRINGREQUEST_FAILURE:
    return { error: action.error };  */


    //Get Hiring Requests Detail For Candidate...
    case userConstants.GETALL_CANDIADATE_HIRINGREQUESTDETAIL_REQUEST:
      return { loading: true };
    case userConstants.GETALL_CANDIADATE_HIRINGREQUESTDETAIL_FAILURE:
      return { error: action.error };
    case userConstants.GETALL_CANDIADATE_HIRINGREQUESTDETAIL_SUCCESS:
      return { candidate_hiring_request_updated_details: action.users };


    default:
      return state
  }
}