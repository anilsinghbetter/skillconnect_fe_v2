import { userConstants } from '../_constants';

export function futurehiringrequests(state = {}, action) {
  switch (action.type) {

    //Add Future Hiring Requests...
    case userConstants.FUTURE_HIRINGREQ_REQUEST:
      return { loading: true };
    case userConstants.FUTURE_HIRINGREQ_FAILURE:
      return { error: action.error };
    case userConstants.FUTURE_HIRINGREQ_SUCCESS:
      return { futurehiringrequests: action.users };



    default:
      return state
  }
}