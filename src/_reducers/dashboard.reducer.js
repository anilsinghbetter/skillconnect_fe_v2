import { dashboardConstants } from '../_constants';

export function dashboardData(state = {}, action) {

    switch (action.type) {

    //Get Common dashboard data...
    case dashboardConstants.GETALL_DASHBOARD_DATA_REQUEST:
      return { loading: true };
    case dashboardConstants.GETALL_DASHBOARD_DATA_FAILURE:
      return { error: action.error };
    case dashboardConstants.GETALL_DASHBOARD_DATA_SUCCESS:
      return { dashboarddata: action.users };

    default:
      return state
  }
}