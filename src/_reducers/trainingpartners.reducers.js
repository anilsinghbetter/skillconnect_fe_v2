import { userConstants } from '../_constants';

export function trainingpartners(state = {}, action) {
  //var updatedState;
  switch (action.type) {

    case userConstants.GETALL_REQUEST:
      return {
        loading: true
      };
    case userConstants.GETALL_SUCCESS:
      return {
        items: action.users
      };
    case userConstants.GETALL_FAILURE:
      return {
        error: action.error
      };
    case userConstants.DELETE_REQUEST:
      // add 'deleting:true' property to user being deleted
      return {
        ...state,
        items: state.items.map(user =>
          user.id === action.id
            ? { ...user, deleting: true }
            : user
        )
      };
    case userConstants.DELETE_SUCCESS:
      // remove deleted user from state
      return {
        items: state.items.filter(user => user.id !== action.id)
      };
    case userConstants.DELETE_FAILURE:
      // remove 'deleting:true' property and add 'deleteError:[error]' property to user 
      return {
        ...state,
        items: state.items.map(user => {
          if (user.id === action.id) {
            // make copy of user without 'deleting:true' property
            const { deleting, ...userCopy } = user;
            // return copy of user with 'deleteError:[error]' property
            return { ...userCopy, deleteError: action.error };
          }

          return user;
        })
      };
    //Candidate Search & Get Reducers.
    case userConstants.GETALL_CANDIDATES_REQUEST:
      return {
        loading: true
      };
    case userConstants.GETALL_CANDIDATES_SUCCESS:
      return {
        items: action.users
      };
    case userConstants.GETALL_CANDIDATES_FAILURE:
      return {
        error: action.error
      };
    case userConstants.GETALL_CANDIDATES_CLEAR:
      return {};


    //Get Candidates List...
    case userConstants.GETALL_CANDIDATELIST_REQUEST:
      if (!!state.items) {
        return { items: state.items, loading: true }
      } else {
        return { loading: true };
      }
    case userConstants.GETALL_CANDIDATELIST_CLEAR:
      return { loading: false };
    case userConstants.GETALL_CANDIDATELIST_FAILURE:
      return { error: action.error, loading: false };
    case userConstants.GETALL_CANDIDATELIST_SUCCESS:
      if (!!state.items) {
        const oldstate = state.items;
        //console.log("Old value ", oldstate);
        const currentstate = action.users;
        //console.log("current value ", currentstate);
        if (oldstate.data !== undefined && currentstate.data.length > 0) {
          let updatedState = [...oldstate.data, ...currentstate.data]
          //console.log("New value here", updatedState);
          if (updatedState !== undefined) {
            //Remove Duplicate Values
            updatedState = updatedState.filter((thing, index, self) =>
              index === self.findIndex((t) => (
                t.candidateid === thing.candidateid
              ))
            )
            //Remove Duplicate Values Code over
            let newobjct = {};
            newobjct.staus = action.users.status;
            newobjct.paginationOptions = action.users.paginationOptions;
            newobjct.data = updatedState;
            // console.log("New value ", newobjct);
            return { items: newobjct, loading: false }
          }
        } else {
          return { items: action.users, loading: false };
        }
      }
      else {
        return { items: action.users, loading: false };
      }
      break;

    //Deleting Candidate..
    case userConstants.DELETECANDIDATE_REQUEST:
      return { items: state.items };
    case userConstants.DELETECANDIDATE_SUCCESS:
      var updatedValues = "";
      if (!!state.items) {
        updatedValues = state.items.data.filter(user => user.candidateid !== action.id)
        let newobjct = {};
        newobjct.staus = state.items.status;
        newobjct.paginationOptions = state.items.paginationOptions;
        newobjct.data = updatedValues;
        //console.log("requests updates new ", newobjct);
        return { items: newobjct }
      } else {
        return { items: state.items }
      }
    case userConstants.DELETECANDIDATE_FAILURE:
      return { error: action.error };


    //Get all form data while adding candidate form..
    case userConstants.GETALL_CANDIDATEFORMDATA_REQUEST:
      return { loading: true };
    case userConstants.GETALL_CANDIDATEFORMDATA_CLEAR:
      return {};
    case userConstants.GETALL_CANDIDATEFORMDATA_FAILURE:
      return { error: action.error };
    case userConstants.GETALL_CANDIDATEFORMDATA_SUCCESS:
      return { candidateformdata: action.users };


    //Get All Centers List...
    case userConstants.GETALL_CENTERS_REQUEST:
      if (!!state.allcenters) {
        return { allcenters: state.allcenters, loading: true }
      } else {
        return { loading: true };
      }
    case userConstants.GETALL_CENTERS_CLEAR:
      return {};
    case userConstants.GETALL_CENTERS_FAILURE:
      return { error: action.error };
    case userConstants.GETALL_CENTERS_SUCCESS:
      //console.log("here ", state);
      if (!!state.allcenters) {
        const oldstate = state.allcenters;
        //console.log("Old value ", oldstate);
        const currentstate = action.users;
        //console.log("current value ", currentstate);
        if (oldstate.data !== undefined && currentstate.data.length > 0) {
          let updatedState = [...oldstate.data, ...currentstate.data]
          //console.log("New value here", updatedState);

          if (updatedState !== undefined) {
            //Remove Duplicate Values
            updatedState = updatedState.filter((thing, index, self) =>
              index === self.findIndex((t) => (
                t.trainingcenterid === thing.trainingcenterid
              ))
            )
            //Remove Duplicate Values Code over
            let newobjct = {};
            newobjct.staus = action.users.status;
            newobjct.paginationOptions = action.users.paginationOptions;
            newobjct.data = updatedState;
            //console.log("New value ", newobjct);
            return { allcenters: newobjct, loading: false }
          }
        } else {
          return { allcenters: action.users, loading: false };
        }
      }
      else {
        return { allcenters: action.users, loading: false };
      }
      break;

    //Delete training center    
    case userConstants.TRAININGCENTER_DELETE_REQUEST:
      if (!!state.allcenters) {
        return { allcenters: state.allcenters }
      } else if(!!state.searchcenters) {
        return { searchcenters : state.searchcenters };
      }
      break;
    case userConstants.TRAININGCENTER_DELETE_SUCCESS:
      var updatedTCValues = "";
      var newobjct = {};
      if (!!state.allcenters) {
        updatedTCValues = state.allcenters.data.filter(user => user.trainingcenterid !== action.id)
        newobjct.staus = state.allcenters.status;
        newobjct.paginationOptions = state.allcenters.paginationOptions;
        newobjct.data = updatedTCValues;
        return { allcenters: newobjct }
      } else if(!!state.searchcenters){
        updatedTCValues = state.searchcenters.data.filter(user => user.trainingcenterid !== action.id)
        newobjct.staus = state.searchcenters.status;
        newobjct.paginationOptions = state.searchcenters.paginationOptions;
        newobjct.data = updatedTCValues;
        //console.log("requests updates new ", newobjct);
        return { searchcenters: newobjct }
      }else {
        return { allcenters: state.allcenters }
      }
    case userConstants.TRAININGCENTER_DELETE_FAILURE:
      return { error: action.error };


    //Add Candidate Other(Hiriing Request) Details
    case userConstants.CANDIDATEDETAILREGISTER_REQUEST:
      return { loading: true };
    case userConstants.CANDIDATEDETAILREGISTER_CLEAR:
      return {};
    case userConstants.CANDIDATEDETAILREGISTER_FAILURE:
      return { error: action.error };
    case userConstants.CANDIDATEDETAILREGISTER_SUCCESS:
      return { otherdetails_hiring_request: action.users };

    //Add Immidiate HR (Hiriing Request) Details
    case userConstants.IMMIDIATEHIRINGREQUESTREGISTER_REQUEST:
      return { loading: true };
    case userConstants.IMMIDIATEHIRINGREQUESTREGISTER_FAILURE:
      return { error: action.error };
    case userConstants.IMMIDIATEHIRINGREQUESTREGISTER_SUCCESS:
      return { immidiate_hiring_request: action.user };  

    //Get candidate search data for Training Partners...
    case userConstants.GETALL_TRAININGPARTNER_SEARCH_CANDIDATES_REQUEST:
      if (!!state.searchitems) { //after search pagination append data
        return { loading: true, searchitems: state.searchitems };
      } else {
        return { loading: true };
      }
    case userConstants.GETALL_TRAININGPARTNER_SEARCH_CANDIDATES_FAILURE:
      return { error: action.error };
    case userConstants.GETALL_TRAININGPARTNER_SEARCH_CANDIDATES_SUCCESS:
      if (!!state.searchitems && action.isFirstTime === 1) { //after search pagination append data
        const oldstate = state.searchitems;
        //console.log("Old value ", oldstate);
        const currentstate = action.users;
        //console.log("current value ", currentstate);
        if (oldstate.data !== undefined && currentstate.data.length > 0) {
          let updatedState = [...oldstate.data, ...currentstate.data]
          //console.log("New value here", updatedState);

          if (updatedState !== undefined) {
            let newobjct = {};
            newobjct.staus = action.users.status;
            newobjct.paginationOptions = action.users.paginationOptions;
            newobjct.data = updatedState;
            // console.log("New value ", newobjct);
            return { searchitems: newobjct, loading: false };
          }
        } else {
          return { searchitems: action.users, loading: false };
        }
      }
      else {
        return { searchitems: action.users, loading: false };
      }
      break;

    //Get training centers search data for Training Partners...
    case userConstants.GETALL_CENTERS_SEARCH_REQUEST:
      if (!!state.searchcenters) {
        return { loading: true, searchcenters: state.searchcenters };
      } else {
        return { loading: true };
      }
    case userConstants.GETALL_CENTERS_SEARCH_FAILURE:
      return { error: action.error, loading: false };
    case userConstants.GETALL_CENTERS_SEARCH_SUCCESS:
      if (!!state.searchcenters && action.isFirstTime === 1) { //after search pagination append data
        const oldstate = state.searchcenters;
        //console.log("Old value ", oldstate);
        const currentstate = action.users;
        //console.log("current value ", currentstate);
        if (oldstate.data !== undefined && currentstate.data.length > 0) {
          let updatedState = [...oldstate.data, ...currentstate.data]
          //console.log("New value here", updatedState);

          if (updatedState !== undefined) {
            let newobjct = {};
            newobjct.staus = action.users.status;
            newobjct.paginationOptions = action.users.paginationOptions;
            newobjct.data = updatedState;
            // console.log("New value ", newobjct);
            return { searchcenters: newobjct, loading: false }
          }
        } else {
          return { searchcenters: action.users, loading: false };
        }
      }
      else {
        return { searchcenters: action.users, loading: false };
      }
      break;

    default:
      return state
  }
}