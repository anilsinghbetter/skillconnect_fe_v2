import { userConstants } from '../_constants';

export function allpreffereddistrictdata(state = {}, action) {
  // console.log("my action",action);
  switch (action.type) {
      //Get all District value while changing the request..
    case userConstants.GETALL_PREFERRED_DISTRICT_FAILURE:
      return { error: action.error };
    case userConstants.GETALL_PREFERRED_DISTRICT_SUCCESS:
      return { districtlistpreferred: action.users };

    default:
      return state
  }
}