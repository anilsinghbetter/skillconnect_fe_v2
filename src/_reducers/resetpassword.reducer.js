import { userConstants } from '../_constants';

export function resetpassword(state = {}, action) {
   
  switch (action.type) {
    case userConstants.RESETPWD_REQUEST:
      return { resetpassword_request: true };
    case userConstants.RESETPWD_SUCCESS:
      return { items: action.user };
     // return {...state, [action.user.data]: action.user.data};
    case userConstants.RESETPWD_FAILURE:
      return {};
    default:
      return state
  }
}