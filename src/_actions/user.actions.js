import { userConstants } from '../_constants';
import { userService } from '../_services';
import { alertActions } from './';
import { history } from '../_helpers';

export const userActions = {
    login,
    logout,
    register,
    registerAsEmployer,
    getAll,
    delete: _delete,
    forgotPassword,
    resetPassword,
    getAllSectors,
    getCandidates,
    getCandidateFormData,
    getSearchCandidate,
    getSectorJobroleMapping,
    getStates,
    getDistrict,
    getDistrictPresent,
    getAllPrefferedCity,
    getJobRoles,
    getSchemeCentralMinistries,
    getCenters,
    addUpdateCandidates,
    updateTrainingPartnerProfile,
    updateIndustryPartnerProfile,
    updateTrainingCenterProfile,
    addupdateTrainingCenter,
    addUpdateCandidateDetailsHiringRequest,
    addUpdateImmidiateFutureHiringRequest,
    getCandidateSearchFormData,
    getHiringRequests,
    getHiringRequestDetailsForCandidate,
    getTrainingCenterDetails,
    getCandidateDetails,
    deleteCandidate,
    deleteTrainingCenter,
    updateHiringRequestStatus,
    addUpdateFutureHiringRequest,
    updateHiringStatus,
    searchClear,
    getEmployer,
    ChangePassword,
    getTrainingPartner,
    updateContactMessage,
    getTrainingPartnerSearchCandidate,
    getTrainingPartnerSearchCenter,
    getCandidateViewMoreDetails,
    getbulkUploadStatus,
    getHomePageStatistics,
    getImmidiateHiringRequestFormData,
    getJobRolesByTP,
    getEmployersForTC,
    deleteFile,
};

function login(tpid, password) {

    return dispatch => {
        dispatch(request({ tpid }));

        userService.login(tpid, password)
            .then(
                user => {
                    if (user.status === false) {
                        //console.log("in action user error", user);

                        dispatch(failure(user.error.message));
                        dispatch(alertActions.error(user.error.message));
                    }
                    else {
                        dispatch(success(user));
                        // console.log("in action user success", user);
                        // console.log("Successfully Logged In. Legal Entity Name :", user.data[0].legal_entity_name);
                        // dispatch(alertActions.success('Successfully Logged In.'));

                        if (user.user_type === 'TP') history.push('/TrainingPartner-dashboard');
                        else if (user.user_type === 'EMP') history.push('/search-candidate');
                        else if (user.user_type === 'TC') history.push('/candidates');
                        //else history.push('/TrainingPartner/check');
                    }
                },
                error => {
                    //console.log("action error", error);
                    dispatch(failure(error));
                    dispatch(alertActions.error(error));

                }
            );
    };

    function request(user) { return { type: userConstants.LOGIN_REQUEST, user } }
    function success(user) { return { type: userConstants.LOGIN_SUCCESS, user } }
    function failure(error) {
        return { type: userConstants.LOGIN_FAILURE, error }
    }
}
function logout() {
    userService.logout();
    return { type: userConstants.LOGOUT };
}
function register(user) {

    // console.log("IN registraiton");
    return dispatch => {
        dispatch(request(user));
        userService.register(user)
            .then(
                user => {

                    if (user.status === false) {
                        // console.log("error while registration", user);
                        dispatch(failure(user.error.message));
                        dispatch(alertActions.error(user.error.message));
                    }
                    else {
                        //  console.log("user details", user);
                        dispatch(success());
                        history.push('/registration-success');
                        // dispatch(alertActions.success('Registration successful'));
                    }

                },
                error => {
                    dispatch(failure(error));
                    dispatch(alertActions.error(error));
                }
            );
    };

    function request(user) { return { type: userConstants.REGISTER_REQUEST, user } }
    function success(user) { return { type: userConstants.REGISTER_SUCCESS, user } }
    function failure(error) {
        return { type: userConstants.REGISTER_FAILURE, error }
    }
}
function registerAsEmployer(user) {

    return dispatch => {
        dispatch(request(user));
        userService.registerAsEmployer(user)
            .then(
                user => {
                    if (user.status === false) {
                        //  console.log("error while registration", user);
                        dispatch(failure(user.error.message));
                        dispatch(alertActions.error(user.error.message));
                    }
                    else {
                        //   console.log("employer details", user);
                        dispatch(success());
                        history.push('/registration-success');
                        // dispatch(alertActions.success('Registration successful'));
                    }

                },
                error => {
                    dispatch(failure(error));
                    dispatch(alertActions.error(error));
                }
            );
    };

    function request(user) { return { type: userConstants.REGISTER_REQUEST, user } }
    function success(user) { return { type: userConstants.REGISTER_SUCCESS, user } }
    function failure(error) {
        return { type: userConstants.REGISTER_FAILURE, error }
    }
}

function addUpdateCandidates(user) {

    return dispatch => {
        dispatch(request(user));
        userService.addUpdateCandidates(user)
            .then(
                user => {
                    if (user.status === false) {
                        // console.log("error while adding candiates", user);
                        // dispatch(failure(user.error.message));
                        dispatch(alertActions.error(user.error.message));
                    }
                    else {
                        //Remove key from local storage
                        var uniqueKey = 'getCandidates';
                        userService.removeLocalStorageCache(uniqueKey);
                        // console.log("candidate details", user);
                        // dispatch(success());
                        dispatch(alertActions.success(user.data.message));
                        //history.push('/candidates');
                        setTimeout(() => {
                            window.location.href = '/candidates';
                        }, 5000);
                    }
                },
                error => {
                    // dispatch(failure(error));
                    dispatch(alertActions.error(error));
                }
            );
    };

    function request(user) { return { type: userConstants.CANDIDATEREGISTER_REQUEST, user } }
    // function success(user) { return { type: userConstants.CANDIDATEREGISTER_SUCCESS, user } }
    // function failure(error) {
    //     return { type: userConstants.CANDIDATEREGISTER_FAILURE, error }
    // }
}

function updateTrainingPartnerProfile(user) {

    return dispatch => {
        dispatch(request(user));
        userService.updateTrainingPartnerProfile(user)
            .then(
                user => {
                    if (user.status === false) {
                        //  console.log("error while updating training partner profile", user);
                        dispatch(failure(user.error.message));
                        dispatch(alertActions.error(user.error.message));
                    }
                    else {
                        // console.log("TP DETAILS IN ACTION", user);
                        dispatch(success());
                        dispatch(alertActions.success(user.data.message));
                        //history.push('/Settings');
                    }
                },
                error => {
                    dispatch(failure(error));
                    dispatch(alertActions.error(error));
                }
            );
    };

    function request(user) { return { type: userConstants.UPDATETRAININGPARTNERPROFILE_REQUEST, user } }
    function success(user) { return { type: userConstants.UPDATETRAININGPARTNERPROFILE_SUCCESS, user } }
    function failure(error) { return { type: userConstants.UPDATETRAININGPARTNERPROFILE_FAILURE, error } }
}

function updateIndustryPartnerProfile(user) {

    return dispatch => {
        dispatch(request(user));
        userService.updateIndustryPartnerProfile(user)
            .then(
                user => {
                    if (user.status === false) {
                        //console.log("error while updating industry partner profile", user);
                        dispatch(failure(user.error.message));
                        dispatch(alertActions.error(user.error.message));
                    }
                    else {
                        // console.log("TP DETAILS IN ACTION", user);
                        dispatch(success());
                        dispatch(alertActions.success(user.data.message));
                        //history.push('/Settings');
                    }
                },
                error => {
                    dispatch(failure(error));
                    dispatch(alertActions.error(error));
                }
            );
    };

    function request(user) { return { type: userConstants.UPDATEINDUSTRYPARTNERPROFILE_REQUEST, user } }
    function success(user) { return { type: userConstants.UPDATEINDUSTRYPARTNERPROFILE_SUCCESS, user } }
    function failure(error) { return { type: userConstants.UPDATEINDUSTRYPARTNERPROFILE_FAILURE, error } }
}


function updateTrainingCenterProfile(user) {

    return dispatch => {
        //   dispatch(request(user));
        userService.updateTrainingCenterProfile(user)
            .then(
                user => {
                    if (user.status === false) {
                        // console.log("error while adding centers", user);
                        dispatch(failure(user.error.message));
                        dispatch(alertActions.error(user.error.message));
                    }
                    else {
                        // console.log("candidate details", user);
                        dispatch(success());
                        dispatch(alertActions.success('Center Updated Successfully.'));
                        // console.log(user,"USER");
                        
                        // window.location.href = "/training-centers";
                        /* 
                        history.push('/training-centers'); */
                    }
                },
                error => {
                    dispatch(failure(error));
                    dispatch(alertActions.error(error));
                }
            );
    };

    //function request(user) { return { type: userConstants.CENTERADD_REQUEST, user } }
    function success(user) { return { type: userConstants.CENTERADD_SUCCESS, user } }
    function failure(error) {
        return { type: userConstants.CENTERADD_FAILURE, error }
    }
}

function addupdateTrainingCenter(user) {

    return dispatch => {
        //   dispatch(request(user));
        userService.addupdateTrainingCenter(user)
            .then(
                user => {
                    if (user.status === false) {
                        // console.log("error while adding centers", user);
                        dispatch(failure(user.error.message));
                        dispatch(alertActions.error(user.error.message));
                    }
                    else {
                        // console.log("candidate details", user);
                        dispatch(success());
                        dispatch(alertActions.success('Center Updated Successfully.'));
                        // console.log(user,"USER");
                        setTimeout(() => {
                            window.location.href = "/training-centers";    
                        }, 3500);
                        
                        /* 
                        history.push('/training-centers'); */
                    }
                },
                error => {
                    dispatch(failure(error));
                    dispatch(alertActions.error(error));
                }
            );
    };

    //function request(user) { return { type: userConstants.CENTERADD_REQUEST, user } }
    function success(user) { return { type: userConstants.CENTERADD_SUCCESS, user } }
    function failure(error) {
        return { type: userConstants.CENTERADD_FAILURE, error }
    }
}
function addUpdateCandidateDetailsHiringRequest(user) {
    return dispatch => {
        dispatch(request(user));
        userService.addUpdateCandidateDetailsHiringRequest(user)
            .then(
                user => {
                    if (user.status === false) {
                        // console.log("error while adding candiate details", user);
                        dispatch(failure(user.error.message));
                        dispatch(alertActions.error(user.error.message));
                    }
                    else {
                        // console.log("Candidates details successfully added.", user);
                        dispatch(success());
                        // dispatch(alertActions.success('Candidates details successfully added.'));
                        history.push('/Employer-hiring-requests');
                    }
                },
                error => {
                    dispatch(failure(error));
                    dispatch(alertActions.error(error));
                }
            );
    };

    function request(user) { return { type: userConstants.CANDIDATEDETAILREGISTER_REQUEST, user } }
    function success(user) { return { type: userConstants.CANDIDATEDETAILREGISTER_SUCCESS, user } }
    function failure(error) {
        return { type: userConstants.CANDIDATEDETAILREGISTER_FAILURE, error }
    }
}

function addUpdateImmidiateFutureHiringRequest(user) {
    return dispatch => {
        dispatch(request(user));
        userService.addUpdateImmidiateFutureHiringRequest(user)
            .then(
                user => {
                    if (user.status === false) {
                        // console.log("error while adding candiate details", user);
                        dispatch(failure(user.error.message));
                        dispatch(alertActions.error(user.error.message));
                    }
                    else {
                        // console.log("Candidates details successfully added.", user);
                        dispatch(success(user));
                        // console.log(user," User ");
                        dispatch(alertActions.success('Your Future Hiring Request has been submitted successfully. SkillConnect Admin will get back to you soon.'));
                        // history.push('/Employer-hiring-requests');
                        setTimeout(() => {
                            window.location.reload(true);
                        }, 4000);
                        // console.log(user,"user==========");
                    }
                },
                error => {
                    dispatch(failure(error));
                    dispatch(alertActions.error(error));
                }
            );
    };

    function request(user) { return { type: userConstants.IMMIDIATEHIRINGREQUESTREGISTER_REQUEST, user } }
    function success(user) { return { type: userConstants.IMMIDIATEHIRINGREQUESTREGISTER_SUCCESS, user } }
    function failure(error) {return { type: userConstants.IMMIDIATEHIRINGREQUESTREGISTER_FAILURE, error }
    }
}

function getHiringRequestDetailsForCandidate(hiringrequestdetailid) {
    return dispatch => {
        dispatch(request(hiringrequestdetailid));
        userService.getHiringRequestDetailsForCandidate(hiringrequestdetailid)
            .then(
                users => {
                    if (users.data.length > 0) {
                        //1
                        if (users.data[0].stateid > 0) {
                            var db_stateid = users.data[0].stateid; //console.log("State ID", users.data[0].stateid); 
                            //call to get District Based on State id.
                            dispatch(userActions.getDistrict(db_stateid));
                        }
                        else {
                            dispatch(failure(users.error.message));
                            dispatch(alertActions.error(users.error.message));
                        }

                    }
                    dispatch(success(users));
                },
                error => {
                    dispatch(failure(error));
                    dispatch(alertActions.error(error));
                }
            );
    };
    function request() { return { type: userConstants.GETALL_CANDIADATE_HIRINGREQUESTDETAIL_REQUEST } }
    function success(users) { return { type: userConstants.GETALL_CANDIADATE_HIRINGREQUESTDETAIL_SUCCESS, users } }
    function failure(error) { return { type: userConstants.GETALL_CANDIADATE_HIRINGREQUESTDETAIL_FAILURE, error } }
}
function getAll() {
    return dispatch => {
        dispatch(request());

        userService.getAll()
            .then(
                users => dispatch(success(users)),
                error => dispatch(failure(error))
            );
    };

    function request() { return { type: userConstants.GETALL_REQUEST } }
    function success(users) { return { type: userConstants.GETALL_SUCCESS, users } }
    function failure(error) { return { type: userConstants.GETALL_FAILURE, error } }
}
function getAllSectors() {
    return dispatch => {
        userService.getAllSectors()
            .then(
                users => dispatch(success(users)),
                error => dispatch(failure(error))
            );
    };
    //function request() { return { type: userConstants.GETALL_SECTORS_REQUEST } }
    function success(users) { return { type: userConstants.GETALL_SECTORS_SUCCESS, users } }
    function failure(error) { return { type: userConstants.GETALL_SECTORS_FAILURE, error } }
}
function getCandidateFormData(trainingpartnerid) {
    return dispatch => {
        userService.getCandidateFormData(trainingpartnerid)
            .then(
                users => dispatch(success(users)),
                error => dispatch(failure(error))
            );
    };
    //function request() { return { type: userConstants.GETALL_CANDIDATEFORMDATA_REQUEST } }
    function success(users) { return { type: userConstants.GETALL_CANDIDATEFORMDATA_SUCCESS, users } }
    function failure(error) { return { type: userConstants.GETALL_CANDIDATEFORMDATA_FAILURE, error } }
}
function getCandidateSearchFormData(trainingpartnerid, type) {
    return dispatch => {
        userService.getCandidateSearchFormData(trainingpartnerid, type)
            .then(
                users => dispatch(success(users)),
                error => dispatch(failure(error))
            );
    };
    //function request() { return { type: userConstants.GETALL_SEARCHFORMDATA_REQUEST } }
    function success(users) { return { type: userConstants.GETALL_SEARCHFORMDATA_SUCCESS, users } }
    function failure(error) { return { type: userConstants.GETALL_SEARCHFORMDATA_FAILURE, error } }
}


function getStates() {
    return dispatch => {
        userService.getStates()
            .then(
                users => dispatch(success(users)),
                error => dispatch(failure(error))
            );

    };
    //function request() { return { type: userConstants.GETALL_STATE_REQUEST } }
    function success(users) { return { type: userConstants.GETALL_STATE_SUCCESS, users } }
    function failure(error) { return { type: userConstants.GETALL_STATE_FAILURE, error } }
}
function getDistrict(value) {
    return dispatch => {
        userService.getDistrict(value)
            .then(
                users => dispatch(success(users)),
                error => dispatch(failure(error))
            );

    };
    //function request() { return { type: userConstants.GETALL_DISTRICT_REQUEST } }
    function success(users) { return { type: userConstants.GETALL_DISTRICT_SUCCESS, users } }
    function failure(error) { return { type: userConstants.GETALL_DISTRICT_FAILURE, error } }
}

function getDistrictPresent(value) {
    return dispatch => {
        userService.getDistrict(value)
            .then(
                users => dispatch(success(users)),
                error => dispatch(failure(error))
            );

    };
    //function request() { return { type: userConstants.GETALL_DISTRICT_REQUEST } }
    function success(users) { return { type: userConstants.GETALL_DISTRICT_PRESENT_SUCCESS, users } }
    function failure(error) { return { type: userConstants.GETALL_DISTRICT_PRESENT_FAILURE, error } }
}

function getAllPrefferedCity(value) {
    return dispatch => {
        userService.getDistrict(value)
            .then(
                
                users => {
                    // console.log(users, "user");
                    dispatch(success(users))
                },
                error => dispatch(failure(error))
            );

    };
    //function request() { return { type: userConstants.GETALL_DISTRICT_REQUEST } }
    function success(users) { return { type: userConstants.GETALL_PREFERRED_DISTRICT_SUCCESS, users } }
    function failure(error) { return { type: userConstants.GETALL_PREFERRED_DISTRICT_FAILURE, error } }
}

function getJobRoles(value) {
    return dispatch => {
        userService.getJobRoles(value)
            .then(
                users => dispatch(success(users)),
                error => dispatch(failure(error))
            );

    };
    //function request() { return { type: userConstants.GETALL_JOBROLES_REQUEST } }
    function success(users) { return { type: userConstants.GETALL_JOBROLES_SUCCESS, users } }
    function failure(error) { return { type: userConstants.GETALL_JOBROLES_FAILURE, error } }
}
function getEmployersForTC(tcid) {
    return dispatch => {
        userService.getEmployersForTC(tcid)
            .then(
                users => {
                    dispatch(success(users));
                },
                error => dispatch(failure(error))
            );

    };
    //function request() { return { type: userConstants.GETALL_JOBROLES_REQUEST } }
    function success(users) { return { type: userConstants.GET_EMPLOYERS_SUCCESS, users } }
    function failure(error) { return { type: userConstants.GET_EMPLOYERS_FAILURE, error } }
}
function getJobRolesByTP(value) {
    return dispatch => {
        userService.getJobRolesByTP(value)
            .then(
                
                users => {
                    // console.log(users," USERS");
                    dispatch(success(users))
                },
                error => dispatch(failure(error))
            );

    };
    //function request() { return { type: userConstants.GETALL_JOBROLES_REQUEST } }
    function success(users) { return { type: userConstants.GETALL_JOBROLES_SUCCESS, users } }
    function failure(error) { return { type: userConstants.GETALL_JOBROLES_FAILURE, error } }
}

function getSchemeCentralMinistries(value) {
    return dispatch => {
        userService.getSchemeCentralMinistries(value)
            .then(
                users => dispatch(success(users)),
                error => dispatch(failure(error))
            );

    };
    //function request() { return { type: userConstants.GETALL_CENTRALMINISTRIES_REQUEST } }
    function success(users) { return { type: userConstants.GETALL_CENTRALMINISTRIES_SUCCESS, users } }
    function failure(error) { return { type: userConstants.GETALL_CENTRALMINISTRIES_FAILURE, error } }
}
function getCenters(trainingpartnerid, pageno) {
    return dispatch => {
        dispatch(request(trainingpartnerid));
        userService.getCenters(trainingpartnerid, pageno)
            .then(
                users => dispatch(success(users)),
                error => dispatch(failure(error))
            );
    };
    function request(trainingpartnerid) { return { type: userConstants.GETALL_CENTERS_REQUEST, trainingpartnerid } }
    function success(users) { return { type: userConstants.GETALL_CENTERS_SUCCESS, users } }
    function failure(error) { return { type: userConstants.GETALL_CENTERS_FAILURE, error } }
}
function getCandidates(trainingcenterid, page) {
    return dispatch => {
        dispatch(request(trainingcenterid));
        userService.getCandidates(trainingcenterid, page)
            .then(
                users => dispatch(success(users)),
                error => dispatch(failure(error))
            );
    };
    function request(trainingcenterid) { return { type: userConstants.GETALL_CANDIDATELIST_REQUEST, trainingcenterid } }
    function success(users) { return { type: userConstants.GETALL_CANDIDATELIST_SUCCESS, users } }
    function failure(error) { return { type: userConstants.GETALL_CANDIDATELIST_FAILURE, error } }
}

function getEmployer(industrypartnerid) {
    //console.log(industrypartnerid,"INDUS ID");
    return dispatch => {
        //dispatch(request());
        userService.getEmployer(industrypartnerid)
            .then(
                users => dispatch(success(users)),
                error => dispatch(failure(error))
            );
    };
    //function request(users) { return { type: userConstants.GETALL_EMPLOYERLIST_REQUEST, users } }
    function success(users) { return { type: userConstants.GETALL_EMPLOYERLIST_SUCCESS, users } }
    function failure(error) { return { type: userConstants.GETALL_EMPLOYERLIST_FAILURE, error } }
}

function getTrainingPartner(trainingpartnerid) {
    //console.log(trainingpartnerid,"<<<<<<<<<<<<<<TP ID>>>>>>>>>>>>>>>>");
    return dispatch => {
        //dispatch(request());
        userService.getTrainingPartner(trainingpartnerid)
            .then(
                users => dispatch(success(users)),
                error => dispatch(failure(error))
            );
    };
    //function request(users) { return { type: userConstants.GETALL_TRAININGPARTNERLIST_REQUEST, users } }
    function success(users) { return { type: userConstants.GETALL_TRAININGPARTNERLIST_SUCCESS, users } }
    function failure(error) { return { type: userConstants.GETALL_TRAININGPARTNERLIST_FAILURE, error } }
}
//View More Data for Candidate
function getCandidateViewMoreDetails(candidate_id) {
    return dispatch => {
        dispatch(request(candidate_id));
        userService.getCandidateViewMoreDetails(candidate_id)
            .then(
                users => {
                    if (users.status === false) {
                        dispatch(failure(users.error.message));
                        dispatch(alertActions.error(users.error.message));
                        history.push('/candidates');
                    } else {
                        if (users.data.length > 0) {
                            dispatch(success(users));
                        }
                    }
                },
                error => dispatch(failure(error))
            );
    };
    function request(candidate_id) { return { type: userConstants.GETALL_CANDIDATE_VIEW_MORE_DETAILS_REQUEST,candidate_id } }
    function success(users) { return { type: userConstants.GETALL_CANDIDATE_VIEW_MORE_DETAILS_SUCCESS, users } }
    function failure(error) { return { type: userConstants.GETALL_CANDIDATE_VIEW_MORE_DETAILS_FAILURE, error } }
}

function getCandidateDetails(candidate_id) {
    return dispatch => {
        userService.getCandidateDetails(candidate_id)
            .then(
                users => {
                    //"CANDIDATE DETAILS ACTION", users);
                    if (users.status === false) {
                        dispatch(failure(users.error.message));
                        dispatch(alertActions.error(users.error.message));
                        history.push('/candidates');
                    } else {
                        if (users.data.length > 0) {
                                //1
                                // if (users.data[0].stateid > 0) {
                                //     var db_stateid = users.data[0].stateid; //console.log("State ID", users.data[0].stateid); 
                                //     //call to get District Based on State id.
                                //     dispatch(userActions.getDistrict(db_stateid));
                                // }
                                // else {
                                //     dispatch(failure(users.error.message));
                                //     dispatch(alertActions.error(users.error.message));
                                // }

                                //1_1
                                var db_stateid
                                if (users.data[0].permanent_stateID > 0) {
                                    db_stateid = users.data[0].permanent_stateID; //console.log("State ID", users.data[0].stateid); 
                                    //call to get District Based on State id.
                                    dispatch(userActions.getDistrict(db_stateid));
                                }
                                // else {
                                //     dispatch(failure(users.error));
                                //     dispatch(alertActions.error(users.error));
                                // }
                                
                                //2
                                if (users.data[0].present_stateID > 0) {
                                    db_stateid = users.data[0].present_stateID; //console.log("State ID", users.data[0].stateid); 
                                    //call to get District Based on State id.
                                    dispatch(userActions.getDistrictPresent(db_stateid));
                                }
                                // else {
                                //     console.log(users, "USERSSSSS")
                                //     dispatch(failure(users.error));
                                //     dispatch(alertActions.error(users.error));
                                // }
                                
                                //3
                                if (users.data[0].sectorid && users.data[0].sectorid > 0) {
                                    var db_sectorid = users.data[0].sectorid; //console.log("State ID", users.data[0].stateid); 
                                    //call to get District Based on State id.
                                    dispatch(userActions.getJobRoles(db_sectorid));
                                }
                                else {
                                    // dispatch(failure(users.error.message));
                                    // dispatch(alertActions.error(users.error.message));
                                }
                                //4
                                
                                if (users.data[0].schemeid > 0) {
                                    var db_central_ministry_id = users.data[0].schemeid;
                                    //call to get District Based on State id.
                                    dispatch(userActions.getSchemeCentralMinistries(db_central_ministry_id));
                                }
                                else {
                                    // dispatch(failure(users.error.message));
                                    // let alertMessgae = (users && users.error && users.error.message) ? users.error.message : 'Failed to fetch scheme';
                                    // dispatch(alertActions.error(alertMessgae));
                                    console.log("SCHEME IS NOT PRESENT...");
                                }
                                if(users.data[0].placement_status === "selected"){
                                    let tcid = users.data[0].trainingcenter_id;
                                    dispatch(userActions.getEmployersForTC(tcid));
                                }
                                
                            dispatch(success(users));
                        }
                    }
                },
                error => dispatch(failure(error))
            );
    };
    //function request() { return { type: userConstants.GETALL_CANDIDATEDETAILSLIST_REQUEST } }
    function success(users) { return { type: userConstants.GETALL_CANDIDATEDETAILS_SUCCESS, users } }
    function failure(error) { return { type: userConstants.GETALL_CANDIDATEDETAILS_FAILURE, error } }
}
// prefixed function name with underscore because delete is a reserved word in javascript
function _delete(id) {
    return dispatch => {
        dispatch(request(id));

        userService.delete(id)
            .then(
                user => {
                    dispatch(success(id));
                },
                error => {
                    dispatch(failure(error));
                }
            );
    };

    function request(id) { return { type: userConstants.DELETE_REQUEST, id } }
    function success(id) { return { type: userConstants.DELETE_SUCCESS, id } }
    function failure(error) { return { type: userConstants.DELETE_FAILURE, error } }
}

/*
function updateProduct(id,tablename) {
    return (dispatch, getState) => {
      const { accountDetails } = getState();
  
      dispatch({
        type: UPDATE_PRODUCT,
        stateOfResidenceId: accountDetails.stateOfResidenceId,
        product,
      });
    };
  }
  */
function updateHiringRequestStatus(hiringrequestid, hiringrequeststatus, currentActiveTab, cancelreason, pageno) {
    var industrypartnerid;
    var type;
    return dispatch => {
        dispatch(request(hiringrequestid));
        userService.updateHiringRequestStatus(hiringrequestid, hiringrequeststatus, cancelreason, pageno)
            .then(
                user => {
                    if (user.status === true) {
                        if (localStorage.getItem('user') !== null) {
                            var userdata = JSON.parse(localStorage.getItem('user'));
                            var hiring_request_status = currentActiveTab;

                            if (userdata.user_type === 'TP') {
                                industrypartnerid = userdata.data[0].trainingpartnerid;
                                type = 'TrainingPartner';
                            } else {
                                industrypartnerid = userdata.data[0].industrypartnerid;
                                type = 'Employer';
                            }
                            dispatch(userActions.getHiringRequests(industrypartnerid, hiring_request_status, type, pageno, ''));
                        }
                    }
                    dispatch(success(hiringrequestid));
                },
                error => {
                    dispatch(failure(hiringrequestid, error));
                }
            );
    };

    function request(id) { return { type: userConstants.UPDATEHIRINGREQUEST_REQUEST, id } }
    function success(id) { return { type: userConstants.UPDATEHIRINGREQUEST_SUCCESS, id } }
    function failure(id, error) { return { type: userConstants.UPDATEHIRINGREQUEST_FAILURE, id, error } }
}

function deleteCandidate(id, tablename, page) {
    //console.log("Action for: Delete Candidate");
    return dispatch => {
        dispatch(request(id, page));
        userService.deleteCandidate(id, tablename)
            .then(
                user => {
                    if (user.status === true) {
                        //Remove key from local storage
                        var uniqueKey = 'getCandidates';
                        userService.removeLocalStorageCache(uniqueKey);

                        dispatch(success(id, page));
                        if (localStorage.getItem('user') !== null) {
                            var userdata = JSON.parse(localStorage.getItem('user'));
                            if (userdata.user_type === 'TC') {
                                var trainingcenterid = userdata.data[0].trainingcenterid;
                            }
                            dispatch(getCandidates(trainingcenterid, page));
                            // dispatch(alertActions.success(user.data.message));
                        }
                    }else{
                        // console.log(user,"USER");
                        // console.log(dispatch(failure(id, user.error)));
                        dispatch(alertActions.error(user.error.message));
                    }
                },
                error => {
                    dispatch(failure(id, error));
                }
            );
    };

    function request(id, page) { return { type: userConstants.DELETECANDIDATE_REQUEST, id, page } }
    function success(id, page) { return { type: userConstants.DELETECANDIDATE_SUCCESS, id, page } }
    function failure(id, error) { return { type: userConstants.DELETECANDIDATE_FAILURE, id, error } }
}
function deleteTrainingCenter(id, tablename, page) {
    return dispatch => {
        dispatch(request(id, page));
        userService.deleteTrainingCenter(id, tablename)
            .then(
                user => {
                    //console.log("After REQUEST DISPACH", user);
                    if (user.status === true) {
                        //console.log("Get Training Center");
                        dispatch(success(id, page));
                        if (localStorage.getItem('user') !== null) {
                            var userdata = JSON.parse(localStorage.getItem('user'));
                            if (userdata.user_type === 'TP') {
                                var trainingpartnerid = userdata.data[0].trainingpartnerid;
                            }
                            dispatch(getCenters(trainingpartnerid, page));
                        }
                    }
                },
                error => {
                    dispatch(failure(id, error));
                }
            );
    };

    function request(id, page) { return { type: userConstants.TRAININGCENTER_DELETE_REQUEST, id, page } }
    function success(id, page) { return { type: userConstants.TRAININGCENTER_DELETE_SUCCESS, id, page } }
    function failure(id, error) { return { type: userConstants.TRAININGCENTER_DELETE_FAILURE, id, error } }
}
function updateContactMessage(user) {
    return dispatch => {
        dispatch(request(user));
        userService.updateContactMessage(user)
            .then(
                user => {
                    if (user.status === false) {
                        // console.log("error submitting message", user);
                        dispatch(failure(user.error.message));
                        dispatch(alertActions.error(user.error.message));
                    }
                    else {
                        // console.log("candidate details", user);
                        dispatch(success());
                        dispatch(alertActions.success('Message Submitted Successfully.'));
                    }
                    setTimeout(() => {
                        window.location.reload();
                    }, 3500);
                },
                error => {
                    dispatch(failure(error));
                    dispatch(alertActions.error(error));
                }
            );
    };

    function request(user) { return { type: userConstants.CONTACTMESSAGE_REQUEST, user } }
    function success(user) { return { type: userConstants.CONTACTMESSAGE_SUCCESS, user } }
    function failure(error) {
        return { type: userConstants.CONTACTMESSAGE_FAILURE, error }
    }
}
function forgotPassword(tpid, email_address) {
    return dispatch => {

        dispatch(request({ tpid, email_address }));

        userService.forgotPassword(tpid, email_address)
            .then(
                user => {
                    if (user.status === false) {
                        console.log("in forgot pwd error", user);
                        dispatch(failure(user.error.message));
                        dispatch(alertActions.error(user.error.message));
                    }
                    else {
                        dispatch(success(user));
                        dispatch(alertActions.success(user.data.message));
                        //history.push('/');
                    }
                },
                error => {
                    dispatch(failure(error));
                    dispatch(alertActions.error(error));

                }
            );
    };

    function request(user) { return { type: userConstants.FORGOTPWD_REQUEST, user } }
    function success(user) { return { type: userConstants.FORGOTPWD_SUCCESS, user } }
    function failure(error) {
        return { type: userConstants.FORGOTPWD_FAILURE, error }
    }
}
function resetPassword(token, password) {
    //  console.log("in action", token);
    return dispatch => {

        userService.resetPassword(token, password)
            .then(
                user => {
                    if (user.status === false) {
                        //console.log("in reset pwd error", user);
                        dispatch(failure(user.error.message));
                        dispatch(alertActions.error(user.error.message));
                    }
                    else {
                        dispatch(success(user));
                        dispatch(alertActions.success(user.data.message));
                    }
                },
                error => {
                    dispatch(failure(error));
                    dispatch(alertActions.error(error));
                }
            );
    };

    //function request(user) { return { type: userConstants.RESETPWD_REQUEST, user } }
    function success(user) { return { type: userConstants.RESETPWD_SUCCESS, user } }
    function failure(error) {
        return { type: userConstants.RESETPWD_FAILURE, error }
    }
}
function getTrainingPartnerSearchCenter(data, page, isFirstTime) {
    return dispatch => {
        dispatch(request({ data, isFirstTime }));
        userService.getSearchCenter(data, page)
            .then(
                users => dispatch(success(users, isFirstTime)),
                error => dispatch(failure(error))

            );
    };
    function request(data, isFirstTime) { return { type: userConstants.GETALL_CENTERS_SEARCH_REQUEST, data, isFirstTime } }
    function success(users, isFirstTime) { return { type: userConstants.GETALL_CENTERS_SEARCH_SUCCESS, users, isFirstTime } }
    function failure(error) {
        return { type: userConstants.GETALL_CENTERS_SEARCH_FAILURE, error }
    }
}
function getTrainingPartnerSearchCandidate(data, page, isFirstTime) {
    return dispatch => {
        dispatch(request({ data }));
        userService.getSearchCandidate(data, page)
            .then(
                users => dispatch(success(users, isFirstTime)),
                error => dispatch(failure(error))

            );
    };
    function request(data, isFirstTime) { return { type: userConstants.GETALL_TRAININGPARTNER_SEARCH_CANDIDATES_REQUEST, data, isFirstTime } }
    function success(users, isFirstTime) { return { type: userConstants.GETALL_TRAININGPARTNER_SEARCH_CANDIDATES_SUCCESS, users, isFirstTime } }
    function failure(error) {
        return { type: userConstants.GETALL_TRAININGPARTNER_SEARCH_CANDIDATES_FAILURE, error }
    }
}
function getSearchCandidate(data, page, isFirstTime, isClear) {
    if(isClear){
        return dispatch => {
            dispatch(clear());
        };
    }

    return dispatch => {
        dispatch(request({ data, isFirstTime }));
        userService.getSearchCandidate(data, page)
            .then(
                users => dispatch(success(users, isFirstTime)),
                error => dispatch(failure(error))
            );
    };
    function request(data, isFirstTime) { return { type: userConstants.GETALL_SEARCH_CANDIDATES_REQUEST, data, isFirstTime } }
    function success(users, isFirstTime) { return { type: userConstants.GETALL_SEARCH_CANDIDATES_SUCCESS, users, isFirstTime } }
    function failure(error) { return { type: userConstants.GETALL_SEARCH_CANDIDATES_FAILURE, error } }
    function clear( ) { return { type: userConstants.GETALL_SEARCH_CANDIDATES_CLEAR,  } }
}

function getSectorJobroleMapping(trainingpartnerid,partnerType) {
    return dispatch => {
        userService.getSectorJobroleMapping(trainingpartnerid,partnerType)
            .then(
                users => {
                    if (users.status === false) {
                        dispatch(failure(users.error.message));
                        dispatch(alertActions.error(users.error.message));
                        // history.push('/TrainingPartner-dashboard');
                    } else {
                        // console.log("Bulk Upload Details Success", users);
                        dispatch(success(users));
                    }
                },
                error => dispatch(failure(error))
            );
    };
    //function request() { return { type: userConstants.GETALL_SECTORJOBROLEMAPPING_REQUEST } }
    function success(users) { return { type: userConstants.GETALL_SECTORJOBROLEMAPPING_SUCCESS, users } }
    function failure(error) { return { type: userConstants.GETALL_SECTORJOBROLEMAPPING_FAILURE, error } }
}

function getTrainingCenterDetails(center_id) {
    return dispatch => {
        userService.getTrainingCenterDetails(center_id)
            .then(
                users => {
                    //console.log("Traing Center DETAILS ACTION", users);
                    if (users.status === false) {
                        dispatch(failure(users.error.message));
                        dispatch(alertActions.error(users.error.message));
                        var userdata = JSON.parse(localStorage.getItem('user'));
                            if (userdata.user_type === 'TP') {
                                history.push('/training-centers');
                            } else {
                                history.push('/candidates');
                            }

                    } else {
                        if (users.data.length > 0) {
                            //1
                            if (users.data[0].stateid > 0) {
                                var db_stateid = users.data[0].stateid; //console.log("State ID", users.data[0].stateid); 
                                var db_tpid = users.data[0].trainingpartnerid;
                                //call to get District Based on State id.
                                
                                dispatch(userActions.getDistrict(db_stateid));
                                dispatch(userActions.getJobRolesByTP(db_tpid));
                            }
                            else {
                                dispatch(failure(users.error.message));
                                dispatch(alertActions.error(users.error.message));
                            }
                        }
                        dispatch(success(users));
                    }
                },
                error => dispatch(failure(error))
            );
    };
    //function request() { return { type: userConstants.GETALL_TRAININGCENTERDETAILS_REQUEST } }
    function success(users) { return { type: userConstants.GETALL_TRAININGCENTERDETAILS_SUCCESS, users } }
    function failure(error) { return { type: userConstants.GETALL_TRAININGCENTERDETAILS_FAILURE, error } }
}


function getHiringRequests(id, hiringrequest_status, type, pageNo, internal_status) {
    return dispatch => {
        dispatch(request({ id, pageNo, hiringrequest_status }));
        userService.getHiringRequests(id, hiringrequest_status, type, pageNo, internal_status)
            .then(
                users => dispatch(success(users)),
                error => dispatch(failure(error))
            );
    };
    function request(id, pageNo, hiringrequest_status) { return { type: userConstants.GETALL_HIRINGREQUESTLIST_REQUEST, id, pageNo, hiringrequest_status } }
    function success(users) { return { type: userConstants.GETALL_HIRINGREQUESTLIST_SUCCESS, users } }
    function failure(error) { return { type: userConstants.GETALL_HIRINGREQUESTLIST_FAILURE, error } }
}
function addUpdateFutureHiringRequest(futurerequestdata) {
    return dispatch => {
        dispatch(request(futurerequestdata));
        userService.addUpdateFutureHiringRequest(futurerequestdata)
            .then(
                user => {
                    if (user.status === false) {
                        //dispatch(failure(user.error.message));
                        dispatch(alertActions.error(user.error));
                    }
                    else {
                        dispatch(success(user));
                        if (user.data.message !== undefined) {
                            dispatch(alertActions.success(user.data.message));
                        } else if (user.data !== undefined) {
                            dispatch(alertActions.success(user.data));
                        }
                        //history.push('/Employer-dashboard');
                    }
                    setTimeout(function () {
                        window.location.reload(true);
                    }, 5000);
                },
                error => {
                    dispatch(failure(error));
                    dispatch(alertActions.error(error));

                    setTimeout(function () {
                        window.location.reload(true);
                    }, 5000);
                }

            );
    };

    function request(user) { return { type: userConstants.FUTURE_HIRINGREQ_REQUEST, user } }
    function success(user) { return { type: userConstants.FUTURE_HIRINGREQ_SUCCESS, user } }
    function failure(error) {
        return { type: userConstants.FUTURE_HIRINGREQ_FAILURE, error }
    }
}
function updateHiringStatus(value) {
    var industrypartnerid;
    var type;
    return dispatch => {
        dispatch(request({ value }));
        userService.updateHiringStatus(value)
            .then(
                users => {
                    //WE are changing interal status from Accepted tab only for now. So we have kept it hiring_request_status=2, In future if we change it then we must need to make it dynamic...
                    if (users.status === true) {
                        if (localStorage.getItem('user') !== null) {
                            var userdata = JSON.parse(localStorage.getItem('user'));
                            var hiring_request_status = 2;//Accepted..
                            if (userdata.user_type === 'EMP') {
                                industrypartnerid = userdata.data[0].industrypartnerid;
                                type = 'Employer';
                            } else {
                                industrypartnerid = userdata.data[0].trainingpartnerid;
                                type = 'TrainingPartner';
                            }
                            dispatch(userActions.getHiringRequests(industrypartnerid, hiring_request_status, type, 1, 2));
                        }
                    }
                    dispatch(success(users));
                },

                error => dispatch(failure(error))
            );
    };
    function request() { return { type: userConstants.UPDATE_HIRING_STATUS_REQUEST } }
    function success(users) { return { type: userConstants.UPDATE_HIRING_STATUS_SUCCESS, users } }
    function failure(error) { return { type: userConstants.UPDATE_HIRING_STATUS_FAILURE, error } }
}
function getHomePageStatistics(){
    return dispatch => {
        dispatch(request({  }));
        userService.getHomePageStatistics()
            .then(
                users => {
                    if (users.status === false) {
                        dispatch(failure(users.error.message));
                        //dispatch(alertActions.error(users.error.message));
                    } else {
                        dispatch(success(users));
                    }
                },
                error => dispatch(failure(error))
            );
    };
    function request() { return { type: userConstants.GET_HOMEPAGESTATISTICS_REQUEST } }
    function success(users) { return { type: userConstants.GET_HOMEPAGESTATISTICS_SUCCESS, users } }
    function failure(error) { return { type: userConstants.GET_HOMEPAGESTATISTICS_FAILURE, error } }
}
function getbulkUploadStatus(){
    return dispatch => {
        dispatch(request({  }));
        userService.getbulkUploadStatus()
            .then(
                users => {
                    if (users.status === false) {
                        dispatch(failure(users.error.message));
                        //dispatch(alertActions.error(users.error.message));
                    } else {
                        dispatch(success(users));
                    }
                },
                error => dispatch(failure(error))
            );
    };
    function request() { return { type: userConstants.GETALL_BULKUPLOADSTATUS_REQUEST } }
    function success(users) { return { type: userConstants.GETALL_BULKUPLOADSTATUS_SUCCESS, users } }
    function failure(error) { return { type: userConstants.GETALL_BULKUPLOADSTATUS_FAILURE, error } }
}
function ChangePassword(partnerID, oldpassword, password, partnerType) {
    return dispatch => {
        userService.ChangePassword(partnerID, oldpassword, password, partnerType)
            .then(
                user => {
                    if (user.status === false) {
                        //console.log("in reset pwd error", user);
                        dispatch(failure(user.error.message));
                        dispatch(alertActions.error(user.error.message));
                    }
                    else {
                        dispatch(success(user));
                        dispatch(alertActions.success(user.data.message));
                    }
                    setTimeout(() => {
                        window.location.reload();
                    }, 3500);
                },
                error => {
                    dispatch(failure(error));
                    dispatch(alertActions.error(error));
                }
            );
    };

    //function request(user) { return { type: userConstants.CHANGEPWD_REQUEST, user } }
    function success(user) { return { type: userConstants.CHANGEPWD_SUCCESS, user } }
    function failure(error) {
        return { type: userConstants.CHANGEPWD_FAILURE, error }
    }
}


function searchClear() {
    return { type: userConstants.GETALL_CANDIDATES_CLEAR };

}


function getImmidiateHiringRequestFormData(industrypartnerid, type) {
    return dispatch => {
        userService.getImmidiateHiringRequestFormData(industrypartnerid, type)
            .then(
                
                users => {
                    // console.log(users,"IN THE ACTIon");
                    dispatch(success(users));
                    
                },
                error => dispatch(failure(error))
            );
    };
    //function request() { return { type: userConstants.GETALL_SEARCHFORMDATA_REQUEST } }
    function success(users) { return { type: userConstants.GETALL_SEARCHFORMDATA_NEW_SUCCESS, users } }
    function failure(error) { return { type: userConstants.GETALL_SEARCHFORMDATA_NEW_FAILURE, error } }
}

function deleteFile(candidateid,file_to_delete){
    return dispatch => {
        userService.deleteFile(candidateid, file_to_delete)
            .then(
                
                users => {
                    // console.log(users,"IN THE ACTIon");
                    if(users.status === true){
                        dispatch(alertActions.success(users.data.message));
                        dispatch(success(users));
                        // setTimeout(() => {
                        //     window.location.reload();
                        // }, 3500);   
                    }else{
                        dispatch(alertActions.error(users.data.message));
                    }
                },
                error => dispatch((error))
            );
    };
    // function request() { return { type: userConstants.DELETE_FILE_REQUEST } }
    function success(users) { return { type: userConstants.DELETE_FILE_SUCCESS, users } }
    // function failure(error) { return { type: userConstants.DELETE_FILE_FAILURE, error } }
}