import { dashboardConstants } from '../_constants';
import { dashboardService } from '../_services';

export const dashboardActions = {
    
    getDashboardData,
    
};

function getDashboardData(id,user_type) {

    return dispatch => {
        dispatch(request(id));

        dashboardService.getDashboardData(id,user_type)
            .then(
                users => dispatch(success(users)),
                error => dispatch(failure(error))
            );
    };

    function request() { return { type: dashboardConstants.GETALL_DASHBOARD_DATA_REQUEST } }
    function success(users) { return { type: dashboardConstants.GETALL_DASHBOARD_DATA_SUCCESS, users } }
    function failure(error) { return { type: dashboardConstants.GETALL_DASHBOARD_DATA_FAILURE, error } }
}