import { pagecontentConstants } from '../_constants';
import { pagecontentService } from '../_services';

export const pagecontentActions = {
    
    getPageContentData,
    
};

function getPageContentData(pagetype) {

    return dispatch => {
        dispatch(request(pagetype));

        pagecontentService.getPageContentData(pagetype)
            .then(
                users => dispatch(success(users)),
                error => dispatch(failure(error))
            );
    };

    function request() { return { type: pagecontentConstants.GET_PAGECONTENT_DATA_REQUEST } }
    function success(users) { return { type: pagecontentConstants.GET_PAGECONTENT_DATA_SUCCESS, users } }
    function failure(error) { return { type: pagecontentConstants.GET_PAGECONTENT_DATA_FAILURE, error } }
}