import { alertConstants } from '../_constants';

export const alertActions = {
    success,
    error,
    warn,
    clear
};

function success(message) {
    return { type: alertConstants.SUCCESS, message };
}

function error(message) {
   // console.log("HERE######",message);
    return { type: alertConstants.ERROR, message };
}

function warn(message) {
    // console.log("HERE######",message);
     return { type: alertConstants.WARN, message };
 }

function clear() {
    return { type: alertConstants.CLEAR };
}