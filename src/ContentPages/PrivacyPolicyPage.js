import React from 'react';
import { connect } from 'react-redux';
import { pagecontentActions } from '../_actions';
import { Header } from '../_components/Header';
import { Footer } from '../_components/Footer';
import { Card, CardBody } from 'reactstrap';
import { constants } from '../constants';
import ReactHtmlParser from 'react-html-parser';



class PrivacyPolicyPage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isAddCandidateClick: false
            //Intial State..
        };
    }

    render() {
        var policydata = "";
        //console.log("Page content Data", this.props.pagecontentdata);
        if(this.props.pagecontentdata !== undefined){
            policydata = this.props.pagecontentdata.data[0].page_content;
        }

        return (
            <div className="content-wrapper user-flow TP-flow">
                {/* <Sidebar {...this.props} /> */}
                <div id="content" className="main-content ml-0">
                    <Header />
                    <div className="page-content static-page">
                        <div className="container">
                            {/* Page Title */}
                            <div className="section-header">
                                <h1 className="page-title">Privacy Policy</h1>
                            </div>
                            {/* Page Title End  */}
                            {/* Content */}
                            <Card className="">
                                <CardBody>
                                {ReactHtmlParser(policydata)}
                                </CardBody>
                            </Card>
                             {/* Content END */}
                        </div>
                    </div>

                    <Footer />
                </div>

            </div>
        );
    }
}

function mapStateToProps(state) {
    const { pagecontentdata } = state.pagecontentData;
    return {
        pagecontentdata
    };
}

function mapDispatchToProps(dispatch, ownProps) {
    var getpagecontentdata = dispatch(pagecontentActions.getPageContentData(constants.PAGE_CONTET_TYPE.PRIVACY_POLICY));
    return { getpagecontentdata }
}

const connectedPrivacyPolicyPage = connect(mapStateToProps, mapDispatchToProps)(PrivacyPolicyPage);
export { connectedPrivacyPolicyPage as PrivacyPolicyPage };