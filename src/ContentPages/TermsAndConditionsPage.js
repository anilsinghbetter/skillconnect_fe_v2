import React from 'react';
import { connect } from 'react-redux';
import { pagecontentActions } from '../_actions';
import { Header } from '../_components/Header';
import { Footer } from '../_components/Footer';
import { Card, CardBody } from 'reactstrap';
import { constants } from '../constants';
import ReactHtmlParser from 'react-html-parser';



class TermsAndConditionsPage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isAddCandidateClick: false
            //Intial State..
        };
    }

    render() {
        var termsdata = "";
        //console.log("Page content Data", this.props.pagecontentdata);
        if (this.props.pagecontentdata !== undefined) {
            termsdata = this.props.pagecontentdata.data[0].page_content;
        }

        return (
            <div className="content-wrapper user-flow TP-flow">
                {/* <Sidebar {...this.props} /> */}
                <div id="content" className="main-content ml-0">
                    <Header />
                    <div className="page-content static-page">
                        <div className="container">
                            {/* Page Title */}
                            <div className="section-header">
                                <h1 className="page-title">Terms & Conditions</h1>
                            </div>
                            {/* Page Title End  */}
                            {/* Content */}
                            <Card className="">
                                <CardBody>
                                {ReactHtmlParser(termsdata)}
                                </CardBody>
                            </Card>
                            {/* Content End */}
                        </div>
                    </div>

                    <Footer />
                </div>

            </div>
        );
    }
}

function mapStateToProps(state) {
    const { pagecontentdata } = state.pagecontentData;
    return {
        pagecontentdata
    };
}

function mapDispatchToProps(dispatch, ownProps) {
    var getpagecontentdata = dispatch(pagecontentActions.getPageContentData(constants.PAGE_CONTET_TYPE.TERMS_CONDITIONS));
    return { getpagecontentdata }
}

const connectedTermsAndConditionsPage = connect(mapStateToProps, mapDispatchToProps)(TermsAndConditionsPage);
export { connectedTermsAndConditionsPage as TermsAndConditionsPage };