export const dashboardConstants = {

    GETALL_DASHBOARD_DATA_REQUEST: 'DASHBOARD_REQUEST',
    GETALL_DASHBOARD_DATA_SUCCESS: 'DASHBOARD_SUCCESS',
    GETALL_DASHBOARD_DATA_FAILURE: 'DASHBOARD_FAILURE',
};
