export const alertConstants = {
    SUCCESS: 'ALERT_SUCCESS',
    ERROR: 'ALERT_ERROR',
    WARN: 'ALERT_WARNING',
    CLEAR: 'ALERT_CLEAR'
};
