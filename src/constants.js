export const constants = {
    "PAGE_CONTET_TYPE" : {
        "PRIVACY_POLICY" :"privacy_policy",
        "TERMS_CONDITIONS" :"terms_conditions",
    },
    "Contact_Us_Text" : "Thanks for your interest in BetterPlace. Please use this form if you have any questions about our portal and we'll get back to you soon.",
    "allowedLength" : 19,
    "slicedLength" : 16, //(allowedLength - 3) To Accomodate "..." after string
    "REFRESH_TIMEOUT": 2000,
    "candidate_csv_bulk_upload_mb" : "30 MB",
    "future_hiring_request_csv_bulk_upload_mb" : "30 MB",
    "future_hiring_request_after_upload_message" : "Future Hiring Request bulk upload process is in progress. You will be notified via email on your provided email address once upload process is completed.",
    "CANDIDATE_STATE_CITY_SELECT_VALIDATION" : "Please select state and district.",
    "CANDIDATE_COMBINATION_STATECITY_SELECT_VALIDATION" : "Invalid selection of state and district. Please select respective district for the given state.",
    "CANDIDATE_SECTOR_JOBROLE_SELECT_VALIDATION" : "Please select sector and jobrole.",
    "CANDIDATE_COMBINATION_SECTORJOBROLE_SELECT_VALIDATION" : "Invalid selection of sector and jobrole. Please select respective jobrole for the given sector.",
};