import React from 'react';
import { Router, Route, Switch } from 'react-router-dom';
import { connect } from 'react-redux';
import { history } from '../_helpers';
import { alertActions } from '../_actions';
//import { PrivateRoute } from '../_components';
//import { HomePage } from '../HomePage';
import { LoginPage } from '../LoginPage';
//import { RegisterPage } from '../RegisterPage';
import { ForgotPasswordPage } from '../ForgotPasswordPage';
import { TermsAndConditionsPage } from '../ContentPages/TermsAndConditionsPage';
import { PrivacyPolicyPage } from '../ContentPages/PrivacyPolicyPage';
import { UserSelectionFlow } from '../UserSelectionFlow';
import { TrainingPartnerPage } from '../UserSelectionFlow/TrainingPartner';
import { EmployerPage } from '../UserSelectionFlow/Employer';
import { RegistrationSuccess } from '../UserSelectionFlow/RegistrationSuccess';
import { TrainingPartner } from '../DashboardPage/TrainingPartner';
import { Employer } from '../DashboardPage/Employer';
import { AddNewCandidate } from '../_components/AddCandidates';
import { AddCandidateDetail } from '../_components/AddCandidateDetail';
import { AddFutureHiringRequest } from '../_components/AddFutureHiringRequest';
import { TrainingPartnerHiringRequest } from '../DashboardPage/TrainingPartnerPages/HiringRequest';
import { EmployerHiringRequest } from '../DashboardPage/EmployerPages/HiringRequest';
import { EditCandidate } from '../_components/EditCandidate';
import { AddEditCandidate } from '../_components/AddEditCandidate';
import { EditCenter } from '../_components/EditCenter';
import { AddEditCenter } from '../_components/AddEditCenter';
import { Contact } from '../_components/Contact';
import { AddNewCenter } from '../_components/AddCenters';
import { AddMultipleCandidate } from '../_components/AddMultipleCandidates';
import { AddMultipleFutureHiringRequest } from '../_components/AddMultipleFutureHiringRequest';
import { TrainingPartnerDashboard } from '../DashboardPage/TrainingPartnerDashboard';
import { EmployerDashboard } from '../DashboardPage/EmployerDashboard';
import { ResetPasswordPage } from '../ResetPasswordPage';
import { TrainingCenters } from '../DashboardPage/TrainingCenters';
import { SearchCandidates } from '../_components/SearchCandidate';
import { EditCandidateDetail } from '../_components/EditCandidateDetail';
import { ChangePasswordPage } from '../_components/ChangePasswordPage';
import { Settings } from '../_components/Settings';
import { EmployerBulkUploadStatus } from '../_components/EmployerBulkUploadStatus';
import { NotFound } from '../_components/NotFound';
import { BulkUploadStatus } from '../_components/BulkUploadStatus';
import { Counters } from '../_components/Counters';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = { dynamicScrollUp: '', loading: true, checkToastId:0 };
        const { dispatch } = this.props;
        history.listen((location, action) => {
            // console.log("History=", location);
            // console.log("Action=", action);
            // clear alert on location change
            dispatch(alertActions.clear());
        });
    }

    componentDidMount() {
        setTimeout(() => this.setState({ loading: false }), 50);
        var allowURL = 0;
        var URL = window.location.pathname;
        var stringCompare = 'reset-password';

        switch (URL) {
            case '/login':
                allowURL = 1;
                break;
            case '/SelectUserType':
                allowURL = 1;
                break;
            case '/training-partner-registration':
                allowURL = 1;
                break;
            case '/employer-registration':
                allowURL = 1;
                break;
            case '/forgot-password':
                allowURL = 1;
                break;
            case '/terms-and-conditions':
                allowURL = 1;
                break;
            case '/privacy-policy':
                allowURL = 1;
                break;
            case '/counters':
                allowURL = 1;
                break;    
            default:
                allowURL = 0;
        }
        var resetPasswordStringRes = URL.match(/reset-password/g);
        if(resetPasswordStringRes !== null){
            if(stringCompare === resetPasswordStringRes[0]){
                allowURL = 1;
            }
        }
        if (localStorage.getItem('user') === null) {
            if (allowURL === 0) {
                window.location.href = '/login';
            }
        }
    }
    componentWillReceiveProps(nextProps){
        // console.log('#1 msg',this.props.alert.message);
        // console.log('#2 msg',nextProps.alert.message);
 
        //Prevent Dupulicate Error Toast.
         toast.dismiss();
         if(this.props.alert.message===nextProps.alert.message)
         {            
          this.setState({checkToastId:1}); 
           
          setTimeout(() => {
             this.setState({ checkToastId: 0 });
         }, 1); 
         }
         //End of Preventing Dupulicate Error Toast.
    } 
    renderIcon() {
        window.scrollTo(0, 0);
    }
    notify(alert_message,alert_type) { 
        // console.log(alert_type);
        if(alert_type === 'alert-success'){
            toast.success(alert_message);
        }else if(alert_type === 'alert-danger'){
            toast.error(alert_message);
        }else if(alert_type === 'alert-warn'){
            toast.warn(alert_message);
        }
        
    }
    render() {
        //const { loading } = this.state;
        const { alert } = this.props;
        // console.log('#Loading', loading);

        return (
            <div>
                {this.state.loading &&
                    <div className="loading"></div>}

                {alert.message && this.state.checkToastId==0 
                // && <div className={`alert ${alert.type}`}>{alert.message} {this.renderIcon()}</div>
                    && this.notify(alert.message,alert.type) 
                }
                <ToastContainer 
                    hideProgressBar={true}
                    autoClose={7000}
                />

                {this.state.loading === false &&

                    <Router history={history}>
                        <div>
                            <Switch>
                                <Route exact path="/" component={LoginPage} />
                                <Route path="/login" component={LoginPage} />
                                <Route path="/SelectUserType" component={UserSelectionFlow} />
                                <Route path="/forgot-password" component={ForgotPasswordPage} />
                                <Route path="/training-partner-registration" component={TrainingPartnerPage} />
                                <Route path="/employer-registration" component={EmployerPage} />
                                <Route path="/registration-success" component={RegistrationSuccess} />
                                <Route path='/reset-password/:token' component={ResetPasswordPage} />
                                <Route exact path='/training-centers' component={TrainingCenters} />
                                <Route path='/Change-Password' component={ChangePasswordPage} />
                                <Route exact path='/Employer-dashboard' component={EmployerDashboard} />
                                <Route exact path='/search-candidate' component={Employer} />
                                <Route path='/add-candidates' component={AddNewCandidate} />
                                <Route path='/add-candidate-detail' component={AddCandidateDetail} />
                                <Route path='/edit-candidate' component={EditCandidate} />
                                <Route path='/add-edit-candidate' component={AddEditCandidate} />
                                <Route path='/edit-center' component={EditCenter} />
                                <Route path='/add-edit-center' component={AddEditCenter} />
                                <Route path='/add-center' component={AddNewCenter} />
                                <Route path='/add-multiple-candidates' component={AddMultipleCandidate} />
                                <Route exact path='/TrainingPartner-dashboard' component={TrainingPartnerDashboard} />
                                {/* <Route exact path='/Bulk-Upload-Status' component={BulkUploadStatus} /> */}
                                <Route path='/Employer-hiring-requests/' component={EmployerHiringRequest} />
                                <Route path='/add-future-hiring-requests' component={AddFutureHiringRequest} />
                                <Route path='/TrainingPartner-hiring-requests/' component={TrainingPartnerHiringRequest} />
                                <Route path='/SearchCandidates' component={SearchCandidates} />
                                <Route exact path='/candidates' component={TrainingPartner} />
                                <Route path='/edit-candidate-detail' component={EditCandidateDetail} />
                                <Route exact path='/Settings' component={Settings} />
                                <Route exact path='/terms-and-conditions' component={TermsAndConditionsPage} />
                                <Route exact path='/privacy-policy' component={PrivacyPolicyPage} />
                                <Route exact path='/Contact' component={Contact} />
                                <Route path='/add-multiple-future-request' component={AddMultipleFutureHiringRequest} />
                                <Route path='/Employer-Bulk-Upload-Status' component={EmployerBulkUploadStatus} />
                                <Route path='/counters' component={Counters} />
                                <Route path="*" component={NotFound} />
                            </Switch>

                        </div>
                    </Router>
                }

            </div>


        );
    }
}

function mapStateToProps(state) {
    const { alert } = state;
    return {
        alert
    };
}

const connectedApp = connect(mapStateToProps)(App);
export { connectedApp as App }; 