// import { authHeader } from '../_helpers';
import { config } from '../config';

export const pagecontentService = {

  getPageContentData,

};
var api_host_url = config.API_HOST_URL;

/************************************************************************************************************************/
var getPageContentDataAPIUrl = api_host_url + 'api/v1/other/getPageContent';


function getPageContentData(pagetype) {
  const requestOptions = {
    method: 'POST',
    headers: beforeLoginHeaders,
    body: JSON.stringify({ pagetype })
  };
  return cachedFetch(getPageContentDataAPIUrl, requestOptions, { seconds: 120 })
  //return fetch(getPageContentDataAPIUrl, requestOptions).then(handleResponse);
}
function handleResponse(response) {
    if (!response.ok) {
      return Promise.reject(response.statusText);
    }
    if (response.status === 200) {
      var cacheKey = getPageContentDataAPIUrl;
      let ct = response.headers.get('Content-Type')
      if (ct && (ct.match(/application\/json/i) || ct.match(/text\//i))) {
        // There is a .json() instead of .text() but 
        // we're going to store it in sessionStorage as 
        // string anyway.
        // If we don't clone the response, it will be 
        // consumed by the time it's returned. This 
        // way we're being un-intrusive. 
        response.clone().text().then(content => {
          localStorage.setItem(cacheKey, content)
          localStorage.setItem(cacheKey + ':ts', Date.now())
        })
      }
    }
    return response.json();
}
var beforeLoginHeaders = {
  "Content-Type": "application/json",
  "api-key": config.APIKEY,
  "UDID": getUDID(),
  "device-type": getDeviceType(),
};
// var afterLoginHeaders = {
//     "Content-Type": "application/json",
//     "api-key": config.APIKEY,
//     "UDID": getUDID(),
//     "device-type": getDeviceType(),
//     "Authorization": getAccessTokenFromLocalStorage()
// };
// function getAccessTokenFromLocalStorage(){
//     var accesstoken = '';

//     if(localStorage.getItem('user') !== null){
//         var userdata = JSON.parse(localStorage.getItem('user'));
//         if(userdata.access_token!=='')accesstoken = userdata.access_token;
//         else
//         accesstoken = userdata.data[0].access_token;
//     }
//     return accesstoken;
// }
function getUDID() {
  return window.navigator.userAgent.replace(/\D+/g, '');
}
function getDeviceType() {
  return "web";
}
const cachedFetch = (url, requestOptions, options) => {
  let expiry = 5 * 60; // 5 min default
  if (typeof options === 'number') {
    expiry = options;
    options = undefined;
  } else if (typeof options === 'object') {
    // I hope you didn't set it to 0 seconds
    expiry = options.seconds || expiry;
  }
  // Use the URL as the cache key to sessionStorage
  let cacheKey = url;
  let cached = localStorage.getItem(cacheKey);
  let whenCached = localStorage.getItem(cacheKey + ':ts');
  if (cached !== null && whenCached !== null) {
    // it was in sessionStorage! Yay!
    // Even though 'whenCached' is a string, this operation
    // works because the minus sign tries to convert the
    // string to an integer and it will work.
    let age = (Date.now() - whenCached) / 1000;
    if (age < expiry) {
      let response = new Response(new Blob([cached]))
      return Promise.resolve(response.json())
    } else {
      // We need to clean up this old key
      localStorage.removeItem(cacheKey)
      localStorage.removeItem(cacheKey + ':ts')
    }
  }
  return fetch(url, requestOptions, options).then(handleResponse);
}