// import { authHeader } from '../_helpers';
import { config } from '../config';

export const dashboardService = {
   
    getDashboardData,
   
};


//var api_host_url='http://localhost:5000/';
var api_host_url = config.API_HOST_URL;

/************************************************************************************************************************/
var getTrainingPartnerDashboardDataAPIUrl = api_host_url + 'api/v1/trainingpartner/getTrainingPartnerDashboardData';
var getIndustryPartnerDashboardDataAPIUrl = api_host_url + 'api/v1/industrypartner/getIndustryPartnerDashboardData';


function getDashboardData(id,user_type) {
    //console.log("Dashboard Service ID:##",trainingpartnerid);
    var URL = getTrainingPartnerDashboardDataAPIUrl;
    var requestBody = JSON.stringify({ trainingpartnerid : id});
    if(user_type === 'EMP'){
        URL = getIndustryPartnerDashboardDataAPIUrl;
        requestBody = JSON.stringify({ industrypartnerid : id});
    }
    const requestOptions = {
        method: 'POST',
        headers: afterLoginHeaders,
        body: requestBody
    };
    return fetch(URL, requestOptions).then(handleResponse);
}
function handleResponse(response) {
    if (!response.ok) {
        return Promise.reject(response.statusText);
    }
    return response.json();
}
// var beforeLoginHeaders = {
//     "Content-Type": "application/json",
//     "api-key": config.APIKEY,
//     "UDID": getUDID(),
//     "device-type": getDeviceType(),
// };
var afterLoginHeaders = {
    "Content-Type": "application/json",
    "api-key": config.APIKEY,
    "UDID": getUDID(),
    "device-type": getDeviceType(),
    "Authorization": getAccessTokenFromLocalStorage()
};
function getAccessTokenFromLocalStorage(){
    var accesstoken = '';
    
    if(localStorage.getItem('user') !== null){
        var userdata = JSON.parse(localStorage.getItem('user'));
        if(userdata.access_token!=='')accesstoken = userdata.access_token;
        else
        accesstoken = userdata.data[0].access_token;
    }
    return accesstoken;
}
function getUDID() {
    return window.navigator.userAgent.replace(/\D+/g, '');
}
function getDeviceType() {
     return "web";
}