import { authHeader } from '../_helpers';
import { config } from '../config';

export const userService = {
    login,
    logout,
    register,
    registerAsEmployer,
    getAll,
    getById,
    update,
    delete: _delete,
    forgotPassword,
    resetPassword,
    getAllSectors,
    getCandidates,
    getEmployer,
    getTrainingPartner,
    getCenters,
    getCandidateFormData,
    uploadCandidateCSVRequestData,
    uploadCandidateSalarySlipsData,
    getStates,
    getDistrict,
    getJobRoles,
    getJobRolesByTP,
    getSchemeCentralMinistries,
    addUpdateCandidates,
    updateTrainingPartnerProfile,
    updateIndustryPartnerProfile,
    updateTrainingCenterProfile,
    addupdateTrainingCenter,
    addUpdateCandidateDetailsHiringRequest,
    getHiringRequestDetailsForCandidate,
    getCandidateSearchFormData,
    getSearchCandidate,
    getSearchCenter,
    getHiringRequests,
    getTrainingCenterDetails,
    getCandidateDetails,
    deleteCandidate,
    deleteTrainingCenter,
    updateHiringRequestStatus,
    addUpdateFutureHiringRequest,
    updateHiringStatus,
    ChangePassword,
    updateContactMessage,
    getCandidateViewMoreDetails,
    getbulkUploadStatus,
    getSectorJobroleMapping,
    uploadFutureHiringCSVRequestData,
    getHomePageStatistics,
    removeLocalStorageCache,
    getImmidiateHiringRequestFormData,
    addUpdateImmidiateFutureHiringRequest,
    getEmployersForTC,
    getUDID,
    getDeviceType,
    getAccessTokenFromLocalStorage,
    handleResponse,
    deleteFile
};


//var api_host_url='http://localhost:5000/';
var api_host_url = config.API_HOST_URL;
// Production API HOST : var api_host_url='http://13.232.24.58:3000/';

/************************************************************************************************************************/
//Training Partner Related APIs..
var loginAsTpAPIUrl = api_host_url + 'api/v1/trainingpartner/trainingpartner-signin';
var registerloginAsTpAPIUrl = api_host_url + 'api/v1/trainingpartner/trainingpartner-signup';
var getListOfCandidatesAPI = api_host_url + 'api/v1/candidate/getCandidates/web';
var getListOfEmployerAPI = api_host_url + 'api/v1/industrypartner/getIndustryPartnerSettings/';
var getListOfTrainingPartnerAPI = api_host_url + 'api/v1/trainingpartner/getTrainingPartnerSettings/';
var getListOfCentersAPI = api_host_url + 'api/v1/trainingcenter/getTrainingCenter/web';
var getCandidateFormDataAPI = api_host_url + 'api/v1/candidate/getCandidateAddFormData/web';
// var addUpdateCandidatesAPI = api_host_url + 'api/v1/candidate/addUpdateCandidate/web';
var updateTrainingPartnerProfileAPI = api_host_url + 'api/v1/trainingpartner/trainingpartner-updateProfile';
var addupdateTrainingCenterAPI = api_host_url + 'api/v1/trainingcenter/addupdateTrainingCenter/web';
var updateHiringRequestStatusAPI = api_host_url + 'api/v1/trainingpartner/updateHiringRequest';
var getSearchCenterAPI = api_host_url + 'api/v1/trainingcenter/getTrainingCenter/web';

var updateTrainingCenterProfileAPI = api_host_url + 'api/v1/trainingcenter/trainingcenter-updateProfile';
var loginAsTcAPIUrl = api_host_url + 'api/v1/trainingcenter/trainingcenter-signin';

/************************************************************************************************************************/
//Industry Partner OR Employer Related APIs..
var loginAsEpAPIUrl = api_host_url + 'api/v1/industrypartner/industrypartner-signin';
var registerloginAsEpAPIUrl = api_host_url + 'api/v1/industrypartner/industrypartner-signup';
var getSearchCandidateAPI = api_host_url + 'api/v1/candidate/getCandidates/web';
var addUpdateCandidateDetailsHiringRequestAPI = api_host_url + 'api/v1/industrypartner/addupdateCandidateDetail/web';
var addUpdateImmidiateFutureHiringRequestAPI = api_host_url + 'api/v1/industrypartner/addupdateFutureHiringRequest/web';
var getHiringRequestsAPI = api_host_url + 'api/v1/industrypartner/getHiringRequests/web';
var updateHiringStatusAPI = api_host_url + 'api/v1/trainingpartner/updateInternalStatusForHiringRequest';
var updateIndustryPartnerProfileAPI = api_host_url + 'api/v1/industrypartner/industrypartner-updateProfile';

var getImmidiateHiringRequestFormDataAPI = api_host_url + 'api/v1/industrypartner/getImmidiateHiringRequestFormData/web';

/************************************************************************************************************************/
//Future Hiring Requests Related APIs..
var addUpdateFutureHiringRequestAPI = api_host_url + 'api/v1/futurehiringrequest/addUpdateFutureHiringRequest/web';

/************************************************************************************************************************/
//General Apis..
var forgotpasswordAsTpAPIUrl = api_host_url + 'api/v1/other/forgotPassword';
var ChangePasswordIndustryPartnerAPIUrl = api_host_url + 'api/v1/industrypartner/industrypartner-changePassword';
var ChangePasswordTrainingPartnerAPIUrl = api_host_url + 'api/v1/trainingpartner/trainingpartner-changePassword';
var ChangePasswordTrainingCenterAPIUrl = api_host_url + 'api/v1/trainingcenter/trainingcenter-changePassword';
var resetPasswordAPIUrl = api_host_url + 'api/v1/other/resetPassword';
var getAllSectorsAPI = api_host_url + 'api/v1/sector/get-sectormaster/web';
var uploadCsvAPI = api_host_url + 'api/v1/other/upload';
var uploadSalaryDocsAPI = api_host_url + 'api/v1/candidate/upload-file';
var getStateAPI = api_host_url + 'api/v1/other/getState';
var deleteCandidateAPI = api_host_url + 'api/v1/other/delete/web';
var deleteTrainingCenterAPI = api_host_url + 'api/v1/other/delete/web';
var updateContactMessageAPI = api_host_url + 'api/v1/other/updateContactMessage';
var getHomePageStatisticsAPI = api_host_url + 'api/v1/other/getHomePageStatistics';
var DeleteFileAPI = api_host_url + 'api/v1/candidate/delete-file/';
/************************************************************************************************************************/

function login(tpid, password) {
    //Check TPID/EMPID
    var splitValue = tpid.split('-');
    if (splitValue.length > 0) {
        var loginCheckURL = "";
        switch (splitValue[0]) {
            case 'EMP':
                loginCheckURL = loginAsEpAPIUrl;
                break;
            case 'TP':
                loginCheckURL = loginAsTpAPIUrl;
                break;
            case 'TC':
                loginCheckURL = loginAsTcAPIUrl;
                break;
            default:
        }
    }
    //ENd of checking..
    const requestOptions = {
        method: 'POST',
        headers: beforeLoginHeaders,
        body: JSON.stringify({ tpid, password })
    };

    //console.log("Login service in..", requestOptions);

    return fetch(loginCheckURL, requestOptions)
        .then(response => {
            if (!response.ok) {
                return Promise.reject(response.statusText);
            }
            // console.log("##here1", response);
            return response.json();
        })
        .then(user => {
            //console.log("##here2", typeof user.status);
            // login successful if there's a jwt token in the response
            if (user.status === true && user.access_token) {
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                //user.loggedin_details = user.data[0];
                localStorage.setItem('user', JSON.stringify(user));
            }
            return user;
        });

}

function logout() {
    // remove user from local storage to log user out
    //localStorage.removeItem('user');
    localStorage.clear();
}
function updateContactMessage(user) {
    const requestOptions = {
        method: 'POST',
        headers: afterLoginHeaders,
        body: JSON.stringify(user)
    };
    //  console.log("AddUpdateCandidate services", requestOptions); 
    return fetch(updateContactMessageAPI, requestOptions).then(handleResponse);

}
function getAll() {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return fetch('/users', requestOptions).then(handleResponse);
}

function getById(id) {
    // const requestOptions = {
    //     method: 'GET',
    //     headers: authHeader()
    // };

    // return fetch('/users/' + _id, requestOptions).then(handleResponse);
}

function register(user) {
    const requestOptions = {
        method: 'POST',
        headers: beforeLoginHeaders,
        body: JSON.stringify(user)
    };
    console.log("register", requestOptions);
    return fetch(registerloginAsTpAPIUrl, requestOptions).then(handleResponse);
}
function registerAsEmployer(user) {
    const requestOptions = {
        method: 'POST',
        headers: beforeLoginHeaders,
        body: JSON.stringify(user)
    };
    // console.log("Employer services", requestOptions);
    return fetch(registerloginAsEpAPIUrl, requestOptions).then(handleResponse);
}

async function addUpdateCandidates(user) {
    // const requestOptions = {
    //     method: 'POST',
    //     headers: afterLoginHeaders,
    //     body: JSON.stringify(user)
    // };
    // //  console.log("AddUpdateCandidate services", requestOptions); 
    // var response = "";
    // console.log("BEFORE", response);
    // response = await fetch(addUpdateCandidatesAPI, requestOptions).then(handleResponse);
    // console.log("AFTER", response);
    return user;
}
function deleteFile(candidateid, file_to_delete) {
    const requestOptions = {
        method: 'POST',
        headers: afterLoginHeaders,
        body: JSON.stringify({ "file_to_delete": file_to_delete })
    };
    //  console.log("SERVICES", candidateid, file_to_delete); 
    var DeleteFile = DeleteFileAPI + candidateid;
    return fetch(DeleteFile, requestOptions).then(handleResponse);
}


function updateTrainingPartnerProfile(user) {
    const requestOptions = {
        method: 'POST',
        headers: afterLoginHeaders,
        body: JSON.stringify(user)
    };
    //  console.log("AddUpdateCandidate services", requestOptions); 
    return fetch(updateTrainingPartnerProfileAPI, requestOptions).then(handleResponse);
}

function updateIndustryPartnerProfile(user) {
    const requestOptions = {
        method: 'POST',
        headers: afterLoginHeaders,
        body: JSON.stringify(user)
    };
    //  console.log("AddUpdateCandidate services", requestOptions); 
    return fetch(updateIndustryPartnerProfileAPI, requestOptions).then(handleResponse);
}


function updateTrainingCenterProfile(user) {
    const requestOptions = {
        method: 'POST',
        headers: afterLoginHeaders,
        body: JSON.stringify(user)
    };
    //console.log("addupdateTrainingCenter services", requestOptions);
    return fetch(updateTrainingCenterProfileAPI, requestOptions).then(handleResponse);
}

function addupdateTrainingCenter(user) {
    const requestOptions = {
        method: 'POST',
        headers: afterLoginHeaders,
        body: JSON.stringify(user)
    };
    //console.log("addupdateTrainingCenter services", requestOptions);
    return fetch(addupdateTrainingCenterAPI, requestOptions).then(handleResponse);
}
function addUpdateCandidateDetailsHiringRequest(user) {
    const requestOptions = {
        method: 'POST',
        headers: afterLoginHeaders,
        body: JSON.stringify(user)
    };
    //console.log("addUpdateCandidateDetailsHiringRequest services", requestOptions);
    return fetch(addUpdateCandidateDetailsHiringRequestAPI, requestOptions).then(handleResponse);
}
function addUpdateImmidiateFutureHiringRequest(user) {
    const requestOptions = {
        method: 'POST',
        headers: afterLoginHeaders,
        body: JSON.stringify(user)
    };
    //console.log("addUpdateCandidateDetailsHiringRequest services", requestOptions);
    return fetch(addUpdateImmidiateFutureHiringRequestAPI, requestOptions).then(handleResponse);
}

function getHiringRequestDetailsForCandidate(hiring_request_detail_id) {
    if (hiring_request_detail_id !== '') var getHiringRequestDetailsForCandidateAPI = api_host_url + 'api/v1/industrypartner/getHiringRequestDetailsForCandidate/' + hiring_request_detail_id;
    const requestOptions = {
        method: 'POST',
        headers: afterLoginHeaders,
    };
    return fetch(getHiringRequestDetailsForCandidateAPI, requestOptions).then(handleResponse);
}
function update(user) {
    const requestOptions = {
        method: 'PUT',
        headers: { ...authHeader(), 'Content-Type': 'application/json' },
        body: JSON.stringify(user)
    };

    return fetch('/users/' + user.id, requestOptions).then(handleResponse);
}

// prefixed function name with underscore because delete is a reserved word in javascript
function _delete(id) {
    const requestOptions = {
        method: 'DELETE',
        headers: authHeader()
    };
    return fetch('/users/' + id, requestOptions).then(handleResponse);
}


function deleteCandidate(id, tablename) {
    //console.log('Delete Candidates in Service',id);
    const requestOptions = {
        method: 'POST',
        headers: afterLoginHeaders,
        body: JSON.stringify({ id, tablename })
    };
    return fetch(deleteCandidateAPI, requestOptions).then(handleResponse);
}

function deleteTrainingCenter(id, tablename) {
    //console.log('Delete Training Center in Service',id);
    const requestOptions = {
        method: 'POST',
        headers: afterLoginHeaders,
        body: JSON.stringify({ id, tablename })
    };
    return fetch(deleteTrainingCenterAPI, requestOptions).then(handleResponse);
}

function forgotPassword(tpid, email_address) {
    //console.log("User forgot pwd serveice", email_address);
    const requestOptions = {
        method: 'POST',
        headers: beforeLoginHeaders,
        body: JSON.stringify({ tpid, email_address })
    };

    return fetch(forgotpasswordAsTpAPIUrl, requestOptions)
        .then(response => {
            if (!response.ok) {
                return Promise.reject(response.statusText);
            }
            // console.log("##here1", response);
            return response.json();
        })
        .then(user => {
            // console.log("##here2", user);
            // login successful if there's a jwt token in the response
            if (user.status === true) {
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                //  user.loggedin_details=user.data[0];                 
                // localStorage.setItem('user', JSON.stringify(user.loggedin_details));
            }
            return user;
        });
}
function resetPassword(token, password) {

    console.log("User reset services", token);

    const requestOptions = {
        method: 'POST',
        headers: beforeLoginHeaders,
        body: JSON.stringify({ token, password })
    };

    return fetch(resetPasswordAPIUrl, requestOptions)
        .then(response => {
            if (!response.ok) {
                return Promise.reject(response.statusText);
            }
            // console.log("##here1", response);
            return response.json();
        })
        .then(user => {
            // console.log("##here2", user);
            // login successful if there's a jwt token in the response
            if (user.status === true) {
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                //  user.loggedin_details=user.data[0];                 
                // localStorage.setItem('user', JSON.stringify(user.loggedin_details));
            }
            return user;
        });
}
function getSearchCandidate(searchData, pageno) {
    if (pageno) getSearchCandidateAPI = api_host_url + 'api/v1/candidate/getCandidates/web?pageno=' + pageno;
    const requestOptions = {
        method: 'POST',
        headers: afterLoginHeaders,
        body: JSON.stringify(searchData)
    };
    return fetch(getSearchCandidateAPI, requestOptions).then(handleResponse);
}
function getSearchCenter(searchData, pageno) {
    if (pageno) getSearchCenterAPI = api_host_url + 'api/v1/trainingcenter/getTrainingCenter/web?pageno=' + pageno;
    const requestOptions = {
        method: 'POST',
        headers: afterLoginHeaders,
        body: JSON.stringify(searchData)
    };
    return fetch(getSearchCenterAPI, requestOptions).then(handleResponse);
}
function getAllSectors() {
    const requestOptions = {
        method: 'POST',
        headers: afterLoginHeaders
    };
    return fetch(getAllSectorsAPI, requestOptions).then(handleResponse);
}
function getCandidates(trainingcenterid, page) {
    if (page) getListOfCandidatesAPI = api_host_url + 'api/v1/candidate/getCandidates/web?pageno=' + page;
    const requestOptions = {
        method: 'POST',
        headers: afterLoginHeaders,
        body: JSON.stringify({ trainingcenterid })
    };
    if (page === 1) {
        return cachedFetch(getListOfCandidatesAPI, requestOptions, '', 'getCandidates');
    } else {
        return fetch(getListOfCandidatesAPI, requestOptions).then(handleResponse);
    }
}

function getEmployer(industrypartnerid) {
    getListOfEmployerAPI = getListOfEmployerAPI + industrypartnerid;
    const requestOptions = {
        method: 'POST',
        headers: afterLoginHeaders,
        //  body: JSON.stringify({ industrypartnerid })
    };

    return fetch(getListOfEmployerAPI, requestOptions).then(handleResponse);
}

function getTrainingPartner(trainingpartnerid) {
    getListOfTrainingPartnerAPI = getListOfTrainingPartnerAPI + trainingpartnerid;
    const requestOptions = {
        method: 'POST',
        headers: afterLoginHeaders,
        //  body: JSON.stringify({ industrypartnerid })
    };

    return fetch(getListOfTrainingPartnerAPI, requestOptions).then(handleResponse);
}

function getTrainingCenterDetails(trainingcenterid) {
    if (trainingcenterid !== '') var getTrainingCenterDetailsAPI = api_host_url + 'api/v1/trainingcenter/getTrainingCenterDetail/' + trainingcenterid;
    const requestOptions = {
        method: 'POST',
        headers: afterLoginHeaders
    };
    return fetch(getTrainingCenterDetailsAPI, requestOptions).then(handleResponse);
}
function getbulkUploadStatus() {
    var getbulkUploadStatusAPI = api_host_url + 'api/v1/other/getBulkUploadErrorStatus';
    var requestBody = '';
    if (localStorage.getItem('user') !== null) {
        var userdata = JSON.parse(localStorage.getItem('user'));
        if (userdata.user_type === 'TP') {
            requestBody = JSON.stringify({ trainingpartnerid: userdata.data[0].trainingpartnerid })
        } else if (userdata.user_type === 'TC') {
            requestBody = JSON.stringify({ trainingcenterid: userdata.data[0].trainingcenterid })
        } else if (userdata.user_type === 'EMP') {
            requestBody = JSON.stringify({ industrypartnerid: userdata.data[0].industrypartnerid })
        }
    }
    const requestOptions = {
        method: 'POST',
        headers: afterLoginHeaders,
        body: requestBody
    };
    return fetch(getbulkUploadStatusAPI, requestOptions).then(handleResponse);
}

function getSectorJobroleMapping(trainingpartnerid, partnerType) {
    var getSectorJobroleMappingAPI = api_host_url + 'api/v1/other/getSectorJobroleMapping';
    const requestOptions = {
        method: 'POST',
        headers: afterLoginHeaders,
        body: JSON.stringify({ trainingpartnerid, partnerType })
    };
    return fetch(getSectorJobroleMappingAPI, requestOptions).then(handleResponse);
}

function getCandidateDetails(candidate_id) {

    if (candidate_id !== '') var getCandidateDetailsAPI = api_host_url + 'api/v1/candidate/getCandidateDetail/' + candidate_id + '/web';
    const requestOptions = {
        method: 'POST',
        headers: afterLoginHeaders,
    };
    return fetch(getCandidateDetailsAPI, requestOptions).then(handleResponse);
}
function getCandidateViewMoreDetails(candidate_id) {
    if (candidate_id !== '') var getCandidateViewMoreDetails = api_host_url + 'api/v1/candidate/get-candidate-viewmoredetail/' + candidate_id + '/web';
    const requestOptions = {
        method: 'POST',
        headers: afterLoginHeaders,
    };
    return fetch(getCandidateViewMoreDetails, requestOptions).then(handleResponse);
}

function getCenters(trainingpartnerid, pageno) {
    if (pageno) getListOfCentersAPI = api_host_url + 'api/v1/trainingcenter/getTrainingCenter/web?pageno=' + pageno;
    const requestOptions = {
        method: 'POST',
        headers: afterLoginHeaders,
        body: JSON.stringify({ trainingpartnerid })
    };
    return fetch(getListOfCentersAPI, requestOptions).then(handleResponse);
}

function getHomePageStatistics() {
    const requestOptions = {
        method: 'POST',
        headers: beforeLoginHeaders,
    };
    return fetch(getHomePageStatisticsAPI, requestOptions).then(handleResponse);
}
function getCandidateFormData(trainingpartnerid) {

    //console.log("Candiate Add Form in Services");
    const requestOptions = {
        method: 'POST',
        headers: afterLoginHeaders,
        body: JSON.stringify({ trainingpartnerid })
    };
    return fetch(getCandidateFormDataAPI, requestOptions).then(handleResponse);
}
function getCandidateSearchFormData(trainingpartnerid, type) {

    // console.log("Search Add Form in Services");
    const requestOptions = {
        method: 'POST',
        headers: afterLoginHeaders,
        body: JSON.stringify({ trainingpartnerid, type })
    };
    return fetch(getCandidateFormDataAPI, requestOptions).then(handleResponse);
}

function uploadCandidateCSVRequestData(file, other_data = '') {
    if (localStorage.getItem('user') !== null) {
        var userdata = JSON.parse(localStorage.getItem('user'));
    }
    const formData = new FormData();
    formData.append('file', file);
    formData.append('table', 'candidate');
    formData.append('trainingpartnerid', userdata.data[0].trainingpartnerid);
    formData.append('trainingcenterid', userdata.data[0].trainingcenterid);

    if (!!other_data && Object.keys(other_data).length > 0) {
        for (let key in other_data) {
            formData.append(key, other_data[key]);
        }
    }

    const config = {
        headers: afterLoginHeaders,
        url: uploadCsvAPI,
        data: formData
    }
    return config;
}

function uploadCandidateSalarySlipsData(month_1, month_2, month_3, appointment_letter, sdms_id) {

    if (localStorage.getItem('user') !== null) {
        var userdata = JSON.parse(localStorage.getItem('user'));
    }
    const formData = new FormData();
    // let candidate_idvalue = window.location.hash.split(":");
    formData.append('month_1', month_1);
    formData.append('month_2', month_2);
    formData.append('month_3', month_3);
    formData.append('appointment_letter', appointment_letter);
    // formData.append('table', 'candidate');
    formData.append('trainingpartnerid', userdata.data[0].trainingpartnerid);
    formData.append('trainingcenterid', userdata.data[0].trainingcenterid);
    formData.append('candidateid', sdms_id);
    formData.append('sdms_id', sdms_id);
    const config = {
        headers: afterLoginHeaders,
        url: uploadSalaryDocsAPI,
        data: formData
    }

    return config;
}

function uploadFutureHiringCSVRequestData(file) {
    if (localStorage.getItem('user') !== null) {
        var userdata = JSON.parse(localStorage.getItem('user'));
    }
    const formData = new FormData();
    formData.append('file', file);
    formData.append('table', 'FutureHiringRequest');
    formData.append('industrypartnerid', userdata.data[0].industrypartnerid);
    const config = {
        headers: afterLoginHeaders,
        url: uploadCsvAPI,
        data: formData
    }
    return config;
}

function getStates(value) {
    const requestOptions = {
        method: 'POST',
        headers: beforeLoginHeaders
    };
    return fetch(getStateAPI, requestOptions).then(handleResponse);
}

function getDistrict(value) {

    if (value !== '') var getDistrictAPI = api_host_url + 'api/v1/other/getCity/' + value;

    const requestOptions = {
        method: 'POST',
        headers: afterLoginHeaders
    };
    return fetch(getDistrictAPI, requestOptions).then(handleResponse);
}
function getJobRoles(value) {

    if (value !== '') var getJobRoleAPI = api_host_url + 'api/v1/other/getJobRoles/' + value;

    const requestOptions = {
        method: 'POST',
        headers: afterLoginHeaders
    };
    return fetch(getJobRoleAPI, requestOptions).then(handleResponse);
}
function getEmployersForTC(value) {
    if (value !== '') var getEmployersForTCAPI = api_host_url + 'api/v1/other/getEmployersForTC/' + value;
    const requestOptions = {
        method: 'POST',
        headers: afterLoginHeaders
    };
    return fetch(getEmployersForTCAPI, requestOptions).then(handleResponse);
}

function getJobRolesByTP(value) {

    if (value !== '') var getJobRoleAPI = api_host_url + 'api/v1/other/getJobRolesByTP/' + value;

    const requestOptions = {
        method: 'POST',
        headers: afterLoginHeaders
    };
    return fetch(getJobRoleAPI, requestOptions).then(handleResponse);
}

function getSchemeCentralMinistries(value) {
    if (value !== '') var getSCMAPI = api_host_url + 'api/v1/other/getSchemeCentralMinistry/' + value;

    const requestOptions = {
        method: 'POST',
        headers: afterLoginHeaders
    };
    return fetch(getSCMAPI, requestOptions).then(handleResponse);
}
function getHiringRequests(industrypartnerid, hiring_request_status, type, pageNo, internal_status) {
    // console.log('#internal_status', pageNo);
    if (pageNo) getHiringRequestsAPI = api_host_url + 'api/v1/industrypartner/getHiringRequests/web?pageno=' + pageNo + '&internalStatus=' + internal_status;
    const requestOptions = {
        method: 'POST',
        headers: afterLoginHeaders,
        body: JSON.stringify({ industrypartnerid, hiring_request_status, type })
    };
    return fetch(getHiringRequestsAPI, requestOptions).then(handleResponse);
}
function updateHiringRequestStatus(hiringrequestid, hiringrequeststatus, cancelreason) {
    var requestBody = JSON.stringify({ hiringrequestid, hiringrequeststatus });
    if (cancelreason !== undefined && cancelreason !== '') {
        requestBody = JSON.stringify({ hiringrequestid, hiringrequeststatus, cancelreason });
    }
    const requestOptions = {
        method: 'POST',
        headers: afterLoginHeaders,
        body: requestBody
    };
    return fetch(updateHiringRequestStatusAPI, requestOptions).then(handleResponse);
}
function addUpdateFutureHiringRequest(futurerequestdata) {
    const requestOptions = {
        method: 'POST',
        headers: afterLoginHeaders,
        body: JSON.stringify(futurerequestdata)
    };
    return fetch(addUpdateFutureHiringRequestAPI, requestOptions).then(handleResponse);
}
function updateHiringStatus(value) {
    const requestOptions = {
        method: 'POST',
        headers: afterLoginHeaders,
        body: JSON.stringify(value)
    };
    console.log("SERVICES - HIRING REWUST", requestOptions);

    return fetch(updateHiringStatusAPI, requestOptions).then(handleResponse);
}
function ChangePassword(parterID, old_password, new_password, partnerType) {
    var requestOptions, fetchREq;
    if (partnerType === 'TP') {
        fetchREq = ChangePasswordTrainingPartnerAPIUrl;
    } else if (partnerType === 'EMP') {
        fetchREq = ChangePasswordIndustryPartnerAPIUrl;
    }
    else if (partnerType === 'TC') {
        fetchREq = ChangePasswordTrainingCenterAPIUrl;
    }
    requestOptions = {
        method: 'POST',
        headers: beforeLoginHeaders,
        body: JSON.stringify({ parterID, old_password, new_password })
    };
    return fetch(fetchREq, requestOptions)
        .then(response => {
            if (!response.ok) {
                return Promise.reject(response.statusText);
            }
            return response.json();
        })
        .then(user => {
            if (user.status === true) {
            }
            return user;
        });

    // if (partnerType === 'TP') {
    //     const trainingpartnerid = parterID;
    //     requestOptions = {
    //         method: 'POST',
    //         headers: beforeLoginHeaders,
    //         body: JSON.stringify({ trainingpartnerid, old_password, new_password })
    //     };
    //     return fetch(ChangePasswordTrainingPartnerAPIUrl, requestOptions)
    //         .then(response => {
    //             if (!response.ok) {
    //                 return Promise.reject(response.statusText);
    //             }
    //             return response.json();
    //         })
    //         .then(user => {
    //             if (user.status === true) {
    //             }
    //             return user;
    //         });
    // } else {
    //     const industrypartnerid = parterID;
    //     requestOptions = {
    //         method: 'POST',
    //         headers: beforeLoginHeaders,
    //         body: JSON.stringify({ industrypartnerid, old_password, new_password })
    //     };
    //     return fetch(ChangePasswordIndustryPartnerAPIUrl, requestOptions)
    //         .then(response => {
    //             if (!response.ok) {
    //                 return Promise.reject(response.statusText);
    //             }

    //             return response.json();
    //         })
    //         .then(user => {
    //             if (user.status === true) {
    //             }
    //             return user;
    //         });
    // }
}
function handleResponse(response, wantToCache, uniqueKey) {

    if (!response.ok) {
        return Promise.reject(response.statusText);
    }
    if (typeof wantToCache !== 'undefined') {
        if (response.status === 200) {
            var cacheKey = wantToCache;
            let ct = response.headers.get('Content-Type')
            if (ct && (ct.match(/application\/json/i) || ct.match(/text\//i))) {
                // There is a .json() instead of .text() but 
                // we're going to store it in sessionStorage as 
                // string anyway.
                // If we don't clone the response, it will be 
                // consumed by the time it's returned. This 
                // way we're being un-intrusive. 
                response.clone().text().then(content => {
                    localStorage.setItem(cacheKey, content)
                    localStorage.setItem(cacheKey + ':ts', Date.now())
                    localStorage.setItem(uniqueKey, cacheKey)
                })
            }
        }
    }
    return response.json();
}
var beforeLoginHeaders = {
    "Content-Type": "application/json",
    "api-key": config.APIKEY,
    "UDID": getUDID(),
    "device-type": getDeviceType(),
};
var afterLoginHeaders = {
    "Content-Type": "application/json",
    "api-key": config.APIKEY,
    "UDID": getUDID(),
    "device-type": getDeviceType(),
    "Authorization": getAccessTokenFromLocalStorage()
};
function getAccessTokenFromLocalStorage() {
    var accesstoken = '';

    if (localStorage.getItem('user') !== null) {
        var userdata = JSON.parse(localStorage.getItem('user'));
        if (userdata.access_token !== '') accesstoken = userdata.access_token;
        else
            accesstoken = userdata.data[0].access_token;
    }
    return accesstoken;
}
function getUDID() {
    return window.navigator.userAgent.replace(/\D+/g, '');
}
function getDeviceType() {
    var Sys = {};
    var ua = navigator.userAgent.toLowerCase();
    var s;
    s = (s = ua.match(/msie ([\d.]+)/)) ? Sys.ie = s[1] :
        (s = ua.match(/firefox\/([\d.]+)/)) ? Sys.firefox = s[1] :
            (s = ua.match(/chrome\/([\d.]+)/)) ? Sys.chrome = s[1] :
                (s = ua.match(/opera.([\d.]+)/)) ? Sys.opera = s[1] :
                    (s = ua.match(/version\/([\d.]+).*safari/)) ? Sys.safari = s[1] : 0;


    if (Sys.ie) return ('IE: ' + Sys.ie);
    if (Sys.firefox) return ('Firefox: ' + Sys.firefox);
    if (Sys.chrome) return ('Chrome: ' + Sys.chrome);
    if (Sys.opera) return ('Opera: ' + Sys.opera);
    if (Sys.safari) return ('Safari: ' + Sys.safari);
    return "web";
}
const cachedFetch = (url, requestOptions, options, uniqueKey) => {
    let expiry = 5 * 60; // 5 min default
    if (typeof options === 'number') {
        expiry = options;
        options = undefined;
    } else if (typeof options === 'object') {
        // I hope you didn't set it to 0 seconds
        expiry = options.seconds || expiry;
    }
    // Use the URL as the cache key to sessionStorage
    let cacheKey = url;
    let cached = localStorage.getItem(cacheKey);
    let whenCached = localStorage.getItem(cacheKey + ':ts');
    if (cached !== null && whenCached !== null) {
        // it was in sessionStorage! Yay!
        // Even though 'whenCached' is a string, this operation
        // works because the minus sign tries to convert the
        // string to an integer and it will work.
        let age = (Date.now() - whenCached) / 1000;
        if (age < expiry) {
            let response = new Response(new Blob([cached]))
            return Promise.resolve(response.json());
        } else {
            removeLocalStorageCache(uniqueKey);
        }
    }
    return fetch(url, requestOptions, options).then(response => {
        return Promise.resolve(handleResponse(response, url, uniqueKey));
    });
}
function removeLocalStorageCache(uniqueKey) {
    // We need to clean up this old key
    let cached = localStorage.getItem(uniqueKey);
    if (cached !== null) {
        localStorage.removeItem(cached);
        localStorage.removeItem(cached + ':ts');
    }
    localStorage.removeItem(uniqueKey);
}

function getImmidiateHiringRequestFormData(industrypartnerid, type) {

    // console.log("Search Add Form in Services");
    const requestOptions = {
        method: 'POST',
        headers: afterLoginHeaders,
        body: JSON.stringify({ industrypartnerid, type })
    };
    return fetch(getImmidiateHiringRequestFormDataAPI, requestOptions).then(handleResponse);
}
