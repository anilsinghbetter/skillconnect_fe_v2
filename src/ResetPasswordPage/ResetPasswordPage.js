import React from 'react';
import { connect } from 'react-redux';
import { reduxForm, Field } from 'redux-form';
import { CardHeader, Card, CardBody, CardText, CardTitle } from 'reactstrap';
import { userActions } from '../_actions';
import { Footer } from '../_components/Footer';
import { Header } from '../_components/Header';

const renderField = ({ input, label, type, meta: { touched, error } }) => (

    <div className=''>
        <div className={'form-group' + (error && touched ? ' has-error' : '')}>
            <label>{label}</label>
            <input {...input} type={type} className="form-control" />
            <div>
                {touched && ((error && <div className="help-block">{error}</div>))}
            </div>
        </div>
    </div>
);

export const minLength = min => value =>
    value && value.length < min ? `Must be ${min} characters or more` : undefined
export const minLength8 = minLength(8)

const passwordsMatch = (value, allValues) =>
    value !== allValues.password ? "Passwords don't match" : undefined;

const validate = values => {

    const errors = {}

    if (!values.password) {
        errors.password = 'Please enter your password.'
    }
    if (!values.confirm_changepwd) {
        errors.confirm_changepwd = 'Please enter your confirm password'
    }

    return errors;
}

class ResetPasswordPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            password: '',
            submitted: false,
            applyWidthClass: ''
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    updateDimensions() {
        const winWidth = window.innerWidth
        const maxWidth = 767
        const winHeight = window.innerHeight
        //console.log("max width", maxWidth);
        //console.log("window width", winWidth);
        //console.log("window height", winHeight);
        if (true || winWidth < maxWidth) {
            this.setState({
                applyWidthClass: winHeight + "px",
            });
        }
    }
    componentWillMount() {
        this.updateDimensions();
    }
    componentDidMount() {
        this.updateDimensions();
        window.addEventListener("resize", this.updateDimensions.bind(this));
    }
    componentWillUnmount() {
        window.removeEventListener("resize", this.updateDimensions.bind(this));
    }
    handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });
    }

    handleSubmit(e) {
        // e.preventDefault();

        this.setState({ submitted: true });
        const { password } = this.state;
        const { dispatch } = this.props;

        let token = this.props.match.params.token;

      //  console.log("url data", token);
       // console.log("send data", token, password);
        // return;
        if (token && password) {
            dispatch(userActions.resetPassword(token, password));
            //Redirect to login page after 5 seconds once we update the new password...
            this.timeoutHandle = setTimeout(()=>{
              // console.log('Redirecting to Login Page...') 
               this.props.history.push('/login');
           }, 5000);
           //End of redirecting login page code...

        }
    }


    render() {
        const { handleSubmit } = this.props;
        const { applyWidthClass } = this.state;
        const styles = {
            minHeight: applyWidthClass//'497px'
        };
        //console.log("user details return from reducers", items);
        return (
            <div className="content-wrapper user-onboarding landing-page">
            <Header/>
                <div className="container">
                    <div className="d-flex justify-content-center align-items-center window-height" style={styles}>
                        <Card className="card card-md z-depth">
                            <CardHeader className="border-bottom-0 bg-transparent section-title text-center">
                                <CardTitle className="title card-title">Set Password</CardTitle>
                            </CardHeader>
                            <CardBody>
                                <form id="frm-forgot" className="form frm-forgot" onSubmit={handleSubmit(this.handleSubmit)}>
                                    <CardText className="text-center"> Set your account password to access</CardText>
                                    <Field name="password" type="password" component={renderField} label="New Password" onChange={this.handleChange} validate={minLength8} />
                                    <Field
                                        name="confirm_changepwd"
                                        type="password"
                                        component={renderField}
                                        label="Confirm Password"
                                        onChange={this.handleChange}
                                        validate={passwordsMatch}
                                    />
                                    <button className="btn btn-primary btn-raised btn-lg btn-block" title="Submit">Submit</button>
                                </form>
                            </CardBody>
                        </Card>
                    </div>
                </div>
                <Footer/>
            </div>

        );
    }
}

ResetPasswordPage = reduxForm({
    form: 'ResetPasswordPage',
    validate,
    enableReinitialize: true,
})(ResetPasswordPage);

function mapStateToProps(state) {
    const { loggingIn, alert } = state;
    const { items } = state.resetpassword;
    return {
        loggingIn, alert, items
    };
}

const connectedResetPasswordPage = connect(mapStateToProps)(ResetPasswordPage);
export { connectedResetPasswordPage as ResetPasswordPage }; 
