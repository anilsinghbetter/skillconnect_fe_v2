import React from 'react';
import Select from 'react-select';

export default (props) => (
    
    <Select
    {...props}
    value={props.input.value}
    onChange={(value) => props.input.onChange(value)}
    onBlur={() => props.input.onBlur(props.input.value)}
    options={props.options}
  />

  

);
/*
<Select 
                                                    closeOnSelect={!stayOpen}
                                                    disabled={disabled}
                                                    multi
                                                    onChange={this.handleSelectChange}
                                                    options={final_allSectors}
                                                    placeholder="Select your relavant sector(s)"
                                                    removeSelected={this.state.removeSelected}
                                                    rtl={this.state.rtl}
                                                    simpleValue
                                                    value={value}                                                    
                                                />
                                                */