import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
//import { userActions } from '../_actions';
//import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import { Footer } from '../_components/Footer';
import { Header } from '../_components/Header';

    class UserSelectionFlow extends React.Component {

        constructor(props) {
            super(props);
            
            this.setUserType= this.setUserType.bind(this);
		    this.state = {
                user_flow:true,//not using now, but it can be use later.
				userType:false,
                nextpage : '',
                applyWidthClass: ''
            };
           
        }		
        updateDimensions() {
            const winWidth = window.innerWidth
            const maxWidth = 767
            const winHeight = window.innerHeight
    
            //console.log("max width", maxWidth);
            //console.log("window width", winWidth);
            //console.log("window height", winHeight);
    
            if (true || winWidth < maxWidth) {
                this.setState({
                    applyWidthClass: winHeight + "px",
                });
            }
            else {
                this.setState({
                    applyWidthClass: '',
                });
            }
        }
        componentWillMount() {
    
            this.updateDimensions();
        }
        componentDidMount() {
            this.updateDimensions();
            window.addEventListener("resize", this.updateDimensions.bind(this));
        }
        componentWillUnmount() {
            window.removeEventListener("resize", this.updateDimensions.bind(this));
        }
    
           setUserType(event) {
            this.setState({ userType: true });
			this.setState({ nextpage: event.target.value });
            //return event.target.value;
        }
        render() {
           // console.log("selected values",event.target.value); 
           const { userType,nextpage,applyWidthClass } = this.state;
		//	console.log("nextpage is ",nextpage);
			const styles = {
                minHeight: applyWidthClass//'497px'
            };
            return (
				<div className="content-wrapper user-onboarding landing-page select-user">
                <Header/>
                <div className="container">
                <div className="d-flex justify-content-center align-items-center window-height" style={styles}>
                  <div className="card z-depth">
                    
                    <div className="section-title card-header text-center border-bottom-0 bg-transparent">
                        <h1 className="card-title">Register as</h1>
                        <span className="card-subtitle subtitle">How do you wish to use SkillConnect?</span>
                    </div>
					
                    <div className="card-body"> 
                        <div className="radio-card d-flex justify-content-center" onChange={this.setUserType}>
                            <div className="form-check">
                                <input className="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="TrainingPartner"/>
                                <label className="form-check-label text-center" htmlFor="exampleRadios1">
                                    <div className="img-block d-flex justify-content-center align-items-center m-auto">
                                        <svg viewBox="0 0 64 64" enableBackground="new 0 0 64 64">
                                            <g>
                                                <g>
                                                    <path d="M34.623,28.103h15.391V25.25H34.623V28.103z M34.623,37.021h15.391v-2.854H34.623V37.021z M58.695,51.213
                                                        c3.041-2.463,3.531-6.953,1.092-10.026c-0.59-0.742-1.322-1.359-2.15-1.813V16.191h3.316V6.063h-24.07V0h-9.176v6.063H3.565
                                                        v10.128h3.317v24.18c-2.879,2.655-3.083,7.168-0.456,10.077c0.25,0.275,0.52,0.531,0.809,0.765
                                                        c-2.623,1.599-4.23,4.465-4.235,7.56v5.351h17.506v-5.351c-0.02-3.108-1.655-5.979-4.306-7.561
                                                        c0.997-0.826,1.753-1.911,2.188-3.139h7.906c0.435,1.228,1.191,2.313,2.188,3.139c-2.651,1.58-4.286,4.451-4.306,7.561v5.351
                                                        h17.576v-5.351c-0.02-3.108-1.654-5.979-4.305-7.561c0.996-0.826,1.752-1.911,2.188-3.139h7.977
                                                        c0.436,1.228,1.191,2.313,2.188,3.139c-2.65,1.58-4.285,4.451-4.305,7.561v5.351H63v-5.351
                                                        C62.98,55.664,61.348,52.793,58.695,51.213z M58.555,45.578c0,2.403-1.93,4.353-4.307,4.353c-2.379,0-4.307-1.949-4.307-4.353
                                                        c0-2.402,1.928-4.351,4.307-4.351C56.625,41.228,58.555,43.176,58.555,45.578z M30.53,2.854h3.529v3.209H30.53V2.854z
                                                         M6.389,13.338V8.916h51.812v4.422H6.389z M11.613,41.299c0.046-0.001,0.093-0.001,0.14,0c2.323,0.038,4.197,1.933,4.235,4.279
                                                        c0.039,2.401-1.857,4.383-4.235,4.422s-4.337-1.877-4.375-4.28C7.339,43.317,9.235,41.338,11.613,41.299z M17.683,58.772
                                                        l-0.07,2.427H5.824v-2.427c0-3.309,2.655-5.99,5.93-5.99C15.028,52.782,17.683,55.465,17.683,58.772z M25.942,45.149h-7.13
                                                        c-0.099-1.815-0.882-3.525-2.188-4.778H28.13C26.824,41.624,26.041,43.334,25.942,45.149z M17.471,37.519V24.751h9.67v12.768
                                                        H17.471z M38.932,58.772v2.498H27.071v-2.498c0-3.309,2.655-5.99,5.93-5.99C36.275,52.782,38.932,55.465,38.932,58.772z
                                                         M28.694,45.647c0-0.022,0-0.047,0-0.069c0.039-2.375,1.955-4.279,4.306-4.279c2.377,0,4.305,1.948,4.305,4.353
                                                        c0,2.401-1.928,4.351-4.306,4.35C30.622,50,28.694,48.052,28.694,45.647z M47.189,45.149h-7.131
                                                        c-0.203-3.934-3.523-6.957-7.417-6.75c-0.928,0.049-1.838,0.283-2.677,0.688v-17.19H14.647v17.118
                                                        c-1.528-0.705-3.257-0.832-4.871-0.354V16.191h45.037v22.253h-0.564C50.521,38.474,47.447,41.396,47.189,45.149z M60.178,61.199
                                                        H48.318v-2.427c0-3.309,2.654-5.99,5.93-5.99s5.93,2.683,5.93,5.99V61.199z"/>
                                                </g>
                                            </g>
                                        </svg>

                                    </div>
                                    <div className="radio-block-content">
                                        <p>Traning Partner</p>
                                        <span className="pmd-display-0 text-muted">
                                            Provides the trained candidates to the employers according to their requirements
                                        </span>
                                    </div>
                                </label>
                            </div>

                            <div className="form-check">
                                <input className="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="Employer"/>
                                <label className="form-check-label text-center" htmlFor="exampleRadios2">
                                    <div className="img-block d-flex justify-content-center align-items-center m-auto">
                                        <svg viewBox="0 0 64 64" enableBackground="new 0 0 64 64">
                                            <g>
                                                <g>
                                                    <path d="M20.833,34.463c-0.048-0.002-0.097-0.002-0.145,0c-0.852,0.042-1.51,0.775-1.471,1.639
                                                        c0.04,0.865,0.764,1.535,1.616,1.494h4.629c0.853,0.012,1.553-0.681,1.565-1.544c0.012-0.865-0.669-1.575-1.522-1.589H20.833z
                                                         M37.831,21.963c0.012-0.865-0.67-1.575-1.522-1.587H20.833c-0.048-0.002-0.097-0.002-0.145,0
                                                        c-0.852,0.041-1.51,0.774-1.471,1.639c0.04,0.865,0.764,1.532,1.616,1.492h15.433C37.117,23.519,37.818,22.828,37.831,21.963z
                                                         M43.641,14.919c0.012-0.865-0.669-1.575-1.52-1.587c-0.016,0-0.029,0-0.046,0H20.833c-0.048-0.002-0.097-0.002-0.145,0
                                                        c-0.852,0.041-1.51,0.774-1.471,1.638c0.04,0.865,0.764,1.533,1.616,1.492h21.242C42.93,16.475,43.629,15.784,43.641,14.919z
                                                         M31.657,29.007c0.012-0.865-0.668-1.576-1.521-1.587h-9.303c-0.048-0.002-0.097-0.002-0.145,0
                                                        c-0.852,0.041-1.51,0.774-1.471,1.639c0.04,0.864,0.764,1.532,1.616,1.492h9.26C30.944,30.563,31.646,29.872,31.657,29.007z
                                                         M61.109,46.375c-1.865-2.074-4.364-3.301-7.115-3.301c-2.749,0-5.265,1.227-7.132,3.301c-0.382,0.425-0.73,0.897-1.056,1.395
                                                        h-30.64c0.174-0.513,0.265-1.057,0.265-1.614V10.202h37.039v18.959c-3.096,0.708-5.43,3.524-5.43,6.867
                                                        c0,3.872,3.127,7.046,6.945,7.046c3.817,0,6.946-3.174,6.946-7.046c0-3.323-2.309-6.125-5.376-6.853V8.637
                                                        c-0.002-0.864-0.692-1.565-1.543-1.565H13.888c-0.048-0.002-0.096-0.002-0.145,0c-0.795,0.076-1.401,0.754-1.398,1.565v4.696
                                                        H4.677C2.116,13.333,0,15.479,0,18.077v24.997c0,0.863,0.69,1.563,1.542,1.563h4.63v1.519c0,2.642,2.188,4.745,4.75,4.745h33.423
                                                        c-0.377,1.219-0.596,2.532-0.596,3.912c0,0.865,0.482,1.565,1.076,1.565h18.319c0.596,0,1.076-0.7,1.076-1.565
                                                        C64.222,51.45,62.979,48.449,61.109,46.375z M57.844,36.028c0,2.181-1.709,3.914-3.857,3.914c-2.149,0-3.858-1.733-3.858-3.914
                                                        c0-2.179,1.709-3.912,3.858-3.912C56.135,32.116,57.844,33.85,57.844,36.028z M6.173,41.507H3.086v-23.43
                                                        c0-0.924,0.692-1.615,1.591-1.615h0.266c0.039,0.019,0.079,0.035,0.121,0.049c0.668,0.167,1.109,0.696,1.109,1.517V41.507z
                                                         M12.346,46.155c0,0.87-0.444,1.413-1.085,1.564c-0.049,0.015-0.097,0.029-0.145,0.049h-0.193
                                                        c-0.899,0.001-1.665-0.734-1.665-1.613V18.028c0-0.542-0.097-1.076-0.265-1.566h3.352V46.155z M46.9,53.248
                                                        c0.261-1.653,0.941-3.173,1.955-4.402c1.324-1.608,3.216-2.642,5.327-2.642c2.109,0,4.003,1.033,5.327,2.642
                                                        c1.014,1.229,1.679,2.749,1.938,4.402H46.9z"/>
                                                </g>
                                            </g>
                                        </svg>
                                    </div>
                                    <div className="radio-block-content">
                                        <p>Employer</p>
                                        <span className="pmd-display-0 text-muted">
                                            Onboard skilled and trained candidates with the help of Training partners
                                        </span>
                                    </div>
                                </label>
                            </div>
                        </div>

                        <div className="d-flex justify-content-center">                       
                            <Link to="/login" className="btn btn-lg btn-outline-primary mr-3" title="Not Yet">Not Yet</Link>
                            {!userType &&                                    
                                <Link to={`/register/`}  className=" disabled btn btn-lg btn-primary next-btn btn-raisedn" title="Register">Register</Link>
                                }
                            {userType && nextpage==='TrainingPartner' &&
                                <Link to={`/training-partner-registration`}  className="btn btn-lg btn-primary next-btn btn-raised" title="Register">Register</Link>
							}
							{userType && nextpage==='Employer' &&
                                <Link to={`/employer-registration`}  className="btn btn-lg btn-primary next-btn btn-raised" title="Register">Register</Link>
							}
							
                        </div>
                    </div>
                </div>
                </div>
                </div>
                <Footer/>
                </div>
                
            );
        }
    }
    function mapStateToProps(state) {
        const { registering } = state.registration;
        return {
            registering
        };
    }

    const connectedUserSelectionFlow = connect(mapStateToProps)(UserSelectionFlow);
    export { connectedUserSelectionFlow as UserSelectionFlow };