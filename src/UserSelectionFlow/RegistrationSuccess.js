import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import {CardIcon} from '../LoginPage/cardIcon';
import { CardHeader,Card,CardBody,CardText,CardTitle } from 'reactstrap';
import { Footer } from '../_components/Footer';
import { Header } from '../_components/Header';
class RegistrationSuccess extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        user_flow:true,//not using now, but it can be use later.
        userType:false,
        applyWidthClass: ''
        };
    }
    updateDimensions() {
        const winWidth = window.innerWidth
        const maxWidth = 767
        const winHeight = window.innerHeight

        //console.log("max width", maxWidth);
        //console.log("window width", winWidth);
        //console.log("window height", winHeight);

        if (true || winWidth < maxWidth) {
            this.setState({
                applyWidthClass: winHeight + "px",
            });
        }
        else {
            this.setState({
                applyWidthClass: '',
            });
        }
    }
    componentWillMount() {

        this.updateDimensions();
    }
    componentDidMount() {
        this.updateDimensions();
        window.addEventListener("resize", this.updateDimensions.bind(this));
    }
    componentWillUnmount() {
        window.removeEventListener("resize", this.updateDimensions.bind(this));
    }
    render() {
        const { applyWidthClass } = this.state;  
        const styles = {
            minHeight: applyWidthClass//'497px'
        };
    return(  
              
	<div className="content-wrapper user-onboarding landing-page">
        <Header/>
		<div className="container">
			<div className="d-flex justify-content-center align-items-center window-height" style={styles}>
                <Card className="card card-md z-depth">
                    <CardIcon/>
                    <CardHeader className="border-bottom-0 bg-transparent section-title text-center">
                        <CardTitle className="title card-title">Congratulations!</CardTitle>
                    </CardHeader>
                    <CardBody className="text-center">
                        <CardText> <strong>Thank you for registering with SkillConnect portal</strong>.
                                <br/>Your request has been sent to SkillConnect Admin for further processing. You will be notified once your account<br/>has been approved.
                        </CardText>
                        <Link to="/login" className="btn btn-lg btn-primary btn-raised" title="Got It">GOT IT</Link>
                    </CardBody>
                </Card>
			</div>
		</div>
        <Footer/>
	</div>
    );
    }
    }

    function mapStateToProps(state) {
    const { registering } = state.registration;
    return {
    registering
    };
    }
    const connectedRegistrationSuccess = connect(mapStateToProps)(RegistrationSuccess);
    export { connectedRegistrationSuccess as RegistrationSuccess };