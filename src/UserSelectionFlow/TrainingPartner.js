import React from 'react';
import { Link } from 'react-router-dom';
import { reduxForm, Field } from 'redux-form';
import { connect } from 'react-redux';
import { userActions } from '../_actions';
import { Label, Input } from 'reactstrap';
import Select from 'react-select';
import { Footer } from '../_components/Footer';
import { Header } from '../_components/Header';
//import SelectInput from '../UserSelectionFlow/SelectInput';

const renderField = ({ input, label, type, meta: { touched, error, warning } }) => (

    <div className="col-lg-6 col-12">

        <div className={'form-group' + (error && touched ? ' has-error' : '')}>

            <label>{label}</label>
            <input {...input} type={type} className="form-control" />
            <div>
                {touched && ((error && <div className="help-block">{error}</div>) || (warning && <span>{warning}</span>))}
            </div>

        </div>
    </div>
);

const validate = values => {

    const errors = {}
   
    if (!values.nsdc_registration_number) {
        errors.nsdc_registration_number = 'Please enter NSDC/SSC Registration Number'
    } /*else if (values.username.length > 15) {
            errors.username = 'Must be 15 characters or less'
            }  */
    if (!values.legal_entity_name) {
        errors.legal_entity_name = 'Please enter Legal Entity Name'
    }
    if (!values.contact_email_address) {
        errors.contact_email_address = 'Please enter an Email Address'
    }
    if (!values.training_partner_name) {
        errors.training_partner_name = 'Please enter Training Partner Name'
    }
    if (!values.contact_person_name) {
        errors.contact_person_name = 'Please enter Contact Person Name'
    }/*
            if (!values.contact_mobile) {
                errors.contact_mobile = 'Please enter Contact Mobile No'
            }*/
    return errors;
}

class TrainingPartnerPage extends React.Component {

    constructor(props) {
        super(props);


        this.state = {
            user: {
                nsdc_registration_number: '',
                legal_entity_name: '',
                contact_email_address: '',
                contact_mobile: '',
                contact_person_name: '',
                training_partner_name: '',
                address: ''
            },
            submitted: false,
            applyWidthClass: '',
            removeSelected: true,
            disabled: false,
            stayOpen: false,
            value: [],
            rtl: false,
            registerClick:0
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleSelectChange = this.handleSelectChange.bind(this);
        this.checkValidation=this.checkValidation.bind(this);
    }
    updateDimensions() {
        const winWidth = window.innerWidth
        const maxWidth = 767
        const winHeight = window.innerHeight

        //console.log("max width", maxWidth);
        //console.log("window width", winWidth);
        //console.log("window height", winHeight);

        if (true || winWidth < maxWidth) {
            this.setState({
                applyWidthClass: winHeight + "px",
            });
        }
    }

    componentWillMount() {
        this.updateDimensions();
    }
    componentDidMount() {
        this.updateDimensions();
        window.addEventListener("resize", this.updateDimensions.bind(this));
        //Gettting all sector values..
        this.props.dispatch(userActions.getAllSectors());
    }
    componentWillUnmount() {
        window.removeEventListener("resize", this.updateDimensions.bind(this));
    }
    handleChange(event) {
        const { name, value } = event.target;
        const { user } = this.state;

        this.setState({
            user: {
                ...user,
                [name]: value
            }
        });
    }
    checkValidation(event) {
       // console.log("Register clickec....");
        this.setState({ registerClick:1 });
    }

    handleSubmit(event) {
        // event.preventDefault();
        this.setState({ submitted: true });
        const { user, value } = this.state;
        const { dispatch } = this.props;

        var sectors = value;
        var userFinal = { sectors, ...user };

       // console.log("send data", userFinal);

        //return;

        if (userFinal.nsdc_registration_number && userFinal.legal_entity_name && userFinal.contact_email_address && userFinal.sectors) {
            dispatch(userActions.register(userFinal));
        }
    }
    //Mutiselect Related Functions... 
    handleSelectChange(value) {
        console.log('You\'ve selected:', value);
        this.setState({ value });
    }
    toggleCheckbox(e) {
        this.setState({
            [e.target.name]: e.target.checked,
        });
    }
    toggleRtl(e) {
        let rtl = e.target.checked;
        this.setState({ rtl });
    }

    //End of multi-select related functions..

    render() {
        // debugger;
        const { handleSubmit} = this.props;
        const { applyWidthClass,registerClick } = this.state;
        const styles = {
            minHeight: applyWidthClass//'497px'
        };
        const {  disabled, stayOpen, value } = this.state;

        if (!!this.props.items) {
            var allSectors = this.props.items.data;
           
            //console.log("main array", allSectors);
            var final_allSectors = [];
            allSectors.map((value) => {
                return final_allSectors.push({ "label": value.sectorName, "value": value.sectorid });
            })
           //  console.log("fina array", final_allSectors);
        }
        return (

            <div className="content-wrapper user-onboarding landing-page TP-resgistration half-bg">
            <Header/>
                <div className="container">
                    <div className="row">
                        <div className="col-md-6 col-12">
                            <div className="left-block half-bg-custom-height" style={styles}>
                                
                                <div className="form-content-card">
                                    <div className="section-title">
                                        <h1 className="title">Register as a <b>Training Partner</b></h1>
                                        <span className="subtitle">Provides the trained candidates to the employers according to their requirements.</span>
                                    </div>

                                    <form id="frm-TP-registration" className="form frm-registarion" onSubmit={handleSubmit(this.handleSubmit)}>
                                        <div className="row">
                                            <Field name="nsdc_registration_number" type="text" component={renderField} label="NSDC/SSC Registration No" onChange={this.handleChange} />
                                            <Field name="training_partner_name" type="text" component={renderField} label="Training Partner Name" onChange={this.handleChange} />
                                        </div>

                                        <div className="row">
                                            <div className={'col-12 form-group' + (registerClick === 1 && value.length === 0 ? ' has-error' : '')}>
                                                <label className="lable">Job Sectors</label>
                                                <Select
                                                    closeOnSelect={!stayOpen}
                                                    disabled={disabled}
                                                    multi
                                                    onChange={this.handleSelectChange}
                                                    options={final_allSectors}
                                                    placeholder="Select your relavant sector(s)"
                                                    removeSelected={this.state.removeSelected}
                                                    rtl={this.state.rtl}
                                                    simpleValue
                                                    value={value}
                                                    component={renderField}
                                                />
                                                {registerClick === 1 && value.length === 0 &&
                                                    <div className="help-block">Please select atleast one sector</div>
                                                }
                                            </div>
                                        </div>
                                        <div className="row">
                                            <Field name="legal_entity_name" type="text" component={renderField} label="Legal Entity Name" onChange={this.handleChange} />
                                            <Field name="contact_person_name" type="text" component={renderField} label="Contact Person Name" onChange={this.handleChange} />
                                        </div>

                                        <div className="row">
                                            <Field name="contact_email_address" type="text" component={renderField} label="Contact Person Email ID" onChange={this.handleChange} />
                                            <Field name="contact_mobile" type="text" component={renderField} label="Contact Person Mobile" onChange={this.handleChange} />
                                        </div>

                                        <div className="form-group">
                                            <Label for="address">Address</Label>
                                            <Input type="textarea" name="address" id="exampleText" onChange={this.handleChange} />
                                        </div>
                                        <button className="btn btn-primary btn-raised btn-lg btn-block" onClick={this.checkValidation} title="Register">Register</button>

                                    </form>

                                    <div className="terms-block pmd-display-0 text-muted">
                                        <p className="mb-0">By registering, you agree to SkillConnect's</p>
                                        <p className="mb-0"><a href="/terms-and-conditions">Terms &amp; Conditions </a> &amp; <a href="/privacy-policy">Privacy Policy.</a></p>
                                    </div>

                                    <div className="or-divider text-center"><span>Or</span></div>

                                    <div className="row form-interlinks">
                                        <div className="col-lg-7 pr-0">
                                            <h3 className="mb-0">Already on <strong>SkillConnect</strong>?</h3>
                                            <span className="subtitle mb-lg-0">Login to your account</span>
                                        </div>
                                        <div className="col-lg-5 text-lg-right text-left">
                                            <Link to="/login" className="btn btn-outline-primary btn-lg" title="Login">Login</Link>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-6 col-12 right-block">
                            <div className="right-content-block theme-inverse half-bg-custom-height" style={styles}>
                                <div className="d-md-block d-none">
                                    <h2 className="pmd-display-4 mb-4">What you will get as registered Training Partner</h2>

                                    <ul className="checklist bottom-border checklist-inverse">
                                        <li>Only digital platform in India with NSDC approved Job roles in 40+ Sectors.</li>
                                        <li>100% verified employers and jobs.</li>
                                        <li>One stop platform for training partners to connect with employers.</li>
                                        <li>Supports placement connect with candidates.</li>
                                        <li>Credible data of present and future job demand based on job role and location.</li>
                                        <li>Chatbot based vernacular learning app for hard and soft skills.</li>
                                    </ul>
                                </div>                              
                            </div> <Footer/>
                        </div>
                    </div></div>
            </div>
        );
    }
}
TrainingPartnerPage = reduxForm({
    form: 'TrainingPartnerPage',
    // fields: ['nsdc_registration_number','legal_entity_name','contact_email_address'],
    validate,
    enableReinitialize: true,
})(TrainingPartnerPage);

function mapStateToProps(state) {
    const { items } = state.users; //From Reducer - users function ...
    return {
        items
    };
}

const connectedTrainingPartnerPage = connect(mapStateToProps)(TrainingPartnerPage);
export { connectedTrainingPartnerPage as TrainingPartnerPage };