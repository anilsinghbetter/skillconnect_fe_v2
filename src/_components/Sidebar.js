import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import {history} from '../_helpers/history';
import {
    Card, CardHeader, CardTitle, CardSubtitle,
    Nav,
    NavItem,
    NavLink,

} from 'reactstrap';

const trainingPartnerNavItems = [
    { to: '/TrainingPartner-dashboard', name: 'dashboard', exact: true, Icon: "../assets/images/ic_dashboard.svg",alt:"Dashboard" },
    // { to: '/candidates', name: 'candidates', exact: true, Icon: "../assets/images/ic_candidates.svg",alt:"Candidates" },
    { to: '/training-centers', name: 'Training Centers', exact: true, Icon: "../assets/images/ic_training_center.svg",alt:"Training Centers" },
    { to: '/TrainingPartner-hiring-requests/1', name: 'Hiring Requests', exact: true, Icon: "../assets/images/ic_hiring_requests.svg",alt:"Hiring Requests" },
    { to: '/Settings', name: 'Settings', exact: true, Icon: "../assets/images/ic_settings.svg",alt:"Settings" },
    { to: '/Contact', name: 'Contact', exact: true, Icon: "../assets/images/ic_contact.svg",alt:"Contact" },
    // { to: '/Bulk-Upload-Status', name: 'Bulk Upload Status', exact: true, Icon: "../assets/images/ic_bulk_upload.svg",alt:"Bilk Upload Status" },
    { to: '/login', name: 'Logout', exact: true, Icon: "../assets/images/ic_logout.svg",alt:"Logout" },
];

const trainingCenterNavItems = [
    // { to: '/TrainingCenter-dashboard', name: 'dashboard', exact: true, Icon: "../assets/images/ic_dashboard.svg",alt:"Dashboard" },
    { to: '/candidates', name: 'candidates', exact: true, Icon: "../assets/images/ic_candidates.svg",alt:"Candidates" },
    // { to: '/training-centers', name: 'Training Centers', exact: true, Icon: "../assets/images/ic_training_center.svg",alt:"Training Centers" },
    // { to: '/TrainingPartner-hiring-requests/1', name: 'Hiring Requests', exact: true, Icon: "../assets/images/ic_hiring_requests.svg",alt:"Hiring Requests" },
    { to: '/Settings', name: 'Settings', exact: true, Icon: "../assets/images/ic_settings.svg",alt:"Settings" },
    // { to: '/Contact', name: 'Contact', exact: true, Icon: "../assets/images/ic_contact.svg",alt:"Contact" },
    // { to: '/Bulk-Upload-Status', name: 'Bulk Upload Status', exact: true, Icon: "../assets/images/ic_bulk_upload.svg",alt:"Bilk Upload Status" },
    { to: '/login', name: 'Logout', exact: true, Icon: "../assets/images/ic_logout.svg",alt:"Logout" },
];


const employerNavItems = [
    // { to: '/search-candidate', name: 'Search Candidate', exact: true, Icon: "../assets/images/ic_search_candidates.svg",alt:"Searchcandidate" },
    { to: '/search-candidate', name: 'Immediate Hiring Request', exact: true, Icon: "../assets/images/ic_search_candidates.svg",alt:"Searchcandidate" },
    { to: '/Employer-dashboard', name: 'dashboard', exact: true, Icon: "../assets/images/ic_dashboard.svg",alt:"Dashboard" },
    { to: '/Employer-hiring-requests/1', name: 'Hiring Requests', exact: true, Icon: "../assets/images/ic_hiring_requests.svg",alt:"Hiring Requests" },
    { to: '/add-future-hiring-requests', name: 'Future Hiring Requests', exact: true, Icon: "../assets/images/ic_future_request.svg",alt:"Future Hiring Requests" },
    { to: '/Settings', name: 'Settings', exact: true, Icon: "../assets/images/ic_settings.svg",alt:"Settings" },
    { to: '/Contact', name: 'Contact', exact: true, Icon: "../assets/images/ic_contact.svg",alt:"Contact" },
    { to: '/Employer-Bulk-Upload-Status', name: 'Bulk-Upload-Status', exact: true, Icon: "../assets/images/ic_bulk_upload.svg",alt:"Bulk-Upload-Status" },
    { to: '/login', name: 'Logout', exact: true, Icon: "../assets/images/ic_logout.svg",alt:"Logout" },
];

class Sidebar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            // partnerType: '',
            //user_flow: true,//not using now, but it can be use later.
            //userType: false,
        };
    }
    
    render() {
        var navItems = '';
        var getActiveClass = "";
        var partnerType = "";
        if (localStorage.getItem('user') !== null) {
            if (this.props.user.user_type === 'EMP') {
                navItems = employerNavItems;
                partnerType = this.props.user.user_type;
            }else if(this.props.user.user_type === 'TC') {
                navItems = trainingCenterNavItems;
                partnerType = this.props.user.user_type;
            }else {
                navItems = trainingPartnerNavItems;
                partnerType = this.props.user.user_type;
            }
        } 
        else
        {
            history.push('/login');
            return ('');
        }
        //Get Name Initials..
        var name = this.props.user.data[0].contact_person_name;
        var initials = name.match(/\b\w/g) || [];
        initials = ((initials.shift() || '') + (initials.pop() || '')).toUpperCase();
        // console.log("Get name initials: ", initials);

        //console.log("Get name Active Class After toggel: ",this.props.filedata);

        
        if (this.props.filedata === true) getActiveClass = "sidebar active";
        else getActiveClass = "sidebar";


        return (<div><aside className={getActiveClass}>
            <Card className="sidebar-header">
                <CardHeader className="d-flex align-items-center flex-column">
                    <div className="circle-lg user-avatar user-color-blue avatar-border">{initials}</div>
                    {/* <div className="circle-lg user-avatar user-color-blue avatar-border">JS</div> */}
                    <CardTitle>{this.props.user.data[0].contact_person_name + ' (' + partnerType + ')'}</CardTitle>
                    <CardSubtitle>{this.props.user.data[0].contact_email_address}</CardSubtitle>
                    {/* {this.props.user.user_type !== 'TC' &&  */}
                        <Link to="/Settings" title="Edit Profile" className="btn btn-secondary btn-raised btn-sm ">Edit Profile</Link>
                    {/* } */}
                    
                </CardHeader>
            </Card>
            <Nav vertical className="list-unstyled">

                {navItems.map(({ to, name, exact, Icon, alt }, index) => (
                    window.location.pathname === to ?
                        <NavItem key={index} className='nav-item'>
                            <NavLink
                                id={`navItem-${name}-${index}`}
                                className='nav-link active'
                                tag={NavLink}
                                href={to}>
                                <img alt={alt} src={Icon} />
                                {name}
                                {/*  <span className="cr-sidebar__nav-item-icon">{name}</span> */}
                            </NavLink>
                        </NavItem>
                        :
                        <NavItem key={index} className='nav-item'>
                            <NavLink
                                id={`navItem-${name}-${index}`}
                                className='nav-link'
                                tag={NavLink}
                                href={to}>
                                <img alt={alt} src={Icon} />
                                {name}
                                {/*  <span className="cr-sidebar__nav-item-icon">{name}</span> */}
                            </NavLink>
                        </NavItem>

                ))}
            </Nav>
        </aside></div>

        );
    }
}
function mapStateToProps(state) {
    return {
    };
}
const connectedSidebar = connect(mapStateToProps)(Sidebar);
export { connectedSidebar as Sidebar };