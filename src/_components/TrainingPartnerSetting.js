import React from 'react';
import { connect } from 'react-redux';
import { userActions } from '../_actions';
import { reduxForm, Field } from 'redux-form';
import { Card, CardTitle, Row, Col,Breadcrumb, BreadcrumbItem, CardBody, CardHeader } from 'reactstrap';
import { constants } from '../constants';
import 'react-day-picker/lib/style.css';

const renderTextAreaField = ({ input, label, type, placeholder, textarea, meta: { touched, error, warning, invalid } }) => {

    const textareaType = <textarea {...input} placeholder={label} type={type} className={`form-control ${touched && invalid ? 'has-danger' : ''}`} />;

    return <div className=''>
        <div className={'form-group' + (error && touched ? ' has-error' : '')}>
            <label>{label}</label>
            <div> {textarea ? textareaType : ""}
                {touched && ((error && <div className="help-block">{error}</div>) || (warning && <span>{warning}</span>))}
            </div>

        </div>
    </div>
};

const renderField = ({ input, label, type, placeholder, textarea, meta: { touched, error, warning, invalid } }) => {
    const textareaType = <textarea {...input} placeholder={label} type={type} className={`form-control ${touched && invalid ? 'has-danger' : ''}`} />;
    return <div className={((label === "contact_person_name" || label === "contact_email_address" || label === "contact_mobile" || label === "address") ? 'col-md-4 col-12' : 'col-md-4 col-12')}>
        <div className={'form-group' + (error && touched ? ' has-error' : '')}>
            <label>{label}</label>
            <div> {textarea ? textareaType : <input {...input} type={type} placeholder={placeholder} className="form-control" />}
                {touched && ((error && <div className="help-block">{error}</div>) || (warning && <span>{warning}</span>))}
            </div>

        </div>
    </div>
};

// const renderError = ({ meta: { touched, error } }) =>
//     touched && error ? <div className="help-block">{error}</div> : false

const validate = values => {

    const errors = {}

    if (!values.contact_person_name) {
        errors.contact_person_name = 'Please enter contact person name'
    }
    if (!values.contact_email_address) {
        errors.contact_email_address = 'Please enter contact person email'
    }
    if (!values.address) {
        errors.address = 'Please enter address'
    }
    
    return errors;
}

class TrainingPartnerSetting extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            submitted: false,
            showState: false,
            registerClick: 0,

        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        //Date Related Pure Functions..
        this.checkValidation = this.checkValidation.bind(this);
    }
    componentDidMount() {
        //get Employer Details
        var partner_id = this.props.user.data[0].trainingpartnerid;
        //console.log(this.props,"pID");
        
        this.props.dispatch(userActions.getTrainingPartner(partner_id));
        //console.log(this.props,"PROPERTY");
        
        
    }

    handleSubmit(event) {
        // event.preventDefault();
        const { user } = this.state;
        // const { dispatch } = this.props;
        this.setState({ submitted: true });
        
        var trainingpartnerid = this.props.user.data[0].trainingpartnerid;
        var userFinal = { trainingpartnerid, ...user };
        
        //console.log("send data", userFinal);
        
        if (userFinal.trainingpartnerid) {
            this.props.dispatch(userActions.updateTrainingPartnerProfile(userFinal));    
        }
        // console.log(userFinal, "MY USER",);
        var localUserData = JSON.parse(localStorage.getItem('user'));
        
        for(let updatedvalues in userFinal){
            if(updatedvalues === 'trainingpartnerid'){
            }else{
                localUserData.data[0][updatedvalues] = userFinal[updatedvalues];
                // console.log(localUserData,"VAL here")
            }
        }
        localStorage.setItem('user', JSON.stringify(localUserData));

        setTimeout(() => {
            window.location.reload(0);
        }, constants.REFRESH_TIMEOUT);

    }
    checkValidation(event) {
        //console.log("Register clicked.");
        this.setState({ registerClick: 1 });
    }

    handleChange(event) {

        const { name, value } = event.target;
        const { user } = this.state;
        
        //console.log("this is state",this.state,"this is prop",this.props);
        // const { dispatch } = this.props;
        // this.setState({ value: event.target.value });
        this.setState({
            user: {
                ...user,
                [name]: value
            }
        });
        //console.log(user,"USER");

    }

    render() {
        const { handleSubmit } = this.props;
        // const { registerClick } = this.state;
        // console.log(this.props.trainingPartnerDetails,"PROPERTY");
        if(!!this.props.trainingPartnerDetails){
            var trainingPartnerDetails = this.props.trainingPartnerDetails.data[0];
        }
        
        return (
            <div className="content-wrapper user-flow TP-flow">
                <div className="d-block d-md-flex align-items-center section-header">
                    <div>
                        <h1 className="page-title">Settings</h1>
                        <Breadcrumb>
                            <BreadcrumbItem><a href="/TrainingPartner-dashboard">Dashboard</a></BreadcrumbItem>
                            <BreadcrumbItem active>Settings</BreadcrumbItem>
                        </Breadcrumb>
                    </div>
                </div>
               
                <form id="TrainingPartnerSetting" className="form frm-edit-candidaten" onSubmit={handleSubmit(this.handleSubmit)}>
                    <Card className="card z-depth-1">
                        <CardHeader className="bg-transparent border-bottom-0 card-header-shadow">
                            <div className="section-title">
                                <CardTitle className="title">Company Details</CardTitle>
                            </div>
                        </CardHeader>    

                        <CardBody className="details-view">
                            <Row>  
                                <Col xs="12" className="mb-3" xl="4">
                                    <label className="subtitle">Training Partner Name</label>
                                    <h5 className="title">{trainingPartnerDetails ? trainingPartnerDetails.training_partner_name : '-'}</h5>  
                                </Col>
                                <Col xs="12" className="mb-3" xl="4">
                                    <label className="subtitle"> Legal Entity Name</label>
                                    <p className="title mb-0">{trainingPartnerDetails ? trainingPartnerDetails.legal_entity_name : '-'}</p>  
                                </Col>
                                <Col xs="12" md="6" xl="4" className="mb-3 mb-xl-0">
                                    <label className="subtitle">Sector</label>
                                    <p className="title mb-0">{trainingPartnerDetails ? trainingPartnerDetails.sectorname : '-' } </p>
                                </Col>
                                <Col xs="12" md="6" xl="4" className="mb-3 mb-xl-0">
                                    <label className="subtitle">NSDC/SSC Registration No</label>
                                    <p className="title mb-0">{trainingPartnerDetails ? trainingPartnerDetails.nsdc_registration_number : '-' } </p>
                                </Col>
                                <Col xs="12" md="6" xl="4" className="mb-3 mb-xl-0">
                                    <label className="subtitle">State</label>
                                    <p className="title mb-0"> {trainingPartnerDetails ? (trainingPartnerDetails.stateName !== undefined &&  trainingPartnerDetails.stateName !== null ? trainingPartnerDetails.stateName : '-') : '-'} </p>
                                </Col>
                                <Col xs="12" md="6" xl="4" className="mb-3 mb-xl-0">
                                    <label className="subtitle">District</label>
                                    <p className="title mb-0">{trainingPartnerDetails ? (trainingPartnerDetails.cityName !== undefined &&  trainingPartnerDetails.cityName !== null ? trainingPartnerDetails.cityName : '-') : '-'}</p>
                                </Col>
                            </Row>
                        </CardBody>
                    </Card>

                    <Card className="card z-depth-1"> 
                        <CardHeader className="bg-transparent border-bottom-0 card-header-shadow">
                            <div className="section-title">
                                <CardTitle className="title">Contact Person Details</CardTitle>
                            </div>
                        </CardHeader>   
                        
                        <CardBody className="details-view">
                            <div className="row">
                                <Field name="contact_person_name" type="text" component={renderField} label="Contact Person Name" placeholder="Contact Person Name" onChange={this.handleChange} />
                                <Field name="contact_email_address" type="text" component={renderField} label="Email Address" placeholder="Email Address" onChange={this.handleChange} />
                                <Field name="contact_mobile" type="text" component={renderField} label="Mobile Number" placeholder="Mobile Number" onChange={this.handleChange} />
                            </div>
                            <div className="">
                                <Field name="address" component={renderTextAreaField} type="textarea" label="Address" textarea={true} onChange={this.handleChange} />
                            </div>

                            <div className="action-btn-block">
                                <button className="btn btn-primary btn-raised mr-3" onClick={this.checkValidation} title="Save">Save Changes</button>
                                <a title="Change Password" href="/Change-Password" className="btn btn-primary btn-raised mr-3" >Change Password</a>
                                <a title="Cancel" className="btn btn-outline " href="/TrainingPartner-dashboard">Cancel</a>
                            </div>
                        </CardBody>
                    </Card>
                </form>
            </div>
        );
    }
}
TrainingPartnerSetting = reduxForm({
    form: 'TrainingPartnerSetting',
    validate,
    enableReinitialize: true,
})(TrainingPartnerSetting);




function mapStateToProps(state, props) {
   
    const { user } = state.authentication;
    const { trainingPartnerDetails } = state.setting;
    var formated_object = {};

    // console.log("in MapTOState", state);

    if (!!trainingPartnerDetails) {
        var EditTrainingPartnerDetails = trainingPartnerDetails.data[0];

        formated_object = {
            address: EditTrainingPartnerDetails.address,
            stateName: EditTrainingPartnerDetails.stateName,
            cityName: EditTrainingPartnerDetails.cityName,
            pincode: EditTrainingPartnerDetails.pincode,
            contact_person_name: EditTrainingPartnerDetails.contact_person_name  ,
            contact_email_address: EditTrainingPartnerDetails.contact_email_address,
            contact_mobile: EditTrainingPartnerDetails.contact_mobile,
            legal_entity_name: EditTrainingPartnerDetails.legal_entity_name,
        }
       
    }
    return {
        user,
        initialValues: formated_object,
        trainingPartnerDetails
    };
}

const connectedTrainingPartnerSetting = connect(mapStateToProps)(TrainingPartnerSetting);
export { connectedTrainingPartnerSetting as TrainingPartnerSetting };