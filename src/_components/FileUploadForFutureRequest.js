import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { post } from 'axios';
import { userService } from '../_services';
import { constants } from '../constants';

class FileUploadForFutureRequest extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            filedata: null,
            fileresponse: "",
            loader: true,
            isSubmit: false,
            showErrorSuccess: false
        };
        this.handleFileUpload = this.handleFileUpload.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleSubmit(e) {
        e.preventDefault() // Stop form submit
        //console.log("which state", this.state.file);
        this.setState({ isSubmit: true,fileresponse : '' });
        this.fileUpload(this.state.file).then((response) => {
            this.setState({
                fileresponse: response.data,
                // loader: false,
                // isSubmit: false
            });
        });
    }
    fileUpload(file) {
        var response = userService.uploadFutureHiringCSVRequestData(file);
        return post(response.url, response.data, response.headers);
    }

    handleFileUpload = (e) => {
        this.setState({ file: e.target.files[0] });
    }
    render() {
        // const { registering } = this.props;
        const { isSubmit } = this.state;
        var showMessage = '';
        var classCustom = "d-none";
        var loaderClass = '';
        //Show loader..
        //if (isSubmit == true && loader == true) {
        if (isSubmit === true) {
            loaderClass = 'loading';
            this.turnOffRedTimeout = setTimeout(() => {
                this.setState(() => ({ isSubmit: false, showErrorSuccess: true }))
            }, 1000); //1 secs
        }
        const styles = {
            top: '65px',
            left: '270px'
        };
        /* const alertstyles = {
            zIndex: '1111',
            marginBottom: '15px',
            maxWidth: '540px',
            marginLeft: '0px'
        }; */

        /* if(loaderClass == 'loading'){
            
        } */

        if (this.state.showErrorSuccess) {
            this.timeoutformsg = setTimeout(() => {
                this.setState(() => ({ showErrorSuccess: false }))
            }, 12000); //12 secs     
        }
        if (!!this.state.fileresponse) {
            if (this.state.fileresponse.data !== undefined && this.state.fileresponse.status === true && this.state.fileresponse.data.successrecords > 0) {
                if (this.state.showErrorSuccess) {
                    /* showMessage = this.state.fileresponse.data.message;
                    classCustom = "pmd-display-0 mt-2 mb-2 customSuccess";

                    this.timeoutHandle = setTimeout(() => {
                        
                    }, 5000); */
                }
            }
            /* else if (this.state.fileresponse.data != undefined && this.state.fileresponse.status == true && this.state.fileresponse.data.errorrecords != undefined && this.state.fileresponse.data.errorrecords.error != undefined && this.state.fileresponse.data.errorrecords.error.length > 0) {
                showMessage = 'File not imported successfully.';
                classCustom = "pmd-display-0 mt-2 mb-2 customError"; */
            else if (this.state.fileresponse.error !== undefined && this.state.fileresponse.status === false) {
                if (this.state.showErrorSuccess) {
                    showMessage = this.state.fileresponse.error.message;
                    classCustom = "alert alert-info alert-danger";
                }
            }else if (this.state.showErrorSuccess) {
                showMessage = constants.future_hiring_request_after_upload_message;
                classCustom = "alert alert-info";
            }
        } else {
            if (this.state.showErrorSuccess) {
                showMessage = constants.future_hiring_request_after_upload_message;
                classCustom = "alert alert-info";
            }
        }

        return (<div>
            <div className={classCustom} role="alert">
                {showMessage}
            </div>
            <div className="fileupload">
                <form name="uploadFutureRequests" className="" id="uploadFutureRequests" onSubmit={this.handleSubmit}>


                    <div className="upload-btn-wrapper">
                        <button className="btn btn-secondary btn-lg btn-raised">
                            <i className="material-icons">arrow_upward</i>
                            Upload a file
                                </button>
                        <input type="file" onChange={this.handleFileUpload} />

                        <div className={loaderClass} style={styles}></div>
                    </div>

                    <p className="pmd-display-0 mt-2 mb-2">Allowed file type:<strong> CSV</strong></p>
                    <p className="pmd-display-0 mb-4">Max. File Size:<strong> {constants.future_hiring_request_csv_bulk_upload_mb}</strong></p>
                    {!!this.state.file &&
                        <p className="pmd-display-0 mb-4">Filename:<strong> {this.state.file.name} </strong></p>
                    }
                    <div className="action-btn">

                        <button title="Upload" className="btn-primary btn mr-2 btn-raised">Submit</button>
                        <Link to="/add-future-hiring-requests" title="Cancel" className="btn-outline btn">Cancel</Link>
                    </div>
                </form>
            </div>
        </div>
        );
    }
}
function mapStateToProps(state) {
    const { registering } = state.registration;
    return {
        registering
    };
}
const connectedFileUploadForFutureRequest = connect(mapStateToProps)(FileUploadForFutureRequest);
export { connectedFileUploadForFutureRequest as FileUploadForFutureRequest };