import React from 'react';
import { connect } from 'react-redux';
import { userActions } from '../_actions';
import { reduxForm, Field, FieldArray } from 'redux-form';
import { Header } from '../_components/Header';
import { Footer } from '../_components/Footer';
import { history } from '../_helpers';
import { Sidebar } from '../_components/Sidebar';
/*import { SelectBox } from '../_components/SelectBox';
 import Select from 'react-select';
import ReactHtmlParser from 'react-html-parser'; */
import { Multiselect } from 'react-widgets';
import 'react-widgets/dist/css/react-widgets.css'

import { FormGroup, Button, Breadcrumb, BreadcrumbItem, Row, Col, CardBody, Card, CardLink, Modal, ModalBody, ModalHeader} from 'reactstrap';
import 'react-day-picker/lib/style.css';


const renderMultiselect = ({ input, data, valueField, textField }) =>

    <Multiselect {...input}
        onBlur={() => input.onBlur()}
        value={input.value || []} // requires value to be an array
        data={data}
        valueField={valueField}
        textField={textField}
        allowCreate={true}
    />


const renderField = ({ input, label, type, placeholder, textarea, meta: { touched, error, warning, invalid } }) => {
    return <div className={((label === "Positions") ? 'col-xl-2 col-md-4 col-12' : 'col-md-6 col-12')}>
        <div className={'form-group' + (error && touched ? ' has-error' : '')}>
            <label>{label}</label>
            <div>
                <input {...input} type={type} placeholder={placeholder} className="form-control" />
                {touched && ((error && <div className="help-block">{error}</div>) || (warning && <span>{warning}</span>))}
            </div>
        </div>
    </div>
};

const validate = values => {
    const errors = {}
    if (!values.sectorname) {
        errors.sectorname = 'Please select a sector'
    }
    if (!values.members || !values.members.length) {
        errors.members = { _error: 'At least one member must be entered' }
    } else {
        const membersArrayErrors = []
        values.members.forEach((member, memberIndex) => {
            const memberErrors = {}
            if (!member || !member.stateid) {
                memberErrors.stateid = 'Please select a state'
                membersArrayErrors[memberIndex] = memberErrors
            }
            if (!member || !member.districtid) {
                memberErrors.districtid = 'Please select a district'
                membersArrayErrors[memberIndex] = memberErrors
            }
            if (!member || !member.jobroleid) {
                memberErrors.jobroleid = 'Please select a jobrole'
                membersArrayErrors[memberIndex] = memberErrors
            }
            /* if (!member || !member.gender) {
                memberErrors.gender = 'Please select gender'
                membersArrayErrors[memberIndex] = memberErrors
            } */
            if (!member || !member.number_of_positions) {
                memberErrors.number_of_positions = 'Please enter number of positions'
                membersArrayErrors[memberIndex] = memberErrors
            }
            return memberErrors
        })
        if (membersArrayErrors.length) {
            errors.members = membersArrayErrors
        }
    }
    return errors
}

class AddFutureHiringRequest extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            submitted: false,
            registerClick: 0,
            updatedstateval: "",
            updatedcities: [],
            disabled: false,
            stayOpen: false,
            value: [],
            rtl: false,
            selectedOptions: [],
            sector: "",
            yearlabel: "",
            jobroleval: undefined,
            getjobroles: 1,
            delCardValue: undefined,
            isOpen: false,
            oldsectorval: '',
            montherrors: []
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.checkValidation = this.checkValidation.bind(this);
        this.getNextMonth = this.getNextMonth.bind(this);
        this.toggleModalforDelete = this.toggleModalforDelete.bind(this);
        this.closeModalBox = this.closeModalBox.bind(this);
    }
    toggle() {
        this.setState({
            dropdownOpen: !this.state.dropdownOpen
        });
    }
    closeModalBox() {
        const { oldsectorval } = this.state;
        this.state.sector = oldsectorval;
        this.setState({ getjobroles: 0, isOpen: !this.state.isOpen });
    }
    toggleModalforDelete = () => {
        this.setState({ delCardValue: 1, isOpen: !this.state.isOpen });
    }
    componentWillMount() {
        if (localStorage.getItem('user') !== null) {
            this.props.dispatch(userActions.getCandidateSearchFormData(this.props.user.data[0].industrypartnerid, this.props.user.user_type));
        }
        else {
            history.push('/login');
            return ('');
        }
    }
    removeLocalStorageKeys() {
        var arrRemove = [];
        for (let h = 0; h < localStorage.length; h++) {
            if (localStorage.key(h).substring(0, 13) === 'districtlist-') {
                arrRemove.push(localStorage.key(h).substring(13));
            }
        }

        if (arrRemove.length > 0) {
            for (let l = 0; l < arrRemove.length; l++) {
                if (localStorage.getItem('districtlist-' + arrRemove[l]) !== null) {
                    localStorage.removeItem('districtlist-' + arrRemove[l]);
                }
            }
        }
    }
    jobrole() {
        /* this.state.getjobroles = 1;
        this.setState({ isOpen: !this.state.isOpen });
        const { sector } = this.state;
        this.props.dispatch(userActions.getJobRoles(sector)); */
        window.location.reload(true);
    }
    getNextMonth() {
        var multiYears = [];
        var currentDate = new Date();
        var currentYear = currentDate.getFullYear();
        // create year array
        for (var y = 0; y <= 1; y++) {
            var forwardYears = currentYear + y;
            multiYears.push(forwardYears);
        }
        this.state.yearlabel = multiYears.join('-');
        //var multiYears = [ "2015", "2016", "2017", "2018", "2019", "2020"];
        var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        //var htmlOptions = "";
        var htmlOptions = [];
        for (var m = 0; m < multiYears.length; m++) {
            var multiYear = multiYears[m];
            var startMonth = 1;

            // if it's the current year, only show from this month onwards
            if (multiYear === currentDate.getYear() + 1900) {
                startMonth = currentDate.getMonth() + 2;
            }
            //var j = 0;
            var uptoMonths = 12;
            if (multiYear === currentYear + 1) {
                uptoMonths = currentDate.getMonth() + 1;
            }
            for (let i = startMonth; i <= uptoMonths; i++) {
                //j++;
                // htmlOptions += '<option value="' + ("0" + i).slice(-2) + "-" + multiYear + '">' + monthNames[i - 1] + " " + multiYear + '</option>';
                //htmlOptions.push({"label":monthNames[i - 1] + " " + multiYear, "value":("0" + i).slice(-2) + "-" + multiYear});
                //new way
                htmlOptions.push(monthNames[i - 1] + "-" + multiYear);
            }
        }

        //console.log("HTMLCOMBO", monthNames[i - 1]);
        return htmlOptions;
    }
    addItem(fields, index) {
        fields.push({})
    }
    componentDidMount() {
        this.removeLocalStorageKeys();
    }
    handleSubmit(event) {
        //event.preventDefault();
        const { sector, yearlabel } = this.state;
        const { dispatch } = this.props;

        //REFS Concept
        const formData = {};
        for (const field in this.refs) {
            formData[field] = this.refs[field].value;
        }
        console.log('-->', formData);
        var arr = [];
        formData.memersArray.forEach((monthval, monthIndex) => {
            //console.log(`${monthIndex}:${monthval.month}`)
            if (monthval.month === undefined) {
                arr[monthIndex] = 'Please select month';
            }
        })
        this.state.montherrors = arr;


        // var node = this.refs.memersArray;
        // console.log('#REFSDATA', node.ref);
        //Enf of Refs...
        if (arr.length === 0) {

            this.setState({ submitted: true });

            var futurerequestid = '-1';
            var industrypartnerid = this.props.user.data[0].industrypartnerid;
            var sectorid = sector;
            var year = yearlabel;
            var members = formData.memersArray;
            var RequestInfoFinal = { futurerequestid, industrypartnerid, sectorid, year, members };

            // console.log('# Send Data Handle', RequestInfoFinal);

            dispatch(userActions.addUpdateFutureHiringRequest(RequestInfoFinal));
        }


    }
    checkValidation(event) {
        //console.log("Register clicked.");
        this.setState({ registerClick: 1 });
    }
    handleRedirectionToAddMultipleFutureRequest(e) {
        history.push('/add-multiple-future-request');
    }
    handleChange(event, index) {

        const { value } = event.target;
        //const { user } = this.state;
        // const { dispatch } = this.props;

        var addnewstate = event.target.name;
        addnewstate = addnewstate.split(".");

        if (addnewstate[0] !== '' && addnewstate[1] === 'stateid') {
            var updatedindex = addnewstate[0];
            updatedindex = updatedindex.charAt(8);
            //console.log('#chirag', updatedindex); 
            this.setState({ updatedstateval: updatedindex });
        }
        //console.log('#HEy', addnewstate[1]);
        if (addnewstate[1] === 'stateid') this.props.dispatch(userActions.getDistrict(event.target.value));
        /* if (addnewstate[1] === 'month') {
            var options = event.target.options;
            var selectedVals = [];
            for (var i = 0, l = options.length; i < l; i++) {
                if (options[i].selected) {
                    selectedVals.push(options[i].value);
                }
            }
            this.setState({
                user: {
                    ...user,
                    [name]: selectedVals
                }
            });
        } else {
            this.setState({
                user: {
                    ...user,
                    [name]: value
                }
            });
        } */
        // console.log("event data change", event.target.name, event.target.value);
        if (event.target.name === 'sectorname') {
            this.state.sector = event.target.value;

            if (this.state.jobroleval !== undefined && event.target.value !== '' && event.target.value !== undefined) {
                this.toggleModalforDelete();
            } else {
                this.props.dispatch(userActions.getJobRoles(value));

            }

        }
        if (addnewstate[1] === 'jobroleid') {
            console.log('# old sector', this.state.sector);
            this.setState({ jobroleval: event.target.value, getjobroles: 0, oldsectorval: this.state.sector });
        }
    }
    removeItem(fields, index) {
        //loader starts
        //console.log('# fields',fields);

        setTimeout(function () {
            var arr = [];
            var arrRemove = [];
            if (localStorage.getItem('districtlist-' + index) !== null) {
                localStorage.removeItem('districtlist-' + index);
            }
            for (var i = 0; i < localStorage.length; i++) {
                if (localStorage.key(i).substring(0, 13) === 'districtlist-') {
                    arrRemove.push(localStorage.key(i).substring(13));
                    arr.push(JSON.parse(localStorage.getItem(localStorage.key(i))));
                }
            }


            if (arrRemove.length > 0) {
                for (let i = 0; i < arrRemove.length; i++) {
                    if (localStorage.getItem('districtlist-' + arrRemove[i]) !== null) {
                        localStorage.removeItem('districtlist-' + arrRemove[i]);
                    }
                }
            }
            //console.log('# local lenght', arr);
            //console.log('# to be deleted',index); 
            //this.timeoutHandle = setTimeout(() => {
            var newlocalStore = arr;
            if (newlocalStore.length > 0) {
                for (let i = 0; i < newlocalStore.length; i++) {
                    console.log('# new local storage', newlocalStore[i]);
                    localStorage.setItem('districtlist-' + i, JSON.stringify(newlocalStore[i]));
                }
            }
            //console.log('# after timeout stateid', index);
            var final_allCitiesnew = [];
            if (localStorage.getItem('districtlist-' + index) !== null) {
                // Iterate over localStorage and insert the keys that meet the condition into arr

                for (let i = 0; i < localStorage.length; i++) {
                    if (localStorage.key(i).substring(0, 13) === 'districtlist-') {
                        //arr.push(localStorage.key(i).substring(13));
                        var mynewdata = JSON.parse(localStorage.getItem(localStorage.key(i)));
                        var newcities = [];
                        mynewdata.map((value, index) => {
                            return newcities.push(<option key={index} value={value.cityid}>{value.cityName}</option>);
                        })
                        final_allCitiesnew[localStorage.key(i).substring(13)] = newcities;
                        // console.log('# new cities',final_allCitiesnew);
                    }
                }
                this.setState({ updatedcities: final_allCitiesnew });
            }
        }.bind(this), 1000);
        fields.remove(index);
        //loader completed
    }


    render() {

        const { handleSubmit } = this.props;

        //const { disabled, stayOpen, value, registerClick, updatedcities } = this.state;
        //var { selectedOptions } = this.state;
        //console.log("##Uded coiti",updatedcities);

        //Confimation Modal Box
        if (this.state.delCardValue !== undefined) {
            var modaval = this.state.delCardValue;
            var RequestModal = (
                <Modal backdrop={false} centered isOpen={this.state.isOpen} className="alert-modal" >
                    <ModalHeader toggle={() => this.toggleModalforDelete(modaval)} className="d-block section-title text-center border-bottom-0 no-border">
                        Changing Sector will lost your saved data.Are you sure you want to proceed?
                    </ModalHeader>
                    <ModalBody>
                        <div className="d-flex justify-content-center">
                            <Button color="primary" className="btn-raised mr-3" onClick={() => this.jobrole()}>Yes</Button>
                            <Button ouline='true' color="default" onClick={this.closeModalBox}>No</Button>
                        </div>
                    </ModalBody>
                </Modal>
            );
        }
        //End of Confimation Modal Box
        if (!!this.props.districtlist) {
            var allCities = this.props.districtlist.data;
            var final_allCities = [];
            var allIndex = [];

            if (this.state.updatedstateval !== undefined) {

                allIndex[this.state.updatedstateval] = allCities;
                //add values in local storage..
                //console.log('# all index', allIndex);
                //localStorage.setItem('districtlist-' + this.state.updatedstateval, JSON.stringify(allIndex[this.state.updatedstateval]));
                localStorage.setItem('districtlist-' + this.state.updatedstateval, JSON.stringify(allIndex[this.state.updatedstateval]));
            }
            if (localStorage.getItem('districtlist-' + this.state.updatedstateval) !== null) {
                // Iterate over localStorage and insert the keys that meet the condition into arr
                //console.log('# stateid', this.state.updatedstateval);
                for (var i = 0; i < localStorage.length; i++) {
                    if (localStorage.key(i).substring(0, 13) === 'districtlist-') {
                        //arr.push(localStorage.key(i).substring(13));
                        var mynewdata = JSON.parse(localStorage.getItem(localStorage.key(i)));
                        var newcities = [];
                        mynewdata.map((value, index) => {
                            return newcities.push(<option key={index} value={value.cityid}>{value.cityName}</option>);
                        })
                        final_allCities[localStorage.key(i).substring(13)] = newcities;
                        //console.log('# new cities',newcities);
                    }
                }
            }

        }

        if (!!this.props.jobroles) {
            var allJobroles = this.props.jobroles.data;
            var final_allJobs = [];
            allJobroles.map((value, index) => {
                return final_allJobs.push(<option value={value.jobroleid} key={index}>{value.jobroleName}</option>);
            })
        }
        if (!!this.props.searchformdata) {
            //Sector list..
            var allsectors = this.props.searchformdata.data.sector[0];
            var final_sectors = [];
            allsectors.map((value, index) => {
                return final_sectors.push(<option value={value.sectorid} key={index}>{value.sectorname}</option>);
            })
            //State values..
            var allStates = this.props.searchformdata.data.state[0];
            var final_allStates = [];
            allStates.map((value, index) => {
                return final_allStates.push(<option key={index} value={value.stateid}>{value.stateName}</option>);
            })
            //Gender Type list..
            var allgendertype = this.props.searchformdata.data.gender[0];
            var final_genderlist = [];
            allgendertype.map((value, index) => {
                return final_genderlist.push(<option key={index} value={value.optionValue}>{value.optionKey}</option>);

            })
        }
        const renderColorSelector = ({ input, label, dynamic_values, meta: { touched, error } }) => (
            <div className={'form-group' + (error && touched ? ' has-error' : '')}>
                <label>{label}</label>
                <select {...input} className="form-control" value={this.state.sector}>
                    <option value="" disabled>Select</option>
                    {dynamic_values}
                </select>
                {touched && error && <div className="help-block">{error}</div>}
            </div>

        )
        const renderSelector = ({ input, label, dynamic_values, meta: { touched, error } }) => (

            <div className={'form-group' + (error && touched ? ' has-error' : '')}>
                <label>{label}</label>
                <select {...input} className="form-control">
                    <option value="">Select</option>
                    {dynamic_values}
                </select>
                {touched && error && <div className="help-block">{error}</div>}
            </div>

        )

        const renderMembers = ({ fields, dynamic_values, test, label, mon, options, meta: { touched, error } }) => (

            <div>
                {touched && error && <span>{error}</span>}
                {fields.map((member, index) =>
                    <div key={index}>
                        <hr className="mt-2" />
                        <Row className="align-items-center">
                            <Col>
                                <Row>
                                    <Col xs="12" md="4" xl="2">
                                        <div className={'form-group' + (mon[index] ? ' has-error' : '')}>
                                            <label>Month</label>
                                            <Field
                                                name={`${member}.month`}
                                                component={renderMultiselect}
                                                // data={['08-2018', '09-2018', '10-2018','11-2018','12-2018']}
                                                data={(test)}
                                            />
                                            <div className="help-block">{mon[index]}</div>
                                        </div>
                                    </Col>

                                    <Col xs="12" md="4" xl="2">
                                        <Field name={`${member}.stateid`} component={renderSelector} dynamic_values={final_allStates} onChange={this.handleChange} label="State" />
                                    </Col>

                                    <Col xs="12" md="4" xl="2">
                                        <Field name={`${member}.districtid`} component={renderSelector} dynamic_values={final_allCities !== undefined ? final_allCities[index] : final_allCities} onChange={this.handleChange} label="District" />
                                    </Col>

                                    <Col xs="12" md="4" xl="2">
                                        <Field name={`${member}.jobroleid`} component={renderSelector} label="Job Role" dynamic_values={final_allJobs} onChange={this.handleChange} />
                                    </Col>

                                    <Col xs="12" md="4" xl="2">
                                        <Field name={`${member}.gender`} component={renderSelector} label="Gender" dynamic_values={final_genderlist} onChange={this.handleChange} />
                                    </Col>

                                    <Field name={`${member}.number_of_positions`} type="text" component={renderField} label="Positions" placeholder="Positions" onChange={this.handleChange} />
                                </Row>
                            </Col>

                            <Col xs="12" md="auto">
                                <FormGroup className="d-none d-md-flex">
                                    <CardLink className="btn-fab btn-fab-primary btn btn-sm mr-2" onClick={() => this.addItem(fields, index)}>
                                        <i className="material-icons">add</i>
                                    </CardLink>
                                    {index > 0 &&
                                        <CardLink className="btn-fab btn-fab-danger btn btn-sm" onClick={() => this.removeItem(fields, index)}>
                                            <i className="material-icons">remove</i>
                                        </CardLink>
                                    }
                                </FormGroup>
                                <FormGroup className="d-flex d-md-none">
                                    <Button color="primary" className="btn-raised btn-icon mr-3" onClick={() => this.addItem(fields, index)}>
                                        <i className="material-icons">add</i>Add
                            </Button>
                                    {index > 0 &&
                                        <Button outline color="danger" className="btn-icon" onClick={() => this.removeItem(fields, index)}>
                                            <i className="material-icons">remove</i>Remove
                            </Button>
                                    }
                                </FormGroup>
                            </Col>
                        </Row>
                    </div>
                )}
            </div>

        )


        return (<div className="content-wrapper user-flow TP-flow">

            <div id="content" className="main-content">
                <Sidebar {...this.props} />
                <Header />

                <div className="page-content">
                    <div className="d-block d-md-flex align-items-center section-header">
                        <div>
                            <h1 className="page-title">Add New Future Hiring Request</h1>
                            <Breadcrumb>
                                <BreadcrumbItem><a href="/Employer-dashboard">Dashboard</a></BreadcrumbItem>
                                <BreadcrumbItem active>Add New Future Hiring Request</BreadcrumbItem>
                            </Breadcrumb>
                        </div>
                        <div className="ml-auto">
                            <Button outline color="primary" title="Bulk Upload" onClick={this.handleRedirectionToAddMultipleFutureRequest} className="sm-floating-btn" >
                                <i className="material-icons">arrow_upward</i>
                                <span>Bulk Upload</span>
                            </Button>
                        </div>
                    </div>
                    <Row>
                        <Col xs="12" xl="12">
                            <form id="frm-add-candidate" className="form frm-edit-candidaten" onSubmit={handleSubmit(this.handleSubmit)}>
                                <Card className="z-depth-1">
                                    <CardBody>
                                        <Row>
                                            <Col xs="12" md="4">
                                                <Field name="sectorname" component={renderColorSelector} dynamic_values={final_sectors} onChange={this.handleChange} label="Sector" />
                                            </Col>
                                        </Row>

                                        <FieldArray ref="memersArray" name="members" component={renderMembers} test={this.getNextMonth()} mon={this.state.montherrors} />

                                        <div className="mobile-btn action-btn-block">
                                            <Button color="primary" className="btn-raised">Submit</Button>
                                        </div>
                                    </CardBody>

                                </Card>
                            </form>
                        </Col>
                    </Row>
                    {RequestModal}
                    <Footer />
                </div>
            </div>
        </div>

        );
    }
}
AddFutureHiringRequest = reduxForm({
    form: 'AddFutureHiringRequest',
    validate,
    enableReinitialize: true,
    initialValues: {
        members: ['']
    }
})(AddFutureHiringRequest);

function mapStateToProps(state) {
    const { user } = state.authentication;
    const { otherdetails_hiring_request } = state.trainingpartners; //From Reducer - trainingpartners function ...
    const { jobroles } = state.jobdata; //From Reducer - JobROleReducer function ...
    const { districtlist } = state.generaldata; //From Reducer - CommonReducer function ...
    const { searchformdata } = state.searchform;
    return {
        user, otherdetails_hiring_request, districtlist, searchformdata, jobroles
    };

}
const connectedAddFutureHiringRequest = connect(mapStateToProps)(AddFutureHiringRequest);
export { connectedAddFutureHiringRequest as AddFutureHiringRequest };