import React from 'react';
import { connect } from 'react-redux';
import { history } from '../_helpers';
import { Header } from '../_components/Header';
import { Footer } from '../_components/Footer';
import { Sidebar } from '../_components/Sidebar';
import { TrainingPartnerSetting } from '../_components/TrainingPartnerSetting';
import { IndustryPartnerSetting } from '../_components/IndustryPartnerSetting';
import { TrainingCenterSetting } from '../_components/TrainingCenterSetting';

class Settings extends React.Component {
  constructor(props) {
  super(props);

  this.state = {
    activeTab: '1',
  };
}





render() {
    
const { user } = this.props;
var loaderClass = '';
    if (user === undefined) {
        // console.log("Redierect to specific Dashboard page");
        history.push('/login');
        return ('');
    }
    //Show loader..
    if (this.props.loading === true) {
        loaderClass = 'loading';
    }
    const styles = {
        top: '115px',
        left: '60px'
    };
    //console.log("PROPS",this.props);

   

        
            return (
                <div className="content-wrapper user-flow TP-flow">
                  <Sidebar {...this.props} />
                  
                  <div id="content" className="main-content">
                    <div className="inner-page">
                      <Header />
          
                      
                      <div className={loaderClass} style={styles}></div>
                      { this.props.user.user_type === 'TP' && 
                      <TrainingPartnerSetting /> }

                      { this.props.user.user_type === 'EMP' && 
                      <IndustryPartnerSetting /> }

                      { this.props.user.user_type === 'TC' && 
                      <TrainingCenterSetting /> }
                        
                            
                            {/* {employerdetails}
                            {trainingpartnerdetails} */}

                      <Footer />
                    </div>
                  </div>
          
                </div>
              );
  }
}


function mapStateToProps(state) {
    //console.log(state,'STATE');
    const { users, authentication } = state;
    const { user } = authentication;
    return {
        user,
        users,
    };
}

const connectedSettings = connect(mapStateToProps)(Settings);
export { connectedSettings as Settings };