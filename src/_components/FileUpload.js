import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { post } from 'axios';
import { reduxForm, Field, formValueSelector } from 'redux-form';
import { FormGroup } from 'reactstrap';
import { userService } from '../_services';
import { constants } from '../constants';
import { submitFailure } from '../../src/utils/submitFailure';
import { userActions } from '../_actions';



const renderError = ({ meta: { touched, error } }) => touched && error ? <div className="help-block">{error}</div> : false


const renderField = ({ input, label, type, placeholder, textarea, meta: { touched, error, warning, invalid } }) => {
    const textareaType = <textarea {...input} placeholder={label} type={type} className={`form-control ${touched && invalid ? 'has-danger' : ''}`} />;
    return <div className={'col-md-12 col-12'}>
        <div className={'form-group' + (error && touched ? ' has-error' : '')}>
            <label>{label}</label>
            <div> {textarea ? textareaType : <input {...input} type={type} placeholder={placeholder} className="form-control" />}
                {touched && ((error && <div className="help-block">{error}</div>) || (warning && <span>{warning}</span>))}
            </div>

        </div>
    </div>
};

const validate = values => {

    const errors = {}

    if (!values.skill_type) {
        errors.skill_type = 'Please select skill type'
    }
    if (!values.sectorname) {
        errors.sectorname = 'Please select sector'
    }
    if (!!values.sectorname && !values.jobrole) {
        errors.jobrole = 'Please select job role'
    }
    // if (values.skill_type === 'skilled') {
    //     if (!values.state_scheme_name) {
    //         errors.state_scheme_name = "Please select state scheme";
    //     }
    // }
    
    // if (!values.sectorname) {
    //     errors.sectorname = "Please select sector name";
    // }
    // if (!values.jobrole) {
    //     errors.jobrole = "Please select job role";
    // }
    return errors;
}



class FileUpload extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            filedata: null,
            fileresponse: "",
            loader: true,
            isSubmit: false,
            showErrorSuccess: false,
            noFile:false
        };
        this.handleFileUpload = this.handleFileUpload.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleRadioChange = this.handleRadioChange.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }
    handleSubmit() {
        //e.preventDefault() // Stop form submit

        let other_data = {
            skill_type: this.props.skill_type,
            sector_id: this.props.sectorname,
            jobrole_id: this.props.jobrole,
        };
        
        // console.log(this.state.file, "FILE");
        // console.log(other_data, "OTHER DATA");
        if(this.state.file === undefined){
            this.setState({
                noFile: true
            });
            return;
        }
        this.setState({ isSubmit: true,fileresponse : '' });
        this.fileUpload(this.state.file, other_data).then((response) => {
            
            this.setState({
                fileresponse: response.data,
                noFile: false
                // loader: false,
                // isSubmit: false
            });
        });
    }
    fileUpload(file, other_data) {
        var response = userService.uploadCandidateCSVRequestData(file, other_data);
        //Remove key from local storage
        var uniqueKey = 'getCandidates';
        userService.removeLocalStorageCache(uniqueKey);
        return post(response.url, response.data, response.headers);
    }

    handleFileUpload = (e) => {
        this.setState({ file: e.target.files[0] });
    }


    handleRadioChange(event) {
        const { name, value } = event.target;
        let other_value = (value === 'skilled') ? 'unskilled' : 'skilled';
        this.props.handle_skill_type(value);
        this.props.change("skill_type", value);
        this.props.change("sectorname", '');
        this.props.change("jobrole", '');
    }

    componentDidMount() {
        this.props.dispatch(userActions.getCandidateFormData(this.props.user.data[0].trainingpartnerid));
    }

    handleChange(event) {
        const { name, value } = event.target;

        
        //if (event.target.name === 'jobrole') { this.setState({ error_jobrole: 0 }) }

        if (name === 'sectorname') {
            if (name !== 'jobrole') {
                //this.setState({ error_jobrole: 1 });
                this.props.dispatch(userActions.getJobRoles(value));
            }
        }
    }


    render() {
        // const { registering } = this.props;
        const { isSubmit } = this.state;
        const { handleSubmit } = this.props;

        var showMessage = '';
        var classCustom = "d-none";
        var loaderClass = '';
        //Show loader..
        //if (isSubmit == true && loader == true) {
        if (isSubmit === true) {
            loaderClass = 'loading';
            this.turnOffRedTimeout = setTimeout(() => {
                this.setState(() => ({ isSubmit: false, showErrorSuccess: true }))
            }, 1000); //1 secs
        }
        const styles = {
            top: '65px',
            left: '270px'
        };
        /* const alertstyles = {
            zIndex: '1111',
            marginBottom: '15px',
            maxWidth: '540px',
            marginLeft: '0px'
        }; */

        /* if(loaderClass == 'loading'){
            
        } */

        if (this.state.showErrorSuccess) {
            this.timeoutformsg = setTimeout(() => {
                this.setState(() => ({ showErrorSuccess: false }))
            }, 12000); //12 secs     
        }
        //console.log('# file respomse',this.state.fileresponse);
        if (!!this.state.fileresponse) {

                        
            if (this.state.fileresponse.data !== undefined && this.state.fileresponse.status === true && this.state.fileresponse.data.successrecords > 0) {
                if (this.state.showErrorSuccess) {
                    showMessage = this.state.fileresponse.data.message;
                    classCustom = "pmd-display-0 mt-2 mb-2 customSuccess";

                    this.timeoutHandle = setTimeout(() => {
                        window.location.href = '/candidates';
                    }, 5000);
                }
            }
            /* else if (this.state.fileresponse.data != undefined && this.state.fileresponse.status == true && this.state.fileresponse.data.errorrecords != undefined && this.state.fileresponse.data.errorrecords.error != undefined && this.state.fileresponse.data.errorrecords.error.length > 0) {
                showMessage = 'File not imported successfully.';
                classCustom = "pmd-display-0 mt-2 mb-2 customError"; */
            else if (this.state.fileresponse.error !== undefined && this.state.fileresponse.status === false) {
                if (this.state.showErrorSuccess) {
                    showMessage = this.state.fileresponse.error.message;
                    classCustom = "alert alert-info alert-danger";
                }
            }
            else if (this.state.showErrorSuccess) {
                showMessage = 'Candidates bulk upload process is in progress. You will be notified via email on your provided email address once upload process is completed.';
                classCustom = "alert alert-info";
            }
        } else {
            if (this.state.showErrorSuccess) {
                showMessage = 'Candidates bulk upload process is in progress. You will be notified via email on your provided email address once upload process is completed.';
                classCustom = "alert alert-info";
            }
        }


        //Sector list..
        if (!!this.props.candidateformdata)
        {
            var allsectors = this.props.candidateformdata.data.sector[0];
            var final_sectors = [];
            allsectors.map((value, index) => {
                return final_sectors.push(<option key={index} value={value.sectorid}>{value.sectorname}</option>);

            });
        }
        if (!!this.props.jobroles) {
            var allJobroles = this.props.jobroles.data;
            var final_allJobs = [];
            allJobroles.map((value, index) => {
                return final_allJobs.push(<option key={index} value={value.jobroleid}>{value.jobroleName}</option>);
            })
        }


        const customSelectBox = ({ input, label, sameValueState, sameValueStatePermanent, disabled, error_state, dynamic_values, meta: { touched, error } }) => (
            <React.Fragment>
                <div className={'form-group' + ((error && touched) || (error_state === 1) ? ' has-error' : '')}>
                    <label>{label}</label>
                    <select {...input} disabled={disabled} className="form-control">
                        <option value="">Select</option>
                        {dynamic_values}
                    </select>
                    {(touched && error) && <div className="help-block">{error}</div>}
                </div>
            </React.Fragment>

        )



        return (
            <div className="row">
                <div className="col-12 col-xl-7">
                    <div className="z-depth-1 card">
                        <div className="card-body">
                            <div className={classCustom} role="alert">
                                {showMessage}
                            </div>
                            <div className="fileupload">
                                <form name="uploadCandidates" className="" id="uploadCandidates" onSubmit={handleSubmit(this.handleSubmit)}>

                                    <div className="">
                                        <div className={'form-group' + (!this.props.skill_type ? ' has-error' : '')}>
                                            <label htmlFor="skill_type" className="d-block">Skill Type</label>
                                            <Field name="skill_type" component="input" type="radio" value="skilled" className="ml-0" onChange={this.handleRadioChange} /> Skilled
                                            <Field name="skill_type" component="input" type="radio" value="unskilled" onChange={this.handleRadioChange} /> Un-Skilled
                                            <Field name="skill_type" component={renderError} />
                                        </div>
                                    </div>
                                    {!!this.props.skill_type && this.props.skill_type === 'skilled' && 
                                        <div className="row">
                                            <div className="col-12 col-md-6">
                                                <Field name="sectorname" component={customSelectBox} dynamic_values={final_sectors} onChange={this.handleChange} label="Sector" />
                                            </div>
                                            <div className="col-12 col-md-6">
                                                <Field name="jobrole" component={customSelectBox} dynamic_values={final_allJobs} onChange={this.handleChange} label="Job Role" />
                                            </div>
                                        </div>
                                    }


                                    <div className="upload-btn-wrapper">
                                        <button className="btn btn-secondary btn-lg btn-raised">
                                            <i className="material-icons">arrow_upward</i>
                                            Upload a file
                                        </button>
                                        <input type="file" onChange={this.handleFileUpload} />

                                        <div className={loaderClass} style={styles}></div>
                                    </div>
                                    {this.state.noFile &&
                                        <div className="help-block text-danger">Please upload file</div>
                                    }

                                    {/* <p className={classCustom}>{showMessage}</p> */}


                                    <p className="pmd-display-0 mt-2 mb-2">Allowed file type:<strong> CSV</strong></p>
                                    <p className="pmd-display-0 mb-4">Max. File Size:<strong> {constants.candidate_csv_bulk_upload_mb}</strong></p>
                                    {!!this.state.file &&
                                        <p className="pmd-display-0 mb-4">Filename:<strong> {this.state.file.name} </strong></p>
                                    }
                                    <div className="action-btn">

                                        <button title="Upload" className="btn-primary btn mr-2 btn-raised">Submit</button>
                                        <Link to="/candidates" title="Cancel" className="btn-outline btn">Cancel</Link>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
        );
    }
}


const fieldList = [
    'skill_type',
    'sectorname',
    'jobrole',
];


const selector = formValueSelector('FileUpload') // <-- same as form name to retrieve form field values

FileUpload = reduxForm(
    {
        form: 'FileUpload',
        validate,
        enableReinitialize: true,
        onSubmitFail: submitFailure(fieldList)
    })(FileUpload);


function mapStateToProps(state) {
    const { user } = state.authentication;
    const { registering } = state.registration;
    const { candidateformdata } = state.trainingpartners; //From Reducer - trainingpartners function ...
    const { jobroles } = state.jobdata; //From Reducer - JobROleReducer function ...

    const { skill_type, sectorname, jobrole } = selector(state, 'sectorname', 'jobrole', 'skill_type');   //retrieve form field values

    return {
        user,
        registering,
        candidateformdata,
        jobroles,
        skill_type,
        sectorname,
        jobrole
    };
}
const connectedFileUpload = connect(mapStateToProps)(FileUpload);
export { connectedFileUpload as FileUpload };