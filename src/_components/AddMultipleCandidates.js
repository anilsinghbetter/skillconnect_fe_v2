import React from 'react';
import { connect } from 'react-redux';
import { history } from '../_helpers';
import { Header } from '../_components/Header';
import { Footer } from '../_components/Footer';
import { Sidebar } from '../_components/Sidebar';
import { FileUpload } from '../_components/FileUpload';
import { userActions } from '../_actions';

import { Breadcrumb, BreadcrumbItem, Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
//import {RadioGroup} from '../_components/RadioGroup';
import 'react-day-picker/lib/style.css';
import { config } from '../config';

class AddMultipleCandidate extends React.Component {
    constructor(props) {
        super(props);
        this.state= {
            skill_type: '',
            dropdownOpen: false
        }
        this.handleSkillType = this.handleSkillType.bind(this);
        this.toggle = this.toggle.bind(this);
    }

    toggle() {
        this.setState(prevState => ({
          dropdownOpen: !prevState.dropdownOpen
        }));
    }

    componentDidMount() {
        var {user} = this.props;
        if(user !== undefined && user.user_type === 'TC' ){
            this.props.dispatch(userActions.getSectorJobroleMapping(user.data[0].trainingpartnerid,'TP'));
        }else{
            // console.log(this.props.user,"MY USER");
        }
        
        // this.setState({ candidateIds: checkboxData });
    }
    
    handleSkillType(skill){
        this.setState({
            skill_type: skill
        });
    }


    render() {
        const { user } = this.props;
        //console.log('config file', config)
        const trainingpartnerid = this.props.user.data[0].trainingpartnerid;
        const URL_skilled = config.API_HOST_URL + 'skilled_candidate_sample_csv';
        const URL_Unskilled = config.API_HOST_URL + 'unskilled_candidate_sample_csv';
        const URLforSectorJobrole = config.API_HOST_URL + 'sectorJobrole/' + trainingpartnerid;
        const URLforStateCity =  config.API_HOST_URL + 'stateCity_sample_csv';
        //const { isAddCandidateClick } = this.state;
        // console.log(this.props,"PROPS");
        // console.log(this.state,"STATE");
        if (user === undefined) {
            //console.log("Redierect to specific Dashboard page");
            history.push('/login');
            return ('');
        }
        return (
            <div className="content-wrapper user-flow TP-flow">
                <Sidebar {...this.props} />

                <div id="content" className="main-content">
                    <Header />
                    <div className="page-content">

                        {/* <div className="d-block d-md-flex align-items-center section-header">
                            <div>
                                <h1 className="page-title">Add Multiple Candidates</h1>
                                <Breadcrumb>
                                    <BreadcrumbItem><a href="/candidates">Candidates</a></BreadcrumbItem>
                                    <BreadcrumbItem active>Add Multiple Candidates</BreadcrumbItem>
                                </Breadcrumb>
                            </div>
                        </div> */}

                        <div className="row">
                            <div className="col-12 col-xl-7">
                                <div className="d-block d-md-flex align-items-center section-header">
                                    <div>
                                        <h1 className="page-title">Add Multiple Candidates</h1>
                                        <Breadcrumb>
                                            <BreadcrumbItem><a href="/candidates">Candidates</a></BreadcrumbItem>
                                            <BreadcrumbItem active>Add Multiple Candidates</BreadcrumbItem>
                                        </Breadcrumb>
                                    </div>
                                    <div className="ml-auto">
                                        <Dropdown isOpen={this.state.dropdownOpen} toggle={this.toggle}>
                                            {/* <button onClick={this.toggle} role="button" className="dropdown-toggle btn-outline-primary btn btn-icon sm-floating-btn no-caret" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="Download Sample">
                                                <i className="material-icons">save_alt</i>
                                                <span>Download Sample</span>
                                            </button>  */}
                                            <DropdownToggle color="primary" outline className="btn-icon sm-floating-btn no-caret">
                                                <i className="material-icons">save_alt</i>
                                                <span>Download Sample</span>
                                            </DropdownToggle>                                        
                                            <DropdownMenu className="z-depth-2 border-0">
                                                <DropdownItem header className="ff-roboto text-muted-4 d-md-none ">Download Sample</DropdownItem>

                                                <DropdownItem className="d-md-none" divider />

                                                    {(this.state.skill_type === '' || this.state.skill_type === 'skilled') &&
                                                <DropdownItem href={URL_skilled} title="Download Skilled sample CSV file">
                                                        <div>
                                                            <i className="material-icons">arrow_downward</i>
                                                            <span>Download Skilled sample file</span>
                                                        </div>
                                                </DropdownItem>
                                                    }

                                                <DropdownItem className="d-md-none" divider />

                                                    {(this.state.skill_type === '' || this.state.skill_type === 'unskilled') &&
                                                <DropdownItem href={URL_Unskilled} title="Download Skilled sample CSV file">
                                                        <div>
                                                            <i className="material-icons">arrow_downward</i>
                                                            <span>Download Unskilled sample file</span>
                                                        </div>
                                                </DropdownItem>
                                                    }

                                                <DropdownItem className="d-md-none" divider />

                                                <DropdownItem href={URLforStateCity} title="Download Skilled sample CSV file">
                                                    <div>
                                                            <i className="material-icons">arrow_downward</i>
                                                            <span>State-City Sample</span>
                                                    </div>
                                                </DropdownItem>
                                            </DropdownMenu>
                                        </Dropdown>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="auto-center-block bulk-upload">
                            <FileUpload handle_skill_type={this.handleSkillType}/>
                            <hr className="sm-divider section-seperator" />
                        </div>

                        {/* <div className="auto-center-block bulk-upload">

                            <div className="">
                                <FileUpload handle_skill_type={this.handleSkillType}/>
                                <hr className="sm-divider section-seperator" />
                                {(this.state.skill_type === '' || this.state.skill_type === 'skilled') &&
                                    <div className="mb-3">
                                        <a href={URL_skilled} title="Download Skilled sample CSV file" className="btn-outline btn mb-3">
                                            <i className="material-icons">arrow_downward</i>
                                            <span>Download Skilled sample file</span>
                                        </a>
                                    </div>
                                }
                                {(this.state.skill_type === '' || this.state.skill_type === 'unskilled') &&
                                    <div className="mb-3">
                                        <a href={URL_Unskilled} title="Download Unskilled sample CSV file" className="btn-outline btn mb-3">
                                            <i className="material-icons">arrow_downward</i>
                                            <span>Download Unskilled sample file</span>
                                        </a>
                                    </div>
                                }
                                <div className="mb-3">
                                    <a href={URLforSectorJobrole} title="Download sample CSV file" className="btn-outline btn mb-3">
                                        <i className="material-icons">arrow_downward</i>
                                        <span>Sector-Jobrole Sample</span>
                                    </a>
                                </div>
                                <div className="mb-3">
                                    <a href={URLforStateCity} title="Download sample CSV file" className="btn-outline btn mb-3">
                                        <i className="material-icons">arrow_downward</i>
                                        <span>State-City Sample</span>
                                    </a>
                                </div>
                            </div>

                        </div> */}
                    </div>

                    <Footer />
                </div>

            </div>
        );
    }
}


function mapStateToProps(state) {
    const { users, authentication } = state;
    const { user } = authentication;
    return {
        user,
        users
    };
}
const connectedAddMultipleCandidate = connect(mapStateToProps)(AddMultipleCandidate);
export { connectedAddMultipleCandidate as AddMultipleCandidate };