import React from 'react';
import { connect } from 'react-redux';
import { reduxForm, Field } from 'redux-form';
import { Button, FormGroup, Input, Row, Col, Card, CardBody, Label } from 'reactstrap';
import { userActions } from '../_actions';

const validate = values => {
    const errors = {}
    return errors;
}

const renderColorSelector = ({ input, label, dynamic_values, meta: { touched, error } }) => (
    <div key='1' className='form-group'>
        <label>{label}</label>
        <select  {...input} className="form-control">
            <option value="">Select</option>
            {dynamic_values}
            ))}
     </select>
    </div>
)

class EmployerCandidateSearch_old extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            submitted: false,
            // isData:0,
            collapse: true,
            updatedValue: this.props.mytest,
            selectedBatchStartDate: undefined,
            selectedBatchEndDate: undefined,           
            //Initial State..
            // data: this.props.items
        };
        this.handleSearchFilter = this.handleSearchFilter.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleBatchStartDateClick = this.handleBatchStartDateClick.bind(this);
        this.handleBatchEndDateClick = this.handleBatchEndDateClick.bind(this);
        this.handleClear = this.handleClear.bind(this);
    }
    handleClick() {
        window.location.reload(true);
    }
    handleChange(event) {
        const { name, value } = event.target;

        const { user } = this.state;
        //Remove/Blank the jobrole id and district name If Exists...         
        if(!!this.state.user)
        {   //console.log("SECID",this.state.user.sectorid);         
            if(this.state.user.sectorid > 0)
            {
              //console.log('#A');
               this.state.user.jobroleid = "";   
              //console.log('#AA',this.state.user.jobroleid);
            }
            if(this.state.user.statename > 0)
            {
              //console.log('#A');
               this.state.user.districtname = "";   
              //console.log('#AA',this.state.user.districtname);
            }
        }
        //End code for Removeing the jobroel id If Exists...   
        this.setState({
            user: {
                ...user,
                [name]: value
            }
        });
        //console.log("event data change", event.target.name, event.target.value);
        if (event.target.name === 'sectorid') { this.props.dispatch(userActions.getJobRoles(value)); }
        if (event.target.name === 'statename') { this.props.dispatch(userActions.getDistrict(value)); }
    }

    componentWillMount() {
        // const { user } = this.props;
        // console.log("Traingin partner id", this.props.user.data[0].industrypartnerid);
        this.props.dispatch(userActions.getCandidateSearchFormData(this.props.user.data[0].industrypartnerid, this.props.user.user_type));
    }

    handleSearchFilter() {
        this.setState({ collapse: !this.state.collapse });
    }
    handleSubmit(e) {
        e.preventDefault();
        // const { user } = this.state;
        // const { dispatch } = this.props;
        this.setState({ submitted: true });
        //console.log("Start Date",typeof this.state.selectedBatchStartDate);

        // console.log("all keywords", userFinal);


        // console.log("search form values", this.state.user);
        var userInputkeywords = this.state.user;

        //  var searchString = userInputkeywords.candidate_name.trim().toLowerCase();

        if (userInputkeywords !== undefined) {

            // if (this.state.selectedBatchStartDate != "" && this.state.selectedBatchStartDate != undefined) {
            //     let batch_start_date = this.state.selectedBatchStartDate;
            //     //   console.log("HEREEEEE");
            //     var userInputkeywords = { batch_start_date, ...userInputkeywords };
            // }
            // if (this.state.selectedBatchEndDate != "" && this.state.selectedBatchEndDate != undefined) {
            //     let batch_end_date = this.state.selectedBatchEndDate;
            //     var userInputkeywords = { batch_end_date, ...userInputkeywords };
            // }

            let has_joined = 0;
            userInputkeywords = {has_joined, ...userInputkeywords };

            //Employer Dashboard Search will do default sector search whenever he has not selected any particular sector search [code start]
            // let defaultSectors = 0;
            // if(this.props.user.data[0].sector !== undefined){
            //     defaultSectors = this.props.user.data[0].sector;
            // }
            // if(userInputkeywords.sectorid !== undefined && userInputkeywords.sectorid > 0){
            //     defaultSectors = 0;
            // }
            // if(defaultSectors !== 0){
            //     userInputkeywords = { defaultSectors, ...userInputkeywords };
            // }
            //Employer Dashboard Search will do default sector search whenever he has not selected any particular sector search [code over]
            // console.log("User Search Keywords : ", userInputkeywords);

            //Search with API
            this.props.dispatch(userActions.getSearchCandidate(userInputkeywords, 1, 0, 0));

            //Passing Search Data to Parent Component -  GetCandidate..
           this.props.onSelectedChange(this.state.user);
        }

    }
    handleClear(e) {
        window.location.reload(true);
    }
    handleBatchStartDateClick(day) {
        this.setState({ selectedBatchStartDate: day });
    }
    handleBatchEndDateClick(day) {
        this.setState({ selectedBatchEndDate: day });
    }

    render() {
        // let posts = decodeURIComponent(this.props.mySearchedData).replace(/\?/g, ' ');
        // console.log("values",posts);
        //        const {posts} = "Architpatel";        

        
        
        if (!!this.props.districtlist) {
            var allCities = this.props.districtlist.data;
            var final_allCities = [];
            //State values..
            allCities.map((value, index) => {
                return final_allCities.push(<option key={index} value={value.cityid}>{value.cityName}</option>);
            })
        }
        if (!!this.props.jobroles) {
            var allJobroles = this.props.jobroles.data;
            var final_allJobs = [];
            allJobroles.map((value, index) => {
                return final_allJobs.push(<option value={value.jobroleid} key={index}>{value.jobroleName}</option>);
            })
        }
        if (!!this.props.searchformdata && this.props.searchformdata.status === true) {
            //Sector list..
            var allsectors = this.props.searchformdata.data.sector[0];
            var final_sectors = [];
            allsectors.map((value, index) => {
                return final_sectors.push(<option value={value.sectorid} key={index}>{value.sectorname}</option>);
            })
            //State values..
            var allStates = this.props.searchformdata.data.state[0];
            var final_allStates = [];
            allStates.map((value, index) => {
                return final_allStates.push(<option key={index} value={value.stateid}>{value.stateName}</option>);
            })

            //Training Center values..
            var final_allTrainingCenters = [];
            if (this.props.searchformdata.data.training_centre !== undefined && this.props.searchformdata.data.training_centre.length > 0) {
                var allTrainingCenters = this.props.searchformdata.data.training_centre[0];
                allTrainingCenters.map((value, index) => {
                    return final_allTrainingCenters.push(<option key={index} value={value.trainingcenterid}>{value.trainingcenter_name}</option>);
                })
            }

            //Training Status list..
            var alltrainingstatus = this.props.searchformdata.data.training_status[0];
            var final_trainingstatus = [];
            alltrainingstatus.map((value, index) => {
                if (value.optionValue === 'completed' || value.optionValue === 'ongoing') {
                    return final_trainingstatus.push(<option value={value.optionValue} key={index} >{value.optionKey}</option>);
                }
            })
            //PlacementStatus list..
            var allplacementlist = this.props.searchformdata.data.placement_status[0];
            var final_placementstatus = [];
            allplacementlist.map((value, index) => {
                return final_placementstatus.push(<option key={index} value={value.optionValue}>{value.optionKey}</option>);

            })

        }

        return (
            <div className="page-content">
                <Card className="z-depth-1 no-hover search-outer theme-inverse">
                    <CardBody>
                        <h5 className="title fw-regular">
                            Explore the pool of
                  <span className="h1 fw-bold d-block white-text">Trained &amp; Certified Candidates PAN India</span>
                        </h5>
                        <form name="CandidateAdvanceSearch" id="CandidateAdvanceSearch" onSubmit={this.handleSubmit}>
                            <Row>
                                <Col xs="12" md="4">
                                    <FormGroup>
                                        <Label for="Candidate Name">Search by Candidate Name</Label>
                                        <Input type="text" name="candidate_name" placeholder="Candidate Name" onChange={this.handleChange} />
                                    </FormGroup>
                                </Col>
                                <Col xs="12" md="4">
                                    <FormGroup>
                                        <Label for="searchPhone">Search by Mobile No.</Label>
                                        <Input type="text" name="phone" placeholder="Mobile No." onChange={this.handleChange} />
                                    </FormGroup>
                                </Col>
                                <Col xs="12" md="4">
                                    <FormGroup>
                                        <Label for="selectGender">Gender</Label>
                                        <Input type="select" name="sex" onChange={this.handleChange}>
                                            <option value="">Select</option>
                                            <option value="male">Male</option>
                                            <option value="female">Female</option>
                                            <option value="other">Other</option>
                                        </Input>
                                    </FormGroup>
                                </Col>
                            </Row>
                            <Row>
                                <Col xs="12" md="4">
                                    <FormGroup>
                                        <Field name="sectorid" component={renderColorSelector} dynamic_values={final_sectors} onChange={this.handleChange} label="Sector" />
                                    </FormGroup>
                                </Col>
                                <Col xs="12" md="4">
                                    <FormGroup>
                                        <Field name="jobroleid" component={renderColorSelector} dynamic_values={final_allJobs} onChange={this.handleChange} label="Job Role" />
                                    </FormGroup>
                                </Col>
                                <Col xs="12" md="4">
                                    <FormGroup>
                                        <Field name="training_status" component={renderColorSelector} dynamic_values={final_trainingstatus} onChange={this.handleChange} label="Training Status" />
                                    </FormGroup>
                                </Col>
                            </Row>
                            <Row>
                                <Col xs="12" md="4">
                                    <FormGroup>
                                        <Field name="statename" component={renderColorSelector} dynamic_values={final_allStates} onChange={this.handleChange} label="State" />
                                    </FormGroup>
                                </Col>
                                <Col xs="12" md="4">
                                    <FormGroup>
                                        <Field name="districtname" component={renderColorSelector} dynamic_values={final_allCities} onChange={this.handleChange} label="District" />

                                    </FormGroup>
                                </Col>
                            </Row>
                            <Row>
                                <Col xs="12" className="mt-2">
                                    <Button color="secondary" className="btn-raised mr-3">Search Candidate</Button>
                                    {!!this.state.submitted && !!this.state.user && <Button color="default" onClick={this.handleClear} outline>Clear</Button>}
                                    {/* {console.log(this.state.user,"USER")} */}
                                </Col>
                            </Row>
                        </form>
                    </CardBody>
                </Card>
            </div>
        );
    }
}

EmployerCandidateSearch_old = reduxForm({
    form: 'EmployerCandidateSearch_old',
    validate,
    renableReinitialize: true,
    asyncBlurFields: [], //to remove validation, we must need to remove it from Blur fields...
})(EmployerCandidateSearch_old);

function mapStateToProps(state, ownProps) {
    // console.log("My statesProps",ownProps);
    const { user } = state.authentication;
    const { items } = state.trainingpartners; //From Reducer - trainingpartners function ...
    const { searchformdata } = state.searchform;
    const { jobroles } = state.jobdata; //From Reducer - JobROleReducer function ...
    const { districtlist } = state.generaldata; //From Reducer - CommonReducer function ...

    return {
        user, searchformdata, jobroles, items, districtlist
        /* initialValues: {
             candidate_name: candidateName,
             sectorid:sectorName,
             jobroleid:jobroleName  
         }*/

    };
}
const connectedEmployerCandidateSearch_old = connect(mapStateToProps)(EmployerCandidateSearch_old);
export { connectedEmployerCandidateSearch_old as EmployerCandidateSearch_old };
