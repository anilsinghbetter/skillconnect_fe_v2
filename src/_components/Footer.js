import React from 'react';
import { connect } from 'react-redux';
// import { Label, FormGroup, Input, InputGroup, InputGroupAddon, Button, Card, CardBody, CardImg, CardText, CardTitle, CardSubtitle } from 'reactstrap';
class Footer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            //user_flow:true,//not using now, but it can be use later.
            //userType:false,
        };
    }
    render() {
        const styles = {
            display: 'none'
        };
        //theme-inverse only in onboardin flow...
        var footerThemeClass;
        if(localStorage.getItem('user') !== null || window.location.pathname  === '/terms-and-conditions' ||  window.location.pathname  === '/privacy-policy'){
            if(window.location.pathname  === '/counters'){
                footerThemeClass = "theme-inverse content-footer-block d-lg-flex d-block align-items-center justify-content-between";
            } else {
                footerThemeClass = "content-footer-block d-lg-flex d-block align-items-center justify-content-between";
            }
        }else{
            footerThemeClass = "theme-inverse content-footer-block d-lg-flex d-block align-items-center justify-content-between";
        }
        
        return (<div><div className= {footerThemeClass}>
            <div className="site-info">
                <p>By <a href="https://www.betterplace.co.in" target="_blank" rel="noopener noreferrer" className="footer-logo"><span style={styles}>betterplace</span></a>
                </p>
            </div>
            <ul className="content-footer-listing clearfix">
                <li><a href="/terms-and-conditions" title="Terms &amp; Conditions">Terms &amp; Conditions</a></li>
                <li><a href="/privacy-policy" title="Privacy Policy">Privacy Policy</a></li>
            </ul>
        </div></div>
        );
    }
}
function mapStateToProps(state) {
    //const { registering } = state.registration;
    return {
        //registering
    };
}
const connectedFooter = connect(mapStateToProps)(Footer);
export { connectedFooter as Footer };