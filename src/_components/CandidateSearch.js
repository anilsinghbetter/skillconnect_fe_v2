import React from 'react';
import { connect } from 'react-redux';
import { reduxForm, Field } from 'redux-form';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import { Button, Nav, NavItem, NavLink, FormGroup, Input, Row, Col, Card, CardBody, CardHeader, CardTitle, Collapse, Label } from 'reactstrap';
import { SearchIcon } from '../utils/SearchIcon';
import { userActions } from '../_actions';

const validate = values => {
    const errors = {}
    return errors;
}

const renderField = ({ input, label, type, placeholder, textarea, meta: { touched, error, warning, invalid } }) => {
    const textareaType = <textarea {...input} placeholder={label} type={type} className={`form-control ClearInput ${touched && invalid ? 'has-danger' : ''}`} />;

    return <div className={((label === "Gender") ? 'col-md-8 col-12' : 'col-md-4 col-12')}>
        <div className={'form-group' + (error && touched ? ' has-error' : '')}>
            <label>{label}</label>
            <div> {textarea ? textareaType : <input {...input} type={type} placeholder={placeholder} className="form-control ClearInput" />}
                {touched && ((error && <div className="help-block">{error}</div>) || (warning && <span>{warning}</span>))}
            </div>

        </div>
    </div>
};

const renderColorSelector = ({ input, label, dynamic_values, meta: { touched, error } }) => (
    <div key='1' className='form-group'>
        <label>{label}</label>
        <select  {...input} className="form-control">
            <option value="">Select</option>
            {dynamic_values}
            ))}
     </select>
    </div>
)

class CandidateSearch extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            submitted: false,
            selectedOption: '',
            // isData:0,
            collapse: false,
            updatedValue: this.props.mytest,
            results: [],
            selectedBatchStartDate: undefined,
            selectedBatchEndDate: undefined,
            cleared: false
            //Initial State..
            // data: this.props.items
        };
        this.handleSearchFilter = this.handleSearchFilter.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleBatchStartDateClick = this.handleBatchStartDateClick.bind(this);
        this.handleBatchEndDateClick = this.handleBatchEndDateClick.bind(this);
        this.handleClear = this.handleClear.bind(this);
    }

    handleChange(event) {
        const { name, value } = event.target;
        const { user } = this.state;
        // console.log('this state',name,value);  
        // console.log(event,"EVENT YAAAY");
         //Remove/Blank the jobroel id If Exists...         
         if(!!this.state.user)
         {   //console.log("SECID",this.state.user.sectorid);         
             if(name === "sectorid" && this.state.user.sectorid > 0)
             {
            //    console.log('#A');
                this.state.user.jobroleid = "";   
            //    console.log('#AA',this.state.user.jobroleid);
             }
             if(name === "stateid" && this.state.user.stateid > 0)
             {
            //    console.log('#B');
                this.state.user.cityid = "";   
            //    console.log('#BB',this.state.user.jobroleid);
             }
         }
         //End code for Removeing the jobroel id If Exists...            
        
        this.setState({
            user: {
                ...user,
                [name]: value
            }
        });
        // console.log("event data change", event.target.name, event.target.value);
        
        if (event.target.name === 'sectorid' && event.target.value) {this.props.dispatch(userActions.getJobRoles(value)); }
        if (event.target.name === 'stateid' && event.target.value)  {this.props.dispatch(userActions.getDistrict(value)); }
    }

    componentWillMount() {
        // const { user } = this.props;
        // console.log("Traingin partner id", this.props.user.data[0].trainingpartnerid);
        this.props.dispatch(userActions.getCandidateSearchFormData(this.props.user.data[0].trainingpartnerid,this.props.user.user_type));
    }

    handleSearchFilter() {
        this.setState({ collapse: !this.state.collapse });
    }
    handleSubmit(e) {
        e.preventDefault();
        // const { user } = this.state;
        // const { dispatch } = this.props;
        //console.log("Start Date",this.state);        
       //Search with API
        if ((this.state.user !== undefined && this.state.user !=="") || (this.state.selectedBatchStartDate!=="" && this.state.selectedBatchStartDate!==null && this.state.selectedBatchStartDate!==undefined) || (this.state.selectedBatchEndDate!==""  && this.state.selectedBatchEndDate!==null && this.state.selectedBatchEndDate!==undefined)) {
            // var trainingpartnerid = this.props.user.data[0].trainingpartnerid;
            var trainingcenterid = this.props.user.data[0].trainingcenterid;
            //if userfinal is exist then not add ... else add it..
            /* var userFinal22 = this.state.user;
            //console.log('#UYSERFINAL AFTER',userFinal22); */

            // var userFinal = {  trainingpartnerid, ...this.state.user };
            var userFinal = {  trainingcenterid, ...this.state.user };
            
            
            //console.log('#UYSERFINAL',userFinal);
            if (this.state.selectedBatchStartDate !== "" && this.state.selectedBatchStartDate!==null && this.state.selectedBatchStartDate !== undefined) {
                let batch_start_date = this.state.selectedBatchStartDate;
                userFinal = { batch_start_date, ...userFinal };
            }
            if (this.state.selectedBatchEndDate !== "" && this.state.selectedBatchEndDate!==null && this.state.selectedBatchEndDate !== undefined) {
                let batch_end_date = this.state.selectedBatchEndDate;
                userFinal = { batch_end_date, ...userFinal };
            }
            this.setState({ submitted: true }); 
            // console.log('#UYSERFINAL',userFinal);
            // return;
            this.props.dispatch(userActions.getTrainingPartnerSearchCandidate(userFinal,1,0));
            // this.props.dispatch(userActions.getTrainingCenterSearchCandidate(userFinal,1,0));

            //Passing Search Data to Parent Component -  GetCandidate..
            this.props.onSelectedChange(this.state.user);
        }
    }

    handleClear(e) {
        var trainingpartnerid = this.props.user.data[0].trainingpartnerid;
       
        this.props.reset();
        document.getElementById("SearchCandidateform").reset();
        document.getElementById("CandidateAdvanceSearch").reset();
        

        this.props.dispatch(userActions.getJobRoles(0));
        this.props.dispatch(userActions.getDistrict(0));
        //Input values
        const input = document.querySelectorAll('.ClearInput');
        for(let input_node of input){
            input_node.value = '';
        }

        this.setState({ 
                        user:'',
                        submitted: false,
                        selectedBatchStartDate: null,
                        selectedBatchEndDate: null,
                        cleared: true
                    });
       
        setTimeout(() => {
            this.setState({ cleared: false });
        }, 0); 
        
        //Select option values
        /* var sel = document.getElementsByClassName("ClearSelect");
        for(var i=0;i<sel.length;i++){
            sel[i].options[sel[i].selectedIndex].text = 'Select';
            sel[i].options[sel[i].selectedIndex].value = '';
        }  */

        //Radio button values
        /* var ele = document.getElementsByClassName("ClearRadio");
        for(var i=0;i<ele.length;i++){
            ele[i].checked = false;
        } */
        
        this.props.dispatch(userActions.getCandidates(trainingpartnerid, 1));
    }

    
    handleBatchStartDateClick(day) {
        this.setState({ cleared: false });
        this.setState({ selectedBatchStartDate: day });
                        // console.log(day,"SELECTED DATE");
        
    }
    handleBatchEndDateClick(day) {
        this.setState({ cleared: false });
        this.setState({ selectedBatchEndDate: day });
            // console.log(day,"SELECTED DATE");
    }

    render() {
        var loaderClass = '';
        
        //Show loader..
        if (this.props.loading === true) {
        //console.log("Data Is Loading");
            loaderClass = 'loading';
        }
        const styles = {
            top: '115px',
            left: '60px'
        };
        // let posts = decodeURIComponent(this.props.mySearchedData).replace(/\?/g, ' ');
        // console.log("PROPS",this.props);
        //var allCandidateValues = [];
        //console.log('# here in candidate search',this.props.items);
        if (!!this.props.items) {
            // console.log("candidatesearch:TP ",this.state);
            // console.log("candidatesearch: Candidate Vlaues", this.props.items.data);
            // console.log("candidatesearch:use search  Vlaues", this.state.user);
            //console.log("From Seach",this.props.search);
              /* Search Based Pagination*/
              /* if(this.props.search===1)
              {
               // console.log("Here111");
                // console.log("From Seach",this.props.search);
                allCandidateValues = this.props.items;
                // console.log("searched data@@@",allCandidateValues);
              }  
              //End of search pagination...
              else {
                //console.log("Here22");
                 allCandidateValues = this.props.items.data;
              } */
 
              var userInputkeywords = this.state.user;

              //console.log("date",this.state);
                //  var searchString = userInputkeywords.candidate_name.trim().toLowerCase();
                if ((userInputkeywords !== undefined) || (this.state.selectedBatchStartDate!=="" && this.state.selectedBatchStartDate!==undefined) || (this.state.selectedBatchEndDate!=="" && this.state.selectedBatchEndDate!==undefined)) {
                    
                    if(this.state.selectedBatchStartDate!=="" && this.state.selectedBatchStartDate!==undefined)
                    {
                        
                        let batch_start_date = this.state.selectedBatchStartDate;
                       /*  var date = new Date(batch_start_date);
                        var tmpDate = date.toDateString();
                        if(tmpDate != ''){

                            tmpDate = tmpDate.split(' ');
                            batch_start_date = tmpDate[2] + ' '+ tmpDate[1] +' '+ tmpDate[3];
                        } */
                    //   console.log("HEREEEEE");
                    userInputkeywords = {batch_start_date, ...userInputkeywords };
                    }
                    if(this.state.selectedBatchEndDate !== "" && this.state.selectedBatchEndDate !== undefined)
                    {
                    let batch_end_date = this.state.selectedBatchEndDate;

                    /* var date = new Date(batch_end_date);
                        var tmpDate = date.toDateString();
                        if(tmpDate != ''){
                            tmpDate = tmpDate.split(' ');
                            batch_end_date = tmpDate[2] + ' '+ tmpDate[1] +' '+ tmpDate[3];
                        } */
                    userInputkeywords = {batch_end_date, ...userInputkeywords };

                    }

                    var updatedkeys=clean(userInputkeywords);

                    function clean(obj) {
                        for (var propName in obj) { 
                            //console.log("@@@",obj);
                            //console.log("%%%%%",propName);
                            //console.log("####",obj[propName]);
                          if (obj[propName] === "" || obj[propName] === null || obj[propName] === undefined) {
                            delete obj[propName];
                          }
                        }
                      }             
                 //console.log("User Search Keywords : ", userInputkeywords);

                    console.log("updated Search Keywords : ", updatedkeys);
                    this.state.user = userInputkeywords;
                    //this.state.results = fillStateValue;
                }
                else {
                    this.state.results = "";
                    //this.setState({results: ""});
                }
            
        }
        if (!!this.props.districtlist) {
            var allCities = this.props.districtlist.data;
            var final_allCities = [];
            //State values..
            allCities.map((value,index) => {
                return final_allCities.push(<option key={index} value={value.cityid}>{value.cityName}</option>);
            })
        }
        if (!!this.props.jobroles) {
            var allJobroles = this.props.jobroles.data;
            var final_allJobs = [];
            allJobroles.map((value, index) => {
                return final_allJobs.push(<option value={value.jobroleid} key={index}>{value.jobroleName}</option>);
            })
        }
        if (!!this.props.searchformdata) {
            //Sector list..
            var allsectors = this.props.searchformdata.data.sector[0];
            var final_sectors = [];
            allsectors.map((value, index) => {
                return final_sectors.push(<option value={value.sectorid} key={index}>{value.sectorname}</option>);
            })
             //State values..
             var allStates = this.props.searchformdata.data.state[0];
             var final_allStates = [];
             allStates.map((value,index) => {
                 return final_allStates.push(<option key={index} value={value.stateid}>{value.stateName}</option>);
             })

             //Training Center values..
             var final_allTrainingCenters = [];
             if(this.props.searchformdata.data.training_centre !== undefined && this.props.searchformdata.data.training_centre.length > 0){
                var allTrainingCenters = this.props.searchformdata.data.training_centre[0];
                allTrainingCenters.map((value,index) => {
                        return final_allTrainingCenters.push(<option key={index} value={value.trainingcenterid}>{value.trainingcenter_name}</option>);
                })
             }             

            //Training Status list..
            var alltrainingstatus = this.props.searchformdata.data.training_status[0];
            var final_trainingstatus = [];
            alltrainingstatus.map((value,index) => {
                return final_trainingstatus.push(<option value={value.optionValue} key={index} >{value.optionKey}</option>);

            })
             //PlacementStatus list..
             var allplacementlist = this.props.searchformdata.data.placement_status[0];
             var final_placementstatus = [];
             allplacementlist.map((value,index) => {
                 return final_placementstatus.push(<option key={index} value={value.optionValue}>{value.optionKey}</option>);
 
             })

        }
        return (

            <Card className="z-depth-1 no-hover">
                <CardBody>
                    <div className="search-bar mb-0">
                        <Row className="align-items-center">
                            <Col className="mb-0 d-none d-md-flex align-items-center">
                                <form name="SearchCandidateform" id="SearchCandidateform" onSubmit={this.handleSubmit} className="w-100">
                                    <div className={'row align-items-center' + (this.state.collapse ? ' d-none' : '')} >
                                        
                                        {/* <div className="col-12 col-md-4 pr-0">
                                            <FormGroup className="form-icon">
                                                <Label for="search">Candidate Name</Label>
                                                <i className="material-icons">search</i>
                                                <Input type="text" className="ClearInput" name="candidate_name" placeholder="Enter Candidate Name" onChange={this.handleChange}/>
                                            </FormGroup>
                                        </div> */}
                                        <div className="col-12 col-md-3 pr-0">
                                            <Field name="stateid" component={renderColorSelector} dynamic_values={final_allStates} onChange={this.handleChange} label="State" />
                                        </div>
                                        <div className="col-12 col-md-2 pr-0">
                                            <FormGroup>
                                            <Field name="cityid" component={renderColorSelector} dynamic_values={final_allCities} onChange={this.handleChange} label="District" />
                                    
                                            </FormGroup>
                                        </div>
                                        <div className="col-12 col-md-3 pr-0">
                                                    <Field name="sectorid" component={renderColorSelector} dynamic_values={final_sectors} onChange={this.handleChange} label="Sector" />
                                            
                                        </div>

                                        <div className="col-12 col-md-2 ">
                                                    <Field name="jobroleid" component={renderColorSelector} dynamic_values={final_allJobs} onChange={this.handleChange} label="Job Role" />
                                               
                                        </div>

                                        <Button color="default">Go</Button>
                                        {!!this.state.submitted && <Button id="clearButton" className="ml-2" color="default" onClick={this.handleClear}>Clear</Button>}
                                        
                                    </div>

                                    <CardHeader className={'bg-transparent border-bottom-0 section-title no-border form-title p-0 m-0' + (this.state.collapse ? '' : ' d-none')} >
                                        <CardTitle className="title">Advanced Search</CardTitle>
                                    </CardHeader>

                                </form>
                            </Col>

                            <Col xs="auto" className="ml-auto order-last d-none d-md-flex justify-content-end">
                                <Nav className="border-0">
                                    <NavItem className="mr-2">
                                        <NavLink href="#" className="btn-fab btn-fab-default btn" onClick={this.handleSearchFilter}>
                                            <SearchIcon />
                                        </NavLink>
                                    </NavItem>
                                </Nav>
                            </Col>

                            
                        </Row>

                        <Collapse isOpen={this.state.collapse}>
                            <hr />

                            <div>
                            <form name="CandidateAdvanceSearch" id="CandidateAdvanceSearch" onSubmit={this.handleSubmit}>
                              
                                <Row>
                                    <Field name="batch_id" type="text"  component={renderField} label="Batch ID" placeholder="Batch ID" onChange={this.handleChange} />
                                    {/* <Field name="candidate_name" type="text" component={renderField} label="Candidate Name" placeholder="Candidate Name" onChange={this.handleChange} /> */}
                                    <Field name="phone" type="text" component={renderField} label="Mobile Number" placeholder="Mobile Number" onChange={this.handleChange} />
                                </Row>

                                <Row>
                                    <Col xs="12" md="4">
                                        <FormGroup>
                                            <Field name="sectorid" component={renderColorSelector} dynamic_values={final_sectors} onChange={this.handleChange} label="Sector" />
                                        </FormGroup>
                                    </Col>

                                    <Col xs="12" md="4">
                                        <FormGroup>
                                            <Field name="jobroleid" component={renderColorSelector} dynamic_values={final_allJobs} onChange={this.handleChange} label="Job Role" />
                                        </FormGroup>
                                    </Col>

                                    <Col xs="12" md="4">
                                        <FormGroup>
                                        <Field name="training_status" component={renderColorSelector} dynamic_values={final_trainingstatus} onChange={this.handleChange} label="Training Status" />
                                        </FormGroup>
                                    </Col>
                                   
                                </Row>

                                <Row>
                                {/* {final_allTrainingCenters !== '' &&
                                    <Col xs="12" md="4">
                                        <FormGroup>
                                            <Field name="trainingcenter_id" component={renderColorSelector} dynamic_values={final_allTrainingCenters} onChange={this.handleChange} label="Training Center" />
                                        </FormGroup>
                                    </Col>
                                } */}
                                    <Col xs="12" md="4">
                                        <Field name="stateid" component={renderColorSelector} dynamic_values={final_allStates} onChange={this.handleChange} label="State" />
                                    </Col>
                                    <Col xs="12" md="4">
                                        <FormGroup>
                                        <Field name="cityid" component={renderColorSelector} dynamic_values={final_allCities} onChange={this.handleChange} label="District" />
                                 
                                        </FormGroup>
                                    </Col>
                                    <Col xs="12" md="4">
                                        <FormGroup>
                                        <Field name="placement_status" component={renderColorSelector} dynamic_values={final_placementstatus} onChange={this.handleChange} label="Placement Status" />
                                        </FormGroup>
                                    </Col>
                                   
                                </Row>

                                <Row>
                                    {/* <Col xs="12" md="4">
                                        <FormGroup>
                                            <Label for="start_date">Batch Start Date</Label>
                                            <div className="input-group date date-picker" id="batchStartDateDIV" data-provide="datepicker">
                                            {!!this.state.cleared &&
                                                <DayPickerInput onDayChange={this.handleBatchStartDateClick} value='' /> 
                                            }
                                            {!this.state.cleared &&
                                            <DayPickerInput onDayChange={this.handleBatchStartDateClick} />
                                            }
                                                <div className="input-group-addon">
                                                    <i className="material-icons">date_range</i>
                                                </div>
                                            </div>
                                        </FormGroup>
                                    </Col>
                                    <Col xs="12" md="4">
                                        <FormGroup>
                                            <Label for="end_date">Batch End Date</Label>
                                            <div className="input-group date date-picker" data-provide="datepicker">
                                            {!!this.state.cleared &&
                                                <DayPickerInput onDayChange={this.handleBatchEndDateClick} value='' />
                                            }
                                            {!this.state.cleared &&
                                                <DayPickerInput onDayChange={this.handleBatchEndDateClick} />
                                            }
                                                
                                                <div className="input-group-addon">
                                                    <i className="material-icons">date_range</i>
                                                </div>
                                            </div>
                                        </FormGroup>
                                    </Col> */}
                                    <Col xs="12" md="4">
                                        <FormGroup>
                                            <Label className="d-block">Willing to Relocate</Label>
                                            <Field name="willing_to_relocate" component="input" type="radio" value="yes" className="ml-0" onChange={this.handleChange} /> Yes
                                                <Field name="willing_to_relocate" component="input" type="radio" value="no" onChange={this.handleChange} /> No
                                            </FormGroup>
                                    </Col>
                                    
                                </Row>

                                <Row>
                                    
                                    <Col xs="12" md="8">
                                        <FormGroup>
                                        <label htmlFor="gender" className="d-block">Gender</label>
                           
                                        <Field name="gender" component="input" type="radio" value="male" className="ml-0" onChange={this.handleChange} /> Male
                                        <Field name="gender" component="input" type="radio" value="female" onChange={this.handleChange} /> Female
                                        <Field name="gender" component="input" type="radio" value="other" onChange={this.handleChange} /> Other
                           
                                        </FormGroup>
                                    </Col>
                                </Row>
                                <Button color="primary" className="btn-raised">Apply</Button>
                                {!!this.state.submitted && <Button className="ml-2" color="btn-raised" onClick={this.handleClear}>Clear</Button>}
                                </form>
                            </div>
                        </Collapse>
                    </div>
                    <div className={loaderClass} style={styles}></div>
                </CardBody>
            </Card>

        );
    }
}

CandidateSearch = reduxForm({
    form: 'CandidateSearch',
    validate,
    renableReinitialize: true,
    asyncBlurFields: [], //to remove validation, we must need to remove it from Blur fields...
})(CandidateSearch);

function mapStateToProps(state, ownProps) {
    // console.log("My statesProps",ownProps);
    const { user } = state.authentication;
    const { loading } = state.trainingpartners; //From Reducer - trainingpartners function ...
    const { searchformdata } = state.searchform;
    const { jobroles } = state.jobdata; //From Reducer - JobROleReducer function ...
    const { districtlist } = state.generaldata; //From Reducer - CommonReducer function ...
   
    /*
    console.log("in MapTOState",ownProps.mySearchedData);
    var candidateName = "";
    var sectorName= "";
    var jobroleName="";

   if('mySearchedData' in ownProps)
               {
                    console.log("success here");
                    candidateName = ownProps.mySearchedData.candidate_name
                    sectorName = ownProps.mySearchedData.sectorid
                    jobroleName =  ownProps.mySearchedData.jobroleid
                }
                    
               else
               {
                console.log("failure here");
                candidateName = "";  
                sectorName="";
                jobroleName =  "";
               } 
               */

    return {
        user, searchformdata, jobroles, districtlist,loading
        /* initialValues: {
             candidate_name: candidateName,
             sectorid:sectorName,
             jobroleid:jobroleName  
         }*/

    };
}
const connectedCandidateSearch = connect(mapStateToProps)(CandidateSearch);
export { connectedCandidateSearch as CandidateSearch};