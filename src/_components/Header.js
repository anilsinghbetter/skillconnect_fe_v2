import React from 'react';
import { connect } from 'react-redux';
import { Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, NavLink } from 'reactstrap';
import logoImage from '../assets/images/skillconnect-logo.png';
/*import skillindialogo from '../assets/images/skill-india-logo.png';
import MSED from '../assets/images/MSED.png';
import NSDC from '../assets/images/NSDC.png';*/
import { Sidebar } from './Sidebar';

/* const trainingPartnerNavItems = [
    { to: '/TrainingPartner-dashboard', name: 'dashboard', exact: true, Icon: "../assets/images/ic_dashboard.svg", alt: "Dashboard" },
    { to: '/candidates', name: 'candidates', exact: true, Icon: "../assets/images/ic_candidates.svg", alt: "Candidates" },
    { to: '/training-centers', name: 'Training Centers', exact: true, Icon: "../assets/images/ic_training_center.svg", alt: "Training Centers" },
    { to: '/TrainingPartner-hiring-requests', name: 'Hiring Requests', exact: true, Icon: "../assets/images/ic_hiring_requests.svg", alt: "Hiring Requests" },
    { to: '/login', name: 'Logout', exact: true, Icon: "../assets/images/ic_logout.svg", alt: "Logout" },
];

const employerNavItems = [
    { to: '/Employer-dashboard', name: 'dashboard', exact: true, Icon: "../assets/images/ic_dashboard.svg", alt: "Dashboard" },
    { to: '/Employer-hiring-requests', name: 'Hiring Requests', exact: true, Icon: "../assets/images/ic_hiring_requests.svg", alt: "Hiring Requests" },
    { to: '/add-future-hiring-requests', name: 'Future Hiring Requests', exact: true, Icon: "../assets/images/ic_hiring_requests.svg", alt: "Future Hiring Requests" },
    { to: '/login', name: 'Logout', exact: true, Icon: "../assets/images/ic_logout.svg", alt: "Logout" },
]; */

class Header extends React.Component {
    constructor(props) {
        super(props);
        this.toggle = this.toggle.bind(this);
        this.state = {
            isOpen: false,
            filedata: false
        };
    }

    toggle() {   
        this.setState({
            isOpen: !this.state.isOpen,
            filedata: !this.state.filedata
        });
        console.log('#CURRENTVALUE',this.state.filedata);   
        
    }
    render() {
        //first 3 links only in onboarding flow....
        if (localStorage.getItem('user') !== null) {
            let user = JSON.parse(localStorage.getItem('user'));
            // console.log("SESSION",user.user_type);
            /* var navItems;
            if (user.user_type == 'EMP') {
                navItems = employerNavItems;
            } else {
                navItems = trainingPartnerNavItems;
            } */
            return (<div><Navbar className='fixed-top z-depth' color="white" light expand="md">
                <div className="container-fluid justify-content-start">
                {user.user_type === 'EMP'
                    ?
                    <NavbarBrand href="/Employer-dashboard" title="SkillConnect"><img src={logoImage} alt="SkillConnect" /></NavbarBrand>
                    :
                    <NavbarBrand href="/TrainingPartner-dashboard" title="SkillConnect"><img src={logoImage} alt="SkillConnect" /></NavbarBrand>
                }
                <NavbarToggler onClick={this.toggle} />                   
                {/* {console.log(this.state.filedata, "this.state.filedata")}
                {this.state.filedata === 1 && console.log("displayed")} */}
                {this.state.filedata === true &&
                    
                    <Sidebar filedata={this.state.filedata} user={user} {...this.props} />
                }
                <Nav className="ml-auto align-items-center list-inline-separator" navbar>
                    <NavItem>
                        <NavLink className="btn btn-secondary text-white text-lowercase btn-raised" href="https://www.betterplace.co.in/" target="_blank" title="www.betterplace.co.in">www.betterplace.co.in</NavLink>
                    </NavItem>
                </Nav>
                {/*
                        <Nav className="ml-auto align-items-center list-inline-separator" navbar>
                            <NavItem>
                                <NavLink className="btn btn-secondary text-white text-lowercase btn-raised" href="https://www.betterplace.co.in/" target="_blank" title="www.betterplace.co.in">www.betterplace.co.in</NavLink>
                            </NavItem>
                        </Nav>
                        */}

                </div>
            </Navbar></div>
            );
        } else {
            return (<div><Navbar className='fixed-top z-depth' color="white" light expand="md">
                <div className="container-fluid justify-content-start">
                    <NavbarBrand href="/login" title="SkillConnect"><img src={logoImage} alt="SkillConnect" /></NavbarBrand>
                    <NavbarToggler onClick={this.toggle} />
                    <Collapse isOpen={this.state.isOpen} navbar>
                        <Nav className="ml-auto align-items-center list-inline-separator" navbar>
                           {/*  <NavItem>
                                <NavLink className="" title="Skill India"><img src={skillindialogo} alt="Skill India" /></NavLink>
                            </NavItem> */}
                            {/* <NavItem>
                                <NavLink className="" title="MSED"><img src={MSED} alt="MSED" /></NavLink>
                            </NavItem> */}
                            {/* <NavItem>
                                <NavLink className="" title="NSDC"><img src={NSDC} alt="NSDC" /></NavLink>
                            </NavItem> */}
                            <NavItem>
                                <NavLink className="btn btn-secondary text-white text-lowercase btn-raised" href="https://www.betterplace.co.in/" target="_blank" title="www.betterplace.co.in">www.betterplace.co.in</NavLink>
                            </NavItem>

                        </Nav>
                    </Collapse>
                </div>
            </Navbar></div>
            );
        }
    }
}
function mapStateToProps(state) {
    return {
    };
}
const connectedHeader = connect(mapStateToProps)(Header);
export { connectedHeader as Header };