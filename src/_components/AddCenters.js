import React from 'react';
import { connect } from 'react-redux';
import { userActions } from '../_actions';
import { reduxForm, Field } from 'redux-form';
import { Header } from '../_components/Header';
import { Footer } from '../_components/Footer';
import { Sidebar } from '../_components/Sidebar';
import { Breadcrumb, BreadcrumbItem } from 'reactstrap';
import 'react-day-picker/lib/style.css';

const renderTextAreaField = ({ input, label, type, placeholder, textarea, meta: { touched, error, warning, invalid } }) => {

    const textareaType = <textarea {...input} placeholder={label} type={type} className={`form-control ${touched && invalid ? 'has-danger' : ''}`} />;

    return <div className=''>
        <div className={'form-group' + (error && touched ? ' has-error' : '')}>
            <label>{label}</label>
            <div> {textarea ? textareaType : ""}
                {touched && ((error && <div className="help-block">{error}</div>) || (warning && <span>{warning}</span>))}
            </div>

        </div>
    </div>
};

const renderField = ({ input, label, type, placeholder, textarea, meta: { touched, error, warning, invalid } }) => {
    const textareaType = <textarea {...input} placeholder={label} type={type} className={`form-control ${touched && invalid ? 'has-danger' : ''}`} />;
    return <div className={((label === "SDMS/NSDC Enrollment No" || label === "Contact Person Name" || label === "Email Address" || label === "Mobile Number" || label === "Pincode") ? 'col-md-4 col-12' : 'col-md-6 col-12')}>
        <div className={'form-group' + (error && touched ? ' has-error' : '')}>
            <label>{label}</label>
            <div> {textarea ? textareaType : <input {...input} type={type} placeholder={placeholder} className="form-control" />}
                {touched && ((error && <div className="help-block">{error}</div>) || (warning && <span>{warning}</span>))}
            </div>

        </div>
    </div>
};

// const renderError = ({ meta: { touched, error } }) =>
//     touched && error ? <div className="help-block">{error}</div> : false

const validate = values => {

    const errors = {}

    if (!values.trainingcenter_name) {
        errors.trainingcenter_name = 'Please enter center name'
    } /*else if (values.username.length > 15) {
            errors.username = 'Must be 15 characters or less'
            }  */
    if (!values.sdms_trainingcenter_id) {
        errors.sdms_trainingcenter_id = 'Please enter SDMS/Training Center ID'
    }
    if (!values.contact_email_address) {
        errors.contact_email_address = 'Please enter an Email Address'
    }
    if (!values.phone) {
        errors.phone = 'Please enter mobile number'
    }
    if (!values.pincode) {
        errors.pincode = 'Please enter Pincode'
    }
    if (!values.address) {
        errors.address = 'Please enter Address'
    }
    if (!values.nsdc_enrollment_no) {
        errors.nsdc_enrollment_no = 'Please enter SDMS/NSDC Enrollment No'
    }
    if (!values.statename) {
        errors.statename = 'Please select a state'
    }
    if (!values.districtname) {
        errors.districtname = 'Please select a district'
    }

    if (!values.contact_person_name) {
        errors.contact_person_name = 'Please enter Contact Person Name'
    }
    return errors;
}

class AddNewCenter extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            submitted: false,
            combovalue: '',
            showState: false,
            registerClick: 0,
            error_flag: 0,

        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        //Date Related Pure Functions..
        this.checkValidation = this.checkValidation.bind(this);
    }
    componentDidMount() {
        //getStates
        this.props.dispatch(userActions.getStates());
    }

    handleSubmit(event) {
        // event.preventDefault();
        const { user } = this.state;
        const { dispatch } = this.props;
        this.setState({ submitted: true });

        var trainingcenterid = '-1';
        var trainingpartnerid = this.props.user.data[0].trainingpartnerid;
        var userFinal = { trainingpartnerid, trainingcenterid, ...user };
        
      //  console.log("send data", userFinal);
//        return;

        if (userFinal.sdms_trainingcenter_id && userFinal.trainingcenter_name && userFinal.contact_email_address) {
            dispatch(userActions.addupdateTrainingCenter(userFinal));
        }
    }
    checkValidation(event) {
        //console.log("Register clicked.");
        this.setState({ registerClick: 1 });
    }

    handleChange(event) {

        const { name, value } = event.target;
        const { user } = this.state;
        // const { dispatch } = this.props;
        // this.setState({ value: event.target.value });
        this.setState({
            user: {
                ...user,
                [name]: value
            }
        });
        //console.log("event data change", event.target.name, event.target.value);

        if (event.target.name === 'districtname') { this.setState({ error_flag: 0 }) }
        if (event.target.name === 'statename') {
            if (event.target.name !== 'districtname') {
                this.setState({ error_flag: 1 });
                this.props.dispatch(userActions.getDistrict(value));
            }
        }

        if (event.target.name === 'statename') { this.props.dispatch(userActions.getDistrict(value)); }

    }

    render() {
        const { handleSubmit } = this.props;
        // console.log("user details", user);
        // console.log("Form Data", candidateformdata);
        //console.log("disctrict value", districtlist);
        //console.log("Job value", jobroles);
      //  console.log("general props", this.props);
        //console.log("get cnetral minstiry data",centralministriesdata);  

        if (!!this.props.districtlist) {
            var allCities = this.props.districtlist.data;
            var final_allCities = [];
            //State values..
            allCities.map((value, index) => {
                return final_allCities.push(<option key={index} value={value.cityid}>{value.cityName}</option>);
            })
        }
        if (!!this.props.statedata) {
            //State values..
            var allStates = this.props.statedata.data;
            var final_allStates = [];
            allStates.map((value, index) => {
                return final_allStates.push(<option key={index} value={value.stateid}>{value.stateName}</option>);
            })
        }

        const renderColorSelector = ({ input, label, dynamic_values, error_state, meta: { touched, error } }) => (
            <div className={'form-group' + ((error && touched) || (error_state === 1) ? ' has-error' : '')}>
                <label>{label}</label>
                <select {...input} className="form-control">
                    <option value="">Select</option>
                    {dynamic_values}
                    ))}
              </select>
                {error_state === 1 && <div className="help-block">{error}</div>}
                {touched && error && <div className="help-block">{error}</div>}
            </div>

        )
        return (<div className="content-wrapper user-flow TP-flow">

            <div id="content" className="main-content">
                <Sidebar {...this.props} />
                <Header />
                <div className="d-block d-md-flex align-items-center section-header">

                    <div>
                        <h1 className="page-title">Add Training Center</h1>
                        <Breadcrumb>
                            <BreadcrumbItem><a href="/TrainingPartner-dashboard">Dashboard</a></BreadcrumbItem>
                            <BreadcrumbItem><a href="/training-centers">Training Centers</a></BreadcrumbItem>
                            <BreadcrumbItem active>Add Center</BreadcrumbItem>
                        </Breadcrumb>
                    </div>
                </div>
                <form id="frm-add-candidate" className="form frm-edit-candidaten" onSubmit={handleSubmit(this.handleSubmit)}>
                    <div className="card z-depth-1">
                        <div className="card-header bg-transparent border-bottom-0 section-title no-border form-title">
                            <h5 className="title card-title">Center's Details</h5>
                        </div>
                        <div className="card-body pb-0">

                            <div className="row">
                                <Field name="trainingcenter_name" type="text" component={renderField} label="Training Center Name" placeholder="Training Center Name" onChange={this.handleChange} />
                                <Field name="sdms_trainingcenter_id" type="text" component={renderField} label="SDMS/Training Center ID" placeholder="SDMS/Training Center ID" onChange={this.handleChange} />
                            </div>
                            <div className="">
                                <Field name="address" component={renderTextAreaField} type="textarea" label="Training Center Address" textarea={true} onChange={this.handleChange} />
                            </div>

                            <div className="row">
                                <div className="col-12 col-md-4">
                                    <div className='form-group'>
                                        <Field name="statename" component={renderColorSelector} dynamic_values={final_allStates} onChange={this.handleChange} label="State" />
                                    </div>
                                </div>

                                <div className="col-12 col-md-4">
                                    <div className='form-group'>
                                        <Field name="districtname" component={renderColorSelector} error_state={this.state.error_flag} dynamic_values={final_allCities} onChange={this.handleChange} label="District" />
                                    </div>
                                </div>
                                <Field name="pincode" type="text" component={renderField} label="Pincode" placeholder="Pincode" onChange={this.handleChange} />

                            </div>
                        </div>

                        <hr className="inner-form-seperator" />

                        <div className="card-header bg-transparent border-bottom-0 section-title no-border form-title">
                            <h5 className="title card-title">CONTACT PERSON DETAILS</h5>
                        </div>
                        <div className="card-body">

                            <div className="row">
                                <Field name="contact_person_name" type="text" component={renderField} label="Contact Person Name" placeholder="Contact Person Name" onChange={this.handleChange} />
                                <Field name="contact_email_address" type="text" component={renderField} label="Email Address" placeholder="Email Address" onChange={this.handleChange} />
                                <Field name="contact_mobile" type="text" component={renderField} label="Mobile Number" placeholder="Mobile Number" onChange={this.handleChange} />
                            </div>

                            <div className="action-btn-block">
                            {this.state.error_flag === 1 ?
                                <button disabled="disabled" className="btn btn-primary btn-raised mr-3" onClick={this.checkValidation} title="Save">Save</button>
                            :
                                <button className="btn btn-primary btn-raised mr-3" onClick={this.checkValidation} title="Save">Save</button>
                            }
                                <a title="Cancel" className="btn btn-outline" href="/training-centers">Cancel</a>
                            </div>



                        </div>


                    </div>
                </form>
                <Footer />
            </div>
        </div>
        );
    }
}
AddNewCenter = reduxForm({
    form: 'AddNewCenter',
    validate,
    enableReinitialize: true,
})(AddNewCenter);

function mapStateToProps(state) {
    const { user } = state.authentication;
    const { districtlist } = state.generaldata; //From Reducer - CommonReducer function ...
    const { statedata } = state.centralministrydata; //From Reducer - CentralMinistry function ...

    return {
        user,  districtlist, statedata
    };
}
const connectedAddNewCenter = connect(mapStateToProps)(AddNewCenter);
export { connectedAddNewCenter as AddNewCenter };