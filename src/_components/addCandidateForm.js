import React from 'react';
import { connect } from 'react-redux';
import { userActions } from '../_actions';
import { reduxForm, Field } from 'redux-form';
import { Header } from '../_components/Header';
import { Footer } from '../_components/Footer';
import { history } from '../_helpers';
import { Sidebar } from '../_components/Sidebar';
import { Label, FormGroup, Input, Button, Breadcrumb, BreadcrumbItem } from 'reactstrap';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import 'react-day-picker/lib/style.css';

class addCandidateForm extends React.Component {
constructor(props) {
super(props);
this.state = {
//Initial State..
};
}
render() {

   
    
    
const validate = values => {

    const errors = {}

    if (!values.name) {
        errors.name = 'Please enter your name'
    } /*else if (values.username.length > 15) {
            errors.username = 'Must be 15 characters or less'
            }  */
    if (!values.aadhar_card_no) {
        errors.aadhar_card_no = 'Please enter your aadhar card number'
    }
    if (!values.contact_email_address) {
        errors.contact_email_address = 'Please enter an Email Address'
    }
    if (!values.phone) {
        errors.phone = 'Please enter your Phone'
    }
    if (!values.pincode) {
        errors.pincode = 'Please enter Pincode'
    }
    if (!values.sex) {
        errors.sex = 'Please select your gender'
    }
    if (!values.address) {
        errors.address = 'Please enter Address'
    }
    if (!values.nsdc_enrollment_no) {
        errors.nsdc_enrollment_no = 'Please enter SDMS/NSDC Enrollment No'
    }
    if (!values.statename) {
        errors.statename = 'Please select a state'
    }
    if (!values.districtname) {
        errors.districtname = 'Please select a district'
    }
    if (!values.training_type) {
        errors.training_type = "Please select a training type";
    }
    if (!values.training_status) {
        errors.training_status = "Please select training status";
    }
    if (!values.placement_status) {
        errors.placement_status = "Please select placement status";
    }
    if (!values.employment_type) {
        errors.employment_type = "Please select employment status";
    }
    if (!values.sectorname) {
        errors.sectorname = "Please select sector name";
    }
    if (!values.jobrole) {
        errors.jobrole = "Please select job role";
    }
    if (!values.batch_id) {
        errors.batch_id = "Please select batch Id";
    }
    if (!values.schemename) {
        errors.schemename = "Please select a scheme";
    }
    if (!values.state_scheme_name) {
        errors.state_scheme_name = "Please select state scheme";
    }
    /*
            if (!values.contact_mobile) {
                errors.contact_mobile = 'Please enter Contact Mobile No'
            }*/
    return errors;
}
    const renderError = ({ meta: { touched, error } }) =>
    
        touched && error ? <div class="help-block">{error}</div> : false
    

return(<div>
       <form id="frm-add-candidate" className="form frm-edit-candidaten" onSubmit={this.props.handleSubmit(this.handleSubmit)}>
                    <div className="card z-depth-1">
                        <div className="card-header bg-transparent border-bottom-0 section-title no-border form-title">
                            <h5 className="title card-title">Personal Details</h5>
                        </div>
                        <div className="card-body pb-0">

                            <div className="row">
                                <Field name="name" type="text" component={this.props.renderField} label="Enter Your Name" placeholder="Enter Your Name" onChange={this.handleChange} />
                                <Field name="aadhar_card_no" type="text" component={this.props.renderField} label="Aadhaar No" placeholder="Aadhaar No" onChange={this.handleChange} />
                            </div>

                            <div className="row">
                                <Field name="aadhaar_enrollment_id" type="text" component={this.props.renderField} label="Aadhaar Enrollment ID" placeholder="Aadhaar Enrollment ID" onChange={this.handleChange} />
                                <div className="col-12 col-md-6">
                                    <FormGroup>
                                        <Label for="Alternate ID Type">Alternate ID Type</Label>
                                        <Input type="select" name="alternate_id_type" id="alternate_id_type" onChange={this.handleChange}>
                                            <option value="">Select</option>
                                            {this.props.final_alternative_ids}
                                        </Input>
                                    </FormGroup>
                                </div>
                            </div>
                            <div className="row">
                                <Field name="altername_id_no" type="text" component={this.props.renderField} label="Alternate ID No" placeholder="Alternate ID No" onChange={this.handleChange} />
                                <Field name="phone" type="text" component={this.props.renderField} label="Phone" placeholder="Phone" onChange={this.handleChange} />
                            </div>

                            <div className="row">
                                <Field name="email_address" type="text" component={this.props.renderField} label="Email Address" placeholder="Email Address" onChange={this.handleChange} />

                                <div className="col-12 col-md-6">
                                    <div className={'form-group' + (!this.state.selectedDay ? ' has-error' : '')}>
                                        <label htmlFor="date_of_birth">Date Of Birth</label>
                                        <div class="input-group date date-picker" data-provide="datepicker">
                                            <DayPickerInput onDayChange={this.handleDayClick} />
                                            <div class="input-group-addon">
                                                <i class="material-icons">date_range</i>
                                            </div>
                                        </div>
                                        <div class="help-block">{!this.state.selectedDay && this.props.registerClick == 1 ? 'Please select your Date Of Birth' : ''}</div>

                                    </div>
                                </div>

                            </div>
                            <div className={'form-group' + (!this.props.male || !this.props.female ? ' has-error' : '')}>
                                <label for="gender" className="d-block">Gender</label>
                                <Field name="sex" component="input" type="radio" value="male" className="ml-0" onChange={this.handleChange} /> Male
                                <Field name="sex" component="input" type="radio" value="female" onChange={this.handleChange} /> Female
                                <Field name="sex" component="input" type="radio" value="other" onChange={this.handleChange} /> Other
                                <Field name="sex" component={this.props.renderError} />
                            </div>

                            <div className="">
                                <Field name="address" component={this.props.renderTextAreaField} type="textarea" label="Address" textarea={true} onChange={this.handleChange} />
                            </div>

                            <div className="row">
                                <div className="col-12 col-md-4">
                                    <div className='form-group'>
                                        <Field name="statename" component={this.props.this.props.renderColorSelector} dynamic_values={this.props.final_allStates} onChange={this.handleChange} label="State" />
                                    </div>
                                </div>

                                <div className="col-12 col-md-4">
                                    <div className='form-group'>
                                        <Field name="districtname" component={this.props.this.props.renderColorSelector} dynamic_values={this.props.final_allCities} onChange={this.handleChange} label="District" />
                                    </div>
                                </div>
                                <Field name="pincode" type="text" component={this.props.this.props.renderField} label="Pincode" placeholder="Pincode" onChange={this.handleChange} />

                            </div>
                        </div>

                        <hr className="inner-form-seperator" />

                        <div className="card-header bg-transparent border-bottom-0 section-title no-border form-title">
                            <h5 className="title card-title">Professional Details</h5>
                        </div>
                        <div className="card-body">
                            <div className="row">
                                <Field name="exp_in_years" type="text" component={this.props.renderField} label="Experience In Years" placeholder="Experience In Years" onChange={this.handleChange} />
                                <div className="col-12 col-md-4">
                                    <FormGroup>
                                        <Field name="training_type" component={this.props.renderColorSelector} dynamic_values={this.props.final_trainingtype} onChange={this.handleChange} label="Training Type" />
                                    </FormGroup>
                                </div>
                                <Field name="nsdc_enrollment_no" type="text" component={this.props.renderField} label="SDMS/NSDC Enrollment No" placeholder="SDMS/NSDC Enrollment No" onChange={this.handleChange} />
                            </div>

                            <div className="row">
                                <div className="col-12 col-md-4">
                                    <FormGroup>
                                        <Field name="training_status" component={this.props.renderColorSelector} dynamic_values={this.props.final_trainingstatus} onChange={this.handleChange} label="Training Status" />
                                    </FormGroup>
                                </div>

                                <Field name="attendance_percent" type="text" component={this.props.renderField} label="Attendance Percent" placeholder="Attendance Percent" onChange={this.handleChange} />
                                <div className="col-12 col-md-4">
                                    <FormGroup>
                                        <Field name="placement_status" component={this.props.renderColorSelector} dynamic_values={this.props.final_placementstatus} onChange={this.handleChange} label="Placement Status" />
                                    </FormGroup>
                                </div>
                            </div>

                            <div className="row">
                                <div className="col-12 col-md-4">
                                    <FormGroup>
                                        <Field name="employment_type" component={this.props.renderColorSelector} dynamic_values={this.props.final_employementstatus} onChange={this.handleChange} label="Employment Type" />
                                    </FormGroup>
                                </div>
                                <Field name="assessment_score" type="text" component={this.props.renderField} label="Assessment Score" placeholder="Assessment Score" onChange={this.handleChange} />
                                <Field name="max_assessment_score" type="text" component={this.props.renderField} label="Max Assessment Score" placeholder="Max Assessment Score" onChange={this.handleChange} />
                            </div>

                            <div className="row">
                                <div className="col-12 col-md-4">
                                    <div className={'form-group' + (!this.state.selectedAssessmentDate ? ' has-error' : '')}>
                                        <label for="assessment_date">Assessment Date</label>
                                        <div class="input-group date date-picker" data-provide="datepicker">
                                            <DayPickerInput onDayChange={this.handleAssessmentClick} />
                                            <div class="input-group-addon">
                                                <i class="material-icons">date_range</i>
                                            </div>
                                        </div>
                                        <div class="help-block">{!this.state.selectedAssessmentDate && this.props.registerClick == 1 ? 'Please enter Assessment Date' : ''}</div>
                                    </div>
                                </div>

                                <div className="col-12 col-md-4">
                                    <div className="form-group">
                                        <FormGroup>
                                            <Field name="sectorname" component={this.props.renderColorSelector} dynamic_values={this.props.final_sectors} onChange={this.handleChange} label="Sector" />
                                        </FormGroup>
                                    </div>
                                </div>

                                <div className="col-12 col-md-4">
                                    <div className="form-group">
                                        <FormGroup>
                                            <Field name="jobrole" component={this.props.renderColorSelector} dynamic_values={this.props.final_allJobs} onChange={this.handleChange} label="Job Role" />
                                        </FormGroup>
                                    </div>
                                </div>
                            </div>

                            <div className="row">

                                <Field name="batch_id" type="text" component={this.props.renderField} label="Batch ID" placeholder="Batch ID" onChange={this.handleChange} />

                                <div className="col-12 col-md-4">
                                    <div className={'form-group' + (!this.state.selectedBatchStartDate ? ' has-error' : '')}>
                                        <label for="Batch Start Date">Batch Start Date</label>
                                        <div class="input-group date date-picker" data-provide="datepicker">
                                            <DayPickerInput onDayChange={this.handleBatchStartDateClick} />
                                            <div class="input-group-addon">
                                                <i class="material-icons">date_range</i>
                                            </div>
                                        </div>
                                        <div class="help-block">{!this.state.selectedBatchStartDate && this.props.registerClick == 1 ? 'Please enter Batch Start Date' : ''}</div>
                                    </div>
                                </div>
                                <div className="col-12 col-md-4">
                                    <div className={'form-group' + (!this.state.selectedBatchEndtDate ? ' has-error' : '')}>
                                        <label for="Batch End Date">Batch End Date</label>
                                        <div class="input-group date date-picker" data-provide="datepicker">
                                            <DayPickerInput onDayChange={this.handleBatchEndDateClick} />
                                            <div class="input-group-addon">
                                                <i class="material-icons">date_range</i>
                                            </div>
                                        </div>
                                        <div class="help-block">{!this.state.selectedBatchEndDate && this.props.registerClick == 1 ? 'Please enter Batch End Date' : ''}</div>
                                    </div>
                                </div>
                            </div>

                            <div className="row">

                                <div className="col-12 col-md-4">
                                    <div className="form-group">
                                        <FormGroup>
                                            <Field name="schemename" component={this.props.renderColorSelector} dynamic_values={this.props.final_schemes} onChange={this.handleChange} label="Scheme" />
                                        </FormGroup>
                                    </div>
                                </div>
                                {this.state.showState &&
                                    <div className="col-12 col-md-4">
                                        <FormGroup>
                                            <Field name="state_scheme_name" component={this.props.renderColorSelector} dynamic_values={this.props.final_allStates} onChange={this.handleChange} label="State Scheme" />
                                        </FormGroup>
                                    </div>
                                }
                                {!this.state.showState &&
                                    this.props.central_minstry_name}
                            </div>

                            <div className="action-btn-block">
                                <button className="btn btn-primary btn-raised mr-3" onClick={this.checkValidation} title="Save">Save</button>
                                <a title="Cancel" className="btn btn-outline" href="/candidates">Cancel</a>
                            </div>
                        </div>


                    </div>
                </form></div>
);
}
}
addCandidateForm = reduxForm({
    form: 'addCandidateForm',
    validate,
    enableReinitialize: true,
})(addCandidateForm);

function mapStateToProps(state) {
//Add state values which we need to integrate with props..
return {
//registering
};
}
const connectedaddCandidateForm = connect(mapStateToProps)(addCandidateForm);
export { connectedaddCandidateForm as addCandidateForm };