import React from 'react';
import { connect } from 'react-redux';
import { userActions } from '../_actions';
import { Header } from '../_components/Header';
import { Footer } from '../_components/Footer';
import { Sidebar } from '../_components/Sidebar';
import { config } from '../config';
import { history } from '../_helpers';
import { Breadcrumb, BreadcrumbItem,Card,CardBody,Table } from 'reactstrap';
class BulkUploadStatus extends React.Component {
  

componentDidMount() {
    var {user} = this.props;
    if(user !== undefined ){
        // console.log(this.props.user,"MY USER");
        if(user.user_type !== 'TC' ){
            history.push('/login');
        }
        this.props.dispatch(userActions.getbulkUploadStatus());
    }
    
    // this.setState({ candidateIds: checkboxData });
}

render() {
    var allErrorRows = [];
    var allErrorListTable = [];
    if (!!this.props.ErrorFileList) {
        allErrorRows = this.props.ErrorFileList.data;
        allErrorRows.map((value, index) => {
            var URL = config.API_HOST_URL + 'error/'+allErrorRows[index].trainingpartner_id+'/'+allErrorRows[index].candidate_meta_id;
            return allErrorListTable.push(<tr key={index}>
                <td className="w-space-n">{index+1}</td>
                <td className="w-space-n">{allErrorRows[index].filename}</td>
                <td className="w-space-n">{allErrorRows[index].created}</td>
                <td className="w-space-n">{allErrorRows[index].error_count}</td>
                <td>
                    <a href={URL} title="Download From Here" className="">
                        <span>Click Here to Download</span>
                    </a>
                </td>
            </tr>);
        });
    } else {
        allErrorListTable.push(<tr key={1}><td colSpan="5">No Data Found</td></tr>);
    }

  return (
      <div className="content-wrapper user-flow TP-flow">
        <Sidebar {...this.props} />
        
        <div id="content" className="main-content">
          <Header />

          <div className="d-block d-md-flex align-items-center section-header">
            <div>
              <h1 className="page-title">Bulk Upload Status</h1>
              <Breadcrumb>
                  <BreadcrumbItem><a href="/candidates">Candidates</a></BreadcrumbItem>
                  <BreadcrumbItem active>Bulk Upload Status</BreadcrumbItem>
              </Breadcrumb>
            </div>
          </div>
          
          <Card className="content-card">
            <CardBody>
                <Table hover responsive className="table-btn mb-0">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>File Name</th>
                            <th>Date</th>
                            <th>Errors</th>
                            <th>Download</th>
                        </tr>
                    </thead>
                    <tbody>
                        {allErrorListTable}
                    </tbody>
                </Table>
            </CardBody>
          </Card>
          
          <Footer />
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
    const { users, authentication} = state;
    const { user } = authentication;
    const { ErrorFileList } = state.allErrorFiles;
    return {
        user,
        users,
        ErrorFileList
    };
}


const connectedBulkUploadStatus = connect(mapStateToProps)(BulkUploadStatus);
export { connectedBulkUploadStatus as BulkUploadStatus };
