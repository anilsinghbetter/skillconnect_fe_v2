import React from 'react';
import { connect } from 'react-redux';
import { reduxForm, Field } from 'redux-form';
import { FormGroup, Row, Col, Card, CardHeader, CardSubtitle, CardLink } from 'reactstrap';
import { userActions } from '../_actions';
import Select from 'react-select';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import 'react-day-picker/lib/style.css';
import {formatDate,parseDate,} from 'react-day-picker/moment';
// import { GetCandidates } from './GetCandidates';
import { GetCandidatesListOnly } from './GetCandidatesListOnly';
import { CandidatesListView } from './CandidatesListView';
import { CandidateSortEmployer } from './CandidateSortEmployer';

const today = new Date();
const startDate = new Date(today.getFullYear(),today.getMonth(),today.getDate() + 15);
const monthLater = startDate.getDate()+30;
const limitDate = new Date(startDate.getFullYear(),startDate.getMonth(),monthLater);
const dateOptions = { year: 'numeric', month: 'long', day: 'numeric' };
var searchObject= {};

function compareDates (Date1, Date2) {
    Date1 = new Date(Date1);
    Date2 = new Date(Date2);
  
    Date1 = new Date(Date1.getFullYear(), Date1.getMonth(), Date1.getDate());
    Date2 = new Date(Date2.getFullYear(), Date2.getMonth(), Date2.getDate());
    
    if(Date1.getTime() - Date2.getTime() > 0){
        return 1;
    }else if(Date1.getTime() - Date2.getTime() < 0 ){
        return -1;
    }else if(Date1.getTime() - Date2.getTime() === 0 ){
        return 0;
    }else{
        return null;
    }
  }

const validate = values => {
    const errors = {}
    if (!values.contact_person_name) {
        errors.contact_person_name = 'Please enter contact person name'
    }
    if (!values.contact_email_address) {
        errors.contact_email_address = 'Please enter an email address'
    }
    if (!values.contact_mobile) {
        errors.contact_mobile = 'Please enter mobile number'
    }
    
    if (!values.jobroleid) {
        errors.jobroleid = 'Please select jobrole'
    }
    if (!values.statename) {
        errors.statename = 'Please select a state'
    }
    if (!values.districtname) {
        errors.districtname = 'Please select a city'
    }
    if (!values.required_candidates) {
        errors.required_candidates = 'Please enter number of candidates required'
    }
    // if (!values.required_candidates_future) {
    //     errors.required_candidates_future = 'Please enter number of candidates required'
    // }
    
    // if (!values.requirement_date) {
    //     errors.requirement_date = 'Please select required by date'
    // }
    // if (!values.salary) {
    //     errors.salary = 'Please enter salary provided'
    // }
    if (!values.salary_future) {
        errors.salary_future = 'Please enter salary provided'
    }
    if(values.salary &&  isNaN(Number(values.salary))){
        errors.salary = 'Please enter salary in number'
    }
    if(values.contact_mobile &&  isNaN(Number(values.contact_mobile))){
        errors.contact_mobile = 'Please enter valid mobile number'
    }
    if(values.contact_mobile && values.contact_mobile.length > 10){
        errors.contact_mobile = 'Mobile number should not exceed 10 digits'
    }
    if(values.contact_mobile && values.contact_mobile.length < 10){
        errors.contact_mobile = 'Mobile number should be 10 digits'
    }

    return errors;
}


const renderField = ({ input, label, type, placeholder, textarea, meta: { touched, error, warning, invalid } }) => {
    const textareaType = <textarea {...input} placeholder={label} type={type} className={`form-control ${touched && invalid ? 'has-danger' : ''}`} />;
    return <div >
        <div className={'form-group' + (error && touched ? ' has-error' : '')}>
            {/* <label>{label}</label> */}
            <div> {textarea ? textareaType : <input {...input} type={type} placeholder={placeholder} className="form-control" />}
                {touched && ((error && <div className="help-block">{error}</div>) || (warning && <span>{warning}</span>))}
            </div>

        </div>
    </div>
};

class EmployerCandidateSearch extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            submitted: false,
            // isData:0,
            updatedValue: this.props.mytest,
            selectedRequirementByDate: undefined,
            selectedBatchEndDate: undefined,
            pf: 'no',
            esic: 'no',
            bonus:'no',
            transportation:'no',
            accomodation: 'no',
            overtime: 'no',
            registerClick: 0,
            value: '',
            determinedRequestType: 'immediate',
            searchObjectState: {},
            searchButttonClicked: false,
            loadResults: true,
            isSentenceFormed: false,
            check:true,
            isFirstTimeLoad:0,
            page:1,
            limit_per_page:10,
            sortingColumn: "",
            sortingDirection: "",
            //Initial State..
            // data: this.props.items
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleRequirementByDateClick = this.handleRequirementByDateClick.bind(this);
        this.handleClear = this.handleClear.bind(this);
        this.checkValidation = this.checkValidation.bind(this);
        this.handleSelectChange = this.handleSelectChange.bind(this);
        this.handleCityChange = this.handleCityChange.bind(this);
        this.handleSearch = this.handleSearch.bind(this);
        this.toggleIsSearchButtonCliked = this.toggleIsSearchButtonCliked.bind(this);
        this.handlePageChange = this.handlePageChange.bind(this);
        this.handleSortAndLimit = this.handleSortAndLimit.bind(this);
        this.changePage = this.changePage.bind(this);
        
    }
    handleClick() {
        window.location.reload(true);
    }
    handleChange(event) {
        const { name, value } = event.target;

        const { user } = this.state;
        
        this.setState({
            user: {
                ...user,
                [name]: value
            },
            // loadResults: true
        });
        // console.log("event data change", event.target.name, event.target.value);
        // if (event.target.name === 'statename') {
        //     this.props.dispatch(userActions.getDistrict(value)); 

        // }
    
        //Additional Code for Ajax on District selection
        // if (event.target.name === 'districtname') {
        //     let selected_district = value;
        //     searchObject = {
        //         ...searchObject,
        //         selected_district
        //     }
        //     this.setState({
        //         searchObjectState: {...searchObject}
        //     });
        //     if(selected_district && selected_district > 0 && this.state.determinedRequestType === 'immediate'){
        //         // this.props.dispatch(userActions.getCandidatesForIHR(searchObject));
        //         this.props.dispatch(userActions.getTrainingPartnerSearchCandidate(searchObject,1,0));
        //     }
        // }
    }
    handleSelectChange(value) {
        // console.log('You\'ve selected:', value);
        if(!value){
            return false;
        }
        this.setState({ value: value });
        var name = 'jobrole_future';
        var jobrole_name = 'jobrole_future_name';
        const { user } = this.state;
        this.setState({
            user: {
                ...user,
                [name]: value.value,
                [jobrole_name]: value.label
            },
        });
        if(this.state.determinedRequestType === 'immediate') this.setState({loadResults:true});
        //Additional Code for Ajax on jobrole selection
        if(value && value.value){
            var selected_jobrole = value.value;
        }
        searchObject = {
            ...searchObject,
            selected_jobrole
        }
        
        // console.log(searchObject, "search");
        // console.log(searchObject, " searchObject ");
        if((!searchObject.selected_district || searchObject.selected_district === '0') && selected_jobrole === '0'){
            this.setState({
                loadResults: false
            });
        }
        if(searchObject.selected_district && searchObject.selected_district !== '0' && selected_jobrole === '0'){
            delete searchObject.selected_jobrole;
        }
        if(searchObject.selected_district === '0'){
            delete searchObject.selected_district;
        }
        if(selected_jobrole && this.state.determinedRequestType === 'immediate'){
            // this.props.dispatch(userActions.getCandidatesForIHR(searchObject));
            this.props.dispatch(userActions.getTrainingPartnerSearchCandidate(searchObject,1,0));
        }
        this.setState({
            searchObjectState: {...searchObject}
        });
    }
    handleCityChange(value) {
        // console.log('You\'ve selected:', value);
        // return;
        if(!value){
            return false;
        }
        this.setState({ cityValue: value.value });
        var name = 'city_future';
        var city_name = 'city_future_name';
        const { user } = this.state;
        this.setState({
            user: {
                ...user,
                [name]: value.value,
                [city_name]: value.label
            },
        });
        if(this.state.determinedRequestType === 'immediate') this.setState({loadResults:true});
        //Additional Code for Ajax on jobrole selection
        let selected_district = value.value;
        searchObject = {
            ...searchObject,
            selected_district
        }
        
        // console.log(searchObject, " searchObject ");
        if((!searchObject.selected_jobrole || searchObject.selected_jobrole === '0') && selected_district === '0'){
            this.setState({
                loadResults: false
            });
            
            // delete searchObject.selected_jobrole;
            // delete searchObject.selected_district;
        }
        if(searchObject.selected_jobrole && searchObject.selected_jobrole !== '0' && selected_district === '0'){
            delete searchObject.selected_district;
        }
        if(searchObject.selected_jobrole === '0'){
            delete searchObject.selected_jobrole;
        }
        if(selected_district && this.state.determinedRequestType === 'immediate'){
            // this.props.dispatch(userActions.getCandidatesForIHR(searchObject));
            this.props.dispatch(userActions.getTrainingPartnerSearchCandidate(searchObject,1,0));
        }
        this.setState({
            searchObjectState: {...searchObject}
        });
    }
    checkValidation(event) {
        //console.log("Register clicked.");
        this.setState({ registerClick: 1 });
    }
    handleSearch(childData){

        // console.log(childData, "CHILD DATA" );
        // return;
        var limit;
        if(this.state.searchObjectState && this.state.searchObjectState.selected_jobrole > 0 && this.state.searchObjectState.selected_district > 0){
            this.setState({ 
                searchButttonClicked: true,
                isSentenceFormed: true
            });
            if(childData){
                // console.log('in child data yes');
                limit = this.state.limit_per_page;
                if(childData.limitperpage){
                    searchObject = { ...childData};    
                }else{
                    searchObject = { ...childData, limitperpage:limit };
                }
                this.setState({
                    page:1,
                    check:false
                }, () => {
                    // console.log("HERE 0");
                    // console.log(searchObject, "searchObject");
                    this.props.dispatch(userActions.getTrainingPartnerSearchCandidate(searchObject,1,0));    
                });
                // return;
            }else{
                // console.log("HERE 1");
                var limit = this.state.limit_per_page;
                searchObject = { ...searchObject, limitperpage:limit };
                // console.log(searchObject, "searchObject");
                this.props.dispatch(userActions.getTrainingPartnerSearchCandidate(searchObject,this.state.page,0));
            }
            
            // else if(this.state.page > 1 && !childData){
            //     console.log("HERE 1");
            //     this.props.dispatch(userActions.getTrainingPartnerSearchCandidate(searchObject,this.state.page,0));
            // }
            // else{
            //     console.log("HERE 2");
            //     this.props.dispatch(userActions.getTrainingPartnerSearchCandidate(searchObject,this.state.page,0));
            // }
        }
    }
    handleSortAndLimit(sortingColumn,sortingDirection,limitperpage){
        // console.log(limitperpage, "LIMIT");
        // console.log(sortingColumn, "sortCol");
        // console.log(sortingDirection, "sortDir");
        var sorted = {};
        
        this.setState({
            limit_per_page: limitperpage
        }, () => {
            sorted = { ...searchObject, limitperpage, sortingColumn, sortingDirection};
            this.handleSearch(sorted);
        });
            
        // if(sortingColumn){
        //     this.setState({
        //         sortingColumn: sortingColumn
        //     },() => sorted = { ...searchObject, ...sorted, sortingColumn});
            
        // } 
        // if(sortingDirection){
        //     this.setState({
        //         sortingDirection: sortingDirection
        //     }, () => sorted = { ...searchObject, ...sorted, sortingDirection});
            
        // }
        // // ,"limitperpage":2,"sortingColumn":"training_status","sortingDirection":"ASC"}
        
    }
    changePage(nextPage){
        this.setState({
            page: nextPage
        },() => {
            this.handleSearch();
        });
    }
    handlePageChange(nextPage, checkCleared){
        // console.log(nextPage, " nextPage");
        if(checkCleared){
            this.setState({
                check:true
            })
        }
        this.setState({
            page:this.state.page+1
        }, () => {
            this.handleSearch();
        });
    }
    toggleIsSearchButtonCliked(){
        
        this.setState({ 
            searchButttonClicked: false,
            isSentenceFormed: false,
            page: 1
        });
        delete searchObject.age_group;
        delete searchObject.gender;
        delete searchObject.training_status;
        
    }
    
    componentDidMount() {
        // console.log("In did mount");
        this.props.dispatch(userActions.getImmidiateHiringRequestFormData(this.props.user.data[0].industrypartnerid, this.props.user.user_type));
    }
    
    handleSubmit(e) {
        // e.preventDefault();
        const { value, pf, esic, bonus, transportation, accomodation, overtime } = this.state;
        // const { dispatch } = this.props;
        const { user } = this.state;
        if(!user.required_candidates || !user.jobrole_future || user.jobrole_future === '0' || !user.city_future || user.city_future === '0' || !user.required_by_future){
            return;
        }
        if(this.state.determinedRequestType === 'immediate'){
            // console.log("here we are...");
            this.handleSearch();
            return;
        }
        
        // console.log(this.state.user, "THIS.STATE.USER");
        this.setState({ submitted: true });
        // console.log("submitted");
        var userInputkeywords = this.state.user;
        var jobroleid = value.value;
        //  var searchString = userInputkeywords.candidate_name.trim().toLowerCase();

        if (userInputkeywords !== undefined) {

            if (this.state.selectedRequirementByDate !== "" && this.state.selectedRequirementByDate !== undefined) {
                let requirement_by = this.state.selectedRequirementByDate;
                userInputkeywords = { requirement_by, ...userInputkeywords };
            }
       
                userInputkeywords = { pf, esic, bonus, transportation, accomodation, overtime, jobroleid, ...userInputkeywords };
            
            var industrypartnerid = JSON.parse(localStorage.getItem('user')).data[0].industrypartnerid;
            
            //Search with API
            userInputkeywords = { ...userInputkeywords, industrypartnerid };
            // console.log("User Search Keywords : ", userInputkeywords);
            // return;
            // this.props.dispatch(userActions.addUpdateFutureHiringRequest(userInputkeywords));
            this.props.dispatch(userActions.addUpdateImmidiateFutureHiringRequest(userInputkeywords, 1, 0, 0));
        }

    }
    handleClear(e) {
        e.preventDefault();
        window.location.reload(true);
    }
    handleRequirementByDateClick(day) {
        var name = 'required_by_future';
        const { user } = this.state;
        this.setState({
            user: {
                ...user,
                [name]: day
            }
        });
        if(compareDates(day, limitDate) > 0){
            // console.log("Date Selected that is equal or greater than 45 Days from today");
            this.setState({
                determinedRequestType : 'future',
                loadResults: false
            });
        }else{
            this.setState({
                determinedRequestType : 'immediate',
                loadResults: true
            });
        }
        this.setState({ selectedRequirementByDate: day });
    }

    render() {
        
        const { handleSubmit } = this.props;
        const { value, registerClick } = this.state;
        const renderColorSelector = ({ input, label, error_state, dynamic_values, meta: { touched, error } }) => (
            <div className={'form-group' + ((error && touched) || (error_state === 1) ? ' has-error' : '')}>
                {/* <label>{label}</label> */}
                <select {...input} className="form-control">
                    <option value="">{label}</option>
                    {dynamic_values}
                    ))}
              </select>
                {error_state === 1 && <div className="help-block">{error}</div>}
                {touched && error && <div className="help-block">{error}</div>}
            </div>

        )
        // var today = new Date();
        // var startDate = new Date(today.getFullYear(),today.getMonth(),today.getDate() + 15);
        // var monthLater = startDate.getDate()+30;
        // console.log(startDate, "startDate");
        // console.log(monthLater, "monthLater");
        // var limitDate = new Date(startDate.getFullYear(),startDate.getMonth(),monthLater);
        const dayPickerProps = {
            disabledDays:{ before: startDate,
                           /* after: limitDate */ },
            fromMonth: startDate,
            /* toMonth: limitDate, */
            month: startDate
        };
       
        
        
        if (!!this.props.searchformdatanew && this.props.searchformdatanew.status === true) {
            var allJobroles = this.props.searchformdatanew.data.jobrole[0];
            var final_allJobs = [{ value: '0', label: 'Select' }];
            allJobroles.map((value, index) => {
                return final_allJobs.push({ value: value.jobroleid, label: value.jobroleName });
                // return final_allJobs.push(<option value={value.jobroleid} key={index}>{value.jobroleName}</option>);
            })
            
        }

        if (!!this.props.searchformdatanew && this.props.searchformdatanew.status === true) {
            var allDistricts = this.props.searchformdatanew.data.allDistrict[0];
            var final_allDistricts = [{ value: '0', label: 'Select' }];
            allDistricts.map((value, index) => {
                return final_allDistricts.push({ value: value.cityid, label: value.cityName });
                // return final_allJobs.push(<option value={value.jobroleid} key={index}>{value.jobroleName}</option>);
            })
            
        }


        //format date
        let formatted_filter_date = '';
        if(!!this.state.user && typeof this.state.user.required_by_future !== 'undefined' && !!this.state.user.required_by_future) {
            let t = 0;
            formatted_filter_date = this.state.user.required_by_future.toLocaleDateString([],dateOptions);
            if(formatted_filter_date.indexOf(',') < 0) {
                formatted_filter_date = formatted_filter_date.replace(/ /g, function (match) {t++;return (t === 2) ? ", " : match; });
            }
        }

        return (
            <React.Fragment>
            {!this.state.isSentenceFormed
             ?
				<div className="card z-depth-1 no-hover search-outer theme-inverse">
                {/* <!-- Card Body --> */}
                <div className="card-body">
                      <h5 className="title fw-regular">
                        Immediate Hiring Request
                      </h5>
                      <form name="CandidateAdvanceSearch" id="CandidateAdvanceSearch" onSubmit={handleSubmit(this.handleSubmit)}>
                      <Row>
                        <Col md="4">
                        <FormGroup>
                            <span>I want</span>
                            {/* {this.state.determinedRequestType === 'immediate'
                             ? */}
                            <Field name="required_candidates" type="number" component={renderField} label="Candidates" placeholder="Number of Candidates" onChange={this.handleChange} />
                             {/* :
                            <Field name="required_candidates_future" type="number" component={renderField} label="Candidates" placeholder="Number of Candidates" onChange={this.handleChange} />
                            } */}
                            {/* <Input type="text" id="#" placeholder="Ex. 10" /> */}
                        </FormGroup>
                        </Col>
                        <Col md="4">
                          <FormGroup>
                          <span>Job Role</span>
                          <div className={'form-group' + ((!this.state.value || this.state.value.value === '0') ? ' has-error' : '')}>
                          <Select
                                className="basic-single"
                                classNamePrefix="select" 
                                // defaultValue={colourOptions[0]}
                                isDisabled={false}
                                isLoading={false}
                                isClearable={true}
                                isRtl={false}
                                isSearchable={true}
                                placeholder ="Enter and Select Jobrole"
                                // component={renderColorSelector}
                                // dynamic_values={final_allJobs}
                                name="jobroleid"
                                value={value}
                                onChange={this.handleSelectChange}
                                options={final_allJobs}
                            />
                            <div className="help-block">{(!this.state.value || this.state.value.value === '0') && registerClick === 1 ? 'Please select jobrole' : ''}</div>
                            
                          </div>
                         </FormGroup>
                         </Col>
                         <Col md="4">
                          <FormGroup>
                          <span>District</span>
                          <div className={'form-group' + ((!this.state.cityValue || this.state.cityValue === '0') ? ' has-error' : '')}>
                          <Select
                                className="basic-single"
                                classNamePrefix="select" 
                                // defaultValue={colourOptions[0]}
                                isDisabled={false}
                                isLoading={false}
                                isClearable={true}
                                isRtl={false}
                                // simpleValue
                                isSearchable={true}
                                placeholder ="Enter and Select District"
                                // component={renderColorSelector}
                                // dynamic_values={final_allJobs}
                                name="districtname"
                                value={this.state.cityValue}
                                onChange={this.handleCityChange}
                                options={final_allDistricts}
                            />
                            <div className="help-block">{(!this.state.cityValue || this.state.cityValue === '0') && registerClick === 1 ? 'Please select District' : ''}</div>
                            
                          </div>
                         </FormGroup>
                         </Col>
                         {/* <Col md="4">
                          <FormGroup>
                            <span>At State</span>
                              <Field name="statename" component={renderColorSelector} dynamic_values={final_allStates} onChange={this.handleChange} label="State" />
                          </FormGroup>
                          </Col> */}
                    
                      </Row>
                      <Row>
                        {/* <Col md="4">
                         <FormGroup>
                            <span>At District</span>
                            <Field name="districtname" component={renderColorSelector} dynamic_values={final_allCities} onChange={this.handleChange} label="District" />	
                          </FormGroup>
                        </Col> */}
                        
                        <Col md="4">
                          <FormGroup>
                                <span>By Date</span>
                                {/* <div className={'form-group' + (!this.state.selectedRequirementByDate ? ' has-error' : '')}> */}
                                <div className={'input-group date date-picker '+ (!this.state.selectedRequirementByDate ? ' has-error' : '') }   data-provide="datepicker">
                                        {/* <Input type="text"  name="end_date" /> */}
                                        <DayPickerInput
                                            format={"D-M-YYYY"}
                                            formatDate={formatDate}
                                            parseDate= {parseDate}
                                            onDayChange={this.handleRequirementByDateClick}
                                            dayPickerProps={dayPickerProps}
                                            placeholder={"Requirement Date"}
                                            name="requirement_date"
                                            inputProps={{ readOnly: true }}
                                            value={this.state.selectedRequirementByDate}
                                        />
                                        <div className="help-block">{!this.state.selectedRequirementByDate && registerClick === 1 ? 'Please enter required by Date' : ''}</div>
                                        <div className="input-group-addon">
                                            <i className="material-icons">date_range</i>
                                        </div>
                                </div>
                                {/* </div> */}
                          </FormGroup>
                          </Col>
                          <Col md="4">
                          <Form-group>
                            <span>Monthly salary will be INR </span>
                            {this.state.determinedRequestType === 'immediate'
                             ?
                            <Field name="salary" type="number" component={renderField} label="Salary" placeholder="&#8377; &#8377; &#8377;" onChange={this.handleChange} />
                             :
                            <Field name="salary_future" type="number" component={renderField} label="Salary" placeholder="&#8377; &#8377; &#8377;" onChange={this.handleChange} />
                            }
                          </Form-group>
                          </Col>
                        
                      </Row>
                     
                     
                      {/* <Row>
                        <Col xs="12">
                            <h5 className="title fw-regular mt-3 mb-2">
                                Contact Person Details
                            </h5>
                        </Col>
                        <Col md="4">
                               <FormGroup>
                                <span>Contact Person Name</span>
                                <Field name="contact_person_name" type="text" component={renderField} placeholder="Contact Person Name" onChange={this.handleChange} />
                                {/* <Input type="text"  id="#" placeholder="John Doe" /> 
                              </FormGroup>
                        </Col>
                        <Col md="4">
                              <FormGroup>
                                <span>Email Address</span>
                                <Field name="contact_email_address" type="email" component={renderField} placeholder="Email Address" onChange={this.handleChange} />
                                {/* <Input type="text" id="#" placeholder="John@Example.com" /> 
                              </FormGroup>
                        </Col>
                        <Col md="4">
                              <FormGroup>
                                <span>Mobile Number</span>
                                <Field name="contact_mobile" type="text" component={renderField} placeholder="Mobile Number" onChange={this.handleChange} />
                                {/* <Input type="text"  id="#" placeholder="+00 12345-45678" /> 
                              </FormGroup>
                        </Col>
                        <Col xs="12">
                            <FormGroup>
                                <button onClick={this.checkValidation} className="btn btn-secondary btn-raised card-link">Place Request</button>
                                
                                {/* <Button color="default" onClick={this.handleClear} outline>Reset</Button> 
                                {/* <a className="btn btn-secondary btn-raised card-link" title="Place Request" href="#" data-toggle="modal" data-target="#Request">Place Request</a> 
                            </FormGroup>
                        </Col>
                      </Row> */}
                    <Row>
                        <Col xs="12">
                                <FormGroup>
                                    {this.state.determinedRequestType === 'immediate'
                                     ?
                                        <div>
                                            <button onClick={this.checkValidation} className="btn btn-secondary btn-raised card-link">Search</button>

                                            <button color="default" className="ml-3 btn btn-grey btn-raised card-link" onClick={this.handleClear} >Reset</button> 
                                        </div>
                                     :
                                        <button onClick={this.checkValidation} className="btn btn-secondary btn-raised card-link">Place Future Request</button>
                                    }
                                </FormGroup>
                        </Col>
                    </Row>
                    </form>
                </div>
            </div>
             :
                <div>
                    <Card className="card z-depth-1 pb-3">
                        <CardHeader className="d-flex justify-content-between bg-transparent align-items-center border-bottom-0 card-header">
                            <div>
                                <h1>Want to hire {this.state.user.required_candidates} <u>{this.state.user.jobrole_future_name}</u> for <u>{this.state.user.city_future_name}</u> </h1>
                                <CardSubtitle className="subtitle ml-1">by {formatted_filter_date}</CardSubtitle>
                            </div>
                            <CardLink title="Edit" onClick={this.toggleIsSearchButtonCliked} className="btn-fab btn-fab-primary btn pb-2" ><i className="material-icons">edit</i></CardLink>
                        </CardHeader>
                    </Card>
                    <CandidateSortEmployer searchparams={this.state.searchObjectState} sortData={this.handleSearch} />
                </div>
            }
            {/* {console.log(this.state.sortingColumn, "sorting col")}
            {console.log(this.state.sortingDirection, "sorting dir")} */}
            {this.state.loadResults && !!this.props.list_data &&
                <CandidatesListView  list_data={this.props.list_data} isListDisplay={this.state.searchButttonClicked} loadListRecords={this.handleSortAndLimit} changePage={this.changePage} sorting={{"column":this.state.sortingColumn, "direction": this.state.sortingDirection}} limit_per_page={this.state.limit_per_page} />
                // <GetCandidatesListOnly searchparams={this.state.searchObjectState} isSearch={this.state.searchButttonClicked} sortData={this.handlePageChange} page={this.state.page||1} check={this.state.check}/>
            }
            </React.Fragment>
        );
    }
}

EmployerCandidateSearch = reduxForm({
    form: 'EmployerCandidateSearch',
    validate,
    renableReinitialize: true,
    // asyncBlurFields: [], //to remove validation, we must need to remove it from Blur fields...
})(EmployerCandidateSearch);

function mapStateToProps(state, ownProps) {
    // console.log("My statesProps",ownProps);
    const { user } = state.authentication;
    const { searchformdatanew } = state.searchformnew;
    const { items, searchitems, loading } = state.trainingpartners; //From Reducer - trainingpartners function ...

    let list_data = (!!searchitems) ? searchitems : items;
    // console.log(list_data, "list_data imp");

    return {
        list_data, loading, user, searchformdatanew
    };
}
const connectedEmployerCandidateSearch = connect(mapStateToProps)(EmployerCandidateSearch);
export { connectedEmployerCandidateSearch as EmployerCandidateSearch };
