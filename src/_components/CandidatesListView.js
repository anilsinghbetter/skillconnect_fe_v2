import React from 'react';
import { connect } from 'react-redux';
import { userActions } from '../_actions';
import { Card, Button, Row, Col, FormGroup, Label, Modal, ModalHeader, ModalBody, Table, Pagination, PaginationItem, PaginationLink } from 'reactstrap';
import { config } from '../config';
import candidatesSVG from '../assets/images/candidates_grey.svg';


class CandidatesListView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpen: false,
            isContactOpen: false,
            positionChecked: [],
        }
        //expect following props
        /*
        sorting = [
            {column: '', direction: ''}
        ]
        list_data
        limit_per_page
        isListDisplay

        loadListRecords(this.props.sorting.column, this.props.sorting.direction, this.props.limit_per_page)
        changePage(page_number);
        */
       

        this.sorting = (typeof this.props.sorting != 'undefined' && !!this.props.sorting) ? this.props.sorting : {column: '', direction: ''};
        this.sorting.column = (typeof this.sorting.column != 'undefined' && !!this.sorting.column) ? this.sorting.column : '';
        this.sorting.direction = (typeof this.sorting.direction != 'undefined' && !!this.sorting.direction && ['asc', 'desc'].indexOf(this.sorting.direction.toLowerCase()) > -1) ? this.sorting.direction.toLowerCase() : 'asc';
        this.invert_sorting_direction = (this.sorting.direction === 'asc') ? 'desc' : 'asc';
        this.default_sorting_direction = 'asc';


        this.toggleModal = this.toggleModal.bind(this);
        this.handleSort = this.handleSort.bind(this);
        this.viewMoreAPICall = this.viewMoreAPICall.bind(this);
        this.changePage = this.changePage.bind(this);
        this.handlePerPageRowsChange = this.handlePerPageRowsChange.bind(this);
        this.handleCheckboxSelection = this.handleCheckboxSelection.bind(this);
        this.handleSelectAll = this.handleSelectAll.bind(this);
        this.handleContactCandidate = this.handleContactCandidate.bind(this);
        this.toggleContactModal = this.toggleContactModal.bind(this);
    }

    createSelectItems(allrecords) {
        let items = [];    
        var limitToRecords = 100;
        if(allrecords > 0){
            limitToRecords = allrecords;
        }
        if(limitToRecords < 5){
            limitToRecords = 5;
        }
        if(limitToRecords > 5 && limitToRecords <= 10){
            limitToRecords = 10;
        }
        if(limitToRecords > 10 && limitToRecords <= 20){
            limitToRecords = 20;
        }
        if(limitToRecords > 20 && limitToRecords <= 30){
            limitToRecords = 30;
        }
        if(limitToRecords > 30 && limitToRecords <= 50){
            limitToRecords = 50;
        }
        if(limitToRecords > 50){
            limitToRecords = 100;
        }
        for (let i = 5; i <= limitToRecords; i++) {
            if(i == 5 || i == 10 || i == 20 || i == 30 || i == 50 || i == 100){
                items.push(<option key={i} value={i}>{i}</option>);
            }
        }
        return items;
    }

    viewMoreAPICall = (e, candidateid) => {
        this.props.dispatch(userActions.getCandidateViewMoreDetails(candidateid));
        this.toggleModal(e);
    }
    toggleModal = (e) => {
        console.log(this.state.CardValue, "CARD")
        this.setState({ isOpen: !this.state.isOpen });
        this.setState({ CardValue: e });
    }
    toggleContactModal = (e) => {
        this.setState({ isContactOpen: !this.state.isContactOpen });
        this.setState({ CardValue: e });
    }

    componentWillMount() {
        //prepare list of columns
        this.column_list = {
            
            full_name: {
                title: 'Name',
                sort_direction: (this.sorting.column === 'full_name') ? this.invert_sorting_direction : this.default_sorting_direction,
            },
            cityName: {
                title: 'City Name',
                sort_direction: (this.sorting.column === 'cityName') ? this.invert_sorting_direction : this.default_sorting_direction,
            },
            age: {
                title: 'Age',
                sort_direction: (this.sorting.column === 'age') ? this.invert_sorting_direction : this.default_sorting_direction,
            },
            sectorName: {
                title: 'Sector Name',
                sort_direction: (this.sorting.column === 'sectorName') ? this.invert_sorting_direction : this.default_sorting_direction,
            },
            training_status: {
                title: 'Training Status',
                sort_direction: (this.sorting.column === 'training_status') ? this.invert_sorting_direction : this.default_sorting_direction,
            },
        };
    }


    handleSort = (sort_column_name, sort_direction) => {//console.log('test = ', sort_column_name, sort_direction);
        let allowed_columns = ['full_name', 'jobroleName', 'cityName', 'age', 'training_center_name', 'sectorName', 'training_status']

        if(allowed_columns.indexOf(sort_column_name) > -1 && ['asc', 'desc'].indexOf(sort_direction) > -1) {
            // console.log(sort_direction, "DIR");
            // console.log(sort_column_name, "COL");
            // console.log(this.column_list[sort_column_name].sort_direction, "FIND");
            this.column_list[sort_column_name].sort_direction === 'asc' ? this.column_list[sort_column_name].sort_direction = 'desc' : this.column_list[sort_column_name].sort_direction = 'asc';
            this.props.loadListRecords(sort_column_name, sort_direction, this.props.limit_per_page);
        }
        return false;
    }

    handlePerPageRowsChange(event) {
        let limitperpage = event.target.value;//console.log('limitperpage = ', limitperpage);
        this.props.loadListRecords(this.sorting.column, this.sorting.direction, limitperpage);
        return false;
    }

    changePage(page_number) {
        //console.log('page_number = ', page_number);
        this.props.changePage(page_number);
        return false;
    }

    handleCheckboxSelection(e) {
        // current array of options
        // console.log(e.target.value, "e.target.value");
        const checkboxArray = this.state.positionChecked;
        let index;

        // check if the check box is checked or unchecked
        if (e.target.checked) {
            // add the numerical value of the checkbox to options array
            checkboxArray.push(+e.target.value)
        }
        else if (this.state.selectAll === true) {
            this.setState({ selectAll: false })
            index = checkboxArray.indexOf(+e.target.value)
            checkboxArray.splice(index, 1)
        }
        else {
            // or remove the value from the unchecked checkbox from the array
            index = checkboxArray.indexOf(+e.target.value)
            checkboxArray.splice(index, 1)
        }
        // console.log('options checked', checkboxArray);
        // update the state with the new array of options
        this.setState({ positionChecked: checkboxArray })

    }
    handleSelectAll(e) {
        this.setState({ selectAll: e.target.checked },/*  () => console.log('selected all', this.state.selectAll) */);
        var rowState = [];
        if (e.target.checked) {
            if (!!this.props.list_data && this.props.list_data.data !== undefined && this.props.list_data.data.length > 0) {
                for (var i = 0; i < this.props.list_data.data.length; i++) {
                    rowState.push(this.props.list_data.data[i].candidateid);
                }
            }
        }
        this.setState({ positionChecked: rowState })
    };
    handleContactCandidate(){
        console.log(this.state.positionChecked, "positionChecked");
        this.toggleContactModal();
    }

    render() {
        const stylesforpaginate = {
            cursor: 'initial',
            background: 'none',
            border: 'none',
            color : '#455056'
        };
        const stylesforcursor = {
            cursor: 'pointer'
        };
        const csvStyle = {
            display : 'none'
        }
        var loaderClass;
        if (this.props.loading === true || this.props.loadingviewmore) {
            loaderClass = 'loading';
        }
        const styles = {
            top: '115px',
            left: '60px'
        };
        var recordFound = '';




        if (!!this.props.list_data)
        {
            var list_data_structure = [];
            var list_data = [];
            var data_obj;
            
            if (this.props.list_data !== undefined) {
                data_obj = this.props.list_data;
            } else  {
                data_obj = [];
            }
            if (data_obj.data.length === 0) {
                recordFound = 0;
            } else {
                recordFound = 1;
            }
            
            //Pagination...
            var totalpages = data_obj.paginationOptions.totalPages;
            var currentpage = data_obj.paginationOptions.curPage;
            var totalRecords = data_obj.paginationOptions.totalcount;
            var perPageRecords = data_obj.paginationOptions.perPage;

            var current = currentpage,
                last = totalpages,
                delta = 2,
                left = current - delta,
                right = current + delta + 1,
                rangeWithDots = [],
                l;

            var finalPaginate = [];
            var paginateArray = [];
            for (var i = 1; i <= totalpages; i++) {
                if (i == 1 || i == last || i >= left && i < right) {
                    paginateArray.push(i);
                }
            }
            for (let i of paginateArray) {
                if (l) {
                    if (i - l === 2) {
                        rangeWithDots.push(l + 1);
                    } else if (i - l !== 1) {
                        rangeWithDots.push('...');
                    }
                }
                rangeWithDots.push(i);
                l = i;
            }
            rangeWithDots.map((value, j) => {
                var classnam = '';
                if(value === currentpage) {
                    classnam = 'active';
                }
                return finalPaginate.push(<PaginationItem className = {classnam} key={j}>
                                            {value === currentpage &&  <PaginationLink>{value}</PaginationLink> }
                                            {value === '...' && <PaginationLink style={stylesforpaginate}>{value}</PaginationLink>}
                                            {value !== currentpage && value !== '...' &&
                                            <PaginationLink onClick={() => this.changePage(value)}>
                                                {value}
                                            </PaginationLink>
                                            }
                                        </PaginationItem>);
            });

            if ('data' in data_obj) {
                list_data = data_obj.data;
            } else {
                list_data = data_obj;
            }
            
            list_data.map((value, index) => {
                var TrainingStatus = list_data[index].training_status ? list_data[index].training_status.charAt(0).toUpperCase() + list_data[index].training_status.substr(1) : '-';
                switch (TrainingStatus) {
                    case 'Duplicateenrollment':
                        TrainingStatus = 'Duplicate Enrollment';
                        break;
                    default:
                }

                let columns = [];
                for (let column_name in this.column_list) {
                    columns.push(<td key={column_name}>{list_data[index][column_name] !== undefined ? list_data[index][column_name] : '-'}</td>);
                }

                return list_data_structure.push(
                    <tr key={index}>
                        {/* <td>
                            <label className="custom-checkbox">
                                <input
                                    onChange={this.handleCheckboxSelection}
                                    checked={(this.state.positionChecked.some(i => i === list_data[index].candidateid))}
                                    key={index}
                                    type="checkbox"
                                    value={list_data[index].candidateid}
                                />
                                <span className="checkmark"></span>
                            </label>
                        </td> */}
                        {columns}
                        {/* <td>{list_data[index].full_name !== undefined ? list_data[index].full_name : '-'}</td>
                        <td>{list_data[index].cityName !== undefined ? list_data[index].cityName : '-'}</td>
                        <td>{list_data[index].age !== undefined ? list_data[index].age : '-'}</td>
                        <td>{list_data[index].sectorName !== undefined ? list_data[index].sectorName : '-'}</td>
                        <td>{list_data[index].TrainingStatus !== undefined ? list_data[index].TrainingStatus : '-'}</td> */}
                        <td>
                            <Button color="link" size="sm" className="btn-fab btn-fab-primary" onClick={() => this.viewMoreAPICall(index, list_data[index].candidateid)}>
                                <i className="material-icons md-24 md-dark">
                                visibility
                                </i>
                            </Button>
                        </td>
                    </tr>
                );
            });
        }

        var contactCandidateModal = (
            <Modal backdrop={false} centered size="lg" isOpen={this.state.isContactOpen} toggle={() => this.toggleContactModal()}>
                <ModalHeader toggle={() => this.toggleContactModal()} className="border-bottom-0 modal-header-shadow align-items-center">
                    <div className="section-title">
                        <h5 className="title modal-title">Contact Candidate(SMS/WhatsApp)</h5>                                 
                    </div>
                </ModalHeader>
                <ModalBody className="details-view">
                    {"Hello Candidate," + this.state.positionChecked.join(', ') + " Digicorp is looking to hire " }
                </ModalBody>
            </Modal>
        )
        

        //#region 'View more' Details Modal
        if (!!this.props.candidate_view_more_details && this.state.CardValue !== undefined)
        {
            var candidatedetaildata = this.props.candidate_view_more_details.data[0];
            var mymodaval = this.state.CardValue;
            var tel = candidatedetaildata.phone ? "tel:" + candidatedetaildata.phone : ''
            var mailTo = candidatedetaildata.email_address ? "mailto:" + candidatedetaildata.email_address : ''
            
            if(candidatedetaildata.permanent_cityName){
                candidatedetaildata.permanent_address +=  ', ' + candidatedetaildata.permanent_cityName;
            }
            if(candidatedetaildata.permanent_stateName){
                candidatedetaildata.permanent_address += ', '  + candidatedetaildata.permanent_stateName;
            }
            if(candidatedetaildata.permanent_pincode){
                candidatedetaildata.permanent_address += ', '  + candidatedetaildata.permanent_pincode;
            }
            if(candidatedetaildata.present_cityName){
                candidatedetaildata.present_address +=  ', ' + candidatedetaildata.present_cityName;
            }
            if(candidatedetaildata.present_stateName){
                candidatedetaildata.present_address += ', '  + candidatedetaildata.present_stateName;
            }
            if(candidatedetaildata.present_pincode){
                candidatedetaildata.present_address += ', '  + candidatedetaildata.present_pincode;
            }
            var allcandidatesModalDetails = (

                <Modal backdrop={false} centered size="lg" isOpen={this.state.isOpen} toggle={() => this.toggleModal(mymodaval)}>
                    <ModalHeader toggle={() => this.toggleModal(mymodaval)} className="border-bottom-0 modal-header-shadow align-items-center">
                        <div className="section-title">
                            <h5 className="title modal-title">{candidatedetaildata.first_name}'s Details - {candidatedetaildata.is_skilled ? candidatedetaildata.is_skilled : ''}</h5>                                 
                        </div>
                    </ModalHeader>
                    <ModalBody className="details-view">
                        <Row>
                            <Col xs="12">
                                <h5 className="title modal-view-title">Personal Details</h5>
                            </Col>
                            <Col xs="12" md="6" xl="4">
                                <label className="subtitle">First Name</label>
                                <p className="title">{candidatedetaildata.first_name ? candidatedetaildata.first_name : 'NA'}</p>
                            </Col>
                            <Col xs="12" md="6" xl="4">
                                <label className="subtitle">Middle Name</label>
                                <p className="title">{candidatedetaildata.middle_name ? candidatedetaildata.middle_name : 'NA'}</p>
                            </Col>
                            <Col xs="12" md="6" xl="4">
                                <label className="subtitle">Last Name</label>
                                <p className="title">{candidatedetaildata.last_name ? candidatedetaildata.last_name : 'NA'}</p>
                            </Col>
                            <Col xs="12" md="6" xl="4">
                                <label className="subtitle">Aadhaar No.</label>
                                {/* <p className="title">{candidatedetaildata.aadhar_card_no ? candidatedetaildata.aadhar_card_no : 'NA'}</p> */}
                                <p className="title">{candidatedetaildata.aadhar_card_no ? '************' : 'NA'}</p> 
                            </Col>
                            <Col xs="12" md="6" xl="4">
                                <label className="subtitle">Alternate ID Type</label>
                                <p className="title">{candidatedetaildata.alternateidtype ? candidatedetaildata.alternateidtype : 'NA'}</p>
                            </Col>
                            <Col xs="12" md="6" xl="4">
                                <label className="subtitle">Alternate ID No.</label>
                                {/* <p className="title">{candidatedetaildata.altername_id_no ? candidatedetaildata.altername_id_no : 'NA'}</p> */}
                                <p className="title">{candidatedetaildata.altername_id_no ? '************' : 'NA'}</p> 
                            </Col>
                            {/* <Col xs="12" md="6" xl="4">
                                <label className="subtitle">Aadhaar Enrollment ID</label>
                                <p className="title">{candidatedetaildata.aadhaar_enrollment_id ? candidatedetaildata.aadhaar_enrollment_id : 'NA'}</p>
                            </Col> */}
                            <Col xs="12" md="6" xl="4">
                                <label className="subtitle">Mobile No</label>
                                {/* <p className="title"> */}
                                    {/* {tel === '' ? 'NA' :
                                        <a href={tel} title={candidatedetaildata.phone}>{candidatedetaildata.phone}</a>
                                    }  */}
                                    <p className="title">{candidatedetaildata.phone ? '**********' : 'NA'}</p> 
                                {/* </p> */}
                            </Col>
                            <Col xs="12" md="6" xl="4">
                                <label className="subtitle">Email Address</label>
                                {/* <p className="title"> */}
                                    {/* {mailTo === '' ? 'NA' :
                                        <a href={mailTo} title={candidatedetaildata.email_address}>{candidatedetaildata.email_address}</a>
                                    } */}
                                    <p className="title">{candidatedetaildata.email_address ? '********************' : 'NA'}</p> 
                                {/* </p> */}
                            </Col>
                            <Col xs="12" md="6" xl="4">
                                <label className="subtitle">Date Of Birth</label>
                                <p className="title">{candidatedetaildata.dob ? candidatedetaildata.dob : 'NA'}</p>
                            </Col>
                            <Col xs="12" md="6" xl="4">
                                <label className="subtitle">Gender</label>
                                <p className="title">{candidatedetaildata.gender ? candidatedetaildata.gender : 'NA'}</p>
                            </Col>
                            {/* <Col xs="12" md="6" xl="4">
                                <label className="subtitle">State</label>
                                <p className="title">{candidatedetaildata.stateName ? candidatedetaildata.stateName : 'NA'}</p>
                            </Col>
                            <Col xs="12" md="6" xl="4">
                                <label className="subtitle">District</label>
                                <p className="title">{candidatedetaildata.cityName ? candidatedetaildata.cityName : 'NA'}</p>
                            </Col> */}
                            {/* <Col xs="12" md="6" xl="4">
                                <label className="subtitle">Pincode</label>
                                <p className="title">{candidatedetaildata.pincode ? candidatedetaildata.pincode : 'NA'}</p>
                            </Col> */}
                            {/* <Col xs="12" md="6" xl="4">
                                <label className="subtitle ">Address</label>
                                <p className="title text-truncate" title={candidatedetaildata.address ? candidatedetaildata.address : 'NA'}>{candidatedetaildata.address ? candidatedetaildata.address : 'NA'}</p>
                            </Col> */}
                            <Col xs="12" md="6" xl="4">
                                <label className="subtitle ">Permanent Address</label>
                                <p className="title text-truncate" title={candidatedetaildata.permanent_address ? candidatedetaildata.permanent_address : 'NA'}>{candidatedetaildata.permanent_address ? candidatedetaildata.permanent_address : 'NA'}</p>
                            </Col>
                            
                            <Col xs="12" md="6" xl="4">
                                <label className="subtitle ">Present Address</label>
                                <p className="title text-truncate" title={candidatedetaildata.present_address ? candidatedetaildata.present_address : 'NA'}>{candidatedetaildata.present_address ? candidatedetaildata.present_address : 'NA'}</p>
                            </Col>
                            <Col xs="12" md="6" xl="4">
                                <label className="subtitle">Age</label>
                                <p className="title">{candidatedetaildata.age ? candidatedetaildata.age : 'NA'}</p>
                            </Col>
                            {/* {candidatedetaildata.is_skilled === 'Skilled' &&  */}
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">Candidate Id</label>
                                    <p className="title">{candidatedetaildata.candidate_id ? candidatedetaildata.candidate_id : 'NA'}</p>
                                </Col>
                            {/* } */}
                        </Row>
                        <hr className="modal-data-seperator" />
                        <Row>

                            <Col xs="12">
                                <h5 className="title modal-view-title">Education Details</h5>
                            </Col>
                            <Col xs="12" md="6" xl="8">
                                {/* <label className="subtitle">EDU </label> */}
                                {/* {candidatedetaildata.education_details ? candidatedetaildata.education_details = candidatedetaildata.education_details.split(',').join(', ') : candidatedetaildata.education_details } */}
                                <p className="title">{candidatedetaildata.education_details ? candidatedetaildata.education_details = candidatedetaildata.education_details.split(',').join(', ') : 'NA'}</p>
                            </Col>
                            {/* <Col xs="12" md="6" xl="8">
                                <label className="subtitle">Skills </label>
                                <p className="title">{candidatedetaildata.is_skilled ? candidatedetaildata.is_skilled : 'NA'}</p>
                            </Col> */}

                        </Row>
                        <hr className="modal-data-seperator" />
                        <Row>

                            <Col xs="12">
                                <h5 className="title modal-view-title">Professional Details</h5>
                            </Col>
                            <Col xs="12" md="6" xl="4">
                                <label className="subtitle">Experience in Years</label>
                                <p className="title">{candidatedetaildata.experience_in_years ? candidatedetaildata.experience_in_years : 'NA'}</p>
                            </Col>
                            <Col xs="12" md="6" xl="4">
                                <label className="subtitle">Sector</label>
                                <p className="title">{candidatedetaildata.sectorName ? candidatedetaildata.sectorName : 'NA'}</p>
                            </Col>
                            <Col xs="12" md="6" xl="4">
                                <label className="subtitle">Job Role</label>
                                <p className="title">{candidatedetaildata.jobroleName ? candidatedetaildata.jobroleName : 'NA'}</p>
                            </Col>
                        </Row>
                            {/* {candidatedetaildata.is_skilled === 'Skilled' &&  */}
                                <div className="view_for_skilled">
                                    <Row>
                                        <Col xs="12" md="6" xl="4">
                                            <label className="subtitle">Training Type</label>
                                            <p className="title">{candidatedetaildata.training_type ? candidatedetaildata.training_type : 'NA'}</p>
                                        </Col>
                                        <Col xs="12" md="6" xl="4">
                                            <label className="subtitle">SDMS/NSDC Enrollment No</label>
                                            <p className="title">{candidatedetaildata.sdmsnsdc_enrollment_number ? candidatedetaildata.sdmsnsdc_enrollment_number : 'NA'}</p>
                                        </Col>
                                        <Col xs="12" md="6" xl="4">
                                            <label className="subtitle">Training Status</label>
                                            <p className="title">{candidatedetaildata.training_status ? candidatedetaildata.training_status : 'NA'}</p>
                                        </Col>
                                        {candidatedetaildata.training_status === 'Ongoing' && 
                                            <Col xs="12" md="6" xl="4">
                                                <label className="subtitle">Pass Out date</label>
                                                <p className="title">{candidatedetaildata.pass_out_date ? candidatedetaildata.pass_out_date : 'NA'}</p>
                                            </Col>
                                        }
                                        
                                        <Col xs="12" md="6" xl="4">
                                            <label className="subtitle">Attendance Percent</label>
                                            <p className="title">{candidatedetaildata.attendence_percentage ? candidatedetaildata.attendence_percentage : 'NA'}</p>
                                        </Col>
                                        <Col xs="12" md="6" xl="4">
                                            <label className="subtitle">Placement Status</label>
                                            <p className="title">{candidatedetaildata.placement_status ? candidatedetaildata.placement_status : 'NA'}</p>
                                        </Col>
                                        {candidatedetaildata.placement_status === 'Selected' &&
                                            <Col xs="12" md="6" xl="4">
                                                <label className="subtitle">Employer Name</label>
                                                <p className="title">{candidatedetaildata.employer_name ? candidatedetaildata.employer_name : 'NA'}</p>
                                            </Col>
                                        }
                                        
                                        <Col xs="12" md="6" xl="4">
                                            <label className="subtitle">Employment Type</label>
                                            <p className="title">{candidatedetaildata.employment_type ? candidatedetaildata.employment_type : 'NA'}</p>
                                        </Col>
                                        <Col xs="12" md="6" xl="4">
                                            <label className="subtitle">Assessment Score</label>
                                            <p className="title">{candidatedetaildata.assessment_score ? candidatedetaildata.assessment_score : 'NA'}</p>
                                        </Col>
                                        <Col xs="12" md="6" xl="4">
                                            <label className="subtitle">Max Assessment Score</label>
                                            <p className="title">{candidatedetaildata.max_assessment_score ? candidatedetaildata.max_assessment_score : 'NA'}</p>
                                        </Col>
                                        {/* <Col xs="12" md="6" xl="4">
                                            <label className="subtitle">Assessment Date</label>
                                            <p className="title">{candidatedetaildata.assessment_date ? candidatedetaildata.assessment_date : 'NA'}</p>
                                        </Col> */}
                                        
                                        <Col xs="12" md="6" xl="4">
                                            <label className="subtitle">Training Center</label>
                                            <p className="title">{candidatedetaildata.trainingcenter_name ? candidatedetaildata.trainingcenter_name : 'NA'}</p>
                                        </Col>
                                        <Col xs="12" md="6" xl="4">
                                            <label className="subtitle">Batch ID</label>
                                            <p className="title">{candidatedetaildata.batch_id ? candidatedetaildata.batch_id : 'NA'}</p>
                                        </Col>
                                        {/* <Col xs="12" md="6" xl="4">
                                            <label className="subtitle">Batch Start Date</label>
                                            <p className="title">{candidatedetaildata.batch_start_date ? candidatedetaildata.batch_start_date : 'NA'}</p>
                                        </Col>
                                        <Col xs="12" md="6" xl="4">
                                            <label className="subtitle">Batch End Date</label>
                                            <p className="title">{candidatedetaildata.batch_end_date ? candidatedetaildata.batch_end_date : 'NA'}</p>
                                        </Col> */}
                                        <Col xs="12" md="6" xl="4">
                                            <label className="subtitle">Scheme</label>
                                            <p className="title">{candidatedetaildata.schemeType ? candidatedetaildata.schemeType : 'NA'}</p>
                                        </Col>
                                        {candidatedetaildata.schemeType === 'State Skill Development Mission Scheme'
                                            ?
                                            <Col xs="12" md="6" xl="4">
                                                <label className="subtitle">State Ministry</label>
                                                <p className="title">{candidatedetaildata.stateministry ? candidatedetaildata.stateministry : 'NA'}</p>
                                            </Col>
                                            :
                                            <Col xs="12" md="6" xl="4">
                                                <label className="subtitle">Central Ministry</label>
                                                <p className="title">{candidatedetaildata.centralministry ? candidatedetaildata.centralministry : 'NA'}</p>
                                            </Col>
                                        }
                                        <Col xs="12" md="6" xl="4">
                                            <label className="subtitle">Is Willing To Relocate </label>
                                            <p className="title">{candidatedetaildata.willing_to_relocate ? candidatedetaildata.willing_to_relocate : 'NA'}</p>
                                        </Col>
                                    </Row>
                                </div>
                            {/* } */}
                        {/* </Row> */}
                        <hr className="modal-data-seperator" />
                        {(!!candidatedetaildata.first_month_payslip || !!candidatedetaildata.second_month_payslip || !!candidatedetaildata.third_month_payslip || !!candidatedetaildata.appointment_letter) 
                        ?
                        <div>
                            <Row>
                                <Col xs="12">
                                    <h5 className="title modal-view-title">Uploaded Documents</h5>
                                </Col>
                                {candidatedetaildata.first_month_payslip &&
                                    <Col xs="12" md="6" xl="4">
                                        <label className="subtitle">1<sup>st</sup> Month Pay Slip </label>
                                        <p className="title" title="Download File">
                                            <a href={config.API_HOST_URL + 'candidate_uploads/'+ candidatedetaildata.candidateid +'/1/'+candidatedetaildata.first_month_payslip}> {candidatedetaildata.first_month_payslip} </a>
                                        </p>
                                    </Col>
                                }
                                {candidatedetaildata.second_month_payslip &&
                                    <Col xs="12" md="6" xl="4">
                                        <label className="subtitle">2<sup>nd</sup> Month Pay Slip </label>
                                        <p className="title" title="Download File">
                                            <a href={config.API_HOST_URL + 'candidate_uploads/'+ candidatedetaildata.candidateid +'/2/'+candidatedetaildata.second_month_payslip}> {candidatedetaildata.second_month_payslip} </a>
                                        </p>
                                    </Col>
                                }
                                {candidatedetaildata.third_month_payslip &&
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">3<sup>rd</sup> Month Pay Slip </label>
                                    <p className="title" title="Download File">
                                        <a href={config.API_HOST_URL + 'candidate_uploads/'+ candidatedetaildata.candidateid +'/3/'+candidatedetaildata.third_month_payslip}> {candidatedetaildata.third_month_payslip} </a>
                                    </p>
                                </Col>
                                }
                                {/* {candidatedetaildata.is_skilled === 'Skilled' && candidatedetaildata.appointment_letter && */}
                                {candidatedetaildata.appointment_letter &&
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">Appointment Letter </label>
                                    <p className="title" title="Download File">
                                        <a href={config.API_HOST_URL + 'candidate_uploads/'+ candidatedetaildata.candidateid +'/appointment_letter/'+candidatedetaildata.appointment_letter}> {candidatedetaildata.appointment_letter} </a>
                                    </p>
                                </Col>
                                }
                            </Row>
                        </div> 
                        :
                        <Row>
                            <Col xs="12">
                                <h5 className="title modal-view-title">No Documents Uploaded</h5>
                            </Col>
                        </Row>
                        }
                    </ModalBody>
                </Modal>
            );
        }
        //#endregion


        let rows = [];
        for (let column_name in this.column_list) {
            let column_obj = this.column_list[column_name];
            rows.push(<td key={column_name}><a /* title={column_obj.sort_direction} */ style={stylesforcursor} onClick={() => this.handleSort(column_name, column_obj.sort_direction)} className={column_obj.sort_direction}>{column_obj.title}</a></td>);
        }
        //this.column_list.map((column_obj, column_name) => <th><a title={column_obj.sort_direction} style={stylesforcursor} onClick={() => this.handleSort(column_name, column_obj.sort_direction)} className={column_obj.sort_direction}>{column_obj.title}</a></th> )

    
        return (
                <div className="content-wrapper user-flow">

                    {/* {contactCandidateModal} */}
                    <div className={loaderClass} style={styles}></div>
                    {recordFound === 0 &&
                        <div className="d-flex justify-content-center align-items-center error-page">
                            <div className="text-center">
                                <div className="img-wrapper mb-4">
                                    <div className="circle-xl user-avatar">
                                        <img src={candidatesSVG} alt="candidates" />
                                    </div>
                                </div>
                                <h1 className="h2 fw-regular mb-0">No Candidate(s) Found!</h1>
                            </div>
                        </div>
                    }

                    {recordFound === 1 &&
                        <div>
                            <FormGroup className="mr-2 mr-md-4 justify-content-between d-flex">
                                <h3 className="page-title mb-3"> Total Number Of Candidates are : {totalRecords.toLocaleString('en-IN')}</h3>
                                {this.state.positionChecked && this.state.positionChecked.length>0 &&
                                    <Button className="align-right" color="secondary" onClick={this.handleContactCandidate}>Contact Candidate(s)</Button>
                                }
                            </FormGroup>
                            
                            {this.props.isListDisplay && 
                            <React.Fragment>
                                <Card className="content-card assets-table">
                                    <Table bordered hover responsive striped className="table-btn mb-0">
                                        <thead>
                                            <tr>    
                                                {/* <th>
                                                    <label className="custom-checkbox ">
                                                        <input
                                                        onChange={this.handleSelectAll}
                                                        checked={this.state.selectAll}
                                                        key={"SelectAll"}
                                                        type="checkbox"
                                                        value={"All"}
                                                        />
                                                        <span className="checkmark">Select All</span>
                                                    </label>
                                                </th> */}
                                                {rows}
                                                {/* <th><a title={(this.props.sorting.column === 'full_name') ? '' : ''} style={stylesforcursor} onClick={() => this.handleSort('full_name', this.state.invertDirectionBrand)} className={this.state.invertDirectionBrand}>Name</a></th>
                                                <th><a title={this.state.invertDirectionType} style={stylesforcursor} onClick={() => this.handleSort('cityName',this.state.invertDirectionType)} className={this.state.invertDirectionType}>City Name</a></th>
                                                <th><a title={this.state.invertDirectionModel} style={stylesforcursor} onClick={() => this.handleSort('age',this.state.invertDirectionModel)} className={this.state.invertDirectionModel}>Age</a></th>
                                                <th><a title={this.state.invertDirectionDate} style={stylesforcursor} onClick={() => this.handleSort('sectorName',this.state.invertDirectionDate)} className={this.state.invertDirectionDate}>Sector</a></th>
                                                <th><a title={this.state.invertDirectionAddress} style={stylesforcursor} onClick={() => this.handleSort('training_status',this.state.invertDirectionAddress)} className={this.state.invertDirectionAddress}>Training Status</a></th> */}
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                    <tbody>
                                        {list_data_structure}
                                    </tbody>
                                    </Table>
                                </Card>



                                <div className="d-flex align-items-center justify-content-end mb-3">
                                    {/*  <p className="mb-0 mr-3">
                                        Showing 1 to {perPageRecords} of {totalRecords} entries
                                        </p> */}
                                    <FormGroup className="d-flex align-items-center mb-0 mr-3">
                                        <Label for="rows" className="mr-2">Rows</Label>
                                        <select value={this.props.limit_per_page} defaultValue="20" onChange={this.handlePerPageRowsChange} className="form-control">
                                            {/* <option value="5">5</option>
                                            <option value="10">10</option>
                                            <option value="20">20</option>
                                            <option value="30">30</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option> */}
                                            {this.createSelectItems(totalRecords)}
                                        </select>
                                    </FormGroup>
                                    <Pagination className="assets-pagination">
                                        {currentpage > 1 && 
                                        <PaginationItem>
                                            <PaginationLink previous onClick={() => this.changePage(currentpage - 1)} className="prev" />
                                        </PaginationItem>
                                        }
                                        {finalPaginate}
                                        {totalpages !== currentpage &&
                                            <PaginationItem>
                                                <PaginationLink next onClick={() => this.changePage(currentpage + 1)} className="next"/>
                                            </PaginationItem>
                                        }
                                    </Pagination>
                                </div>
                            </React.Fragment>
                            }
                        </div>
                    }
                    
                    
                    {allcandidatesModalDetails}

                </div>
            );
        }
    }

function mapStateToProps(state) {
    const { user } = state.authentication;
    // const { items, searchitems, loading } = state.trainingpartners; //From Reducer - trainingpartners function ...
    const { candidate_view_more_details, loadingviewmore } = state.getcandidateviewmoredetails; //From Reducer - candidatedetails function ...
    
    // let list_data = (!!searchitems) ? searchitems : items;
    // let loader = !!loading ? loading : loadingviewmore

    return {
        user, candidate_view_more_details, loadingviewmore
    };
}

const component_var = connect(mapStateToProps)(CandidatesListView);
export { component_var as CandidatesListView };