import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { userActions } from '../_actions';
import { Header } from '../_components/Header';
import { Footer } from '../_components/Footer';
import { Sidebar } from '../_components/Sidebar';
import { CandidateSearch } from '../_components/CandidateSearch';
import { Button, ButtonDropdown, DropdownItem, DropdownMenu, DropdownToggle, Breadcrumb, BreadcrumbItem, Row, Col, Card, CardHeader, CardSubtitle, CardTitle, CardLink, ListGroup, ListGroupItem, Modal, ModalBody, ModalHeader } from 'reactstrap';
import { history } from '../_helpers';
import candidatesSVG from '../assets/images/candidates_grey.svg';
import { config } from '../config';

class GetCandidates extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isOpen: false,
            CardValue: undefined,
            delCardValue: undefined,
            mydata: "",
            page: 1,
            pagePrev: 0,
            mycustomVal: 1,
            showmore: 0,
            isDeleted: 0,
            searchparams: undefined,
            fromsearch: 0
        };
        this.viewMoreAPICall = this.viewMoreAPICall.bind(this);
        this.toggleModal = this.toggleModal.bind(this);
        this.toggle = this.toggle.bind(this);
        this.toggleModalforDelete = this.toggleModalforDelete.bind(this);
        this.closeModalBox = this.closeModalBox.bind(this);
        this.deleteCandidate = this.deleteCandidate.bind(this);
        // this.displaySearchParams = this.displaySearchParams.bind(this);
    }

    closeModalBox() {
        this.setState({ isOpen: !this.state.isOpen });
    }
    toggle() {
        this.setState({
            dropdownOpen: !this.state.dropdownOpen
        });
    }
    handleChild = (langValue) => {
        //this.setState({ mydata: langValue });
        this.state.page = 1;
        this.state.fromsearch = 1;
        this.setState({ searchparams: langValue, isOpen: false, delCardValue: undefined, CardValue: undefined });
    }
    handleRedirectionToAddCandidate(e) {
        // window.location.href = "add-candidates";
        window.location.href = "add-edit-candidate";
        //history.push('/add-candidates');
    }
    handleRedirectionToAddMultipleCandidate(e) {
        history.push('/add-multiple-candidates');
    }
    viewMoreAPICall = (e, candidateid) => {
        this.props.dispatch(userActions.getCandidateViewMoreDetails(candidateid));
        this.toggleModal(e);
    }
    toggleModal = (e) => {
        //console.log('# here in toggle', this.state);
        this.setState({ isOpen: !this.state.isOpen, delCardValue: undefined });
        this.setState({ CardValue: e });
    }
    toggleModalforDelete = (e) => {
        this.setState({ hideModalBox: true });
        this.setState({ isOpen: !this.state.isOpen, CardValue: undefined });
        this.setState({ delCardValue: e });
    }
    componentDidMount() {
        this.loadData();
    }
    loadData() {
        const { page } = this.state
        // console.log("NEXT PAGE NO", page);
        if (localStorage.getItem('user') !== null) {
            var userdata = JSON.parse(localStorage.getItem('user'));
            var training_center_id = userdata.data[0].trainingcenterid;
            if (this.state.searchparams !== undefined) {
                var trainingcenterid = training_center_id;
                var userFinal = { trainingcenterid, ...this.state.searchparams };
                //console.log("NEXT PAGE NO", page);
                this.props.dispatch(userActions.getTrainingPartnerSearchCandidate(userFinal, page, 1));
            } else {
                this.props.dispatch(userActions.getCandidates(training_center_id, page));
            }
        }
        else {
            history.push('/login');
            return ('');
        }
    }

    loadMore = () => {
        //First update the state and then use callback method to call next page call...
        this.setState(prevState => ({
            page: prevState.page + 1
        }), this.loadData)
    }

    deleteCandidate(e) {
        const { page } = this.state
        this.props.dispatch(userActions.deleteCandidate(e, 'candidate', page));
        this.setState({ isOpen: !this.state.isOpen, isDeleted: 1 });
        /*  this.props.dispatch(userActions.deleteCandidate(e, paginationValue, 'candidate'));
         this.setState({ isOpen: !this.state.isOpen, isDeleted: 1 }); */
        // console.log("Now going to get new canddiates");
        // this.loadData();
    }

    

    /* displaySearchParams(searchParams){
        var searchedFor = '' ;
        for(let [index, params] of Object.entries(searchParams)){
            console.log(params.length,"param length");
            if(params.length > 0){
                searchedFor += ' ' + '"' +  params + '"';
            }
        }
        if(searchedFor.length > 0){
            console.log(searchedFor);
            console.log(searchedFor.length,"len");
            return  "You Searched for" + searchedFor;
        }else{
            return;
        }       
    } */
    render() {
        var loaderClass = '';
        //Show loader..
        //console.log('#this.props.loading',this.props.loading);
        if (this.props.loading === true || this.props.loadingviewmore === true) {
            loaderClass = 'loading';
        }
        const styles = {
            top: '115px',
            left: '60px'
        };

        const { user } = this.props;
        if (user === undefined) {
            // console.log("Redierect to specific Dashboard page");
            history.push('/login');
            return ('');
        }

        var recordFound = '';
        if (!!this.props.items || !!this.props.searchitems) {
            var final_allCandiates = [];
            var allcandidates = [];
            var candiateitems = "";

            if (this.props.searchitems !== undefined) {
                candiateitems = this.props.searchitems;
            } else {
                candiateitems = this.props.items;
            }
            if (candiateitems.data.length === 0) {
                recordFound = 0;
            } else {
                recordFound = 1;
            }
            // console.log("Candidate Details : ", this.props.items);
            //Pagination...
            var loadMore = "";
            var totalpages = candiateitems.paginationOptions.totalPages;
            var currentpage = candiateitems.paginationOptions.curPage;
            //console.log('#candiateitems', candiateitems);
            if (currentpage < totalpages) {
                loadMore = (<div className="text-center"><a title="Load More" onClick={this.loadMore} className="btn btn-outline-secondary card-link">Load More</a></div>);
            }
            //Need to verify this logic....
            //End of Pagination..
            //var fromcandidateSearch = 0;
            /* if (!!this.state.mydata) {
                loadMore = "";//If search data then remove Load More option...
                //console.log('typeof delCard',typeof this.state.delCardValue);

                if (typeof this.state.delCardValue == 'number' && this.state.delCardValue > 0 && this.state.isDeleted == 1) {
                    this.state.mydata = this.state.mydata.filter(user => user.candidateid !== this.state.delCardValue);
                }
                allcandidates = this.state.mydata;
                fromcandidateSearch = 1;
                // console.log('all candidates data', allcandidates);

            } */
            if ('data' in candiateitems) {
                // console.log("2");
                allcandidates = candiateitems.data;
            }
            else {
                // console.log("3");
                allcandidates = candiateitems;
            }
            //console.log("DATA", allcandidates);

            //DeleteModal Box
            if (!!candiateitems && this.state.delCardValue !== undefined) {
                var delmymodaval = this.state.delCardValue;
                /* var sendPaginationObject = "";

                if (candiateitems.paginationOptions != undefined) sendPaginationObject = candiateitems.paginationOptions;
                if (this.props.nextPagination != undefined) sendPaginationObject = this.props.nextPagination;
                //console.log("Requred paginatin params",sendPaginationObject);        */
                var deteleCandidatemodal = (
                    <Modal backdrop={false} centered isOpen={this.state.isOpen} className="alert-modal">
                        <ModalHeader toggle={() => this.toggleModalforDelete(delmymodaval)} className="d-block section-title text-center border-bottom-0 no-border">
                            Are you sure you want to remove this candidate?
                        </ModalHeader>
                        <ModalBody>
                            <div className="d-flex justify-content-center">
                                <Button color="primary" className="btn-raised mr-3" onClick={() => this.deleteCandidate(delmymodaval)}>Yes</Button>
                                <Button ouline='true' color="default" onClick={this.closeModalBox}>No</Button>
                            </div>
                        </ModalBody>
                    </Modal>
                );
            }
            //End of Delete Modal box

            //For View more Details MOdal  Box...
            if (!!this.props.candidate_view_more_details && this.state.CardValue !== undefined) {
                var candidatedetaildata = this.props.candidate_view_more_details.data[0];
                //console.log('# here',candidatedetaildata);
                var mymodaval = this.state.CardValue;
                var tel = candidatedetaildata.phone ? "tel:" + candidatedetaildata.phone : ''
                var mailTo = candidatedetaildata.email_address ? "mailto:" + candidatedetaildata.email_address : ''
                
                if(candidatedetaildata.permanent_cityName){
                    candidatedetaildata.permanent_address +=  ', ' + candidatedetaildata.permanent_cityName;
                }
                if(candidatedetaildata.permanent_stateName){
                    candidatedetaildata.permanent_address += ', '  + candidatedetaildata.permanent_stateName;
                }
                if(candidatedetaildata.permanent_pincode){
                    candidatedetaildata.permanent_address += ', '  + candidatedetaildata.permanent_pincode;
                }
                if(candidatedetaildata.present_cityName){
                    candidatedetaildata.present_address +=  ', ' + candidatedetaildata.present_cityName;
                }
                if(candidatedetaildata.present_stateName){
                    candidatedetaildata.present_address += ', '  + candidatedetaildata.present_stateName;
                }
                if(candidatedetaildata.present_pincode){
                    candidatedetaildata.present_address += ', '  + candidatedetaildata.present_pincode;
                }
                var allcandidatesModalDetails = (

                    <Modal backdrop={false} centered size="lg" isOpen={this.state.isOpen} toggle={() => this.toggleModal(mymodaval)}>
                        <ModalHeader toggle={() => this.toggleModal(mymodaval)} className="border-bottom-0 modal-header-shadow align-items-center">
                            <div className="section-title">
                                <h5 className="title modal-title">{candidatedetaildata.first_name}'s Details - {candidatedetaildata.is_skilled ? candidatedetaildata.is_skilled : ''}</h5>                                 
                            </div>
                        </ModalHeader>
                        <ModalBody className="details-view">
                            <Row>
                                <Col xs="12">
                                    <h5 className="title modal-view-title">Personal Details</h5>
                                </Col>
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">First Name</label>
                                    <p className="title">{candidatedetaildata.first_name ? candidatedetaildata.first_name : 'NA'}</p>
                                </Col>
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">Middle Name</label>
                                    <p className="title">{candidatedetaildata.middle_name ? candidatedetaildata.middle_name : 'NA'}</p>
                                </Col>
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">Last Name</label>
                                    <p className="title">{candidatedetaildata.last_name ? candidatedetaildata.last_name : 'NA'}</p>
                                </Col>
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">Aadhaar No.</label>
                                    <p className="title">{candidatedetaildata.aadhar_card_no ? candidatedetaildata.aadhar_card_no : 'NA'}</p>
                                    {/* <p className="title">{candidatedetaildata.aadhar_card_no ? '************' : 'NA'}</p>  */}
                                </Col>
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">Alternate ID Type</label>
                                    <p className="title">{candidatedetaildata.alternateidtype ? candidatedetaildata.alternateidtype : 'NA'}</p>
                                </Col>
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">Alternate ID No.</label>
                                    <p className="title">{candidatedetaildata.altername_id_no ? candidatedetaildata.altername_id_no : 'NA'}</p>
                                    {/* <p className="title">{candidatedetaildata.altername_id_no ? '************' : 'NA'}</p>  */}
                                </Col>
                                {/* <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">Aadhaar Enrollment ID</label>
                                    <p className="title">{candidatedetaildata.aadhaar_enrollment_id ? candidatedetaildata.aadhaar_enrollment_id : 'NA'}</p>
                                </Col> */}
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">Mobile No</label>
                                    <p className="title">
                                        {tel === '' ? 'NA' :
                                            <a href={tel} title={candidatedetaildata.phone}>{candidatedetaildata.phone}</a>
                                        } 
                                        {/* <p className="title">{candidatedetaildata.phone ? '**********' : 'NA'}</p>  */}
                                    </p>
                                </Col>
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">Email Address</label>
                                    <p className="title">
                                        {mailTo === '' ? 'NA' :
                                            <a href={mailTo} title={candidatedetaildata.email_address}>{candidatedetaildata.email_address}</a>
                                        }
                                        {/* <p className="title">{candidatedetaildata.email_address ? '********************' : 'NA'}</p>  */}
                                    </p>
                                </Col>
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">Date Of Birth</label>
                                    <p className="title">{candidatedetaildata.dob ? candidatedetaildata.dob : 'NA'}</p>
                                </Col>
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">Gender</label>
                                    <p className="title">{candidatedetaildata.gender ? candidatedetaildata.gender : 'NA'}</p>
                                </Col>
                                {/* <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">State</label>
                                    <p className="title">{candidatedetaildata.stateName ? candidatedetaildata.stateName : 'NA'}</p>
                                </Col>
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">District</label>
                                    <p className="title">{candidatedetaildata.cityName ? candidatedetaildata.cityName : 'NA'}</p>
                                </Col> */}
                                {/* <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">Pincode</label>
                                    <p className="title">{candidatedetaildata.pincode ? candidatedetaildata.pincode : 'NA'}</p>
                                </Col> */}
                                {/* <Col xs="12" md="6" xl="4">
                                    <label className="subtitle ">Address</label>
                                    <p className="title text-truncate" title={candidatedetaildata.address ? candidatedetaildata.address : 'NA'}>{candidatedetaildata.address ? candidatedetaildata.address : 'NA'}</p>
                                </Col> */}
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle ">Permanent Address</label>
                                    <p className="title text-truncate" title={candidatedetaildata.permanent_address ? candidatedetaildata.permanent_address : 'NA'}>{candidatedetaildata.permanent_address ? candidatedetaildata.permanent_address : 'NA'}</p>
                                </Col>
                                
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle ">Present Address</label>
                                    <p className="title text-truncate" title={candidatedetaildata.present_address ? candidatedetaildata.present_address : 'NA'}>{candidatedetaildata.present_address ? candidatedetaildata.present_address : 'NA'}</p>
                                </Col>
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">Age</label>
                                    <p className="title">{candidatedetaildata.age ? candidatedetaildata.age : 'NA'}</p>
                                </Col>
                                {/* {candidatedetaildata.is_skilled === 'Skilled' &&  */}
                                    <Col xs="12" md="6" xl="4">
                                        <label className="subtitle">Candidate Id</label>
                                        <p className="title">{candidatedetaildata.candidate_id ? candidatedetaildata.candidate_id : 'NA'}</p>
                                    </Col>
                                {/* } */}
                            </Row>
                            <hr className="modal-data-seperator" />
                            <Row>

                                <Col xs="12">
                                    <h5 className="title modal-view-title">Education Details</h5>
                                </Col>
                                <Col xs="12" md="6" xl="8">
                                    {/* <label className="subtitle">EDU </label> */}
                                    {/* {candidatedetaildata.education_details ? candidatedetaildata.education_details = candidatedetaildata.education_details.split(',').join(', ') : candidatedetaildata.education_details } */}
                                    <p className="title">{candidatedetaildata.education_details ? candidatedetaildata.education_details = candidatedetaildata.education_details.split(',').join(', ') : 'NA'}</p>
                                </Col>
                                {/* <Col xs="12" md="6" xl="8">
                                    <label className="subtitle">Skills </label>
                                    <p className="title">{candidatedetaildata.is_skilled ? candidatedetaildata.is_skilled : 'NA'}</p>
                                </Col> */}

                            </Row>
                            <hr className="modal-data-seperator" />
                            <Row>

                                <Col xs="12">
                                    <h5 className="title modal-view-title">Professional Details</h5>
                                </Col>
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">Experience in Years</label>
                                    <p className="title">{candidatedetaildata.experience_in_years ? candidatedetaildata.experience_in_years : 'NA'}</p>
                                </Col>
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">Sector</label>
                                    <p className="title">{candidatedetaildata.sectorName ? candidatedetaildata.sectorName : 'NA'}</p>
                                </Col>
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">Job Role</label>
                                    <p className="title">{candidatedetaildata.jobroleName ? candidatedetaildata.jobroleName : 'NA'}</p>
                                </Col>
                            </Row>
                                {/* {candidatedetaildata.is_skilled === 'Skilled' &&  */}
                                    <div className="view_for_skilled">
                                        <Row>
                                            <Col xs="12" md="6" xl="4">
                                                <label className="subtitle">Training Type</label>
                                                <p className="title">{candidatedetaildata.training_type ? candidatedetaildata.training_type : 'NA'}</p>
                                            </Col>
                                            <Col xs="12" md="6" xl="4">
                                                <label className="subtitle">SDMS/NSDC Enrollment No</label>
                                                <p className="title">{candidatedetaildata.sdmsnsdc_enrollment_number ? candidatedetaildata.sdmsnsdc_enrollment_number : 'NA'}</p>
                                            </Col>
                                            <Col xs="12" md="6" xl="4">
                                                <label className="subtitle">Training Status</label>
                                                <p className="title">{candidatedetaildata.training_status ? candidatedetaildata.training_status : 'NA'}</p>
                                            </Col>
                                            {candidatedetaildata.training_status === 'Ongoing' && 
                                                <Col xs="12" md="6" xl="4">
                                                    <label className="subtitle">Pass Out date</label>
                                                    <p className="title">{candidatedetaildata.pass_out_date ? candidatedetaildata.pass_out_date : 'NA'}</p>
                                                </Col>
                                            }
                                            
                                            <Col xs="12" md="6" xl="4">
                                                <label className="subtitle">Attendance Percent</label>
                                                <p className="title">{candidatedetaildata.attendence_percentage ? candidatedetaildata.attendence_percentage : 'NA'}</p>
                                            </Col>
                                            <Col xs="12" md="6" xl="4">
                                                <label className="subtitle">Placement Status</label>
                                                <p className="title">{candidatedetaildata.placement_status ? candidatedetaildata.placement_status : 'NA'}</p>
                                            </Col>
                                            {candidatedetaildata.placement_status === 'Selected' &&
                                                <Col xs="12" md="6" xl="4">
                                                    <label className="subtitle">Employer Name</label>
                                                    <p className="title">{candidatedetaildata.employer_name ? candidatedetaildata.employer_name : 'NA'}</p>
                                                </Col>
                                            }
                                            
                                            <Col xs="12" md="6" xl="4">
                                                <label className="subtitle">Employment Type</label>
                                                <p className="title">{candidatedetaildata.employment_type ? candidatedetaildata.employment_type : 'NA'}</p>
                                            </Col>
                                            <Col xs="12" md="6" xl="4">
                                                <label className="subtitle">Assessment Score</label>
                                                <p className="title">{candidatedetaildata.assessment_score ? candidatedetaildata.assessment_score : 'NA'}</p>
                                            </Col>
                                            <Col xs="12" md="6" xl="4">
                                                <label className="subtitle">Max Assessment Score</label>
                                                <p className="title">{candidatedetaildata.max_assessment_score ? candidatedetaildata.max_assessment_score : 'NA'}</p>
                                            </Col>
                                            {/* <Col xs="12" md="6" xl="4">
                                                <label className="subtitle">Assessment Date</label>
                                                <p className="title">{candidatedetaildata.assessment_date ? candidatedetaildata.assessment_date : 'NA'}</p>
                                            </Col> */}
                                            
                                            <Col xs="12" md="6" xl="4">
                                                <label className="subtitle">Training Center</label>
                                                <p className="title">{candidatedetaildata.trainingcenter_name ? candidatedetaildata.trainingcenter_name : 'NA'}</p>
                                            </Col>
                                            <Col xs="12" md="6" xl="4">
                                                <label className="subtitle">Batch ID</label>
                                                <p className="title">{candidatedetaildata.batch_id ? candidatedetaildata.batch_id : 'NA'}</p>
                                            </Col>
                                            {/* <Col xs="12" md="6" xl="4">
                                                <label className="subtitle">Batch Start Date</label>
                                                <p className="title">{candidatedetaildata.batch_start_date ? candidatedetaildata.batch_start_date : 'NA'}</p>
                                            </Col>
                                            <Col xs="12" md="6" xl="4">
                                                <label className="subtitle">Batch End Date</label>
                                                <p className="title">{candidatedetaildata.batch_end_date ? candidatedetaildata.batch_end_date : 'NA'}</p>
                                            </Col> */}
                                            <Col xs="12" md="6" xl="4">
                                                <label className="subtitle">Scheme</label>
                                                <p className="title">{candidatedetaildata.schemeType ? candidatedetaildata.schemeType : 'NA'}</p>
                                            </Col>
                                            {candidatedetaildata.schemeType === 'State Skill Development Mission Scheme'
                                                ?
                                                <Col xs="12" md="6" xl="4">
                                                    <label className="subtitle">State Ministry</label>
                                                    <p className="title">{candidatedetaildata.stateministry ? candidatedetaildata.stateministry : 'NA'}</p>
                                                </Col>
                                                :
                                                <Col xs="12" md="6" xl="4">
                                                    <label className="subtitle">Central Ministry</label>
                                                    <p className="title">{candidatedetaildata.centralministry ? candidatedetaildata.centralministry : 'NA'}</p>
                                                </Col>
                                            }
                                            <Col xs="12" md="6" xl="4">
                                                <label className="subtitle">Is Willing To Relocate </label>
                                                <p className="title">{candidatedetaildata.willing_to_relocate ? candidatedetaildata.willing_to_relocate : 'NA'}</p>
                                            </Col>
                                        </Row>
                                    </div>
                                {/* } */}
                            {/* </Row> */}
                            <hr className="modal-data-seperator" />
                            {(!!candidatedetaildata.first_month_payslip || !!candidatedetaildata.second_month_payslip || !!candidatedetaildata.third_month_payslip || !!candidatedetaildata.appointment_letter) 
                            ?
                            <div>
                                <Row>
                                    <Col xs="12">
                                        <h5 className="title modal-view-title">Uploaded Documents</h5>
                                        {console.log(candidatedetaildata, " candidatedetaildata ")}
                                    </Col>
                                    {candidatedetaildata.first_month_payslip &&
                                        <Col xs="12" md="6" xl="4">
                                            <label className="subtitle">1<sup>st</sup> Month Pay Slip </label>
                                            <p className="title" title="Download File">
                                                <a href={config.API_HOST_URL + 'candidate_uploads/'+ candidatedetaildata.candidateid +'/1/'+candidatedetaildata.first_month_payslip}> {candidatedetaildata.first_month_payslip} </a>
                                            </p>
                                        </Col>
                                    }
                                    {candidatedetaildata.second_month_payslip &&
                                        <Col xs="12" md="6" xl="4">
                                            <label className="subtitle">2<sup>nd</sup> Month Pay Slip </label>
                                            <p className="title" title="Download File">
                                                <a href={config.API_HOST_URL + 'candidate_uploads/'+ candidatedetaildata.candidateid +'/2/'+candidatedetaildata.second_month_payslip}> {candidatedetaildata.second_month_payslip} </a>
                                            </p>
                                        </Col>
                                    }
                                    {candidatedetaildata.third_month_payslip &&
                                    <Col xs="12" md="6" xl="4">
                                        <label className="subtitle">3<sup>rd</sup> Month Pay Slip </label>
                                        <p className="title" title="Download File">
                                            <a href={config.API_HOST_URL + 'candidate_uploads/'+ candidatedetaildata.candidateid +'/3/'+candidatedetaildata.third_month_payslip}> {candidatedetaildata.third_month_payslip} </a>
                                        </p>
                                    </Col>
                                    }
                                    {/* {candidatedetaildata.is_skilled === 'Skilled' && candidatedetaildata.appointment_letter && */}
                                    {candidatedetaildata.appointment_letter &&
                                    <Col xs="12" md="6" xl="4">
                                        <label className="subtitle">Appointment Letter </label>
                                        <p className="title" title="Download File">
                                            <a href={config.API_HOST_URL + 'candidate_uploads/'+ candidatedetaildata.candidateid +'/appointment_letter/'+candidatedetaildata.appointment_letter}> {candidatedetaildata.appointment_letter} </a>
                                        </p>
                                    </Col>
                                    }
                                </Row>
                            </div> 
                            :
                            <Row>
                                <Col xs="12">
                                    <h5 className="title modal-view-title">No Documents Uploaded</h5>
                                </Col>
                            </Row>
                            }
                        </ModalBody>
                        {/* <ModalFooter>

                        </ModalFooter> */}
                    </Modal>
                );
            }
            //End of view more details for Modal box...

            //Card Details
            allcandidates.map((value, index) => {
                var TrainingStatus = allcandidates[index].training_status ? allcandidates[index].training_status.charAt(0).toUpperCase() + allcandidates[index].training_status.substr(1) : '-';
                switch (TrainingStatus) {
                    case 'Duplicateenrollment':
                        TrainingStatus = 'Duplicate Enrollment';
                        break;
                    default:
                }

                return final_allCandiates.push(
                    <Col xs="12" md="6" xl="4" key={index}>
                        <Card className="card z-depth-1">
                            <CardHeader className="d-flex justify-content-between bg-transparent align-items-center border-bottom-0 card-header-shadow">
                                <div className="section-title">
                                    <CardTitle className="title card-title">{ allcandidates[index].first_name + ' ' + ( allcandidates[index].last_name ?  allcandidates[index].last_name : '') }</CardTitle>
                                    {/* <CardSubtitle className="subtitle">{allcandidates[index].is_skilled.charAt(0).toUpperCase()+allcandidates[index].is_skilled.slice(1)}</CardSubtitle> */}
                                    <CardSubtitle className="subtitle">{allcandidates[index].batch_id}</CardSubtitle>
                                </div>
                                <CardLink title="Delete" onClick={() => this.toggleModalforDelete(allcandidates[index].candidateid)} className="btn-fab btn-fab-danger btn" ><i className="material-icons">delete_outline</i></CardLink>
                            </CardHeader>
                            <ListGroup className="no-border">
                                <ListGroupItem>
                                    <label className="subtitle">Job Role</label>
                                    <p className="title mb-0 text-truncate" title={allcandidates[index].jobroleName}>{allcandidates[index].jobroleName}</p>
                                </ListGroupItem>
                                <ListGroupItem>
                                    <label className="subtitle">Mobile No.</label>
                                    <p className="title mb-0"><a href={"tel:" + allcandidates[index].phone}>{allcandidates[index].phone}</a></p>
                                    {/* <p className="title mb-0">{allcandidates[index].phone ? '**********' : 'NA'}</p>  */}
                                </ListGroupItem>


                                {allcandidates[index].training_center_name &&
                                    <ListGroupItem>
                                        <label className="subtitle">Training Center</label>
                                        <p className="title mb-0 text-truncate" title={allcandidates[index].training_center_name}>{allcandidates[index].training_center_name ? allcandidates[index].training_center_name : '-'}</p>
                                    </ListGroupItem>}
                                <ListGroupItem>
                                    <label className="subtitle">Sector</label>
                                    <p className="title mb-0 text-truncate" title={allcandidates[index].sectorName}>{allcandidates[index].sectorName ? allcandidates[index].sectorName : '-'}</p>
                                </ListGroupItem>
                                <ListGroupItem>
                                    <label className="subtitle">Training Status</label>
                                    <p className="title mb-0">{TrainingStatus}</p>
                                </ListGroupItem>

                                <ListGroupItem className="d-flex">
                                    <CardLink title="View More" onClick={() => this.viewMoreAPICall(index, allcandidates[index].candidateid)} className="btn btn-primary btn-raised mr-3" >View More</CardLink>
                                    <Link to={`/add-edit-candidate/#id:${allcandidates[index].candidateid}`} title="Edit" className="btn btn-outline">Edit</Link>
                                </ListGroupItem>
                            </ListGroup>
                        </Card>
                    </Col>
                );

            })
        }

        return (<div>

            <div className="content-wrapper user-flow TP-flow">
                <Sidebar {...this.props} />

                <div id="content" className="main-content fixed-bottom-btn">
                    <Header />
                    <div className="d-block d-md-flex align-items-center section-header">
                        <div>
                            <h1 className="page-title">Candidate(s)</h1>
                            <Breadcrumb>
                                <BreadcrumbItem><a href="/candidates">Home</a></BreadcrumbItem>
                                <BreadcrumbItem active>Candidates</BreadcrumbItem>
                            </Breadcrumb>
                        </div>

                        <div className="ml-auto d-flex">
                            <Button outline color="primary" title="Add Candidate" onClick={this.handleRedirectionToAddCandidate} className="mr-0 mr-md-2 sm-floating-btn d-md-inline-block d-none" >
                                <i className="material-icons">add</i>
                                <span>Add Candidate</span>
                            </Button>

                            {<Button outline color="primary" title="Bulk Upload" onClick={this.handleRedirectionToAddMultipleCandidate} className="mr-2 sm-floating-btn d-md-inline-block d-none" >
                                <i className="material-icons">arrow_upward</i>
                                <span>Bulk Upload</span>
                            </Button>}

                            <ButtonDropdown isOpen={this.state.dropdownOpen} toggle={this.toggle} direction="up" className="d-block d-md-none floating-dropdown-btn">
                                <DropdownToggle outline color="primary" className="mr-0 mr-md-2 sm-floating-btn">
                                    <i className="material-icons">add</i>
                                </DropdownToggle>
                                <DropdownMenu className="z-depth-2 border-0">
                                    <DropdownItem onClick={this.handleRedirectionToAddCandidate}>Add Candidate</DropdownItem>
                                    <DropdownItem divider />
                                    {/* <DropdownItem onClick={this.handleRedirectionToAddMultipleCandidate}>Bulk Upload</DropdownItem> */}
                                </DropdownMenu>
                            </ButtonDropdown>
                        </div>
                    </div>

                    <div className={loaderClass} style={styles}></div>
                    {(recordFound === 1 || this.state.fromsearch === 1) &&
                        <CandidateSearch onSelectedChange={this.handleChild} />
                    } 
                    {/* {console.log(this.state,"STATE")}
                                        {console.log(this.props,"PROPS")}
                                        {!!this.state.searchparams && "You Searched for " } 
                                         <b>
                                            {!!this.state.searchparams && this.displaySearchParams(this.state.searchparams) }    
                                        </b> */}
                    {recordFound === 1 &&
                        (<h3 className="page-title mb-3">
                            {!!this.props.items ? " Total Number Of Candidates are : " + this.props.items.paginationOptions.totalcount.toLocaleString('en-IN') : ''}
                            {(!!this.props.searchitems && !!this.props.searchitems.paginationOptions) ? this.props.searchitems.paginationOptions.totalcount.toLocaleString('en-IN') + ' Candidate(s) Found ' : ''}
                        </h3>)
                    }

                    {recordFound === 1 &&
                        <Row className="details-view">
                            {final_allCandiates}
                        </Row>
                    }
                    {recordFound === 0 &&

                        <div className="d-flex justify-content-center align-items-center error-page">
                            <div className="text-center">
                                <div className="img-wrapper mb-4">
                                    <div className="circle-xl user-avatar">
                                        <img src={candidatesSVG} alt="candidates" />
                                    </div>
                                </div>


                                {this.state.fromsearch === 0 ?
                                    <h1 className="h2 fw-regular mb-0">No Candidate(s) Added Yet!</h1>
                                    :
                                    <h1 className="h2 fw-regular mb-0">No Candidate(s) Found!</h1>
                                }
                                {this.state.fromsearch === 0 &&
                                    <span className="h6 ff-roboto d-block mt-0">
                                        You'll see the list once you add the candidates!
                                </span>
                                }

                            </div>

                        </div>
                    }

                    {allcandidatesModalDetails}
                    {deteleCandidatemodal}
                    {loadMore}

                    <Footer />
                </div>

            </div>
        </div >
        );
    }
}
function mapStateToProps(state) {

    const { user } = state.authentication;
    const { items, searchitems, loading } = state.trainingpartners; //From Reducer - trainingpartners function ...
    const { candidate_view_more_details, loadingviewmore } = state.getcandidateviewmoredetails; //From Reducer - candidatedetails function ...
    // console.log("in MapStateTOProps for GetCandidate",state);
    return {
        items, user, searchitems, loading, candidate_view_more_details, loadingviewmore
    };
}

const connectedGetCandidates = connect(mapStateToProps)(GetCandidates);
export { connectedGetCandidates as GetCandidates };