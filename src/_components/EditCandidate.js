import React from 'react';
import { connect } from 'react-redux';
import { userActions } from '../_actions';
import { reduxForm, Field } from 'redux-form';
import { Header } from '../_components/Header';
import { Footer } from '../_components/Footer';
import { history } from '../_helpers';
import { Sidebar } from '../_components/Sidebar';
import { FormGroup, Breadcrumb, BreadcrumbItem } from 'reactstrap';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import 'react-day-picker/lib/style.css';
import { submitFailure } from '../../src/utils/submitFailure';
import { constants } from '../constants';

const currentYear = new Date().getFullYear();
const currentMonth = new Date().getMonth();
const fromMonth = new Date(currentYear - 40, currentMonth);
const toMonth = new Date(currentYear, currentMonth);

function YearMonthForm({ date, localeUtils, onChange }) {
    const months = localeUtils.getMonths();

    const years = [];
    for (let i = fromMonth.getFullYear(); i <= toMonth.getFullYear(); i += 1) {
        years.push(i);
    }

    const handleChange = function handleChange(e) {
        const { year, month } = e.target.form;
        onChange(new Date(year.value, month.value));
    };

    return (
        <div className="DayPicker-Caption">
            <select name="month" onChange={handleChange} value={date.getMonth()}>
                {months.map((month, i) => (
                    <option key={month} value={i}>
                        {month}
                    </option>
                ))}
            </select>
            <select name="year" onChange={handleChange} value={date.getFullYear()}>
                {years.map(year => (
                    <option key={year} value={year}>
                        {year}
                    </option>
                ))}
            </select>
        </div>
    );
}

const renderTextAreaField = ({ input, label, type, placeholder, textarea, meta: { touched, error, warning, invalid } }) => {

    const textareaType = <textarea {...input} placeholder={label} type={type} className={`form-control ${touched && invalid ? 'has-danger' : ''}`} />;

    return <div className=''>
        <div className={'form-group' + (error && touched ? ' has-error' : '')}>
            <label>{label}</label>
            <div> {textarea ? textareaType : ""}
                {touched && ((error && <div className="help-block">{error}</div>) || (warning && <span>{warning}</span>))}
            </div>

        </div>
    </div>
};
const renderField = ({ input, label, type, placeholder, textarea, meta: { touched, error, warning, invalid } }) => {
    const textareaType = <textarea {...input} placeholder={label} type={type} className={`form-control ${touched && invalid ? 'has-danger' : ''}`} />;
    return <div className={((label === "SDMS/NSDC Enrollment No" || label === "Attendance Percent" || label === "Pincode" || label === "Experience In Years" || label === "Assessment Score" || label === "Max Assessment Score" || label === "Batch ID") ? 'col-md-4 col-12' : 'col-md-6 col-12')}>
        <div className={'form-group' + (error && touched ? ' has-error' : '')}>
            <label>{label}</label>
            <div> {textarea ? textareaType : <input {...input} type={type} placeholder={placeholder} className="form-control" />}
                {touched && ((error && <div className="help-block">{error}</div>) || (warning && <span>{warning}</span>))}
            </div>

        </div>
    </div>
};
const renderError = ({ meta: { touched, error } }) =>

    touched && error ? <div className="help-block">{error}</div> : false

const validate = values => {

    const errors = {}

    if (!values.name) {
        errors.name = 'Please enter name of candidate'
    } /*else if (values.username.length > 15) {
            errors.username = 'Must be 15 characters or less'
            }  */
    /* if (!values.aadhar_card_no) {
        errors.aadhar_card_no = 'Please enter aadhar card number'
    }
    if (!values.contact_email_address) {
        errors.contact_email_address = 'Please enter an Email Address'
    } */
    if (!values.age) {
        errors.age = 'Please enter age'
    }
    if (!values.phone) {
        errors.phone = 'Please enter mobile number'
    }
    if (!values.pincode) {
        errors.pincode = 'Please enter Pincode'
    }
    if (!values.sex) {
        errors.sex = 'Please select gender'
    }
    /* if (!values.is_willing_to_relocate) {
        errors.is_willing_to_relocate = 'Please select willing to relocate'
    } */
    if (!values.address) {
        errors.address = 'Please enter Address'
    }
    if (!values.nsdc_enrollment_no) {
        errors.nsdc_enrollment_no = 'Please enter SDMS/NSDC Enrollment No'
    }
    if (!values.statename) {
        errors.statename = 'Please select a state'
    }
    if (!values.districtname) {
        errors.districtname = 'Please select a district'
    }
    if (!values.training_type) {
        errors.training_type = "Please select a training type";
    }
    if (!values.training_status) {
        errors.training_status = "Please select training status";
    }
    /* if (!values.placement_status) {
        errors.placement_status = "Please select placement status";
    }
    if (!values.employment_type) {
        errors.employment_type = "Please select employment status";
    } */
    if (!values.sectorname) {
        errors.sectorname = "Please select sector name";
    }
    if (!values.jobrole) {
        errors.jobrole = "Please select job role";
    }
    if (!values.batch_id) {
        errors.batch_id = "Please select batch Id";
    }
    if (!values.schemename) {
        errors.schemename = "Please select a scheme";
    }
    /* if (!values.state_scheme_name) {
         errors.state_scheme_name = "Please select state scheme";
     }*/
    if (!values.training_center) {
        errors.training_center = "Please select Training Center";
    }
    return errors;
}

class EditCandidate extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            submitted: false,
            combovalue: '',
            startDate: "",
            selectedDay: undefined,
            selectedDOB: 0,
            selectedAssessmentDate: undefined,
            selectedBatchStartDate: undefined,
            selectedBatchEndDate: undefined,
            showState: false,
            female: "",
            male: "",
            yes: "",
            no: "",
            registerClick: 0,
            error_flag: 0,
            error_jobrole: 0,
            error_dist_flag: 0,
            candidate_id: "",
            onchangedistrict: "",
            candidate_details_edit: "",
            isCMState: 0,
            hideState: "",
            showStates: 0,
            month: fromMonth,
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        //Date Related Pure Functions..
        this.handleDayClick = this.handleDayClick.bind(this);
        this.handleAssessmentClick = this.handleAssessmentClick.bind(this);
        this.handleBatchStartDateClick = this.handleBatchStartDateClick.bind(this);
        this.handleBatchEndDateClick = this.handleBatchEndDateClick.bind(this);
        this.checkValidation = this.checkValidation.bind(this);
        this.handleYearMonthChange = this.handleYearMonthChange.bind(this);
    }
    handleYearMonthChange(month) {
        this.setState({ month });
    }
    componentDidMount() {
        // const { dispatch, candidate_updated_details } = this.props;
        // console.log("Edit Id",this.props.location.hash);
        let candidate_idvalue = this.props.location.hash.split(":");
        if (candidate_idvalue[0] === '#id' && candidate_idvalue[1] !== "") {
            this.setState({ "candidate_id": candidate_idvalue[1] });
        }
        this.props.dispatch(userActions.getCandidateFormData(this.props.user.data[0].trainingpartnerid));
    }
    handleRedirectionToAddMultipleCandidate(e) {
        history.push('/add-multiple-candidates');
    }
    handleSubmit(event) {
        // event.preventDefault();
        const { user } = this.state;
        this.setState({ submitted: true });

        var candidateid = this.state.candidate_id;
        // var candidateid = '-1';
        var trainingpartnerid = this.props.user.data[0].trainingpartnerid;
        var userFinal;


        if (this.state.selectedDay !== "" && this.state.selectedDay !== undefined) {
            var dob = this.state.selectedDay;
            userFinal = { dob };
        } 
        if(this.state.selectedDOB === 1){
            let dob = '';
            userFinal = { dob };
        }
       
        if (this.state.selectedAssessmentDate !== "" && this.state.selectedAssessmentDate !== undefined) {
            var assessment_date = this.state.selectedAssessmentDate;
            userFinal = { ...userFinal, assessment_date };
        }
        if (this.state.selectedBatchStartDate !== "" && this.state.selectedBatchStartDate !== undefined) {
            var batch_start_date = this.state.selectedBatchStartDate;
            userFinal = { ...userFinal, batch_start_date };
        }
        if (this.state.selectedBatchEndDate !== "" && this.state.selectedBatchEndDate !== undefined) {
            var batch_end_date = this.state.selectedBatchEndDate;
            userFinal = { ...userFinal, batch_end_date };
        }

        var userFinalOne = { trainingpartnerid, candidateid, ...userFinal, ...user };

        console.log("send data", userFinalOne);

        //if empty data then call on Clear Call...
        // if (userFinal.aadhar_card_no && userFinal.name && userFinal.dob) {

        this.props.dispatch(userActions.addUpdateCandidates(userFinalOne));
        // }
    }
    checkValidation(event) {
        //console.log("Register clicked.");
        this.setState({ registerClick: 1 });
    }

    handleChange(event) {


        const { name, value } = event.target;
        const { user } = this.state;
        // const { dispatch } = this.props;
        // this.setState({ value: event.target.value });
        this.setState({
            user: {
                ...user,
                [name]: value
            }
        });
        //console.log("event data change", event.target.name, event.target.value);

        if (event.target.name === 'districtname') { this.setState({ error_flag: 0 }) }
        if (event.target.name === 'statename') {
            if (event.target.name !== 'districtname') {
                this.setState({ error_flag: 1 });
                this.props.dispatch(userActions.getDistrict(value));
            }
        }
        if (event.target.name === 'jobrole') { this.setState({ error_jobrole: 0 }) }
        if (event.target.name === 'sectorname') {
            if (event.target.name !== 'jobrole') {
                this.setState({ error_jobrole: 1 });
                this.props.dispatch(userActions.getJobRoles(value));
            }
        }
        if (event.target.name === 'schemename' && event.target.value === '5') { this.setState({ showState: true, hideState: 0 }); }
        if (event.target.name === 'schemename' && event.target.value !== '5') {

            this.setState({ showState: false, isCMState: 0, hideState: 1 });

            this.props.dispatch(userActions.getSchemeCentralMinistries(value));
        }
    }
    handleDayClick(day) {
        this.setState({ selectedDay: day });
        if(typeof day === 'undefined'){
            this.setState({ selectedDOB: 1 });
        }
    }
    handleAssessmentClick(day) {
        this.setState({ selectedAssessmentDate: day });
    }
    handleBatchStartDateClick(day) {
        this.setState({ selectedBatchStartDate: day });
    }
    handleBatchEndDateClick(day) {
        this.setState({ selectedBatchEndDate: day });
    }
    render() {

        var newFlag = 0;
        const { handleSubmit } = this.props;
        const { female, male, yes, no } = this.state;

        const dayPickerProps = {
            month: this.state.month,
            fromMonth: fromMonth,
            toMonth: toMonth,
            captionElement: ({ date, localeUtils }) => (
                <YearMonthForm
                    date={date}
                    localeUtils={localeUtils}
                    onChange={this.handleYearMonthChange}
                />
            )
        };
        //console.log("EDIT CANDIDAT ID", this.state.candidate_id);
        // console.log("Candidate Details", this.props.candidate_updated_details);
        //  console.log("Candidate Form Details", this.props.candidateformdata);
        //console.log("disctrict value", districtlist);
        //console.log("Job value", jobroles);
        //  console.log("get cnetral minstiry data",this.props);  

        /****For on seletion change data events like , State-District, Sector - Job...*/

        var central_minstry_name = "";
        var allStates;
        var final_allStates;
        if (!!this.props.districtlist) {
            var allCities = this.props.districtlist.data;
            var final_allCities = [];
            //State values..
            allCities.map((value, index) => {
                return final_allCities.push(<option key={index} value={value.cityid}>{value.cityName}</option>);
            })
        }
        if (!!this.props.jobroles) {
            var allJobroles = this.props.jobroles.data;
            var final_allJobs = [];
            allJobroles.map((value, index) => {
                return final_allJobs.push(<option key={index} value={value.jobroleid}>{value.jobroleName}</option>);
            })
        }
        if (!!this.props.centralministriesdata) {
            //  console.log("CM data",this.props.centralministriesdata);
            if (this.props.centralministriesdata.data.length > 0) {
                central_minstry_name = this.props.centralministriesdata.data[0].centralministryname;

            }
        }
        if (!!this.props.candidate_updated_details) {
            if (this.props.candidate_updated_details.data[0].schemeid === 5) {
                if (!!this.props.candidateformdata) {
                    this.state.isCMState = 1;
                    newFlag = 1;

                    //State values..
                    allStates = this.props.candidateformdata.data.state[0];
                    //  console.log("CMSTATES",allStates);
                    final_allStates = [];
                    allStates.map((value, index) => {
                        return final_allStates.push(<option key={index} value={value.stateid}>{value.stateName}</option>);
                    })
                }
            }
        }
        /****End of getting data on change selection criteria...*/

        if (!!this.props.candidateformdata) {
            //State values..
            allStates = this.props.candidateformdata.data.state[0];
            final_allStates = [];
            allStates.map((value, index) => {
                return final_allStates.push(<option key={index} value={value.stateid}>{value.stateName}</option>);
            })


            //Alternative IDs..
            var allAlternativeIds = this.props.candidateformdata.data.alternateidtype[0];
            var final_alternative_ids = [];
            allAlternativeIds.map((value, index) => {
                return final_alternative_ids.push(<option key={index} value={value.optionValue}>{value.optionKey}</option>);
            })

            //Employment Status values..
            var allEmploymentStatus = this.props.candidateformdata.data.employment_status[0];
            var final_employementstatus = [];
            allEmploymentStatus.map((value, index) => {
                return final_employementstatus.push(<option key={index} value={value.optionValue}>{value.optionKey}</option>);
            })
            //PlacementStatus list..
            var allplacementlist = this.props.candidateformdata.data.placement_status[0];
            var final_placementstatus = [];
            allplacementlist.map((value, index) => {
                return final_placementstatus.push(<option key={index} value={value.optionValue}>{value.optionKey}</option>);

            })
            //Training Center list..
            var allTrainingCenterList = this.props.candidateformdata.data.training_centre[0];
            var final_TrainingCenter = [];
            allTrainingCenterList.map((value, index) => {
                return final_TrainingCenter.push(<option key={index} value={value.trainingcenterid}>{value.trainingcenter_name}</option>);

            })
            //Gender Type list..
            var allgendertype = this.props.candidateformdata.data.gender[0];
            var final_genderlist = [];
            allgendertype.map((value, index) => {
                return final_genderlist.push(<option key={index} value={value.optionValue}>{value.optionKey}</option>);
            })

            //Scheme list..
            var allschemes = this.props.candidateformdata.data.scheme[0];
            var final_schemes = [];
            allschemes.map((value, index) => {
                return final_schemes.push(<option key={index} value={value.pk_schemeID}>{value.schemeType}</option>);

            })
            //Sector list..
            var allsectors = this.props.candidateformdata.data.sector[0];
            var final_sectors = [];
            allsectors.map((value, index) => {
                return final_sectors.push(<option key={index} value={value.sectorid}>{value.sectorname}</option>);

            })
            //Training Status list..
            var alltrainingstatus = this.props.candidateformdata.data.training_status[0];
            var final_trainingstatus = [];
            alltrainingstatus.map((value, index) => {
                return final_trainingstatus.push(<option key={index} value={value.optionValue}>{value.optionKey}</option>);

            })
            //Training Type list..
            var alltrainingtype = this.props.candidateformdata.data.training_type[0];
            var final_trainingtype = [];
            alltrainingtype.map((value, index) => {
                return final_trainingtype.push(<option key={index} value={value.optionValue}>{value.optionKey}</option>);

            })
            //console.log("fina array", final_allSectors);
        }
        const renderColorSelector = ({ input, label, error_state,error_jobroleerror, dynamic_values, meta: { touched, error } }) => (
            <div className={'form-group' + ((error && touched) || (error_state === 1)  || (error_jobroleerror===1) ? ' has-error' : '')}>
                <label>{label}</label>
                <select {...input} className="form-control">
                    <option value="">Select</option>
                    {dynamic_values}
                    ))}
              </select>
                {error_state === 1 && <div className="help-block">{error}</div>}
                {error_jobroleerror === 1 && <div className="help-block">{error}</div>}
                
                {(touched && error) && <div className="help-block">{error}</div>}
            </div>

        )
        return (

            <div className="content-wrapper user-flow TP-flow">

                <div id="content" className="main-content">
                    <Sidebar {...this.props} />
                    <Header />
                    <div className="d-block d-md-flex align-items-center section-header">

                        <div>
                            <h1 className="page-title">Edit Candidate</h1>
                            <Breadcrumb>
                                <BreadcrumbItem><a href="/TrainingPartner-dashboard">Dashboard</a></BreadcrumbItem>
                                <BreadcrumbItem><a href="/candidates">Candidates</a></BreadcrumbItem>
                                <BreadcrumbItem active>Edit Candidate</BreadcrumbItem>
                            </Breadcrumb>
                        </div>

                    </div>
                    <form id="frm-add-candidate" className="form frm-edit-candidaten" onSubmit={handleSubmit(this.handleSubmit)}>
                        <div className="card z-depth-1">
                            <div className="card-header bg-transparent border-bottom-0 section-title no-border form-title">
                                <h5 className="title card-title">Personal Details</h5>
                            </div>
                            <div className="card-body pb-0">

                                <div className="row">
                                    <Field name="name" type="text" component={renderField} label="Name of Candidate" placeholder="Name of Candidate" onChange={this.handleChange} />
                                    <Field name="aadhar_card_no" type="text" component={renderField} label="Aadhaar No" placeholder="Aadhaar No" onChange={this.handleChange} />
                                </div>
                                <div className="row">
                                    <Field name="age" type="text" component={renderField} label="Age" placeholder="Age" onChange={this.handleChange} />
                                    <Field name="candidate_id" type="text" component={renderField} label="Candidate ID" placeholder="Candidate ID" onChange={this.handleChange} />
                                </div>

                                <div className="row">
                                    <Field name="aadhaar_enrollment_id" type="text" component={renderField} label="Aadhaar Enrollment ID" placeholder="Aadhaar Enrollment ID" onChange={this.handleChange} />
                                    <div className="col-12 col-md-6">
                                        <FormGroup>

                                            <div className='form-group'>
                                                <Field name="alternate_id_type" component={renderColorSelector} dynamic_values={final_alternative_ids} onChange={this.handleChange} label="Alternate ID Type" />
                                            </div>
                                        </FormGroup>
                                    </div>
                                </div>
                                <div className="row">
                                    <Field name="altername_id_no" type="text" component={renderField} label="Alternate ID No" placeholder="Alternate ID No" onChange={this.handleChange} />
                                    <Field name="phone" type="text" component={renderField} label="Mobile No" placeholder="Mobile No" onChange={this.handleChange} />
                                </div>

                                <div className="row">
                                    <Field name="email_address" type="text" component={renderField} label="Email Address" placeholder="Email Address" onChange={this.handleChange} />

                                    <div className="col-12 col-md-6">
                                        <div className='form-group'>
                                            <label htmlFor="date_of_birth">Date Of Birth</label>
                                            <div className="input-group date date-picker" data-provide="datepicker">
                                                <DayPickerInput
                                                    placeholder= {!!this.props.candidate_updated_details && this.props.candidate_updated_details.data[0].dob !== '' && this.props.candidate_updated_details.data[0].dob !== null ? this.props.candidate_updated_details.data[0].dob : 'Date of Birth'}
                                                    value= {!!this.props.candidate_updated_details && this.props.candidate_updated_details.data[0].dob !== '' && this.props.candidate_updated_details.data[0].dob !== null ? this.props.candidate_updated_details.data[0].dob : ''}
                                                    onDayChange={this.handleDayClick}
                                                    dayPickerProps={dayPickerProps}
                                                />
                                                <div className="input-group-addon">
                                                    <i className="material-icons">date_range</i>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                </div>
                                <div className={'form-group' + (!male || !female ? ' has-error' : '')}>
                                    <label htmlFor="gender" className="d-block">Gender</label>
                                    <Field name="sex" component="input" type="radio" value="male" className="ml-0" onChange={this.handleChange} /> Male
                                <Field name="sex" component="input" type="radio" value="female" onChange={this.handleChange} /> Female
                                <Field name="sex" component="input" type="radio" value="other" onChange={this.handleChange} /> Other
                                <Field name="sex" component={renderError} />
                                </div>

                                <div className={'form-group' + (!yes || !no ? ' has-error' : '')}>
                                    <label htmlFor="relocate" className="d-block">Willing To Relocate</label>
                                    <Field name="is_willing_to_relocate" component="input" type="radio" value="yes" className="ml-0" onChange={this.handleChange} /> Yes
                                <Field name="is_willing_to_relocate" component="input" type="radio" value="no" onChange={this.handleChange} /> No
                                {/* <Field name="is_willing_to_relocate" component={renderError} /> */}
                                </div>

                                <div className="">
                                    <Field name="address" component={renderTextAreaField} type="textarea" label="Address" textarea={true} onChange={this.handleChange} />
                                </div>

                                <div className="row">
                                    <div className="col-12 col-md-4">
                                        <div className='form-group'>
                                            <Field name="statename" component={renderColorSelector} dynamic_values={final_allStates} onChange={this.handleChange} label="State" />
                                        </div>
                                    </div>

                                    <div className="col-12 col-md-4">
                                        <div className='form-group'>
                                            <Field name="districtname" component={renderColorSelector} error_state={this.state.error_flag} dynamic_values={final_allCities} onChange={this.handleChange} label="District" />
                                        </div>
                                    </div>
                                    <Field name="pincode" type="text" component={renderField} label="Pincode" placeholder="Pincode" onChange={this.handleChange} />

                                </div>
                            </div>

                            <hr className="inner-form-seperator" />

                            <div className="card-header bg-transparent border-bottom-0 section-title no-border form-title">
                                <h5 className="title card-title">Professional Details</h5>
                            </div>
                            <div className="card-body">
                                <div className="row">
                                    <Field name="exp_in_years" type="text" component={renderField} label="Experience In Years" placeholder="Experience In Years" onChange={this.handleChange} />
                                    <div className="col-12 col-md-4">
                                        <FormGroup>
                                            <Field name="training_type" component={renderColorSelector} dynamic_values={final_trainingtype} onChange={this.handleChange} label="Training Type" />
                                        </FormGroup>
                                    </div>
                                    <Field name="nsdc_enrollment_no" type="text" component={renderField} label="SDMS/NSDC Enrollment No" placeholder="SDMS/NSDC Enrollment No" onChange={this.handleChange} />
                                </div>

                                <div className="row">
                                    <div className="col-12 col-md-4">
                                        <FormGroup>
                                            <Field name="training_status" component={renderColorSelector} dynamic_values={final_trainingstatus} onChange={this.handleChange} label="Training Status" />
                                        </FormGroup>
                                    </div>

                                    <Field name="attendance_percent" type="text" component={renderField} label="Attendance Percent" placeholder="Attendance Percent" onChange={this.handleChange} />
                                    <div className="col-12 col-md-4">
                                        <FormGroup>
                                            <Field name="placement_status" component={renderColorSelector} dynamic_values={final_placementstatus} onChange={this.handleChange} label="Placement Status" />
                                        </FormGroup>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col-12 col-md-4">
                                        <FormGroup>
                                            <Field name="employment_type" component={renderColorSelector} dynamic_values={final_employementstatus} onChange={this.handleChange} label="Employment Type" />
                                        </FormGroup>
                                    </div>
                                    <Field name="assessment_score" type="text" component={renderField} label="Assessment Score" placeholder="Assessment Score" onChange={this.handleChange} />
                                    <Field name="max_assessment_score" type="text" component={renderField} label="Max Assessment Score" placeholder="Max Assessment Score" onChange={this.handleChange} />
                                </div>

                                <div className="row">
                                    <div className="col-12 col-md-4">
                                        <div className='form-group'>
                                            <label htmlFor="assessment_date">Assessment Date</label>
                                            <div className="input-group date date-picker" data-provide="datepicker">
                                                <DayPickerInput
                                                    placeholder={(!!this.props.candidate_updated_details) ? this.props.candidate_updated_details.data[0].assessment_date : ""}
                                                    value={(!!this.props.candidate_updated_details) ? this.props.candidate_updated_details.data[0].assessment_date : ""}
                                                    onDayChange={this.handleAssessmentClick} />
                                                <div className="input-group-addon">
                                                    <i className="material-icons">date_range</i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="col-12 col-md-4">
                                        <div className={'form-group' + (!this.state.selectedBatchStartDate ? ' has-error' : '')}>
                                            <label htmlFor="Batch Start Date">Batch Start Date</label>
                                            <div className="input-group date date-picker" data-provide="datepicker">
                                                <DayPickerInput
                                                    placeholder={(!!this.props.candidate_updated_details) ? this.props.candidate_updated_details.data[0].batch_start_date : ""}
                                                    value={(!!this.props.candidate_updated_details) ? this.props.candidate_updated_details.data[0].batch_start_date : ""}
                                                    onDayChange={this.handleBatchStartDateClick} />
                                                <div className="input-group-addon">
                                                    <i className="material-icons">date_range</i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-12 col-md-4">
                                        <div className='form-group'>
                                            <label htmlFor="Batch End Date">Batch End Date</label>
                                            <div className="input-group date date-picker" data-provide="datepicker">
                                                <DayPickerInput
                                                    placeholder={(!!this.props.candidate_updated_details) ? this.props.candidate_updated_details.data[0].batch_end_date : ""}
                                                    value={(!!this.props.candidate_updated_details) ? this.props.candidate_updated_details.data[0].batch_end_date : ""}
                                                    onDayChange={this.handleBatchEndDateClick} />
                                                <div className="input-group-addon">
                                                    <i className="material-icons">date_range</i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                                <div className="row">
                                    <div className="col-12 col-md-4">
                                        <div className="form-group">
                                            <FormGroup>
                                                <Field name="sectorname" component={renderColorSelector} dynamic_values={final_sectors} onChange={this.handleChange} label="Sector" />
                                            </FormGroup>
                                        </div>
                                    </div>

                                    <div className="col-12 col-md-4">
                                        <div className="form-group">
                                            <FormGroup>
                                                <Field name="jobrole" component={renderColorSelector} error_jobroleerror={this.state.error_jobrole}  dynamic_values={final_allJobs} onChange={this.handleChange} label="Job Role" />
                                            </FormGroup>
                                        </div>
                                    </div>

                                    <div className="col-12 col-md-4">
                                        <div className="form-group">
                                            <FormGroup>
                                                <Field name="training_center" component={renderColorSelector} dynamic_values={final_TrainingCenter} onChange={this.handleChange} label="Training Center" />
                                            </FormGroup>
                                        </div>
                                    </div>
                                </div>

                                <div className="row">

                                    <Field name="batch_id" type="text" component={renderField} label="Batch ID" placeholder="Batch ID" onChange={this.handleChange} />

                                    <div className="col-12 col-md-4">
                                        <div className="form-group">
                                            <FormGroup>
                                                <Field name="schemename" component={renderColorSelector} dynamic_values={final_schemes} onChange={this.handleChange} label="Scheme" />
                                            </FormGroup>
                                        </div>
                                    </div>

                                    <div className={'col-12 col-md-4' + ((newFlag === 1 && this.state.hideState === '') || (newFlag === 0 && this.state.hideState === 0) ? '' : ' d-none')} >
                                        <FormGroup>
                                            <Field name="state_scheme_name" component={renderColorSelector} dynamic_values={final_allStates} onChange={this.handleChange} label="State Scheme" />
                                        </FormGroup>
                                    </div>


                                    {!this.state.showState && central_minstry_name &&
                                        <div className="form-group col-12 col-md-4">
                                            <label>Ministry Name</label>
                                            <div>
                                                <input type="text" value={central_minstry_name} title={central_minstry_name} className="form-control" readOnly />
                                            </div>
                                        </div>
                                    }
                                </div>

                                <div className="action-btn-block">
                                    {/*    <button disabled={this.state.error || pristine}  className="btn btn-primary btn-raised mr-3" onClick={this.checkValidation} title="Save">Save</button> */}

                                    {(this.state.error_flag === 1 || this.state.error_jobrole===1) ?
                                        <button disabled="disabled" className="btn btn-primary btn-raised mr-3" onClick={this.checkValidation} title="Save">Save</button>
                                        :
                                        <button className="btn btn-primary btn-raised mr-3" onClick={this.checkValidation} title="Save">Save</button>
                                    }
                                    <a title="Cancel" className="btn btn-outline" href="/candidates">Cancel</a>
                                </div>
                            </div>


                        </div>
                    </form>
                    {/*
                    <addCandidateForm 
                    {...this.props}
                    //initialValues={fields}
                    onSubmit={handleSubmit(this.handleSubmit)}
                    mode = 'add'
                  />*/
                    }
                    <Footer />
                </div>
            </div>
        );
    }
}
const fieldList = [
    'name',
    'age',
    'phone',
    'sex',
    'address',
    'statename',
    'districtname',
    'pincode',
    'training_type',
    'nsdc_enrollment_no',
    'training_status',
    'sectorname',
    'jobrole',
    'training_center',
    'batch_id',
    'schemename'
]
EditCandidate = reduxForm(
    {
        form: 'EditCandidate',
        validate,
        enableReinitialize: true,
        onSubmitFail: submitFailure(fieldList)
    })(EditCandidate);

function mapStateToProps(state, props) {
    const { user } = state.authentication;
    const { candidateformdata } = state.trainingpartners; //From Reducer - trainingpartners function ...
    const { districtlist } = state.generaldata; //From Reducer - CommonReducer function ...
    const { jobroles } = state.jobdata; //From Reducer - JobROleReducer function ...
    const { centralministriesdata } = state.centralministrydata; //From Reducer - CentralMinistry function ...
    const { candidate_updated_details } = state.getcandidatedetails; //From Reducer - trainingpartners function ...
    const { type,message } = state.alert; //From Alert reducer
    // const { message } = state;
    var formated_object = {};
    //console.log("in MapTOState", state);
    if (!!state.getcandidatedetails.candidate_updated_details) {
        var EditCandidateData = state.getcandidatedetails.candidate_updated_details.data[0];
        // console.log("Edit Details", state.getcandidatedetails.candidate_updated_details.data[0]);

        if(EditCandidateData.experience_in_years > 0){
            EditCandidateData.experience_in_years = EditCandidateData.experience_in_years
        }else{
            EditCandidateData.experience_in_years =''
        }
       // (EditCandidateData.experience_in_years > 0) ? EditCandidateData.experience_in_years : "";
        //  if(EditCandidateData.schemeid==5)  this.props.showStates  = 1; 

        formated_object = {

            aadhar_card_no: EditCandidateData.aadhaar_number,
            aadhaar_enrollment_id: EditCandidateData.aadhaar_enrollment_id,
            age: EditCandidateData.age,
            candidate_id: EditCandidateData.candidate_id,
            name: EditCandidateData.candidate_name,
            alternate_id_type: EditCandidateData.alternateidtype,
            altername_id_no: EditCandidateData.alternate_id_number,
            phone: EditCandidateData.phone,
            email_address: EditCandidateData.email_address,
            sex: EditCandidateData.gender,
            is_willing_to_relocate: EditCandidateData.willing_to_relocate,
            address: EditCandidateData.address,
            statename: (state.alert.type !== undefined && (state.alert.message === constants.CANDIDATE_STATE_CITY_SELECT_VALIDATION || state.alert.message === constants.CANDIDATE_COMBINATION_STATECITY_SELECT_VALIDATION) ? '' : EditCandidateData.stateid),
            districtname: (state.alert.type !== undefined && (state.alert.message === constants.CANDIDATE_STATE_CITY_SELECT_VALIDATION || state.alert.message === constants.CANDIDATE_COMBINATION_STATECITY_SELECT_VALIDATION) ? '' : EditCandidateData.cityid),
            pincode: EditCandidateData.pincode,
            nsdc_enrollment_no: EditCandidateData.sdmsnsdc_enrollment_number,
            training_status: EditCandidateData.training_status,
            training_type: EditCandidateData.training_type,
            attendance_percent: EditCandidateData.attendence_percentage,
            placement_status: EditCandidateData.placement_status,
            employment_type: EditCandidateData.employment_type,
            assessment_score: EditCandidateData.assessment_score,
            max_assessment_score: EditCandidateData.max_assessment_score,
            sectorname: (state.alert.type !== undefined && (state.alert.message === constants.CANDIDATE_SECTOR_JOBROLE_SELECT_VALIDATION || state.alert.message === constants.CANDIDATE_COMBINATION_SECTORJOBROLE_SELECT_VALIDATION) ? '' : EditCandidateData.sectorid),
            jobrole: (state.alert.type !== undefined && (state.alert.message === constants.CANDIDATE_SECTOR_JOBROLE_SELECT_VALIDATION || state.alert.message === constants.CANDIDATE_COMBINATION_SECTORJOBROLE_SELECT_VALIDATION) ? '' : EditCandidateData.jobroleid),
            training_center: EditCandidateData.trainingcenter_id,
            batch_id: EditCandidateData.batch_id,
            schemename: EditCandidateData.schemeid,
            state_scheme_name: EditCandidateData.stateministryid,
            exp_in_years: EditCandidateData.experience_in_years
        }

    }
    return {
        user, districtlist, jobroles, centralministriesdata, candidateformdata, candidate_updated_details,type,message,
        initialValues: formated_object
    };
}
function mapDispatchToProps(dispatch, ownProps) {
    let candidate_idvalue = window.location.hash.split(":");
    //Verify candidate id and it that is true then only below step should proceed...
    var getData = "";
    if (localStorage.getItem('user') !== null) {
        if (candidate_idvalue[0] === '#id' && candidate_idvalue[1] !== "") {
            getData = dispatch(userActions.getCandidateDetails(candidate_idvalue[1]));
            return { getData }
        }
    }

    else {
        history.push('/login');
        return ('');
    }
}

const connectedEditCandidate = connect(mapStateToProps, mapDispatchToProps)(EditCandidate);
export { connectedEditCandidate as EditCandidate };