import React from 'react';
import { connect } from 'react-redux';
import { history } from '../_helpers';
import { Header } from '../_components/Header';
import { Footer } from '../_components/Footer';
import { Sidebar } from '../_components/Sidebar';
import { FileUploadForFutureRequest } from '../_components/FileUploadForFutureRequest';
import { userActions } from '../_actions';

import { Breadcrumb, BreadcrumbItem } from 'reactstrap';
//import {RadioGroup} from '../_components/RadioGroup';
import 'react-day-picker/lib/style.css';
import { config } from '../config';

class AddMultipleFutureHiringRequest extends React.Component {
    /* constructor(props) {
        super(props);
    } */

    componentDidMount() {
        var { user } = this.props;
        if (user !== undefined && user.user_type === 'EMP') {
            this.props.dispatch(userActions.getSectorJobroleMapping(user.data[0].industrypartnerid, 'EMP'));
        }
    }

    render() {
        const { user } = this.props;
        //console.log('config file', config)
        const URL = config.API_HOST_URL + 'future_hiring_request_sample_csv';
        const industrypartnerid = this.props.user.data[0].industrypartnerid;
        const URLforSectorJobrole = config.API_HOST_URL + 'sectorJobrole_emp/' + industrypartnerid;
        const URLforStateCity = config.API_HOST_URL + 'stateCity_sample_csv';


        //const { isAddCandidateClick } = this.state;
        if (user === undefined) {
            //console.log("Redierect to specific Dashboard page");
            history.push('/login');
            return ('');
        }
        return (
            <div className="content-wrapper user-flow TP-flow">
                <Sidebar {...this.props} />

                <div id="content" className="main-content">
                    <Header />
                    <div className="page-content">

                        <div className="d-block d-md-flex align-items-center section-header">
                            <div>
                                <h1 className="page-title">Add Multiple Future Hiring Request</h1>
                                <Breadcrumb>
                                    <BreadcrumbItem><a href="/Employer-dashboard">Dashboard</a></BreadcrumbItem>
                                    <BreadcrumbItem><a href="/add-future-hiring-requests">Add Future Hiring Request</a></BreadcrumbItem>
                                    <BreadcrumbItem active>Add Multiple Future Hiring Request</BreadcrumbItem>
                                </Breadcrumb>
                            </div>
                        </div>


                        <div className="d-flex justify-content-center align-items-center auto-center-block bulk-upload">

                            <div className="text-center">
                                <FileUploadForFutureRequest />
                                <hr className="sm-divider section-seperator" />
                                <div className="mb-3">
                                    <a href={URL} title="Download sample CSV file" className="btn-outline btn mb-3">
                                        <i className="material-icons">arrow_downward</i>
                                        <span>Download sample file</span>
                                    </a>
                                </div>
                                <div className="mb-3">
                                    <a href={URLforSectorJobrole} title="Download sample CSV file" className="btn-outline btn mb-3">
                                        <i className="material-icons">arrow_downward</i>
                                        <span>Sector-Jobrole Sample</span>
                                    </a>
                                </div>
                                <div className="mb-3">
                                    <a href={URLforStateCity} title="Download sample CSV file" className="btn-outline btn mb-3">
                                        <i className="material-icons">arrow_downward</i>
                                        <span>State-City Sample</span>
                                    </a>
                                </div>
                            </div>

                        </div>
                    </div>

                    <Footer />
                </div>

            </div>
        );
    }
}


function mapStateToProps(state) {
    const { users, authentication } = state;
    const { user } = authentication;
    return {
        user,
        users
    };
}
const connectedAddMultipleFutureHiringRequest = connect(mapStateToProps)(AddMultipleFutureHiringRequest);
export { connectedAddMultipleFutureHiringRequest as AddMultipleFutureHiringRequest };