import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import DatePicker from 'react-datepicker'
import classnames from 'history'
import moment from 'moment'
import 'react-datepicker/dist/react-datepicker.css'

class DatePickerInput extends PureComponent {

    handleChange = (date) => {
        
        this.props.input.onChange(moment(date));
       // console.log("ARCHIT DATE",date);
    } 

    render() {
        const { input, className, meta: { error, touched }, required, ...rest } = this.props

        return (
            <div className="col-lg-6 col-12">
                <div className={classnames('form-group', { 'has-danger': error })}>
                    <label className='form-col-form-label' htmlFor={input.name}> {required ? '*' : ''}</label>
                    <DatePicker {...rest}
                        name="dob" 
                        autoOk
                        className={classnames(className, { 'form-control-danger': error })}
                        onChange={this.handleChange}
                        selected={input.value ? moment(input.value) : null} />
                    {touched && error && <span className='error-block'>{error}</span>}
                </div>
            </div>
        )
    }
}

DatePickerInput.propTypes = {
    className: PropTypes.string.isRequired,
    input: PropTypes.object.isRequired,
    meta: PropTypes.object.isRequired,
    required: PropTypes.bool
}

DatePickerInput.defaultProps = {
    className: 'form-control',
    required: false
}

export default DatePickerInput

