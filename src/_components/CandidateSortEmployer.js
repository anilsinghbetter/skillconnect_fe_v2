import React from 'react';
import { connect } from 'react-redux';
import { reduxForm, Field } from 'redux-form';
// import DayPickerInput from 'react-day-picker/DayPickerInput';
import { Button, FormGroup, Row, Col, Card, CardBody } from 'reactstrap';
// import { SearchIcon } from '../utils/SearchIcon';
import { userActions } from '../_actions';

// const validate = values => {
//     const errors = {}
//     return errors;
// }


const renderColorSelector = ({ input, label, dynamic_values, meta: { touched, error } }) => (
    <div key='1' className='form-group'>
        <label>{label}</label>
        <select  {...input} className="form-control ClearSelect">
            <option value="">Select</option>
            {dynamic_values}
            ))}
     </select>
    </div>
)

class CandidateSortEmployer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            submitted: false,
            selectedOption: '',
            collapse: false,
            updatedValue: this.props.mytest,
            results: [],
            cleared: false
        };
        this.handleSearchFilter = this.handleSearchFilter.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleClear = this.handleClear.bind(this);
    }

    handleChange(event) {
        const { name, value } = event.target;
        const { user } = this.state;

        this.setState({
            user: {
                ...user,
                [name]: value
            }
        }, () => this.handleSubmit());
    }

    componentDidMount() {
        this.props.dispatch(userActions.getCandidateSearchFormData(this.props.user.data[0].industrypartnerid,this.props.user.user_type));
    }

    handleSearchFilter() {
        this.setState({ collapse: !this.state.collapse });
    }
    handleSubmit(e) {
            
        if ((this.state.user !== undefined && this.state.user !== "") ) {
            var userFinal = {  ...this.state.user };
            userFinal = { ...userFinal, ...this.props.searchparams };
            this.setState({ submitted: true }); 
            
            this.props.sortData(userFinal,"Sort");
        }
    }

    handleClear(e) {
        var trainingpartnerid = this.props.user.data[0].trainingpartnerid;
        document.getElementById("SearchCandidateform").reset();
        
        this.setState({
            user:'',
            submitted: false,
            cleared: true
        });
        
        this.props.change("training_status", '');
        this.props.change("age_group", '');
        this.props.change("gender", '');

        var userFinal = {...this.props.searchparams};
        this.props.sortData(userFinal);
        // this.props.dispatch(userActions.getTrainingPartnerSearchCandidate(userFinal,1,0));
    }
    componentWillUnmount(){
        document.getElementById("SearchCandidateform").reset();
        // this.props.reset();
        this.setState({ 
            user:'',
            submitted: false,
            cleared: true
        });
        var userFinal = {...this.props.searchparams};
        this.props.dispatch(userActions.getTrainingPartnerSearchCandidate(userFinal,1,0));
        // console.log("UNMOUNTING>>>>>>>>>>>>>>>");
    }

    render() {
        
        if (!!this.props.searchformdata) {
           
            //Training Status list..
            var alltrainingstatus = this.props.searchformdata.data.training_status[0];
            var final_trainingstatus = [];
            alltrainingstatus.map((value,index) => {
                return final_trainingstatus.push(<option value={value.optionValue} key={index} >{value.optionKey}</option>);

            })

            //Age Group list..
            var allAgeGroup = this.props.searchformdata.data.age_group[0];
            var final_age_group = [];
            allAgeGroup.map((value,index) => {
                return final_age_group.push(<option value={value.optionValue} key={index} >{value.optionKey}</option>);

            });
           
        }
        return (

            <Card className="z-depth-1 no-hover">
                
                <CardBody>
                    <div className="search-bar mb-0">
                        <Row className="align-items-center">
                            <Col className="mb-0 d-none d-md-flex align-items-center">
                                <form name="SearchCandidateform" id="SearchCandidateform" onSubmit={this.handleSubmit} className="w-100">
                                    <div className={'row align-items-center' + (this.state.collapse ? ' d-none' : '')} >
                                        <div className="col-12 col-md-4 pr-0">
                                            <FormGroup>
                                            <Field name="training_status" component={renderColorSelector} dynamic_values={final_trainingstatus} onChange={this.handleChange} label="Training Status" />
                                            </FormGroup>
                                        </div>
                                        <div className="col-12 col-md-4 pr-0">
                                            <FormGroup>
                                            <Field name="age_group" component={renderColorSelector} dynamic_values={final_age_group} onChange={this.handleChange} label="Age Group" />
                                            </FormGroup>
                                        </div>
                                        <div className="col-12 col-md-4 pr-0">
                                            <FormGroup>
                                            <label htmlFor="gender" className="d-block">Gender</label>
                                
                                            <Field name="gender" component="input" type="radio" value="male" className="ml-0 ClearRadio" onChange={this.handleChange} /> Male
                                            <Field name="gender" component="input" type="radio" value="female" className="ClearRadio" onChange={this.handleChange} /> Female
                                            <Field name="gender" component="input" type="radio" value="other" className="ClearRadio" onChange={this.handleChange} /> Other
                                
                                            </FormGroup>
                                        </div>
                                       
                                        {/* <Button color="default">Go</Button> */}
                                        <div className="col-12 col-md-2 pr-0">
                                            {!!this.state.submitted && <Button color="default" id="clearButton" onClick={this.handleClear}>Clear</Button>} 
                                        </div>
                                        
                                        
                                    </div>
                                </form>
                            </Col>
                        </Row>
                    </div>
                </CardBody>
            </Card>

        );
    }
}

CandidateSortEmployer = reduxForm({
    form: 'CandidateSortEmployer',
    // validate,
    renableReinitialize: true,
    asyncBlurFields: [], //to remove validation, we must need to remove it from Blur fields...
})(CandidateSortEmployer);

function mapStateToProps(state, ownProps) {
    // console.log("My statesProps",ownProps);
    const { user } = state.authentication;
    const { loading } = state.trainingpartners; //From Reducer - trainingpartners function ...
    const { searchformdata } = state.searchform;
    const { jobroles } = state.jobdata; //From Reducer - JobROleReducer function ...
    const { districtlist } = state.generaldata; //From Reducer - CommonReducer function ...
    // console.log(searchformdata, "searchformdata");
    

    return {
        user, searchformdata, jobroles, districtlist,loading
    };
}
const connectedCandidateSortEmployer = connect(mapStateToProps)(CandidateSortEmployer);
export { connectedCandidateSortEmployer as CandidateSortEmployer};