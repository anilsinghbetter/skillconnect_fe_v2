import React from 'react';
import { connect } from 'react-redux';
import { userActions } from '../_actions';
import { reduxForm, Field } from 'redux-form';
import { Header } from '../_components/Header';
import { Footer } from '../_components/Footer';
import { history } from '../_helpers';
import { Sidebar } from '../_components/Sidebar';
import { Label, FormGroup, Input, Button, Breadcrumb, BreadcrumbItem } from 'reactstrap';
import trainingCentreSVG from '../assets/images/training_center_grey.svg';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import 'react-day-picker/lib/style.css';
import { submitFailure } from '../../src/utils/submitFailure';

const currentYear = new Date().getFullYear();
const currentMonth = new Date().getMonth();
const fromMonth = new Date(currentYear -40, currentMonth);
const toMonth = new Date(currentYear, currentMonth);

function YearMonthForm({ date, localeUtils, onChange }) {
    const months = localeUtils.getMonths();
  
    const years = [];
    for (let i = fromMonth.getFullYear(); i <= toMonth.getFullYear(); i += 1) {
      years.push(i);
    }
  
    const handleChange = function handleChange(e) {
      const { year, month } = e.target.form;
      onChange(new Date(year.value, month.value));
    };
  
    return (
        <div className="DayPicker-Caption">
            <select name="month" onChange={handleChange} value={date.getMonth()}>
            {months.map((month, i) => (
                <option key={month} value={i}>
                {month}
                </option>
            ))}
            </select>
            <select name="year" onChange={handleChange} value={date.getFullYear()}>
            {years.map(year => (
                <option key={year} value={year}>
                {year}
                </option>
            ))}
            </select>
        </div>
    );
}

const renderTextAreaField = ({ input, label, type, placeholder, textarea, meta: { touched, error, warning, invalid } }) => {

    const textareaType = <textarea {...input} placeholder={label} type={type} className={`form-control ${touched && invalid ? 'has-danger' : ''}`} />;

    return <div className=''>
        <div className={'form-group' + (error && touched ? ' has-error' : '')}>
            <label>{label}</label>
            <div> {textarea ? textareaType : ""}
                {touched && ((error && <div className="help-block">{error}</div>) || (warning && <span>{warning}</span>))}
            </div>

        </div>
    </div>
};

const renderField = ({ input, label, type, placeholder, textarea, meta: { touched, error, warning, invalid } }) => {
    const textareaType = <textarea {...input} placeholder={label} type={type} className={`form-control ${touched && invalid ? 'has-danger' : ''}`} />;
    return <div className={((label === "SDMS/NSDC Enrollment No" || label === "Attendance Percent" || label === "Pincode" || label === "Experience In Years" || label === "Assessment Score" || label === "Max Assessment Score" || label === "Batch ID") ? 'col-md-4 col-12' : 'col-md-6 col-12')}>
        <div className={'form-group' + (error && touched ? ' has-error' : '')}>
            <label>{label}</label>
            <div> {textarea ? textareaType : <input {...input} type={type} placeholder={placeholder} className="form-control" />}
                {touched && ((error && <div className="help-block">{error}</div>) || (warning && <span>{warning}</span>))}
            </div>

        </div>
    </div>
};

const renderError = ({ meta: { touched, error } }) =>

    touched && error ? <div className="help-block">{error}</div> : false

const validate = values => {

    const errors = {}

    if (!values.name) {
        errors.name = 'Please enter name of candidate'
    } /*else if (values.username.length > 15) {
            errors.username = 'Must be 15 characters or less'
            }  */
    /* if (!values.aadhar_card_no) {
        errors.aadhar_card_no = 'Please enter aadhar card number'
    }  if (!values.candidate_id) {
        errors.candidate_id = 'Please enter candidate id'
    } 
    if (!values.contact_email_address) {
        errors.contact_email_address = 'Please enter an Email Address'
    }*/
    if (!values.age) {
        errors.age = 'Please enter age'
    }
    if (!values.phone) {
        errors.phone = 'Please enter mobile number'
    }
    if (!values.pincode) {
        errors.pincode = 'Please enter Pincode'
    }
    if (!values.sex) {
        errors.sex = 'Please select gender'
    }
   /*  if (!values.is_willing_to_relocate) {
        errors.is_willing_to_relocate = 'Please select willing to relocate'
    } */
    if (!values.address) {
        errors.address = 'Please enter Address'
    }
    if (!values.nsdc_enrollment_no) {
        errors.nsdc_enrollment_no = 'Please enter SDMS/NSDC Enrollment No'
    }
    if (!values.statename) {
        errors.statename = 'Please select a state'
    }
    if (!values.districtname) {
        errors.districtname = 'Please select a district'
    }
    if (!values.training_type) {
        errors.training_type = "Please select a training type";
    }
    if (!values.training_status) {
        errors.training_status = "Please select training status";
    }
    /* if (!values.placement_status) {
        errors.placement_status = "Please select placement status";
    }
    if (!values.employment_type) {
        errors.employment_type = "Please select employment status";
    } */
    if (!values.sectorname) {
        errors.sectorname = "Please select sector name";
    }
    if (!values.jobrole) {
        errors.jobrole = "Please select job role";
    }
    if (!values.batch_id) {
        errors.batch_id = "Please select batch Id";
    }
    if (!values.schemename) {
        errors.schemename = "Please select a scheme";
    }
    if (!values.state_scheme_name) {
        errors.state_scheme_name = "Please select state scheme";
    }
    if (!values.training_center) {
        errors.training_center = "Please select Training Center";
    }
    return errors;
}
class AddNewCandidate extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            submitted: false,
            combovalue: '',
            startDate: "",
            selectedDay: undefined,
            selectedAssessmentDate: undefined,
            selectedBatchStartDate: undefined,
            selectedBatchEndDate: undefined,
            showState: false,
            female: "",
            male: "",
            yes: "",
            no: "",
            registerClick: 0,
            error_flag: 0,
            error_dist_flag: 0,
            month: fromMonth,
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        //Date Related Pure Functions..
        this.handleDayClick = this.handleDayClick.bind(this);
        this.handleAssessmentClick = this.handleAssessmentClick.bind(this);
        this.handleBatchStartDateClick = this.handleBatchStartDateClick.bind(this);
        this.handleBatchEndDateClick = this.handleBatchEndDateClick.bind(this);
        this.checkValidation = this.checkValidation.bind(this);
        this.handleYearMonthChange = this.handleYearMonthChange.bind(this);
    }
    handleYearMonthChange(month) {
        this.setState({ month });
    }
    componentDidMount() {
        // const { user } = this.props;
       // this.props.dispatch(userActions.getAll());
        // console.log("Traingin partner id", this.props.user.data[0].trainingpartnerid);
       // this.props.dispatch(userActions.getCandidateFormData(this.props.user.data[0].trainingpartnerid));
    }
    handleRedirectionToAddMultipleCandidate(e) {
        history.push('/add-multiple-candidates');
    }
    handleRedirectionToAddCenter(e) {
        history.push('/add-edit-center');
    }

    handleSubmit(event) {
        // event.preventDefault();
        const { user } = this.state;
        const { dispatch } = this.props;
        this.setState({ submitted: true });
        
        //if (this.state.selectedDay != "") var dob = this.state.selectedDay.toLocaleDateString();
        if (this.state.selectedDay !== "") var dob = this.state.selectedDay;
        if (this.state.selectedAssessmentDate !== "") var assessment_date = this.state.selectedAssessmentDate;

        if (this.state.selectedBatchStartDate !== "") var batch_start_date = this.state.selectedBatchStartDate;
        if (this.state.selectedBatchEndDate !== "") var batch_end_date = this.state.selectedBatchEndDate;

        var candidateid = '-1';
        var trainingpartnerid = this.props.user.data[0].trainingpartnerid;
        var userFinal = { trainingpartnerid, candidateid, dob, assessment_date, batch_start_date, batch_end_date, ...user };
        //console.log("send data", userFinal);

        if (userFinal.nsdc_enrollment_no && userFinal.name) {
            dispatch(userActions.addUpdateCandidates(userFinal));
        }
    }
    checkValidation(event) {
        //console.log("Register clicked.");
        this.setState({ registerClick: 1 });
    }

    handleChange(event) {

        const { name, value } = event.target;
        const { user } = this.state;
        // const { dispatch } = this.props;
        // this.setState({ value: event.target.value });
        this.setState({
            user: {
                ...user,
                [name]: value
            }
        });
        // console.log("event data change", typeof event.target.value);

        if (event.target.name === 'districtname') { this.setState({ error_dist_flag: 1 }) }
        if (event.target.name === 'statename') { this.setState({ error_flag: 1 }); this.props.dispatch(userActions.getDistrict(value)); }
        if (event.target.name === 'sectorname') { this.props.dispatch(userActions.getJobRoles(value)); }
        if (event.target.name === 'schemename' && event.target.value === '5') { this.setState({ showState: true }); }
        if (event.target.name === 'schemename' && event.target.value !== '5') {
            this.setState({ showState: false });
            this.props.dispatch(userActions.getSchemeCentralMinistries(value));
        }
    }
    handleDayClick(day) {
        this.setState({ selectedDay: day });
        //console.log(day, "SELECTED DAY");
    }
    handleAssessmentClick(day) {
        this.setState({ selectedAssessmentDate: day });
    }
    handleBatchStartDateClick(day) {
        this.setState({ selectedBatchStartDate: day });
    }
    handleBatchEndDateClick(day) {
        this.setState({ selectedBatchEndDate: day });
    }

    render() {

        const { handleSubmit } = this.props;
        const { female, male, yes, no, registerClick } = this.state;

        const dayPickerProps = {
            month: this.state.month,
            fromMonth: fromMonth,
            toMonth: toMonth,
            captionElement: ({ date, localeUtils }) => (
              <YearMonthForm
                date={date}
                localeUtils={localeUtils}
                onChange={this.handleYearMonthChange}
              />
            )
        };


        // console.log("user details", user);
        //console.log("Form Data", this.props);
        //console.log("disctrict value", districtlist);
        //console.log("Job value", jobroles);
        //console.log("general props", this.props);
        //console.log("get cnetral minstiry data",centralministriesdata);  

        if (!!this.props.districtlist) {
            var allCities = this.props.districtlist.data;
            var final_allCities = [];
            //State values..
            allCities.map((value, index) => {
                return final_allCities.push(<option key={index} value={value.cityid}>{value.cityName}</option>);
            })
        }
        if (!!this.props.centralministriesdata) {
            var central_minstry_name = this.props.centralministriesdata.data[0].centralministryname;
        }
        if (!!this.props.jobroles) {
            var allJobroles = this.props.jobroles.data;
            var final_allJobs = [];
            allJobroles.map((value, index) => {
                return final_allJobs.push(<option key={index} value={value.jobroleid}>{value.jobroleName}</option>);
            })
        }
        if (!!this.props.candidateformdata) {
          console.log("###",this.props.candidateformdata);
            //Training Center list..
            var isTrainingCenter = 0;
            var allTrainingCenterList = this.props.candidateformdata.data.training_centre[0];
            var final_TrainingCenter = [];
            if (allTrainingCenterList !== undefined) {
                isTrainingCenter = 1;
                allTrainingCenterList.map((value, index) => {
                    return final_TrainingCenter.push(<option key={index} value={value.trainingcenterid}>{value.trainingcenter_name}</option>);

                })
            }
            else {
                isTrainingCenter = 0;
                //console.log('candidate tc',allTrainingCenterList);
                //this.props.dispatch(alertActions.error("Please add Training Center"));
            }

            //State values..
            var allStates = this.props.candidateformdata.data.state[0];
            var final_allStates = [];
            allStates.map((value, index) => {
                return final_allStates.push(<option key={index} value={value.stateid}>{value.stateName}</option>);
            })

            //Alternative IDs..
            var allAlternativeIds = this.props.candidateformdata.data.alternateidtype[0];
            var final_alternative_ids = [];
            allAlternativeIds.map((value, index) => {
                return final_alternative_ids.push(<option key={index} value={value.optionValue}>{value.optionKey}</option>);
            })

            //Employment Status values..
            var allEmploymentStatus = this.props.candidateformdata.data.employment_status[0];
            var final_employementstatus = [];
            allEmploymentStatus.map((value, index) => {
                return final_employementstatus.push(<option key={index} value={value.optionValue}>{value.optionKey}</option>);
            })
            //PlacementStatus list..
            var allplacementlist = this.props.candidateformdata.data.placement_status[0];
            var final_placementstatus = [];
            allplacementlist.map((value, index) => {
                return final_placementstatus.push(<option key={index} value={value.optionValue}>{value.optionKey}</option>);

            })


            //Gender Type list..
            var allgendertype = this.props.candidateformdata.data.gender[0];
            var final_genderlist = [];
            allgendertype.map((value, index) => {
                return final_genderlist.push(<option key={index} value={value.optionValue}>{value.optionKey}</option>);

            })
            //Scheme list..
            var allschemes = this.props.candidateformdata.data.scheme[0];
            var final_schemes = [];
            allschemes.map((value, index) => {
                return final_schemes.push(<option key={index} value={value.pk_schemeID}>{value.schemeType}</option>);

            })
            //Sector list..
            var allsectors = this.props.candidateformdata.data.sector[0];
            var final_sectors = [];
            allsectors.map((value, index) => {
                return final_sectors.push(<option key={index} value={value.sectorid}>{value.sectorname}</option>);

            })
            //Training Status list..
            var alltrainingstatus = this.props.candidateformdata.data.training_status[0];
            var final_trainingstatus = [];
            alltrainingstatus.map((value, index) => {
                return final_trainingstatus.push(<option key={index} value={value.optionValue}>{value.optionKey}</option>);

            })
            //Training Type list..
            var alltrainingtype = this.props.candidateformdata.data.training_type[0];
            var final_trainingtype = [];
            alltrainingtype.map((value, index) => {
                return final_trainingtype.push(<option key={index} value={value.optionValue}>{value.optionKey}</option>);

            })
            //console.log("fina array", final_allSectors);
        }
        const renderColorSelector = ({ input, label, dynamic_values, meta: { touched, error } }) => (
            <div className={'form-group' + (error && touched ? ' has-error' : '')}>
                <label>{label}</label>
                <select {...input} className="form-control">
                    <option value="">Select</option>
                    {dynamic_values}
                    ))}
              </select>
                {touched && error && <div className="help-block">{error}</div>}
            </div>

        )
        return (<div className="content-wrapper user-flow TP-flow">

            <div id="content" className="main-content">
                <Sidebar {...this.props} />
                <Header />

                {isTrainingCenter === 0 ?
                    <div>
                        <div className="d-block d-md-flex align-items-center section-header">
                            <div>
                                <h1 className="page-title">Add Training Center</h1>
                                <Breadcrumb>
                                    <BreadcrumbItem><a href="/TrainingPartner-dashboard">Dashboard</a></BreadcrumbItem>
                                    <BreadcrumbItem><a href="/training-centers">Training Centers</a></BreadcrumbItem>
                                    <BreadcrumbItem active>Add Center</BreadcrumbItem>
                                </Breadcrumb>
                            </div>
                        </div>

                        <div className="d-flex justify-content-center align-items-center error-page">
                            <div className="text-center">
                                <div className="img-wrapper mb-4">
                                    <div className="circle-xl user-avatar">
                                        <img src={trainingCentreSVG} alt="trainingCenter" />
                                    </div>
                                </div>
                                <h1 className="h2 fw-regular mb-0">Add Training Center!</h1>
                                <span className="h6 ff-roboto d-block mt-0">
                                    To add candidate, you will first have to add training center
                            </span>
                                <Button color="primary" className="btn-raised" title="Add Training Center" onClick={this.handleRedirectionToAddCenter}>
                                    <i className="material-icons">add</i>
                                    Add Training Center
                            </Button>
                            </div>
                        </div>
                    </div>
                    :
                    <div>
                        <div className="d-block d-md-flex align-items-center section-header">
                            <div>
                                <h1 className="page-title">Add Candidate</h1>
                                <Breadcrumb>
                                    <BreadcrumbItem><a href="/TrainingPartner-dashboard">Dashboard</a></BreadcrumbItem>
                                    <BreadcrumbItem><a href="/candidates">Candidates</a></BreadcrumbItem>
                                    <BreadcrumbItem active>Add Candidate</BreadcrumbItem>
                                </Breadcrumb>
                            </div>
                            <div className="ml-auto">
                                <Button outline color="primary" title="Bulk Upload" onClick={this.handleRedirectionToAddMultipleCandidate} className="sm-floating-btn" >
                                    <i className="material-icons">arrow_upward</i>
                                    <span>Bulk Upload</span>
                                </Button>
                            </div>
                        </div>

                        <form id="frm-add-candidate" className="form frm-edit-candidaten" onSubmit={handleSubmit(this.handleSubmit)}>
                            <div className="card z-depth-1">
                                <div className="card-header bg-transparent border-bottom-0 section-title no-border form-title">
                                    <h5 className="title card-title">Personal Details</h5>
                                </div>
                                <div className="card-body pb-0">

                                    <div className="row">
                                        <Field name="name" type="text" component={renderField} label="Name of Candidate" placeholder="Name of Candidate" onChange={this.handleChange} />
                                        <Field name="aadhar_card_no" type="text" component={renderField} label="Aadhaar No" placeholder="Aadhaar No" onChange={this.handleChange} />
                                    </div>
                                    <div className="row">
                                        <Field name="age" type="text" component={renderField} label="Age" placeholder="Age" onChange={this.handleChange} />
                                        <Field name="candidate_id" type="text" component={renderField} label="Candidate ID" placeholder="Candidate ID" onChange={this.handleChange} />
                                    </div>
                                    <div className="row">
                                        <Field name="aadhaar_enrollment_id" type="text" component={renderField} label="Aadhaar Enrollment ID" placeholder="Aadhaar Enrollment ID" onChange={this.handleChange} />
                                        <div className="col-12 col-md-6">
                                            <FormGroup>
                                                <Label htmlFor="Alternate ID Type">Alternate ID Type</Label>
                                                <Input type="select" name="alternate_id_type" id="alternate_id_type" onChange={this.handleChange}>
                                                    <option value="">Select</option>
                                                    {final_alternative_ids}
                                                </Input>
                                            </FormGroup>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <Field name="altername_id_no" type="text" component={renderField} label="Alternate ID No" placeholder="Alternate ID No" onChange={this.handleChange} />
                                        <Field name="phone" type="text" component={renderField} label="Mobile No" placeholder="Mobile No" onChange={this.handleChange} />
                                    </div>

                                    <div className="row">
                                        <Field name="email_address" type="text" component={renderField} label="Email Address" placeholder="Email Address" onChange={this.handleChange} />

                                        <div className="col-12 col-md-6">
                                            <div className={'form-group' + (!this.state.selectedDay ? ' has-error' : '')}>
                                                <label htmlFor="date_of_birth">Date Of Birth</label>
                                                <div onClick={this.showDatePicker} className="input-group date date-picker" data-provide="datepicker">
                                                    <DayPickerInput dayPickerProps={dayPickerProps} onDayChange={this.handleDayClick} />
                                                    <div className="input-group-addon">
                                                        <i className="material-icons">date_range</i>
                                                    </div>
                                                </div>
                                                {/* <div className="help-block">{!this.state.selectedDay && registerClick === 1 ? 'Please select Date Of Birth' : ''}</div> */}

                                            </div>
                                        </div>
                                    </div>
                                    <div className={'form-group' + (!male || !female ? ' has-error' : '')}>
                                        <label htmlFor="gender" className="d-block">Gender</label>
                                        <Field name="sex" component="input" type="radio" value="male" className="ml-0" onChange={this.handleChange} /> Male
                                <Field name="sex" component="input" type="radio" value="female" onChange={this.handleChange} /> Female
                                <Field name="sex" component="input" type="radio" value="other" onChange={this.handleChange} /> Other
                                <Field name="sex" component={renderError} />
                                    </div>

                                    <div className={'form-group' + (!yes || !no ? ' has-error' : '')}>
                                        <label htmlFor="relocate" className="d-block">Willing To Relocate</label>
                                        <Field name="is_willing_to_relocate" component="input" type="radio" value="yes" className="ml-0" onChange={this.handleChange} /> Yes
                                <Field name="is_willing_to_relocate" component="input" type="radio" value="no" onChange={this.handleChange} /> No
                                {/* <Field name="is_willing_to_relocate" component={renderError} /> */}
                                    </div>

                                    <div className="">
                                        <Field name="address" component={renderTextAreaField} type="textarea" label="Address" textarea={true} onChange={this.handleChange} />
                                    </div>

                                    <div className="row">
                                        <div className="col-12 col-md-4">
                                            <div className='form-group'>
                                                <Field name="statename" component={renderColorSelector} dynamic_values={final_allStates} onChange={this.handleChange} label="State" />
                                            </div>
                                        </div>

                                        <div className="col-12 col-md-4">
                                            <div className='form-group'>
                                                <Field name="districtname" component={renderColorSelector} dynamic_values={final_allCities} onChange={this.handleChange} label="District" />
                                            </div>
                                        </div>
                                        <Field name="pincode" type="text" component={renderField} label="Pincode" placeholder="Pincode" onChange={this.handleChange} />

                                    </div>
                                </div>

                                <hr className="inner-form-seperator" />

                                <div className="card-header bg-transparent border-bottom-0 section-title no-border form-title">
                                    <h5 className="title card-title">Professional Details</h5>
                                </div>
                                <div className="card-body">
                                    <div className="row">
                                        <Field name="exp_in_years" type="text" component={renderField} label="Experience In Years" placeholder="Experience In Years" onChange={this.handleChange} />
                                        <div className="col-12 col-md-4">
                                            <FormGroup>
                                                <Field name="training_type" component={renderColorSelector} dynamic_values={final_trainingtype} onChange={this.handleChange} label="Training Type" />
                                            </FormGroup>
                                        </div>
                                        <Field name="nsdc_enrollment_no" type="text" component={renderField} label="SDMS/NSDC Enrollment No" placeholder="SDMS/NSDC Enrollment No" onChange={this.handleChange} />
                                    </div>

                                    <div className="row">
                                        <div className="col-12 col-md-4">
                                            <FormGroup>
                                                <Field name="training_status" component={renderColorSelector} dynamic_values={final_trainingstatus} onChange={this.handleChange} label="Training Status" />
                                            </FormGroup>
                                        </div>

                                        <Field name="attendance_percent" type="text" component={renderField} label="Attendance Percent" placeholder="Attendance Percent" onChange={this.handleChange} />
                                        <div className="col-12 col-md-4">
                                            <FormGroup>
                                                <Field name="placement_status" component={renderColorSelector} dynamic_values={final_placementstatus} onChange={this.handleChange} label="Placement Status" />
                                            </FormGroup>
                                        </div>
                                    </div>

                                    <div className="row">
                                        <div className="col-12 col-md-4">
                                            <FormGroup>
                                                <Field name="employment_type" component={renderColorSelector} dynamic_values={final_employementstatus} onChange={this.handleChange} label="Employment Type" />
                                            </FormGroup>
                                        </div>
                                        <Field name="assessment_score" type="text" component={renderField} label="Assessment Score" placeholder="Assessment Score" onChange={this.handleChange} />
                                        <Field name="max_assessment_score" type="text" component={renderField} label="Max Assessment Score" placeholder="Max Assessment Score" onChange={this.handleChange} />
                                    </div>

                                    <div className="row">
                                        <div className="col-12 col-md-4">
                                            <div className={'form-group' + (!this.state.selectedAssessmentDate ? ' has-error' : '')}>
                                                <label htmlFor="assessment_date">Assessment Date</label>
                                                <div className="input-group date date-picker" data-provide="datepicker">
                                                    <DayPickerInput onDayChange={this.handleAssessmentClick} />
                                                    <div className="input-group-addon">
                                                        <i className="material-icons">date_range</i>
                                                    </div>
                                                </div>
                                                <div className="help-block">{!this.state.selectedAssessmentDate && registerClick === 1 ? 'Please enter Assessment Date' : ''}</div>
                                            </div>
                                        </div>
                                        <div className="col-12 col-md-4">
                                            <div className={'form-group' + (!this.state.selectedBatchStartDate ? ' has-error' : '')}>
                                                <label htmlFor="Batch Start Date">Batch Start Date</label>
                                                <div className="input-group date date-picker" data-provide="datepicker">
                                                    <DayPickerInput onDayChange={this.handleBatchStartDateClick} />
                                                    <div className="input-group-addon">
                                                        <i className="material-icons">date_range</i>
                                                    </div>
                                                </div>
                                                <div className="help-block">{!this.state.selectedBatchStartDate && registerClick === 1 ? 'Please enter Batch Start Date' : ''}</div>
                                            </div>
                                        </div>
                                        <div className="col-12 col-md-4">
                                            <div className={'form-group' + (!this.state.selectedBatchEndtDate ? ' has-error' : '')}>
                                                <label htmlFor="Batch End Date">Batch End Date</label>
                                                <div className="input-group date date-picker" data-provide="datepicker">
                                                    <DayPickerInput onDayChange={this.handleBatchEndDateClick} />
                                                    <div className="input-group-addon">
                                                        <i className="material-icons">date_range</i>
                                                    </div>
                                                </div>
                                                <div className="help-block">{!this.state.selectedBatchEndDate && registerClick === 1 ? 'Please enter Batch End Date' : ''}</div>
                                            </div>
                                        </div>

                                    </div>

                                    <div className="row">

                                        <div className="col-12 col-md-4">
                                            <div className="form-group">
                                                <FormGroup>
                                                    <Field name="sectorname" component={renderColorSelector} dynamic_values={final_sectors} onChange={this.handleChange} label="Sector" />
                                                </FormGroup>
                                            </div>
                                        </div>

                                        <div className="col-12 col-md-4">
                                            <div className="form-group">
                                                <FormGroup>
                                                    <Field name="jobrole" component={renderColorSelector} dynamic_values={final_allJobs} onChange={this.handleChange} label="Job Role" />
                                                </FormGroup>
                                            </div>
                                        </div>


                                        <div className="col-12 col-md-4">
                                            <div className="form-group">
                                                <FormGroup>
                                                    <Field name="training_center" component={renderColorSelector} dynamic_values={final_TrainingCenter} onChange={this.handleChange} label="Training Center" />
                                                </FormGroup>
                                            </div>
                                        </div>
                                    </div>


                                    <div className="row">

                                        <Field name="batch_id" type="text" component={renderField} label="Batch ID" placeholder="Batch ID" onChange={this.handleChange} />

                                        <div className="col-12 col-md-4">
                                            <div className="form-group">
                                                <FormGroup>
                                                    <Field name="schemename" component={renderColorSelector} dynamic_values={final_schemes} onChange={this.handleChange} label="Scheme" />
                                                </FormGroup>
                                            </div>
                                        </div>
                                        {this.state.showState &&
                                            <div className="col-12 col-md-4">
                                                <FormGroup>
                                                    <Field name="state_scheme_name" component={renderColorSelector} dynamic_values={final_allStates} onChange={this.handleChange} label="State Scheme" />
                                                </FormGroup>
                                            </div>
                                        }
                                        {!this.state.showState && central_minstry_name &&
                                            <div className="form-group col-12 col-md-4">
                                                <label>Ministry Name</label>
                                                <div>
                                                    <input type="text" value={central_minstry_name} title={central_minstry_name} className="form-control" readOnly />
                                                </div>
                                            </div>
                                        }
                                    </div>

                                    <div className="action-btn-block">
                                        <button className="btn btn-primary btn-raised mr-3" onClick={this.checkValidation} title="Save">Save</button>
                                        <a title="Cancel" className="btn btn-outline" href="/candidates">Cancel</a>
                                    </div>
                                </div>


                            </div>
                        </form>
                    </div>
                }
                {/*
                    <addCandidateForm 
                    {...this.props}
                    //initialValues={fields}
                    onSubmit={handleSubmit(this.handleSubmit)}
                    mode = 'add'
                  />*/
                }
                <Footer />
            </div>
        </div>
        );
    }
}
const fieldList = [
    'name',
    'age',
    'phone',
    'sex',
    'address',
    'statename',
    'districtname',
    'pincode',
    'training_type',
    'nsdc_enrollment_no',
    'training_status',
    'sectorname',
    'jobrole',
    'training_center',
    'batch_id',
    'schemename'
]
AddNewCandidate = reduxForm({
    form: 'AddNewCandidate',
    validate,
    enableReinitialize: true,
    onSubmitFail: submitFailure(fieldList)
})(AddNewCandidate);

function mapStateToProps(state) {
    const { user } = state.authentication;
    const { candidateformdata } = state.trainingpartners; //From Reducer - trainingpartners function ...
    const { districtlist } = state.generaldata; //From Reducer - CommonReducer function ...
    const { jobroles } = state.jobdata; //From Reducer - JobROleReducer function ...
    const { centralministriesdata } = state.centralministrydata; //From Reducer - CentralMinistry function ...

    return {
        user, candidateformdata, districtlist, jobroles, centralministriesdata
    };
}
function mapDispatchToProps(dispatch, ownProps) {

    //let candidate_idvalue = window.location.hash.split(":");
    //Verify candidate id and it that is true then only below step should proceed...
    //let getData = "";

    if (localStorage.getItem('user') !== null) {
        let user = JSON.parse(localStorage.getItem('user'));
        let tpid= user.data[0].trainingpartnerid;
        console.log("TPID",tpid);

       let getdata = dispatch(userActions.getCandidateFormData(tpid));
        return {getdata}
    }
    else {
        history.push('/login');
        return ('');
    }
}
const connectedAddNewCandidate = connect(mapStateToProps,mapDispatchToProps)(AddNewCandidate);
export { connectedAddNewCandidate as AddNewCandidate };