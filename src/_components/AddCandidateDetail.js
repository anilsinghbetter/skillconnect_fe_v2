import React from 'react';
import { connect } from 'react-redux';
import { userActions } from '../_actions';
import { reduxForm, Field } from 'redux-form';
import { Header } from '../_components/Header';
import { Footer } from '../_components/Footer';
import { history } from '../_helpers';
import { Sidebar } from '../_components/Sidebar';
import { Breadcrumb, BreadcrumbItem, Row, Col, CardBody } from 'reactstrap';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import {formatDate,parseDate,} from 'react-day-picker/moment';
import 'react-day-picker/lib/style.css';

const renderTextAreaField = ({ input, label, type, placeholder, textarea, meta: { touched, error, warning, invalid } }) => {

    const textareaType = <textarea {...input} placeholder={label} type={type} className={`form-control ${touched && invalid ? 'has-danger' : ''}`} />;

    return <div className=''>
        <div className={'form-group' + (error && touched ? ' has-error' : '')}>
            <label>{label}</label>
            <div> {textarea ? textareaType : ""}
                {touched && ((error && <div className="help-block">{error}</div>) || (warning && <span>{warning}</span>))}
            </div>

        </div>
    </div>
};

const renderField = ({ input, label, type, placeholder, textarea, meta: { touched, error, warning, invalid } }) => {
    const textareaType = <textarea {...input} placeholder={label} type={type} className={`form-control ${touched && invalid ? 'has-danger' : ''}`} />;
    return <div className={((label === "Interview Time" || label === "Monthly Salary" || label === "Name" || label === "Email" || label === "Mobile Number") ? 'col-md-4 col-12' : 'col-md-6 col-12')}>
        <div className={'form-group' + (error && touched ? ' has-error' : '')}>
            <label>{label}</label>
            <div> {textarea ? textareaType : <input {...input} type={type} placeholder={placeholder} className="form-control" />}
                {touched && ((error && <div className="help-block">{error}</div>) || (warning && <span>{warning}</span>))}
            </div>

        </div>
    </div>
};

const renderError = ({ meta: { touched, error } }) =>

    touched && error ? <div className="help-block">{error}</div> : false

const validate = values => {

    const errors = {}

    if (!values.interview_time) {
        errors.interview_time = 'Please select time'
    }
    if (!values.salary) {
        errors.salary = 'Please enter monthly salary'
    }
    if (!values.statename) {
        errors.statename = 'Please select a state'
    }
    if (!values.districtname) {
        errors.districtname = 'Please select a district'
    }
    if (!values.is_pf) {
        errors.is_pf = 'Please select PF'
    }
    if (!values.is_esic) {
        errors.is_esic = 'Please select ESIC'
    }
    /* if (!values.is_food) {
        errors.is_food = 'Please select Food'
    }
    if (!values.is_transportation) {
        errors.is_transportation = 'Please select Transportation'
    }
    if (!values.is_accomodation) {
        errors.is_accomodation = "Please select Accomodation";
    } */
    if (!values.other_perks) {
        errors.other_perks = 'Please enter other perks'
    }
    if (!values.contact_person_name) {
        errors.contact_person_name = "Please enter contact person name";
    }
    if (!values.contact_person_email) {
        errors.contact_person_email = "Please enter contact person email";
    }
    if (!values.contact_person_phone) {
        errors.contact_person_phone = "Please enter contact person mobile";
    }
    return errors;
}
class AddCandidateDetail extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            submitted: false,
            startDate: "",
            selectedDay: undefined,
            showState: false,
            pf_yes: "",
            pf_no: "",
            esic_yes: "",
            esic_no: "",
            food_yes: "",
            food_no: "",
            trp_yes: "",
            trp_no: "",
            acm_yes: "",
            acm_no: "",
            registerClick: 0,
            candidateIds: [],
            error_flag: 0,
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        //Date Related Pure Functions..
        this.handleDayClick = this.handleDayClick.bind(this);
        this.checkValidation = this.checkValidation.bind(this);
    }

    componentDidMount() {
        //console.log('HERE HISTORY',history.location);
        var checkboxData = [];
        if(history.location.state !== undefined && history.location.state.addcandiatedetailids !== undefined){
            checkboxData = JSON.parse(history.location.state.addcandiatedetailids);
        }    
        if (checkboxData.length <= 0) {
            history.push('/search-candidate');
        }
        this.props.dispatch(userActions.getStates());
        this.setState({ candidateIds: checkboxData });
    }


    handleSubmit(event) {
        // event.preventDefault();
        const { user } = this.state;
        const { dispatch } = this.props;
        this.setState({ submitted: true });
        //console.log('candidate checked data',this.state.candidateIds);
        if (this.state.selectedDay !== "") var interview_date = this.state.selectedDay;
        var candidatedetailid = '-1';
        var candidateid = this.state.candidateIds;
        candidateid = candidateid.join();
        var industrypartnerid = this.props.user.data[0].industrypartnerid;
        var candidateInfoFinal = { candidatedetailid, industrypartnerid, candidateid, interview_date, ...user };
        //console.log("send data", candidateInfoFinal);

        if (candidateInfoFinal.interview_date && candidateInfoFinal.interview_time) {
            dispatch(userActions.addUpdateCandidateDetailsHiringRequest(candidateInfoFinal));
        }
    }
    checkValidation(event) {
        //console.log("Register clicked.");
        this.setState({ registerClick: 1 });
    }

    handleChange(event) {

        const { name, value } = event.target;
        const { user } = this.state;
        // const { dispatch } = this.props;
        // this.setState({ value: event.target.value });
        this.setState({
            user: {
                ...user,
                [name]: value
            }
        });
        //console.log("event data change", event.target.name, event.target.value);

        if (event.target.name === 'districtname') { this.setState({ error_flag: 0 }) }
        if (event.target.name === 'statename') {
            if (event.target.name !== 'districtname') {
                this.setState({ error_flag: 1 });
                this.props.dispatch(userActions.getDistrict(value));
            }
        }
        if (event.target.name === 'statename') { this.props.dispatch(userActions.getDistrict(value)); }
    }
    handleDayClick(day) {
        this.setState({ selectedDay: day });
    }
    render() {
        const { handleSubmit } = this.props;
        const { pf_yes, pf_no, esic_yes, esic_no, food_yes, food_no, trp_yes, trp_no, acm_yes, acm_no, registerClick } = this.state;

        // console.log("Form Data", candidateformdata);
        //console.log("disctrict value", districtlist);
        //console.log("Job value", jobroles);
        //console.log("general props", this.props);
        //console.log("get cnetral minstiry data",centralministriesdata);  

        if (!!this.props.districtlist) {
            var allCities = this.props.districtlist.data;
            var final_allCities = [];
            //State values..
            allCities.map((value, index) => {
                return final_allCities.push(<option key={index} value={value.cityid}>{value.cityName}</option>);
            })
        }
        if (!!this.props.statedata) {
            //State values..
            var allStates = this.props.statedata.data;
            var final_allStates = [];
            allStates.map((value, index) => {
                return final_allStates.push(<option value={value.stateid} key={index}>{value.stateName}</option>);
            })
        }
        const renderSelector = ({ input, label, error_state, dynamic_values, meta: { touched, error } }) => (
            <div className={'form-group' + ((error && touched) || (error_state === 1) ? ' has-error' : '')}>
                <label>{label}</label>
                <select {...input} className="form-control">
                    <option value="">Select</option>
                    {dynamic_values}
                    ))}
              </select>
                {error_state === 1 && <div className="help-block">{error}</div>}
                {touched && error && <div className="help-block">{error}</div>}
            </div>

        )
        return (<div className="content-wrapper user-flow TP-flow">

                <div id="content" className="main-content">
                    <Sidebar {...this.props} />
                    <Header />

                    <div className="page-content">
                        <Row>
                            <Col xs="12" xl="10">
                                <div className="d-block d-md-flex align-items-center section-header">

                                    <div>
                                        <h1 className="page-title">Add Interview Details</h1>
                                        <Breadcrumb>
                                            <BreadcrumbItem><a href="/search-candidate">Search Candidate</a></BreadcrumbItem>
                                            <BreadcrumbItem active>Add Interview Details</BreadcrumbItem>
                                        </Breadcrumb>
                                    </div>


                                </div>
                            </Col>
                        </Row>
                        <Row>
                            <Col xs="12" xl="10">
                                <form id="frm-add-candidate" className="form frm-edit-candidaten" onSubmit={handleSubmit(this.handleSubmit)}>
                                    <div className="card z-depth-1">
                                        <div className="card-header bg-transparent border-bottom-0 section-title no-border form-title">
                                            <h5 className="title card-title">Tentative Interview Details</h5>
                                        </div>
                                        <div className="card-body pb-0">

                                            <div className="row">
                                                <div className="col-12 col-md-4">
                                                    <div className={'form-group' + (!this.state.selectedDay ? ' has-error' : '')}>
                                                        <label htmlFor="interview_date">Interview Date</label>
                                                        <div className="input-group date date-picker" data-provide="datepicker">
                                                            <DayPickerInput
                                                            format={"D-M-YYYY"}
                                                            formatDate={formatDate}
                                                            parseDate= {parseDate}
                                                            placeholder={"DD-MM-YYYY"}
                                                            onDayChange={this.handleDayClick}
                                                            inputProps={{ readOnly: true }} />
                                                            <div className="input-group-addon">
                                                                <i className="material-icons">date_range</i>
                                                            </div>
                                                        </div>
                                                        <div className="help-block">{!this.state.selectedDay && registerClick === 1 ? 'Please enter Interview Date' : ''}</div>
                                                    </div>
                                                </div>
                                                <Field name="interview_time" type="time" component={renderField} label="Interview Time" placeholder="Interview Time" onChange={this.handleChange} />
                                            </div>

                                        </div>

                                        <hr className="inner-form-seperator" />

                                        <div className="card-header bg-transparent border-bottom-0 section-title no-border form-title">
                                            <h5 className="title card-title">Other Details</h5>
                                        </div>

                                        <CardBody>
                                            {/* <div className="card-body"> */}
                                            <Row>
                                                <Field name="salary" type="text" component={renderField} label="Monthly Salary" placeholder="Monthly Salary" onChange={this.handleChange} />
                                                <div className="col-12 col-md-4">
                                                    <div className='form-group'>
                                                        <Field name="statename" component={renderSelector} dynamic_values={final_allStates} onChange={this.handleChange} label="State" />
                                                    </div>
                                                </div>

                                                <div className="col-12 col-md-4">
                                                    <div className='form-group'>
                                                        <Field name="districtname" component={renderSelector} error_state={this.state.error_flag} dynamic_values={final_allCities} onChange={this.handleChange} label="District" />
                                                    </div>
                                                </div>
                                            </Row>

                                            <Row>
                                                <Col xs="12" md="4">
                                                    <div className={'form-group' + (!pf_yes || !pf_no ? ' has-error' : '')}>
                                                        <label htmlFor="pf" className="d-block">PF</label>
                                                        <Field name="is_pf" component="input" type="radio" value="yes" className="ml-0" onChange={this.handleChange} /> Yes
                            <Field name="is_pf" component="input" type="radio" value="no" onChange={this.handleChange} /> No
                            <Field name="is_pf" component={renderError} />
                                                    </div>
                                                </Col>

                                                <Col xs="12" md="4">
                                                    <div className={'form-group' + (!esic_yes || !esic_no ? ' has-error' : '')}>
                                                        <label htmlFor="esic" className="d-block">ESIC</label>
                                                        <Field name="is_esic" component="input" type="radio" value="yes" className="ml-0" onChange={this.handleChange} /> Yes
                            <Field name="is_esic" component="input" type="radio" value="no" onChange={this.handleChange} /> No
                            <Field name="is_esic" component={renderError} />
                                                    </div>
                                                </Col>

                                                <Col xs="12" md="4">
                                                    <div className={'form-group' + (!food_yes || !food_no ? ' has-error' : '')}>
                                                        <label htmlFor="food" className="d-block">Food</label>
                                                        <Field name="is_food" component="input" type="radio" value="yes" className="ml-0" onChange={this.handleChange} /> Yes
                            <Field name="is_food" component="input" type="radio" value="no" onChange={this.handleChange} /> No
                            <Field name="is_food" component={renderError} />
                                                    </div>
                                                </Col>
                                            </Row>

                                            <Row>
                                                <Col xs="12" md="4">
                                                    <div className={'form-group' + (!trp_yes || !trp_no ? ' has-error' : '')}>
                                                        <label htmlFor="transportation" className="d-block">Transportation</label>
                                                        <Field name="is_transportation" component="input" type="radio" value="provided" className="ml-0" onChange={this.handleChange} /> Provided
                            <Field name="is_transportation" component="input" type="radio" value="notprovided" onChange={this.handleChange} /> Not provided
                            <Field name="is_transportation" component={renderError} />
                                                    </div>
                                                </Col>

                                                <Col xs="12" md="4">
                                                    <div className={'form-group' + (!acm_yes || !acm_no ? ' has-error' : '')}>
                                                        <label htmlFor="accomodation" className="d-block">Accomodation</label>
                                                        <Field name="is_accomodation" component="input" type="radio" value="provided" className="ml-0" onChange={this.handleChange} /> Provided
                            <Field name="is_accomodation" component="input" type="radio" value="notprovided" onChange={this.handleChange} /> Not provided
                            <Field name="is_accomodation" component={renderError} />
                                                    </div>
                                                </Col>
                                            </Row>

                                            <Field name="other_perks" component={renderTextAreaField} type="textarea" label="Other Perks" textarea={true} onChange={this.handleChange} />
                                        </CardBody>

                                        <hr className="inner-form-seperator" />

                                        <div className="card-header bg-transparent border-bottom-0 section-title no-border form-title">
                                            <h5 className="title card-title">Contact Person Details</h5>
                                        </div>
                                        <CardBody>
                                            <Row>
                                                <Field name="contact_person_name" type="text" component={renderField} label="Name" placeholder="Name" onChange={this.handleChange} />

                                                <Field name="contact_person_email" type="text" component={renderField} label="Email" placeholder="Email" onChange={this.handleChange} />

                                                <Field name="contact_person_phone" type="text" component={renderField} label="Mobile Number" placeholder="Mobile Number" onChange={this.handleChange} />
                                            </Row>
                                            <Row>
                                                <Col xs="12">
                                                    <Field name="other_information" component={renderTextAreaField} type="textarea" label="Other Additional Information" textarea={true} onChange={this.handleChange} />
                                                </Col>
                                            </Row>
                                            <div className="action-btn-block">
                                                {this.state.error_flag === 1 ?
                                                    <button disabled="disabled" className="btn btn-primary btn-raised mr-3" onClick={this.checkValidation} title="Save">Save</button>
                                                :
                                                    <button className="btn btn-primary btn-raised mr-3" onClick={this.checkValidation} title="Save">Save</button>
                                                }
                                                <a title="Cancel" className="btn btn-outline" href="/search-candidate">Cancel</a>
                                            </div>

                                        </CardBody>
                                    </div>
                                </form>
                            </Col>
                        </Row>

                        <Footer />
                    </div>
                </div>
            </div>

        );
    }
}
AddCandidateDetail = reduxForm({
    form: 'AddCandidateDetail',
    validate,
    enableReinitialize: true,
})(AddCandidateDetail);

function mapStateToProps(state) {
    const { user } = state.authentication;
    const { otherdetails_hiring_request } = state.trainingpartners; //From Reducer - trainingpartners function ...
    const { statedata } = state.centralministrydata; //From Reducer - trainingpartners function ...
    const { districtlist } = state.generaldata; //From Reducer - CommonReducer function ...

    return {
        user, otherdetails_hiring_request, districtlist, statedata
    };

}
const connectedAddCandidateDetail = connect(mapStateToProps)(AddCandidateDetail);
export { connectedAddCandidateDetail as AddCandidateDetail };