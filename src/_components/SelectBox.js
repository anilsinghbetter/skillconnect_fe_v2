import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { reduxForm, Field, FieldArray } from 'redux-form';
import { Label, FormGroup, Input, InputGroup, InputGroupAddon, Button, Card, CardBody, CardImg, CardText, CardTitle, CardSubtitle } from 'reactstrap';
class SelectBox extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            //Initial State..
            
        };        
        this.handleSelectionChange=this.handleSelectionChange.bind(this);

        handleSelectionChange(e)
         {
            if (typeof this.props.onChange === 'function') {
                this.props.onChange(e.target.value);
            }
        }

    }
    render() {
        console.log('#SELECTCOMP', this.props);
        return (<div>
            {/* <select name={this.props.month} multiple="true" className="form-control" type="multiple"> */}
            <select name={this.props.name} multiple="true" onChange={this.handleSelectionChange} >
                <option value="" hidden>Select</option>
                {this.props.data}
            </select>

            {/*  <Field
                month={this.props.name} component="select" multiple={true} type="select-multiple">
                <option value="" hidden>Select</option>
                {this.props.data}
            </Field> */}
        </div>
        );
    }
}
function mapStateToProps(state) {
    //Add state values which we need to integrate with props..
    return {
        //registering
    };
}
const connectedSelectBox = connect(mapStateToProps)(SelectBox);
export { connectedSelectBox as SelectBox };