import React from 'react';
import { connect } from 'react-redux';
import { userActions } from '../_actions';
import { Button, Row, Col, Card, CardHeader, CardSubtitle, CardTitle, CardLink, ListGroup, ListGroupItem, Modal, ModalBody, ModalHeader, FormGroup } from 'reactstrap';
import { history } from '../_helpers';
import candidatesSVG from '../assets/images/candidates_grey.svg';
import { config } from '../config';

class GetCandidatesListOnly extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isOpen: false,
            CardValue: undefined,
            delCardValue: undefined,
            mydata: "",
            page: 1,
            pagePrev: 0,
            mycustomVal: 1,
            showmore: 0,
            isDeleted: 0,
            searchparams: undefined,
            fromsearch: 0,
            positionChecked: [],
            selectAll: false,
        };
        this.viewMoreAPICall = this.viewMoreAPICall.bind(this);
        this.toggleModal = this.toggleModal.bind(this);
        this.toggle = this.toggle.bind(this);
        this.closeModalBox = this.closeModalBox.bind(this);
        // this.deleteCandidate = this.deleteCandidate.bind(this);
        this.handleCheckboxSelection = this.handleCheckboxSelection.bind(this);
        this.handleSelectAll = this.handleSelectAll.bind(this);
        this.handleContactCandidate = this.handleContactCandidate.bind(this);
        this.formatted_date = this.formatted_date.bind(this);
        // this.displaySearchParams = this.displaySearchParams.bind(this);
    }

    closeModalBox() {
        this.setState({ isOpen: !this.state.isOpen });
    }
    toggle() {
        this.setState({
            dropdownOpen: !this.state.dropdownOpen
        });
    }
    
    
    viewMoreAPICall = (e, candidateid) => {
        this.props.dispatch(userActions.getCandidateViewMoreDetails(candidateid));
        this.toggleModal(e);
    }
    toggleModal = (e) => {
        //console.log('# here in toggle', this.state);
        this.setState({ isOpen: !this.state.isOpen, delCardValue: undefined });
        this.setState({ CardValue: e });
    }
    
    componentDidMount() {
        this.setState({
            page:this.props.page,
            positionChecked: []
        })
    }
    

    loadMore = () => {
        // console.log("LOAD MORE CLICKED...");
        if (localStorage.getItem('user') !== null) {
            this.props.sortData();
        }
        else {
            history.push('/login');
            return ('');
        }
    }


    handleCheckboxSelection(e) {
        // current array of options

        const checkboxArray = this.state.positionChecked
        let index

        // check if the check box is checked or unchecked
        if (e.target.checked) {
            // add the numerical value of the checkbox to options array
            checkboxArray.push(+e.target.value)
        }
        else if (this.state.selectAll === true) {
            this.setState({ selectAll: false })
            index = checkboxArray.indexOf(+e.target.value)
            checkboxArray.splice(index, 1)
        }
        else {
            // or remove the value from the unchecked checkbox from the array
            index = checkboxArray.indexOf(+e.target.value)
            checkboxArray.splice(index, 1)
        }
        // console.log('options checked', checkboxArray);
        // update the state with the new array of options
        this.setState({ positionChecked: checkboxArray })

    }
    handleSelectAll(e) {
        this.setState({ selectAll: e.target.checked },/*  () => console.log('selected all', this.state.selectAll) */);
        var rowState = [];
        if (e.target.checked) {
            if (!!this.props.searchitems && this.props.searchitems.data !== undefined && this.props.searchitems.data.length > 0) {
                for (var i = 0; i < this.props.searchitems.data.length; i++) {
                    rowState.push(this.props.searchitems.data[i].candidateid);
                }
            }
        }
        this.setState({ positionChecked: rowState })
    };
    handleContactCandidate(){
        console.log(this.state.positionChecked, "positionChecked");
    }
    
    componentWillReceiveProps(){
        // console.log(this.props, "PROPS IN LIST");
        if(!this.props.check){
            // this.props.sortData(this.state.page, true);
            this.setState({
                positionChecked: [],
                selectAll: false,
                // page:this.props.page
            });
        }
        // console.log(this.state.positionChecked, "POS positionChecked...");
        // console.log(this.props.check, "Props BOOLEAN ...");
    }
    formatted_date(date){
        //format date
        if(typeof date !== 'undefined' && !!date) {
            let t = 0;
            return date.replace(/ /g, function(match) {t++;return (t === 2) ? ", " : match; });
        }
    }
    render() {
        var loaderClass = '';
        //Show loader..
        //console.log('#this.props.loading',this.props.loading);
        if (this.props.loading === true || this.props.loadingviewmore === true) {
            loaderClass = 'loading';
        }
        const styles = {
            top: '115px',
            left: '60px'
        };

        const { user } = this.props;
        if (user === undefined) {
            // console.log("Redierect to specific Dashboard page");
            history.push('/login');
            return ('');
        }

        var recordFound = '';
        if (!!this.props.items || !!this.props.searchitems) {
            var final_allCandiates = [];
            var allcandidates = [];
            var candiateitems = "";

            // console.log("search",this.props.searchitems);
            // console.log("search",this.props.items);

            if (this.props.searchitems !== undefined) {
                candiateitems = this.props.searchitems;
                
            } else {
                candiateitems = this.props.items;
            }
            if (candiateitems.data.length === 0) {
                recordFound = 0;
            } else {
                recordFound = 1;
            }
            // console.log("Candidate Details : ", this.props.items);
            //Pagination...
            var loadMore = "";
            var totalpages = candiateitems.paginationOptions.totalPages;
            var currentpage = candiateitems.paginationOptions.curPage;
            //console.log('#candiateitems', candiateitems);
            if (currentpage < totalpages) {
                loadMore = (<div className="text-center"><a title="Load More" onClick={this.loadMore} className="btn btn-outline-secondary card-link">Load More</a></div>);
            }
            
            if ('data' in candiateitems) {
                // console.log("2");
                allcandidates = candiateitems.data;
            }
            else {
                // console.log("3");
                allcandidates = candiateitems;
            }
            

            //For View more Details MOdal  Box...
            if (!!this.props.candidate_view_more_details && this.state.CardValue !== undefined) {
                var candidatedetaildata = this.props.candidate_view_more_details.data[0];
                //console.log('# here',candidatedetaildata);
                var mymodaval = this.state.CardValue;
                var tel = candidatedetaildata.phone ? "tel:" + candidatedetaildata.phone : ''
                var mailTo = candidatedetaildata.email_address ? "mailto:" + candidatedetaildata.email_address : ''
                
                if(candidatedetaildata.permanent_cityName){
                    candidatedetaildata.permanent_address +=  ', ' + candidatedetaildata.permanent_cityName;
                }
                if(candidatedetaildata.permanent_stateName){
                    candidatedetaildata.permanent_address += ', '  + candidatedetaildata.permanent_stateName;
                }
                if(candidatedetaildata.permanent_pincode){
                    candidatedetaildata.permanent_address += ', '  + candidatedetaildata.permanent_pincode;
                }
                if(candidatedetaildata.present_cityName){
                    candidatedetaildata.present_address +=  ', ' + candidatedetaildata.present_cityName;
                }
                if(candidatedetaildata.present_stateName){
                    candidatedetaildata.present_address += ', '  + candidatedetaildata.present_stateName;
                }
                if(candidatedetaildata.present_pincode){
                    candidatedetaildata.present_address += ', '  + candidatedetaildata.present_pincode;
                }
                var allcandidatesModalDetails = (

                    <Modal backdrop={false} centered size="lg" isOpen={this.state.isOpen} toggle={() => this.toggleModal(mymodaval)}>
                        <ModalHeader toggle={() => this.toggleModal(mymodaval)} className="border-bottom-0 modal-header-shadow align-items-center">
                            <div className="section-title">
                                <h5 className="title modal-title">{candidatedetaildata.first_name}'s Details - {candidatedetaildata.is_skilled ? candidatedetaildata.is_skilled : ''}</h5>                                 
                            </div>
                        </ModalHeader>
                        <ModalBody className="details-view">
                            <Row>
                                <Col xs="12">
                                    <h5 className="title modal-view-title">Personal Details</h5>
                                </Col>
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">First Name</label>
                                    <p className="title">{candidatedetaildata.first_name ? candidatedetaildata.first_name : 'NA'}</p>
                                </Col>
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">Middle Name</label>
                                    <p className="title">{candidatedetaildata.middle_name ? candidatedetaildata.middle_name : 'NA'}</p>
                                </Col>
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">Last Name</label>
                                    <p className="title">{candidatedetaildata.last_name ? candidatedetaildata.last_name : 'NA'}</p>
                                </Col>
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">Aadhaar No.</label>
                                    {/* <p className="title">{candidatedetaildata.aadhar_card_no ? candidatedetaildata.aadhar_card_no : 'NA'}</p> */}
                                    <p className="title">{candidatedetaildata.aadhar_card_no ? '************' : 'NA'}</p> 
                                </Col>
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">Alternate ID Type</label>
                                    <p className="title">{candidatedetaildata.alternateidtype ? candidatedetaildata.alternateidtype : 'NA'}</p>
                                </Col>
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">Alternate ID No.</label>
                                    {/* <p className="title">{candidatedetaildata.altername_id_no ? candidatedetaildata.altername_id_no : 'NA'}</p> */}
                                    <p className="title">{candidatedetaildata.altername_id_no ? '************' : 'NA'}</p> 
                                </Col>
                                {/* <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">Aadhaar Enrollment ID</label>
                                    <p className="title">{candidatedetaildata.aadhaar_enrollment_id ? candidatedetaildata.aadhaar_enrollment_id : 'NA'}</p>
                                </Col> */}
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">Mobile No</label>
                                    {/* <p className="title"> */}
                                        {/* {tel === '' ? 'NA' :
                                            <a href={tel} title={candidatedetaildata.phone}>{candidatedetaildata.phone}</a>
                                        }  */}
                                        <p className="title">{candidatedetaildata.phone ? '**********' : 'NA'}</p> 
                                    {/* </p> */}
                                </Col>
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">Email Address</label>
                                    {/* <p className="title"> */}
                                        {/* {mailTo === '' ? 'NA' :
                                            <a href={mailTo} title={candidatedetaildata.email_address}>{candidatedetaildata.email_address}</a>
                                        } */}
                                        <p className="title">{candidatedetaildata.email_address ? '********************' : 'NA'}</p> 
                                    {/* </p> */}
                                </Col>
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">Date Of Birth</label>
                                    <p className="title">{candidatedetaildata.dob ? this.formatted_date(candidatedetaildata.dob) : 'NA'}</p>
                                </Col>
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">Gender</label>
                                    <p className="title">{candidatedetaildata.gender ? candidatedetaildata.gender : 'NA'}</p>
                                </Col>
                                {/* <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">State</label>
                                    <p className="title">{candidatedetaildata.stateName ? candidatedetaildata.stateName : 'NA'}</p>
                                </Col>
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">District</label>
                                    <p className="title">{candidatedetaildata.cityName ? candidatedetaildata.cityName : 'NA'}</p>
                                </Col> */}
                                {/* <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">Pincode</label>
                                    <p className="title">{candidatedetaildata.pincode ? candidatedetaildata.pincode : 'NA'}</p>
                                </Col> */}
                                {/* <Col xs="12" md="6" xl="4">
                                    <label className="subtitle ">Address</label>
                                    <p className="title text-truncate" title={candidatedetaildata.address ? candidatedetaildata.address : 'NA'}>{candidatedetaildata.address ? candidatedetaildata.address : 'NA'}</p>
                                </Col> */}
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle ">Permanent Address</label>
                                    <p className="title text-truncate" title={candidatedetaildata.permanent_address ? candidatedetaildata.permanent_address : 'NA'}>{candidatedetaildata.permanent_address ? candidatedetaildata.permanent_address : 'NA'}</p>
                                </Col>
                                
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle ">Present Address</label>
                                    <p className="title text-truncate" title={candidatedetaildata.present_address ? candidatedetaildata.present_address : 'NA'}>{candidatedetaildata.present_address ? candidatedetaildata.present_address : 'NA'}</p>
                                </Col>
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">Age</label>
                                    <p className="title">{candidatedetaildata.age ? candidatedetaildata.age : 'NA'}</p>
                                </Col>
                                {/* {candidatedetaildata.is_skilled === 'Skilled' &&  */}
                                    <Col xs="12" md="6" xl="4">
                                        <label className="subtitle">Candidate Id</label>
                                        <p className="title">{candidatedetaildata.candidate_id ? candidatedetaildata.candidate_id : 'NA'}</p>
                                    </Col>
                                {/* } */}
                            </Row>
                            <hr className="modal-data-seperator" />
                            <Row>

                                <Col xs="12">
                                    <h5 className="title modal-view-title">Education Details</h5>
                                </Col>
                                <Col xs="12" md="6" xl="8">
                                    {/* <label className="subtitle">EDU </label> */}
                                    {/* {candidatedetaildata.education_details ? candidatedetaildata.education_details = candidatedetaildata.education_details.split(',').join(', ') : candidatedetaildata.education_details } */}
                                    <p className="title">{candidatedetaildata.education_details ? candidatedetaildata.education_details = candidatedetaildata.education_details.split(',').join(', ') : 'NA'}</p>
                                </Col>
                                {/* <Col xs="12" md="6" xl="8">
                                    <label className="subtitle">Skills </label>
                                    <p className="title">{candidatedetaildata.is_skilled ? candidatedetaildata.is_skilled : 'NA'}</p>
                                </Col> */}

                            </Row>
                            <hr className="modal-data-seperator" />
                            <Row>

                                <Col xs="12">
                                    <h5 className="title modal-view-title">Professional Details</h5>
                                </Col>
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">Experience in Years</label>
                                    <p className="title">{candidatedetaildata.experience_in_years ? candidatedetaildata.experience_in_years : 'NA'}</p>
                                </Col>
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">Sector</label>
                                    <p className="title">{candidatedetaildata.sectorName ? candidatedetaildata.sectorName : 'NA'}</p>
                                </Col>
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">Job Role</label>
                                    <p className="title">{candidatedetaildata.jobroleName ? candidatedetaildata.jobroleName : 'NA'}</p>
                                </Col>
                            </Row>
                                {/* {candidatedetaildata.is_skilled === 'Skilled' &&  */}
                                    <div className="view_for_skilled">
                                        <Row>
                                            <Col xs="12" md="6" xl="4">
                                                <label className="subtitle">Training Type</label>
                                                <p className="title">{candidatedetaildata.training_type ? candidatedetaildata.training_type : 'NA'}</p>
                                            </Col>
                                            <Col xs="12" md="6" xl="4">
                                                <label className="subtitle">SDMS/NSDC Enrollment No</label>
                                                <p className="title">{candidatedetaildata.sdmsnsdc_enrollment_number ? candidatedetaildata.sdmsnsdc_enrollment_number : 'NA'}</p>
                                            </Col>
                                            <Col xs="12" md="6" xl="4">
                                                <label className="subtitle">Training Status</label>
                                                <p className="title">{candidatedetaildata.training_status ? candidatedetaildata.training_status : 'NA'}</p>
                                            </Col>
                                            {candidatedetaildata.training_status === 'Ongoing' && 
                                                <Col xs="12" md="6" xl="4">
                                                    <label className="subtitle">Pass Out date</label>
                                                    <p className="title">{candidatedetaildata.pass_out_date ? candidatedetaildata.pass_out_date : 'NA'}</p>
                                                </Col>
                                            }
                                            
                                            <Col xs="12" md="6" xl="4">
                                                <label className="subtitle">Attendance Percent</label>
                                                <p className="title">{candidatedetaildata.attendence_percentage ? candidatedetaildata.attendence_percentage : 'NA'}</p>
                                            </Col>
                                            <Col xs="12" md="6" xl="4">
                                                <label className="subtitle">Placement Status</label>
                                                <p className="title">{candidatedetaildata.placement_status ? candidatedetaildata.placement_status : 'NA'}</p>
                                            </Col>
                                            {candidatedetaildata.placement_status === 'Selected' &&
                                                <Col xs="12" md="6" xl="4">
                                                    <label className="subtitle">Employer Name</label>
                                                    <p className="title">{candidatedetaildata.employer_name ? candidatedetaildata.employer_name : 'NA'}</p>
                                                </Col>
                                            }
                                            
                                            <Col xs="12" md="6" xl="4">
                                                <label className="subtitle">Employment Type</label>
                                                <p className="title">{candidatedetaildata.employment_type ? candidatedetaildata.employment_type : 'NA'}</p>
                                            </Col>
                                            <Col xs="12" md="6" xl="4">
                                                <label className="subtitle">Assessment Score</label>
                                                <p className="title">{candidatedetaildata.assessment_score ? candidatedetaildata.assessment_score : 'NA'}</p>
                                            </Col>
                                            <Col xs="12" md="6" xl="4">
                                                <label className="subtitle">Max Assessment Score</label>
                                                <p className="title">{candidatedetaildata.max_assessment_score ? candidatedetaildata.max_assessment_score : 'NA'}</p>
                                            </Col>
                                            {/* <Col xs="12" md="6" xl="4">
                                                <label className="subtitle">Assessment Date</label>
                                                <p className="title">{candidatedetaildata.assessment_date ? candidatedetaildata.assessment_date : 'NA'}</p>
                                            </Col> */}
                                            
                                            <Col xs="12" md="6" xl="4">
                                                <label className="subtitle">Training Center</label>
                                                <p className="title">{candidatedetaildata.trainingcenter_name ? candidatedetaildata.trainingcenter_name : 'NA'}</p>
                                            </Col>
                                            <Col xs="12" md="6" xl="4">
                                                <label className="subtitle">Batch ID</label>
                                                <p className="title">{candidatedetaildata.batch_id ? candidatedetaildata.batch_id : 'NA'}</p>
                                            </Col>
                                            {/* <Col xs="12" md="6" xl="4">
                                                <label className="subtitle">Batch Start Date</label>
                                                <p className="title">{candidatedetaildata.batch_start_date ? candidatedetaildata.batch_start_date : 'NA'}</p>
                                            </Col>
                                            <Col xs="12" md="6" xl="4">
                                                <label className="subtitle">Batch End Date</label>
                                                <p className="title">{candidatedetaildata.batch_end_date ? candidatedetaildata.batch_end_date : 'NA'}</p>
                                            </Col> */}
                                            <Col xs="12" md="6" xl="4">
                                                <label className="subtitle">Scheme</label>
                                                <p className="title">{candidatedetaildata.schemeType ? candidatedetaildata.schemeType : 'NA'}</p>
                                            </Col>
                                            {candidatedetaildata.schemeType === 'State Skill Development Mission Scheme'
                                                ?
                                                <Col xs="12" md="6" xl="4">
                                                    <label className="subtitle">State Ministry</label>
                                                    <p className="title">{candidatedetaildata.stateministry ? candidatedetaildata.stateministry : 'NA'}</p>
                                                </Col>
                                                :
                                                <Col xs="12" md="6" xl="4">
                                                    <label className="subtitle">Central Ministry</label>
                                                    <p className="title">{candidatedetaildata.centralministry ? candidatedetaildata.centralministry : 'NA'}</p>
                                                </Col>
                                            }
                                            <Col xs="12" md="6" xl="4">
                                                <label className="subtitle">Is Willing To Relocate </label>
                                                <p className="title">{candidatedetaildata.willing_to_relocate ? candidatedetaildata.willing_to_relocate : 'NA'}</p>
                                            </Col>
                                        </Row>
                                    </div>
                                {/* } */}
                            {/* </Row> */}
                            <hr className="modal-data-seperator" />
                            {(!!candidatedetaildata.first_month_payslip || !!candidatedetaildata.second_month_payslip || !!candidatedetaildata.third_month_payslip || !!candidatedetaildata.appointment_letter) 
                            ?
                            <div>
                                <Row>
                                    <Col xs="12">
                                        <h5 className="title modal-view-title">Uploaded Documents</h5>
                                        {/* {console.log(candidatedetaildata, " candidatedetaildata ")} */}
                                    </Col>
                                    {candidatedetaildata.first_month_payslip &&
                                        <Col xs="12" md="6" xl="4">
                                            <label className="subtitle">1<sup>st</sup> Month Pay Slip </label>
                                            <p className="title" title="Download File">
                                                <a href={config.API_HOST_URL + 'candidate_uploads/'+ candidatedetaildata.candidateid +'/1/'+candidatedetaildata.first_month_payslip}> {candidatedetaildata.first_month_payslip} </a>
                                            </p>
                                        </Col>
                                    }
                                    {candidatedetaildata.second_month_payslip &&
                                        <Col xs="12" md="6" xl="4">
                                            <label className="subtitle">2<sup>nd</sup> Month Pay Slip </label>
                                            <p className="title" title="Download File">
                                                <a href={config.API_HOST_URL + 'candidate_uploads/'+ candidatedetaildata.candidateid +'/2/'+candidatedetaildata.second_month_payslip}> {candidatedetaildata.second_month_payslip} </a>
                                            </p>
                                        </Col>
                                    }
                                    {candidatedetaildata.third_month_payslip &&
                                    <Col xs="12" md="6" xl="4">
                                        <label className="subtitle">3<sup>rd</sup> Month Pay Slip </label>
                                        <p className="title" title="Download File">
                                            <a href={config.API_HOST_URL + 'candidate_uploads/'+ candidatedetaildata.candidateid +'/3/'+candidatedetaildata.third_month_payslip}> {candidatedetaildata.third_month_payslip} </a>
                                        </p>
                                    </Col>
                                    }
                                    {/* {candidatedetaildata.is_skilled === 'Skilled' && candidatedetaildata.appointment_letter && */}
                                    {candidatedetaildata.appointment_letter &&
                                    <Col xs="12" md="6" xl="4">
                                        <label className="subtitle">Appointment Letter </label>
                                        <p className="title" title="Download File">
                                            <a href={config.API_HOST_URL + 'candidate_uploads/'+ candidatedetaildata.candidateid +'/appointment_letter/'+candidatedetaildata.appointment_letter}> {candidatedetaildata.appointment_letter} </a>
                                        </p>
                                    </Col>
                                    }
                                </Row>
                            </div> 
                            :
                            <Row>
                                <Col xs="12">
                                    <h5 className="title modal-view-title">No Documents Uploaded</h5>
                                </Col>
                            </Row>
                            }
                        </ModalBody>
                        {/* <ModalFooter>

                        </ModalFooter> */}
                    </Modal>
                );
            }
            //End of view more details for Modal box...

            //Card Details
            allcandidates.map((value, index) => {
                var TrainingStatus = allcandidates[index].training_status ? allcandidates[index].training_status.charAt(0).toUpperCase() + allcandidates[index].training_status.substr(1) : '-';
                switch (TrainingStatus) {
                    case 'Duplicateenrollment':
                        TrainingStatus = 'Duplicate Enrollment';
                        break;
                    default:
                }

                return final_allCandiates.push(
                    <Col xs="12" md="6" xl="4" key={index}>
                        <Card className="card z-depth-1">
                            <CardHeader className="d-flex justify-content-between bg-transparent align-items-center border-bottom-0 card-header-shadow">
                                <div className="section-title">
                                    <CardTitle className="title card-title">{ allcandidates[index].first_name + ' ' + ( allcandidates[index].last_name ?  allcandidates[index].last_name : '') }</CardTitle>
                                    {/* <CardSubtitle className="subtitle">{allcandidates[index].is_skilled.charAt(0).toUpperCase()+allcandidates[index].is_skilled.slice(1)}</CardSubtitle> */}
                                    <CardSubtitle className="subtitle">{allcandidates[index].cityName}</CardSubtitle>
                                </div>
                                {/* <label className="custom-checkbox">
                                    <input
                                        onChange={this.handleCheckboxSelection}
                                        checked={(this.state.positionChecked.some(i => i === allcandidates[index].candidateid))}
                                        key={index}
                                        type="checkbox"
                                        value={allcandidates[index].candidateid}
                                    />
                                    <span className="checkmark"></span>
                                </label> */}
                            </CardHeader>
                            <ListGroup className="no-border">
                                <ListGroupItem>
                                    <label className="subtitle">Job Role</label>
                                    <p className="title mb-0 text-truncate" title={allcandidates[index].jobroleName}>{allcandidates[index].jobroleName}</p>
                                </ListGroupItem>
                                <ListGroupItem>
                                    <label className="subtitle">Age</label>
                                    <p className="title mb-0">{allcandidates[index].age}</p>
                                </ListGroupItem>
                                {/* <ListGroupItem>
                                    <label className="subtitle">Mobile No.</label>
                                    <p className="title mb-0"><a href={"tel:" + allcandidates[index].phone}>{allcandidates[index].phone}</a></p>
                                </ListGroupItem> */}


                                {allcandidates[index].training_center_name &&
                                    <ListGroupItem>
                                        <label className="subtitle">Training Center</label>
                                        <p className="title mb-0 text-truncate" title={allcandidates[index].training_center_name}>{allcandidates[index].training_center_name ? allcandidates[index].training_center_name : '-'}</p>
                                    </ListGroupItem>}
                                <ListGroupItem>
                                    <label className="subtitle">Sector</label>
                                    <p className="title mb-0 text-truncate" title={allcandidates[index].sectorName}>{allcandidates[index].sectorName ? allcandidates[index].sectorName : '-'}</p>
                                </ListGroupItem>
                                <ListGroupItem>
                                    <label className="subtitle">Training Status</label>
                                    <p className="title mb-0">{TrainingStatus}</p>
                                </ListGroupItem>

                                <ListGroupItem className="d-flex">
                                    <CardLink title="View More" onClick={() => this.viewMoreAPICall(index, allcandidates[index].candidateid)} className="btn btn-primary btn-raised mr-3" >View More</CardLink>
                                    {/* <Link to={`/add-edit-candidate/#id:${allcandidates[index].candidateid}`} title="Edit" className="btn btn-outline">Edit</Link> */}
                                </ListGroupItem>
                            </ListGroup>
                        </Card>
                    </Col>
                );

            })
        }

        return (<div>

                    <div className={loaderClass} style={styles}></div>

                    {recordFound === 1 &&
                        (<h3 className="page-title mb-3">
                            {!!this.props.items ? " Total Number Of Candidates are : " + this.props.items.paginationOptions.totalcount.toLocaleString('en-IN') : ''}
                            {(!!this.props.searchitems && !!this.props.searchitems.paginationOptions) ? this.props.searchitems.paginationOptions.totalcount.toLocaleString('en-IN') + ' Candidate(s) Found ' : ''}
                        </h3>)
                    }
                    {recordFound === 1 && this.props.isSearch && 
                        <FormGroup className="mr-2 mr-md-4 justify-content-between d-flex">
                            {/* <label className="custom-checkbox ">
                                <input
                                    onChange={this.handleSelectAll}
                                    checked={this.state.selectAll}
                                    key={"SelectAll"}
                                    type="checkbox"
                                    value={"All"}
                                />
                                <span className="checkmark">Select All</span>
                            </label> */}
                            {/* {console.log(this.state.positionChecked, "this.state.positionChecked")}
                            {console.log(this.state.positionChecked.length, "this.state.positionChecked.length")} */}
                            {this.state.positionChecked && this.state.positionChecked.length>0 &&
                                <Button className="align-right" color="secondary" onClick={this.handleContactCandidate}>Contact Candidate(s)</Button>
                            }
                        </FormGroup>
                    }
                    {/* {console.log(this.props.isSearch, "IS SEARCH")}
                    {console.log(recordFound, " recordFound ")} */}
                    {recordFound === 1 && this.props.isSearch && 
                        <Row className="details-view">
                            {final_allCandiates}
                        </Row>
                    }
                    {recordFound === 0 && 

                        <div className="d-flex justify-content-center align-items-center error-page">
                            <div className="text-center">
                                <div className="img-wrapper mb-4">
                                    <div className="circle-xl user-avatar">
                                        <img src={candidatesSVG} alt="candidates" />
                                    </div>
                                </div>


                                
                                <h1 className="h2 fw-regular mb-0">No Candidate(s) Found!</h1>
                                

                            </div>

                        </div>
                    }
                    { this.props.isSearch &&
                        <div>
                            {allcandidatesModalDetails}
                            {/* {deteleCandidatemodal} */}
                            {loadMore}
                        </div>
                    }
                    
                    {/* <Footer /> */}
                    {/* <h1>HELLO </h1> */}
                </div>
        );
    }
}
function mapStateToProps(state) {

    const { user } = state.authentication;
    const { items, searchitems, loading } = state.trainingpartners; //From Reducer - trainingpartners function ...
    const { candidate_view_more_details, loadingviewmore } = state.getcandidateviewmoredetails; //From Reducer - candidatedetails function ...
    // console.log("in MapStateTOProps for GetCandidate",state);
    return {
        items, user, searchitems, loading, candidate_view_more_details, loadingviewmore
    };
}

const connectedGetCandidates = connect(mapStateToProps)(GetCandidatesListOnly);
export { connectedGetCandidates as GetCandidatesListOnly };