/* import React from 'react';
import { connect } from 'react-redux';
import { Header } from '../_components/Header';
import { Footer } from '../_components/Footer';
import { Sidebar } from '../_components/Sidebar';
import { history } from '../_helpers';
import { CardSubtitle, CardLink, ListGroup, ListGroupItem, Breadcrumb, BreadcrumbItem, Button, ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem, Row, Col, Card, CardHeader, CardTitle, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { CandidateSearch } from '../_components/CandidateSearch';

class SearchCandidates extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isOpen: false,
            CardValue: undefined,
        };
        this.toggleModal = this.toggleModal.bind(this);

    }
    toggleModal = (e) => {
        this.setState({ isOpen: !this.state.isOpen });
        this.setState({ CardValue: e });
    }
    componentDidMount() {
            // if (localStorage.getItem('user') !== null) {
            //     var userdata = JSON.parse(localStorage.getItem('user'));
            // }
        // var training_partner_id = userdata.data[0].trainingpartnerid;
        // this.props.dispatch(userActions.getCandidates(training_partner_id));

    }
    render() {

        console.log("Search Candidate Page's Result:", history.location.state.detail);
        // console.log("data coming from main",this.props);
        var recordFound = 0;
        if (!!history.location.state.detail) {

            var final_allCandiates = [];
            var allcandidates = [];
            if (history.location.state.detail.length > 0 )      recordFound = 1;

            //if(history.location.state)
            allcandidates = history.location.state.detail;

            // console.log("DATA",allcandidates);
            // console.log("State Card val",this.state.CardValue);

            if (!!this.props.items && this.state.CardValue != undefined) {

                var mymodaval = this.state.CardValue;
                var tel = allcandidates[this.state.CardValue].phone ? "tel:" + allcandidates[this.state.CardValue].phone : ''
                var mailTo = allcandidates[this.state.CardValue].email_address ? "mailto:" + allcandidates[this.state.CardValue].email_address : ''
                var allcandidatesModalDetails = (

                    <Modal centered size="lg" isOpen={this.state.isOpen} toggle={() => this.toggleModal(mymodaval)}>
                        <ModalHeader toggle={() => this.toggleModal(mymodaval)} className="border-bottom-0 modal-header-shadow align-items-center">
                            <div className="section-title">
                                <h5 className="title modal-title">{allcandidates[this.state.CardValue].candidate_name}'s Details</h5>
                            </div>
                        </ModalHeader>
                        <ModalBody className="details-view">
                            <Row>
                                <Col xs="12">
                                    <h5 className="title modal-view-title">Personal Details</h5>
                                </Col>
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">Name</label>
                                    <p className="title">{allcandidates[this.state.CardValue].candidate_name ? allcandidates[this.state.CardValue].candidate_name : 'NA'}</p>
                                </Col>
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">Aadhaar No.</label>
                                    <p className="title">{allcandidates[this.state.CardValue].aadhaar_number ? allcandidates[this.state.CardValue].aadhaar_number : 'NA'}</p>
                                </Col>
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">Alternate ID Type</label>
                                    <p className="title">{allcandidates[this.state.CardValue].alternateidtype ? allcandidates[this.state.CardValue].alternateidtype : 'NA'}</p>
                                </Col>
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">Alternate ID No.</label>
                                    <p className="title">{allcandidates[this.state.CardValue].alternate_id_number ? allcandidates[this.state.CardValue].alternate_id_number : 'NA'}</p>
                                </Col>
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">Aadhaar Enrollment ID</label>
                                    <p className="title">{allcandidates[this.state.CardValue].aadhaar_enrollment_id ? allcandidates[this.state.CardValue].aadhaar_enrollment_id : 'NA'}</p>
                                </Col>
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">Mobile No</label>
                                    <p className="title">
                                        {tel === '' ? 'NA' :
                                            <a href={tel} title={allcandidates[this.state.CardValue].phone}>{allcandidates[this.state.CardValue].phone}</a>
                                        }
                                    </p>
                                </Col>
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">Email Address</label>
                                    <p className="title">
                                        {mailTo === '' ? 'NA' :
                                            <a href={mailTo} title={allcandidates[this.state.CardValue].email_address}>{allcandidates[this.state.CardValue].email_address}</a>
                                        }
                                    </p>
                                </Col>
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">Date Of Birth</label>
                                    <p className="title">{allcandidates[this.state.CardValue].dob ? allcandidates[this.state.CardValue].dob : 'NA'}</p>
                                </Col>
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">Gender</label>
                                    <p className="title">{allcandidates[this.state.CardValue].gender ? allcandidates[this.state.CardValue].gender : 'NA'}</p>
                                </Col>
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">State</label>
                                    <p className="title">{allcandidates[this.state.CardValue].stateName ? allcandidates[this.state.CardValue].stateName : 'NA'}</p>
                                </Col>
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">District</label>
                                    <p className="title">{allcandidates[this.state.CardValue].cityName ? allcandidates[this.state.CardValue].cityName : 'NA'}</p>
                                </Col>
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">Pincode</label>
                                    <p className="title">{allcandidates[this.state.CardValue].pincode ? allcandidates[this.state.CardValue].pincode : 'NA'}</p>
                                </Col>
                                <Col xs="12" md="12" xl="8">
                                    <label className="subtitle">Address</label>
                                    <p className="title">{allcandidates[this.state.CardValue].address ? allcandidates[this.state.CardValue].address : 'NA'}</p>
                                </Col>
                            </Row>
                            <hr className="modal-data-seperator" />
                            <Row>

                                <Col xs="12">
                                    <h5 className="title modal-view-title">Professional Details</h5>
                                </Col>
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">Experience in Years</label>
                                    <p className="title">{allcandidates[this.state.CardValue].experience_in_years ? allcandidates[this.state.CardValue].experience_in_years : 'NA'}</p>
                                </Col>
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">Training Type</label>
                                    <p className="title">{allcandidates[this.state.CardValue].training_type ? allcandidates[this.state.CardValue].training_type : 'NA'}</p>
                                </Col>
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">SDMS/NSDC Enrollment No</label>
                                    <p className="title">{allcandidates[this.state.CardValue].sdmsnsdc_enrollment_number ? allcandidates[this.state.CardValue].sdmsnsdc_enrollment_number : 'NA'}</p>
                                </Col>
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">Training Status</label>
                                    <p className="title">{allcandidates[this.state.CardValue].training_status ? allcandidates[this.state.CardValue].training_status : 'NA'}</p>
                                </Col>
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">Attendance Percent</label>
                                    <p className="title">{allcandidates[this.state.CardValue].attendence_percentage ? allcandidates[this.state.CardValue].attendence_percentage : 'NA'}</p>
                                </Col>
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">Placement Status</label>
                                    <p className="title">{allcandidates[this.state.CardValue].placement_status ? allcandidates[this.state.CardValue].placement_status : 'NA'}</p>
                                </Col>
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">Employment Type</label>
                                    <p className="title">{allcandidates[this.state.CardValue].employment_type ? allcandidates[this.state.CardValue].employment_type : 'NA'}</p>
                                </Col>
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">Assessment Score</label>
                                    <p className="title">{allcandidates[this.state.CardValue].assessment_score ? allcandidates[this.state.CardValue].assessment_score : 'NA'}</p>
                                </Col>
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">Max Assessment Score</label>
                                    <p className="title">{allcandidates[this.state.CardValue].max_assessment_score ? allcandidates[this.state.CardValue].max_assessment_score : 'NA'}</p>
                                </Col>
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">Assessment Date</label>
                                    <p className="title">{allcandidates[this.state.CardValue].assessment_date ? allcandidates[this.state.CardValue].assessment_date : 'NA'}</p>
                                </Col>
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">Sector</label>
                                    <p className="title">{allcandidates[this.state.CardValue].sectorName ? allcandidates[this.state.CardValue].sectorName : 'NA'}</p>
                                </Col>
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">Job Role</label>
                                    <p className="title">{allcandidates[this.state.CardValue].jobroleName ? allcandidates[this.state.CardValue].jobroleName : 'NA'}</p>
                                </Col>
                               <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">Batch ID</label>
                                    <p className="title">{allcandidates[this.state.CardValue].batch_id ? allcandidates[this.state.CardValue].batch_id : 'NA'}</p>
                                </Col>
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">Batch Start Date</label>
                                    <p className="title">{allcandidates[this.state.CardValue].batch_start_date ? allcandidates[this.state.CardValue].batch_start_date : 'NA'}</p>
                                </Col>
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">Batch End Date</label>
                                    <p className="title">{allcandidates[this.state.CardValue].batch_end_date ? allcandidates[this.state.CardValue].batch_end_date : 'NA'}</p>
                                </Col>
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">Scheme</label>
                                    <p className="title">{allcandidates[this.state.CardValue].schemeType ? allcandidates[this.state.CardValue].schemeType : 'NA'}</p>
                                </Col>
                                {allcandidates[this.state.CardValue].schemeType === 'State Skill Development Mission Scheme'
                                    ?
                                    <Col xs="12" md="6" xl="4">
                                        <label className="subtitle">State Ministry</label>
                                        <p className="title">{allcandidates[this.state.CardValue].stateministry ? allcandidates[this.state.CardValue].stateministry : 'NA'}</p>
                                    </Col>
                                    :
                                    <Col xs="12" md="6" xl="4">
                                        <label className="subtitle">Central Ministry</label>
                                        <p className="title">{allcandidates[this.state.CardValue].centralministry ? allcandidates[this.state.CardValue].centralministry : 'NA'}</p>
                                    </Col>
                                }


                            </Row>
                        </ModalBody>
                        <ModalFooter>
                            <Button onClick={() => this.toggleModal(mymodaval)}>Close</Button>
                        </ModalFooter>
                    </Modal>
                );
            }
            //Card Details
            allcandidates.map((value, index) => {
                final_allCandiates.push(

                    <Col xs="12" md="6" xl="4" key={index}>
                        <Card className="card z-depth-1">
                            <CardHeader className="d-flex justify-content-between bg-transparent align-items-center border-bottom-0 card-header-shadow">
                                <div className="section-title">
                                    <CardTitle className="title card-title">{allcandidates[index].candidate_name}</CardTitle>
                                    <CardSubtitle className="subtitle">{allcandidates[index].batch_id}</CardSubtitle>
                                </div>
                                <CardLink title="Delete" href="javascript:void(0);" className="btn-fab btn-fab-danger btn disabled" data-toggle="modal" data-target="#delete_candidate_modal"><i className="material-icons">delete_outline</i></CardLink>
                            </CardHeader>
                            <ListGroup className="no-border">
                                <ListGroupItem>
                                    <label className="subtitle">Job Role</label>
                                    <p className="title mb-0">{allcandidates[index].jobroleName}</p>
                                </ListGroupItem>
                                <ListGroupItem>
                                    <label className="subtitle">Mobile No.</label>
                                    <p className="title mb-0"><a href={"tel:" + allcandidates[index].phone}>{allcandidates[index].phone}</a></p>
                                </ListGroupItem>

                                {allcandidates[index].training_center_name &&
                                    <ListGroupItem>
                                        <label className="subtitle">Training Center</label>
                                        <p className="title mb-0">{allcandidates[index].phone}</p>
                                    </ListGroupItem>}
                                <ListGroupItem>
                                    <label className="subtitle">Sector</label>
                                    <p className="title mb-0">{allcandidates[index].sectorName}</p>
                                </ListGroupItem>
                                <ListGroupItem>
                                    <label className="subtitle">Training Status</label>
                                    <p className="title mb-0">{allcandidates[index].training_status}</p>
                                </ListGroupItem>


                                <ListGroupItem className="d-flex">
                                    <CardLink title="View More" onClick={() => this.toggleModal(index)} className="btn btn-primary btn-raised mr-3" >View More</CardLink>
                                    <CardLink title="Edit" href="javascript:void(0);" className="btn btn-outline disabled">Edit</CardLink>
                                </ListGroupItem>
                            </ListGroup>
                        </Card>
                    </Col>

                );

            })
        }
        console.log('records',recordFound);
        return (<div>

            <div className="content-wrapper user-flow TP-flow">
                <Sidebar {...this.props} />

                <div id="content" className="main-content fixed-bottom-btn">
                    <Header />
                    <div className="d-block d-md-flex align-items-center section-header">
                        <div>
                            <h1 className="page-title">Candidate(s)</h1>
                            <Breadcrumb>
                                <BreadcrumbItem><a href="/TrainingPartner-dashboard">Dashboard</a></BreadcrumbItem>
                                <BreadcrumbItem active>Candidates</BreadcrumbItem>
                            </Breadcrumb>
                        </div>

                        <div className="ml-auto d-flex">
                            <Button outline color="primary" title="Add Candidate" onClick={this.handleRedirectionToAddCandidate} className="mr-0 mr-md-2 sm-floating-btn d-md-inline-block d-none" >
                                <i className="material-icons">add</i>
                                <span>Add Candidate</span>
                            </Button>

                            <Button outline color="primary" title="Bulk Upload" onClick={this.handleRedirectionToAddMultipleCandidate} className="mr-2 sm-floating-btn d-md-inline-block d-none" >
                                <i className="material-icons">arrow_upward</i>
                                <span>Bulk Upload</span>
                            </Button>

                            <ButtonDropdown isOpen={this.state.dropdownOpen} toggle={this.toggle} direction="up" className="d-block d-md-none floating-dropdown-btn">
                                <DropdownToggle outline color="primary" className="mr-0 mr-md-2 sm-floating-btn">
                                    <i className="material-icons">add</i>
                                </DropdownToggle>
                                <DropdownMenu className="z-depth-2 border-0">
                                    <DropdownItem>Add Candidate</DropdownItem>
                                    <DropdownItem divider />
                                    <DropdownItem>Bulk Upload</DropdownItem>
                                </DropdownMenu>
                            </ButtonDropdown>
                        </div>
                    </div>
                    <CandidateSearch />
                    {recordFound == 0 &&
                      <div className="blankstate">No Candidate Found.</div>
                    }
                    <Row className="details-view">
                        {final_allCandiates}
                    </Row>

                    {allcandidatesModalDetails}

                    <Footer />
                </div>

            </div>


        </div>
        );
    }
}
function mapStateToProps(state) {

    const { user } = state.authentication;
    const { items } = state.trainingpartners; //From Reducer - trainingpartners function ...
    return {
        user, items
    };
}
const connectedSearchCandidates = connect(mapStateToProps)(SearchCandidates);
export { connectedSearchCandidates as SearchCandidates }; */
