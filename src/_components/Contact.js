import React from 'react';
import { connect } from 'react-redux';
import { userActions } from '../_actions';
import { reduxForm, Field } from 'redux-form';
import { Header } from '../_components/Header';
import { Footer } from '../_components/Footer';
import { constants } from '../constants';
import { Sidebar } from '../_components/Sidebar';
import { Card, Row, Col,Breadcrumb, BreadcrumbItem, CardBody, FormGroup, } from 'reactstrap';
import 'react-day-picker/lib/style.css';

var Contact_Us_Text = constants.Contact_Us_Text;
const renderTextAreaField = ({ input, label, type, placeholder, textarea, meta: { touched, error, warning, invalid } }) => {
    const textareaType = <textarea {...input} placeholder={placeholder} type={type} className={`form-control ${touched && invalid ? 'has-danger' : ''}`} />;
    return <div className=''>
        <div className={'form-group' + (error && touched ? ' has-error' : '')}>
            <label>{label}</label>
            <div> {textarea ? textareaType : ""}
                {touched && ((error && <div className="help-block">{error}</div>) || (warning && <span>{warning}</span>))}
            </div>

        </div>
    </div>
};

const renderField = ({ input, label, type, placeholder, textarea, meta: { touched, error, warning, invalid } }) => {
    const textareaType = <textarea {...input} placeholder={label} type={type} className={`form-control ${touched && invalid ? 'has-danger' : ''}`} />;
    return <div >
        <div className={(error && touched ? ' has-error' : '')}>
            <label>{label}</label>
            <div> {textarea ? textareaType : <input {...input} type={type} placeholder={placeholder} className="form-control" />}
                {touched && ((error && <div className="help-block">{error}</div>) || (warning && <span>{warning}</span>))}
            </div>

        </div>
    </div>
};

/* const renderError = ({ meta: { touched, error } }) =>

    touched && error ? <div className="help-block">{error}</div> : false */

const validate = values => {

    const errors = {}

    if (!values.firstname) {
        errors.firstname = 'Please enter your name'
    }
     if (!values.email) {
        errors.email = 'Please enter your email address'
    }
    if (!values.address) {
        errors.address = 'Please enter address'
    }
    if (!values.subject) {
        errors.subject = 'Please enter subject'
    }
    if (!values.message) {
        errors.message = 'Please enter your message'
    }
    return errors;
}

class Contact extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            submitted: false,
            showState: false,
            registerClick: 0,

        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        //Date Related Pure Functions..
        this.checkValidation = this.checkValidation.bind(this);
    }
    componentDidMount() {
        

    }

    handleSubmit(event) {
        // event.preventDefault();
         const { user } = this.state;
         //const { dispatch } = this.props;
         const partner_type = this.props.user.user_type;
         var partner_id;
         if(partner_type === 'TP'){
             partner_id = this.props.user.data[0].trainingpartnerid;
         }else{
            partner_id = this.props.user.data[0].industrypartnerid;
         }
         this.setState({ submitted: true });
         var messageFinal = { partner_id, partner_type, ...user };
        
        //  console.log("send data", messageFinal);

        if (messageFinal.partner_id && messageFinal.partner_type && messageFinal.firstname && messageFinal.email && messageFinal.subject && messageFinal.message) {
             this.props.dispatch(userActions.updateContactMessage(messageFinal));
        }
        // console.log("Submitted Successfully");
    }
    checkValidation(event) {
        // console.log("Register clicked.");
        this.setState({ registerClick: 1 });
    }

    handleChange(event) {

        const { name, value } = event.target;
        const { user } = this.state;
        // const { dispatch } = this.props;
        // this.setState({ value: event.target.value });
        this.setState({
            user: {
                ...user,
                [name]: value
            }
        });
        //console.log("event data change", event.target.name, event.target.value);

        //if (event.target.name === 'statename') { this.props.dispatch(userActions.getDistrict(value)); }

    }

    render() {
        const { handleSubmit } = this.props;
        //const { registerClick } = this.state;
       
        return (<div className="content-wrapper user-flow TP-flow">

            <div id="content" className="main-content">
                <Sidebar {...this.props} />
                <Header />
                <div className="d-block d-md-flex align-items-center section-header">

                    <div>
                        <h1 className="page-title">Contact</h1>
                        <Breadcrumb>
                            {this.props.user.user_type === 'TP' ? 
                            <BreadcrumbItem><a href="/TrainingPartner-dashboard">Dashboard</a></BreadcrumbItem>
                            :
                            <BreadcrumbItem><a href="/Employer-dashboard">Dashboard</a></BreadcrumbItem>
                            }
                            <BreadcrumbItem active>Contact</BreadcrumbItem>
                        </Breadcrumb>
                    </div>
                </div>
               
                <Card className="z-depth-1">
                    <CardBody className="details-view">
                    <p className="subtitle">
                        {Contact_Us_Text}
                    </p>
                    <form id="frm-edit-candidate" className="form frm-Contact" onSubmit={handleSubmit(this.handleSubmit)}>
                    <Row>
                        <Col xs="12" md="6">
                        <FormGroup>
                            <Field name="firstname" type="text" component={renderField} placeholder="First Name"  label="First Name" onChange={this.handleChange} />
                        </FormGroup>
                        </Col>
                        <Col xs="12" md="6">
                        <FormGroup>
                                <Field name="lastname" type="text" component={renderField} placeholder="Last Name"  label="Last Name" onChange={this.handleChange} />  
                        </FormGroup>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs="12" md="6">
                        <FormGroup>
                                <Field name="email" type="text" component={renderField} placeholder="Email Address"  label="Email Address" onChange={this.handleChange} />  
                        </FormGroup>
                        </Col>
                        <Col xs="12" md="6">
                        <FormGroup>
                                <Field name="mobile" type="text" component={renderField} placeholder="Mobile Number"  label="Mobile Number" onChange={this.handleChange} />
                        </FormGroup>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs="12">
                        <FormGroup>
                                <Field name="subject" type="text" component={renderField} placeholder="Subject"  label="Subject" onChange={this.handleChange} />      
                        </FormGroup>
                        </Col>
                        <Col xs="12">
                        <FormGroup>
                            <Field name="message" type="textarea" component={renderTextAreaField} placeholder="Please Enter Your Message Here." textarea={true} label="Message" onChange={this.handleChange} />      
                        </FormGroup>
                        </Col>
                    </Row>
                    <div className="action-btn-block">
                        <button className="btn btn-primary btn-raised mr-3" onClick={this.checkValidation} title="Send Message">Send Message</button>
                        { this.props.user.user_type === 'TP' 
                            ? <a title="Cancel" className="btn btn-outline" href="/TrainingPartner-dashboard">Cancel</a>
                            : <a title="Cancel" className="btn btn-outline" href="/search-candidate">Cancel</a>
                        }
                        
                        
                    </div>
                    </form>
                    </CardBody>
                  </Card> 
                <Footer />
            </div>
        </div>
        );
    }
}
Contact = reduxForm({
    form: 'Contact',
    validate,
    enableReinitialize: true,
})(Contact);




function mapStateToProps(state, props) {
   
    const { user } = state.authentication;
    
    return {
        user,
    };
}

const connectedContact = connect(mapStateToProps)(Contact);
export { connectedContact as Contact };