import React from 'react';
import { connect } from 'react-redux';
import { Row, Col, Media } from 'reactstrap';
import { userActions } from '../_actions';
import { Footer } from '../_components/Footer';
import { Header } from '../_components/Header';
import candidate from '../assets/images/ic_candidates_white.svg';
import hiringRequest from '../assets/images/ic_hiring_requests_white.svg';
import TP from '../assets/images/ic_TP_white.svg';
import EMP from '../assets/images/ic_EMP_white.svg';
import CandidateDemand from '../assets/images/ic_candidate_demand.svg';

class Counters extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            applyWidthClass: '',

        };
    }


    componentDidMount() {
        this.updateDimensions();
        window.addEventListener("resize", this.updateDimensions.bind(this));
        this.props.dispatch(userActions.getHomePageStatistics());
    }
    componentWillUnmount() {
        window.removeEventListener("resize", this.updateDimensions.bind(this));
    }
    componentWillMount() {
        this.updateDimensions();
    }
    updateDimensions() {
        const winWidth = window.innerWidth
        const maxWidth = 319
        const winHeight = window.innerHeight
        //console.log("updating state",winWidth,maxWidth);
        //console.log("max width", maxWidth);
        //console.log("window width", winWidth);
        //console.log("window height", winHeight);
        if (true || winWidth < maxWidth) {
            this.setState({
                applyWidthClass: winHeight + "px",
            });
        }
    }



    render() {
        const { loading } = this.props;
		var loaderClass;
		if (loading === true) {
            loaderClass = 'loading';
        }
        /* const styles_loader = {
            top: '20px',
            left: '500px'
        }; */
        const { applyWidthClass } = this.state;
        const styles = {
            minHeight: applyWidthClass//'497px'
        };
        var totalFutureRequests = 0;
		var totalCandidates = 0;
		var totalTrainingPartners = 0;
		var totalEmployers = 0;
		var totalHiringRequests = 0;
		if (!!this.props.homepagetatistics) {
			//console.log('# homepagetatistics',this.props.homepagetatistics.data.counts[0]);
			totalFutureRequests = this.props.homepagetatistics.data.counts[0].totalFutureRequests > 0 ? this.props.homepagetatistics.data.counts[0].totalFutureRequests : 0;
			totalCandidates =  this.props.homepagetatistics.data.counts[0].totalCandidates > 0 ? this.props.homepagetatistics.data.counts[0].totalCandidates : 0;
			totalTrainingPartners =  this.props.homepagetatistics.data.counts[0].totalTrainingPartners > 0 ? this.props.homepagetatistics.data.counts[0].totalTrainingPartners : 0;
			totalEmployers =  this.props.homepagetatistics.data.counts[0].totalEmployers > 0 ? this.props.homepagetatistics.data.counts[0].totalEmployers : 0;
			totalHiringRequests =  this.props.homepagetatistics.data.counts[0].totalHiringRequests > 0 ? this.props.homepagetatistics.data.counts[0].totalHiringRequests : 0;
		}

        return (
            <div className="content-wrapper user-onboarding landing-page login-page right-block login-content">
                <Header />
                <div className={loaderClass}></div>
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-lg-8 col-12">
                            <div className="right-content-block theme-inverse half-bg-custom-height onboarding-text-content" style={styles} >
                                <div className="">
                                    <ul className="list-inline">
                                        <li className="list-inline-item"><h2 className="h1 fw-bold">Build</h2></li>
                                        <li className="list-inline-item"><h2 className="h1 fw-bold">Connect</h2></li>
                                        <li className="list-inline-item"><h2 className="h1 fw-bold">Hire</h2></li>
                                    </ul>
                                    <p>
                                        A Platform for Blue Collar Skilling to Meet Demands of the Industry.
                            </p>
                                    {this.props.loading === false && 
                                    <div className="info-counter info-counter-lg no-border mb-0">
                                        <Media className="d-flex align-items-center mb-3 mb-lg-4">
                                            <Media left className="circle-lg user-avatar avatar-filled mr-3">
                                                <img src={CandidateDemand} alt="CandidateDemand" />
                                            </Media>
                                            <Media body>
                                                <h3 className="ff-roboto h1">{totalFutureRequests.toLocaleString('en-IN')}</h3>
                                                <p className="mb-0">Demand (Next 12-months)</p>
                                            </Media>
                                        </Media>
                                    </div>
                                    }
                                    {this.props.loading === false && 
                                    <div className="info-counter">
                                        <Row>
                                            <Col xl="6">
                                                <Media className="d-flex align-items-center mb-3 mb-lg-4">
                                                    <Media left className="circle-lg user-avatar avatar-outline mr-3">
                                                        <img src={candidate} alt="candidate" />
                                                    </Media>
                                                    <Media body>
                                                        <h3>{totalCandidates.toLocaleString('en-IN')}</h3>
                                                        <p className="subtitle">Candidates</p>
                                                    </Media>
                                                </Media>
                                            </Col>

                                            <Col xl="6">
                                                <Media className="d-flex align-items-center mb-3 mb-lg-4">
                                                    <Media left className="circle-lg user-avatar avatar-outline mr-3">
                                                        <img src={hiringRequest} alt="Hiring Request" />
                                                    </Media>
                                                    <Media body>
                                                        <h3>{totalHiringRequests.toLocaleString('en-IN')}</h3>
                                                        <p className="subtitle">Hiring Requests</p>
                                                    </Media>
                                                </Media>
                                            </Col>

                                            <Col xl="6">
                                                <Media className="d-flex align-items-center mb-3 mb-lg-4 mb-xl-0">
                                                    <Media left className="circle-lg user-avatar avatar-outline mr-3">
                                                        <img src={TP} alt="Training Partners" />
                                                    </Media>
                                                    <Media body>
                                                        <h3>{totalTrainingPartners.toLocaleString('en-IN')}</h3>
                                                        <p className="subtitle">Training Partners</p>
                                                    </Media>
                                                </Media>
                                            </Col>

                                            <Col xl="6">
                                                <Media className="d-flex align-items-center">
                                                    <Media left className="circle-lg user-avatar avatar-outline mr-3">
                                                        <img src={EMP} alt="Employers" />
                                                    </Media>
                                                    <Media body>
                                                        <h3>{totalEmployers.toLocaleString('en-IN')}</h3>
                                                        <p className="subtitle">Employers</p>
                                                    </Media>
                                                </Media>
                                            </Col>
                                        </Row>
                                    </div>
                                    }
                                    <ul className="list-inline small">
                                        <li className="list-inline-item"><h3>Demand Aggregation</h3></li>
                                        <li className="list-inline-item"><h3>Training</h3></li>
                                        <li className="list-inline-item"><h3>Certification</h3></li>
                                        <li className="list-inline-item"><h3>Placement</h3></li>
                                    </ul>
                                </div>
                            </div>
                            <Footer />
                        </div>
                    </div>
                </div>
            </div>
        );


    }
}

function mapStateToProps(state) {
    const { homepagetatistics,loading } = state.generaldata;
    return {
		homepagetatistics,loading
	};
}


const connectedCounters = connect(mapStateToProps)(Counters);
export { connectedCounters as Counters };
