import React from 'react';
import { connect } from 'react-redux';
import { reduxForm, Field } from 'redux-form';
import { userActions } from '../_actions';
import { Footer } from '../_components/Footer';
import { Header } from '../_components/Header';
import { Sidebar } from '../_components/Sidebar';
import {  Card, CardTitle, Row, Col, CardBody,  CardHeader, CardLink, Form } from 'reactstrap';


const renderField = ({ input, label, type, meta: { touched, error } }) => (

    <div className=''>
        <div className={'form-group' + (error && touched ? ' has-error' : '')}>
            <label>{label}</label>
            <input {...input} type={type} className="form-control" />
            <div>
                {touched && ((error && <div className="help-block">{error}</div>))}
            </div>
        </div>
    </div>
);

export const minLength = min => value =>
    value && value.length < min ? `Must be ${min} characters or more` : undefined
export const minLength8 = minLength(8)

const passwordsMatch = (value, allValues) =>
    value !== allValues.password ? "Passwords don't match" : undefined;

const validate = values => {

    const errors = {}
    if (!values.oldpassword) {
        errors.oldpassword = 'Please enter your current password.'
    }
    if (!values.password) {
        errors.password = 'Please enter your password.'
    }
    if (!values.confirm_changepwd) {
        errors.confirm_changepwd = 'Please enter your confirm password'
    }

    return errors;
}

class ChangePasswordPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            password: '',
            submitted: false,
            applyWidthClass: ''
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    updateDimensions() {
        const winWidth = window.innerWidth
        const maxWidth = 767
        const winHeight = window.innerHeight
        //console.log("max width", maxWidth);
        //console.log("window width", winWidth);
        //console.log("window height", winHeight);
        if (true || winWidth < maxWidth) {
            this.setState({
                applyWidthClass: winHeight + "px",
            });
        }
    }
    componentWillMount() {
        this.updateDimensions();
    }
    componentDidMount() {
        this.updateDimensions();
        window.addEventListener("resize", this.updateDimensions.bind(this));
    }
    componentWillUnmount() {
        window.removeEventListener("resize", this.updateDimensions.bind(this));
    }
    handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });
    }

    handleSubmit(e) {
        // e.preventDefault();

        this.setState({ submitted: true });
        const { password,oldpassword } = this.state;
        const { dispatch } = this.props;
        var partnerID;
        var partnerType;
        if(this.props.user.user_type === 'TP'){
            partnerID = this.props.user.data[0].trainingpartnerid;
            partnerType = 'TP';
        }else if(this.props.user.user_type === 'EMP'){
            partnerID = this.props.user.data[0].industrypartnerid;
            partnerType = 'EMP';
        }else if(this.props.user.user_type === 'TC'){
            partnerID = this.props.user.data[0].trainingcenterid;
            partnerType = 'TC';
        }
        
       
        // console.log("send data",partnerID,oldpassword, password,partnerType);
        // return;
        if (partnerID && oldpassword && password && partnerType) {
            dispatch(userActions.ChangePassword(partnerID, oldpassword, password, partnerType));
            //Redirect to Settings page after 5 seconds once we update the new password...
        //     this.timeoutHandle = setTimeout(()=>{
        //        console.log('Redirecting to Settings Page...') 
        //        this.props.history.push('/Settings');
        //    }, 5000);
           //End of redirecting login page code...

        }
    }


    render() {
        const { handleSubmit } = this.props;
        
        return (
            <div className="content-wrapper user-flow TP-flow">
                  <Sidebar {...this.props} />
                  
                  <div id="content" className="main-content">
                    <div className="inner-page">
                      <Header />

                         <Row>
                        <Col xs="12" xl="6">
                          <Card className="z-depth-1">
                            <CardHeader className="bg-transparent border-bottom-0 card-header-shadow">
                              <div className="section-title">
                                <CardTitle className="title">Change Password</CardTitle>
                              </div>
                            </CardHeader>
          
                            <CardBody>
                              <Form id="frm-forgot" className="form frm-forgot" onSubmit={handleSubmit(this.handleSubmit)}>
                                <Row>
                                  <Col xs="12">
                                  <Field name="oldpassword" type="password" component={renderField} label="Current Password" onChange={this.handleChange} validate={minLength8} />
                                  <Field name="password" type="password" component={renderField} label="New Password" onChange={this.handleChange} validate={minLength8} />
                                            <Field
                                                name="confirm_changepwd"
                                                type="password"
                                                component={renderField}
                                                label="Confirm Password"
                                                onChange={this.handleChange}
                                                validate={passwordsMatch}
                                            />
                                            
                                  </Col>
                                </Row>
          
                                <Row>
                                  <Col xs="12">
                                    <div className="action-btn-block mobile-btn">
                                    <button className="btn btn-primary btn-raised mr-2" title="Submit">Update Password </button>
                                      <CardLink title="Cancel" href="/Settings" className="btn btn-outline">Cancel</CardLink>
                                    </div>
                                  </Col>
                                </Row>
                              </Form>
                            </CardBody>
                          </Card>
                        </Col>
                      </Row>

                      <Footer />
                    </div>
                  </div>
          
                </div>

            

        );
    }
}

ChangePasswordPage = reduxForm({
    form: 'ChangePasswordPage',
    validate,
    enableReinitialize: true,
})(ChangePasswordPage);

function mapStateToProps(state) {
    const { loggingIn, alert, users, authentication } = state;
    const { user } = authentication;
    const { items } = state.resetpassword;
    return {
        loggingIn, alert, items, user, users
    };
}

const connectedChangePasswordPage = connect(mapStateToProps)(ChangePasswordPage);
export { connectedChangePasswordPage as ChangePasswordPage }; 