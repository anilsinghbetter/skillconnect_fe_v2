import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
//import { Header } from '../_components/Header';
//import { Footer } from '../_components/Footer';
import { Sidebar } from '../_components/Sidebar';
//import { userActions, alertActions } from '../_actions';

//import { Label, FormGroup, Input, InputGroup, InputGroupAddon, Button, Card, CardBody, CardImg, CardText, CardTitle, CardSubtitle } from 'reactstrap';
class NotFound extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            //Initial State..
        };
    }
    render() {
        //console.log('#@@',this.props.authentication.user.user_type);
        var user_redirect="";

        if(this.props.authentication.user.user_type==='EMP')user_redirect='/search-candidate';
        else if(this.props.authentication.user.user_type==='TP')user_redirect='/TrainingPartner-dashboard';
        else user_redirect='/login';
       
        return (
            <div className="content-wrapper user-flow TP-flow">
                <Sidebar {...this.props} />
                <div className="page-content">
                    <div className="d-flex justify-content-center align-items-center error-page">
                        <div className="text-center">
                            <div className="img-wrapper mb-4">
                                <img src="../assets/images/404.png" alt="404" />
                            </div>
                            <h1 className="h2 fw-regular mb-0">Oops! Page not found</h1>
                            <span className="h6 ff-roboto d-block">
                                Sorry, but the page you are looking for is not found. <br />
                                Please make sure you have typed the right URL.
						</span>
                            <Link title="Back to home" to={user_redirect} className="btn btn-primary btn-lg btn-raised btn-block">Back to home</Link>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
function mapStateToProps(state) {
    const { users, authentication } = state;
    const { user } = authentication;
    //Add state values which we need to integrate with props..
    return {
        user, users, authentication
        //registering
    };
}
const connectedNotFound = connect(mapStateToProps)(NotFound);
export { connectedNotFound as NotFound };