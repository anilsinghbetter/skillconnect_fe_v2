import React from 'react';
import { connect } from 'react-redux';
import { userActions } from '../_actions';
import { reduxForm, Field } from 'redux-form';
import { Card, CardTitle, Row, Col,Breadcrumb, BreadcrumbItem, CardBody, CardHeader } from 'reactstrap';
import { constants } from '../constants';
import Select from 'react-select';
import 'react-day-picker/lib/style.css';

const renderTextAreaField = ({ input, label, type, placeholder, textarea, meta: { touched, error, warning, invalid } }) => {

    const textareaType = <textarea {...input} placeholder={label} type={type} className={`form-control ${touched && invalid ? 'has-danger' : ''}`} />;

    return <div className=''>
        <div className={'form-group' + (error && touched ? ' has-error' : '')}>
            <label>{label}</label>
            <div> {textarea ? textareaType : ""}
                {touched && ((error && <div className="help-block">{error}</div>) || (warning && <span>{warning}</span>))}
            </div>

        </div>
    </div>
};

const renderField = ({ input, label, type, placeholder, textarea, meta: { touched, error, warning, invalid } }) => {
    const textareaType = <textarea {...input} placeholder={label} type={type} className={`form-control ${touched && invalid ? 'has-danger' : ''}`} />;
    return <div className={((label === "contact_person_name" || label === "contact_email_address" || label === "contact_mobile" || label === "address") ? 'col-md-4 col-12' : 'col-md-4 col-12')}>
        <div className={'form-group' + (error && touched ? ' has-error' : '')}>
            <label>{label}</label>
            <div> {textarea ? textareaType : <input {...input} type={type} placeholder={placeholder} className="form-control" />}
                {touched && ((error && <div className="help-block">{error}</div>) || (warning && <span>{warning}</span>))}
            </div>

        </div>
    </div>
};

// const renderError = ({ meta: { touched, error } }) =>
//     touched && error ? <div className="help-block">{error}</div> : false

const validate = values => {

    const errors = {}

    if (!values.contact_person_name) {
        errors.contact_person_name = 'Please enter contact person name'
    }
    if (!values.contact_email_address) {
        errors.contact_email_address = 'Please enter contact person email'
    }
    if (!values.address) {
        errors.address = 'Please enter address'
    }
    if (!values.jobrole) {
        errors.jobrole = 'Please select atleast one jobrole'
    }
    
    return errors;
}

class TrainingCenterSetting extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            submitted: false,
            showState: false,
            registerClick: 0,
            jobrole_value: '',
            is_value_set: 0,
            edited_jobrole_value: [],

        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        //Date Related Pure Functions..
        this.handleSelectChange = this.handleSelectChange.bind(this);
        this.checkValidation = this.checkValidation.bind(this);
    }
    componentDidMount() {
        //get Employer Details
        var center_id = this.props.user.data[0].trainingcenterid;
        
        this.props.dispatch(userActions.getTrainingCenterDetails(center_id));
        //console.log(this.props,"PROPERTY");
        
        
    }

    handleSubmit(event) {
        // event.preventDefault();
        const { user,jobrole_value } = this.state;
        this.setState({ submitted: true });

        var trainingcenterid = this.props.user.data[0].trainingcenterid;
        // var trainingpartnerid = this.props.user.data[0].trainingpartnerid;
        
        // var userFinal = { trainingcenterid, ...user };
        // if (jobrole_value) {
            var userFinal = { trainingcenterid, jobrole_value, ...user };
        // }
        // console.log("send data", userFinal);
        // return;
        if (userFinal.trainingcenterid && jobrole_value) {
            this.props.dispatch(userActions.updateTrainingCenterProfile(userFinal));
        }else{
            return;
        }
        var localUserData = JSON.parse(localStorage.getItem('user'));

        for(let updatedvalues in userFinal){
            if(updatedvalues === 'trainingcenterid'){
            }else{
                localUserData.data[0][updatedvalues] = userFinal[updatedvalues];
            }                
                // console.log(localUserData,"VAL here")
        }
        localStorage.setItem('user', JSON.stringify(localUserData));

        setTimeout(() => {
            window.location.reload(0);
        }, constants.REFRESH_TIMEOUT);
    }
    checkValidation(event) {
        //console.log("Register clicked.");
        this.setState({ registerClick: 1 });
    }
    //Mutiselect Related Functions... 
    handleSelectChange(value) {
        // console.log('You\'ve selected:', value);
        //this.setState( { value } );
        if(!value){
            this.setState({error_flag_jobrole:1});
        }else{
            this.setState({error_flag_jobrole:0});
        }
        
        this.setState({ jobrole_value: value });
    }
    handleChange(event) {

        const { name, value } = event.target;
        const { user } = this.state;
        
        //console.log("this is state",this.state,"this is prop",this.props);
        // const { dispatch } = this.props;
        // this.setState({ value: event.target.value });
        this.setState({
            user: {
                ...user,
                [name]: value
            }
        });
        //console.log(user,"USER");

    }

    render() {
        const { handleSubmit } = this.props;
        const { registerClick, jobrole_value  } = this.state;
        // console.log(this.props.trainingcenterdata,"PROPERTY");
        // console.log(this.props.trainingcenterdata,"CONSOLE");
        if(!!this.props.trainingcenterdata){
            var trainingCenterDetails = this.props.trainingcenterdata.data[0];
        }
        if (!!this.props.jobroles) {
            var allJobroles = this.props.jobroles.data;
            var final_allJobroles = [];
            
            allJobroles.map((value) => {
                // console.log(value.jobrole_id, '==>',value.jobrole_name);
                return final_allJobroles.push({ "label": value.jobrole_name, "value": value.jobrole_id });
            })
             //console.log("fina array", final_allSectors);
        }
        if(!!this.props.trainingcenterdata && !registerClick && this.state.is_value_set === 0)  {
            var jobrolevalue = this.props.trainingcenterdata.data[0].jobrole;
            // console.log(jobrolevalue,"jobrolevalue");
            if(!!jobrolevalue){
                jobrolevalue = jobrolevalue.split(',');
                this.state.jobrole_value = jobrolevalue;
                this.state.is_value_set = 1;
            }
        }
       
        return (
            <div className="content-wrapper user-flow TP-flow">
                <div className="d-block d-md-flex align-items-center section-header">
                    <div>
                        <h1 className="page-title">Settings</h1>
                        <Breadcrumb>
                            <BreadcrumbItem><a href="/candidates">Candidates</a></BreadcrumbItem>
                            <BreadcrumbItem active>Settings</BreadcrumbItem>
                        </Breadcrumb>
                    </div>
                </div>
               
                <form id="TrainingCenterSetting" className="form frm-edit-candidaten" onSubmit={handleSubmit(this.handleSubmit)}>
                    <Card className="card z-depth-1">
                        <CardHeader className="bg-transparent border-bottom-0 card-header-shadow">
                            <div className="section-title">
                                <CardTitle className="title">Center Details</CardTitle>
                            </div>
                        </CardHeader>    

                        <CardBody className="details-view">
                            <Row>  
                                <Col xs="12" className="mb-3" xl="4">
                                    <label className="subtitle">Training Center Name</label>
                                    <h5 className="title">{!!trainingCenterDetails ? trainingCenterDetails.trainingcenter_name : '-'}</h5>  
                                </Col>

                                <Col xs="12" className="mb-3" xl="4">
                                    <label className="subtitle">Training Partner</label>
                                    <h5 className="title">{!!trainingCenterDetails ? trainingCenterDetails.training_partner_name : '-'}</h5>  
                                </Col>
                               
                                <Col xs="12" md="6" xl="4" className="mb-3 mb-xl-0">
                                    <label className="subtitle">SDMS/Training Center ID</label>
                                    <p className="title mb-0">{!!trainingCenterDetails ? trainingCenterDetails.sdms_trainingcenter_id : '-' } </p>
                                </Col>
                                <Col xs="12" md="6" xl="4" className="mb-3 mb-xl-0">
                                    <label className="subtitle">State</label>
                                    <p className="title mb-0"> {!!trainingCenterDetails ? (trainingCenterDetails.stateName !== undefined &&  trainingCenterDetails.stateName !== null ? trainingCenterDetails.stateName : '-') : '-'} </p>
                                </Col>
                                <Col xs="12" md="6" xl="4" className="mb-3 mb-xl-0">
                                    <label className="subtitle">District</label>
                                    <p className="title mb-0">{!!trainingCenterDetails ? (trainingCenterDetails.cityName !== undefined &&  trainingCenterDetails.cityName !== null ? trainingCenterDetails.cityName : '-') : '-'}</p>
                                </Col>
                                <Col xs="12" md="6" xl="4" className="mb-3 mb-xl-0">
                                    <label className="subtitle">Pincode</label>
                                    <p className="title mb-0">{!!trainingCenterDetails ? (trainingCenterDetails.pincode !== undefined &&  trainingCenterDetails.pincode !== null ? trainingCenterDetails.pincode : '-') : '-'}</p>
                                </Col>
                            </Row>
                        </CardBody>
                    </Card>

                    <Card className="card z-depth-1"> 
                        <CardHeader className="bg-transparent border-bottom-0 card-header-shadow">
                            <div className="section-title">
                                <CardTitle className="title">Contact Person Details</CardTitle>
                            </div>
                        </CardHeader>   
                        
                        <CardBody className="details-view">
                            <div className="row">
                                <Field name="contact_person_name" type="text" component={renderField} label="Contact Person Name" placeholder="Contact Person Name" onChange={this.handleChange} />
                                <Field name="contact_email_address" type="text" component={renderField} label="Email Address" placeholder="Email Address" onChange={this.handleChange} />
                                <Field name="contact_mobile" type="text" component={renderField} label="Mobile Number" placeholder="Mobile Number" onChange={this.handleChange} />
                                <div className="col-12 col-md-12">
                                    <div className={registerClick === 1 && jobrole_value.length === 0 ? ' has-error' : ''}>
                                            <label className="lable">Courses/Job Role offered</label>
                                            <Select
                                                // closeOnSelect={!stayOpen}
                                                disabled={false}
                                                multi
                                                onChange={this.handleSelectChange}
                                                options={final_allJobroles}
                                                placeholder="Select your relavant Jobrole(s)"
                                                removeSelected={this.state.removeSelected}
                                                // rtl={this.state.rtl}
                                                // defaultValue = {final_allJobroles}
                                                simpleValue
                                                value={jobrole_value}
                                                // component={renderColorSelector}
                                                name="jobrole"
                                            />
                                        {registerClick === 1  && jobrole_value.length === 0 &&
                                            <div className="help-block">Please select atleast one Jobrole</div>
                                        }
                                    </div>
                                </div>
                            </div>
                            <div className="">
                                <Field name="address" component={renderTextAreaField} type="textarea" label="Address" textarea={true} onChange={this.handleChange} />
                            </div>

                            <div className="action-btn-block">
                                <button className="btn btn-primary btn-raised mr-3" onClick={this.checkValidation} title="Save">Save Changes</button>
                                <a title="Change Password" href="/Change-Password" className="btn btn-primary btn-raised mr-3" >Change Password</a>
                                <a title="Cancel" className="btn btn-outline " href="/candidates">Cancel</a>
                            </div>
                        </CardBody>
                    </Card>
                </form>
            </div>
        );
    }
}
TrainingCenterSetting = reduxForm({
    form: 'TrainingCenterSetting',
    validate,
    enableReinitialize: true,
})(TrainingCenterSetting);




function mapStateToProps(state, props) {
   
    const { user } = state.authentication;
    const { trainingcenterdata} = state.getcenterdetails; //From Reducer - trainingcenters function ...
    const { jobroles } = state.jobdata; //From Reducer - JobROleReducer function ...
    var formated_object = {};

    // console.log("in MapTOState tc data", trainingcenterdata );

    if (!!trainingcenterdata) {
        var EditTrainingCenterDetails = trainingcenterdata.data[0];

        formated_object = {
            address: EditTrainingCenterDetails.address,
            stateName: EditTrainingCenterDetails.stateName,
            cityName: EditTrainingCenterDetails.cityName,
            pincode: EditTrainingCenterDetails.pincode,
            jobrole: EditTrainingCenterDetails.jobrole,
            contact_person_name: EditTrainingCenterDetails.contact_person_name  ,
            contact_email_address: EditTrainingCenterDetails.contact_email_address,
            contact_mobile: EditTrainingCenterDetails.contact_mobile,
            legal_entity_name: EditTrainingCenterDetails.legal_entity_name,
        }
       
    }
    return {
        user,
        initialValues: formated_object,
        trainingcenterdata,
        jobroles,
    };
}

const connectedTrainingCenterSetting = connect(mapStateToProps)(TrainingCenterSetting);
export { connectedTrainingCenterSetting as TrainingCenterSetting };