import React from 'react';
import { connect } from 'react-redux';
import { reduxForm, Field } from 'redux-form';
import {Button, FormGroup, Label, Input, Row, Col, Card, CardBody } from 'reactstrap';
import { userActions } from '../_actions';

// const validate = values => {
//     const errors = {}
//     return errors;
// }

// const renderField = ({ input, label, type, placeholder, textarea, meta: { touched, error, warning, invalid } }) => {
//     const textareaType = <textarea {...input} placeholder={label} type={type} className={`form-control ${touched && invalid ? 'has-danger' : ''}`} />;

//     return <div className={((label === "Gender") ? 'col-md-8 col-12' : 'col-md-4 col-12')}>
//         <div className={'form-group' + (error && touched ? ' has-error' : '')}>
//             <label>{label}</label>
//             <div> {textarea ? textareaType : <input {...input} type={type} placeholder={placeholder} className="form-control" />}
//                 {touched && ((error && <div className="help-block">{error}</div>) || (warning && <span>{warning}</span>))}
//             </div>

//         </div>
//     </div>
// };

const renderColorSelector = ({ input, label, dynamic_values, meta: { touched, error } }) => (
    <div key='1' className='form-group'>
        <label>{label}</label>
        <select  {...input} className="form-control">
            <option value="">Select</option>
            {dynamic_values}
            ))}
     </select>
    </div>
)

class TrainingCenterSearch extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            submitted: false,
            selectedOption: '',
            // isData:0,
            collapse: false,
            updatedValue: this.props.mytest,
            results: [],
            //Initial State..
            // data: this.props.allcenters
        };
        this.handleSearchFilter = this.handleSearchFilter.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleClear = this.handleClear.bind(this);

    }

    handleChange(event) {
        const { name, value } = event.target;
        const { user } = this.state;

        //Remove/Blank the jobroel id If Exists...         
        if(!!this.state.user)
        {   if(this.state.user.stateid > 0)
            {
              //console.log('#A');
               this.state.user.cityid = "";   
              //console.log('#AA',this.state.user.jobroleid);
            }
        }
        //End code for Removeing the jobroel id If Exists... 
    
        this.setState({
            user: {
                ...user,
                [name]: value
            }
        });
        //console.log("event data change", event.target.name, event.target.value);
       // if (event.target.name === 'sectorid') { this.props.dispatch(userActions.getJobRoles(value)); }
        if (event.target.name === 'stateid') { this.props.dispatch(userActions.getDistrict(value)); }
    }
    handleClear(e) {
        var trainingpartnerid = this.props.user.data[0].trainingpartnerid;
        
        this.props.reset();
        document.getElementById("SearchTrainingCenterform").reset();
        
        this.props.dispatch(userActions.getDistrict(0));

        this.setState({
            user:'',
            submitted: false
        });
        
        const input = document.querySelector('#trainingcenter_name');
        input.value = '';
        
        this.props.dispatch(userActions.getCenters(trainingpartnerid, 1));
    }

    componentDidMount() {
        //console.log('#HISTORY',history.location);
        // const { user } = this.props;
        // console.log("Traingin partner id", this.props.user.data[0].trainingpartnerid);
        //this.props.dispatch(userActions.getTrainingCenterSearchFormData(this.props.user.data[0].trainingpartnerid));
        this.props.dispatch(userActions.getCandidateSearchFormData(this.props.user.data[0].trainingpartnerid, this.props.user.user_type));
    }

    handleSearchFilter() {
        this.setState({ collapse: !this.state.collapse });
    }
    handleSubmit(e) {
        e.preventDefault();
        /* const { user } = this.state;
        const { dispatch } = this.props; */
        
        //console.log("Start Date",typeof this.state.selectedBatchStartDate);        
        // console.log("all keywords", userFinal);

        //console.log("search keywords", this.state.user);
        /* Need to check this..
         history.push({
            pathname: '/training-centers',
            search: '?query='+JSON.stringify(this.state.user),
            state:   this.state.results
          })*/
        if (this.state.user !== undefined && this.state.user !== "") {
            var trainingpartnerid = this.props.user.data[0].trainingpartnerid;
            var userFinal = { trainingpartnerid, ...this.state.user };

            this.setState({ submitted: true });

            this.props.dispatch(userActions.getTrainingPartnerSearchCenter(userFinal, 1, 0));

            //Passing Search Data to Parent Component -  GetCenters..
            this.props.onSelectedChange(this.state.user);
        }
    }

    render() {
        var loaderClass = '';

        //Show loader..
        if (this.props.loading === true) {
            //console.log("Data Is Loading");
            loaderClass = 'loading';
        }
        const styles = {
            top: '115px',
            left: '60px'
        };
        // let posts = decodeURIComponent(this.props.mySearchedData).replace(/\?/g, ' ');

        /* console.log("PROPS", this.props);
        console.log("PROPS.allcenters", this.props.allcenters); */

        //const { searchRes } = this.props;
        //const { handleRedirect, results } = this.state;
        //const { selectedOption } = this.state;

        //var allTrainingCenterValues = [];
        if (this.props.allcenters) {
            // console.log("TrainingCenterSearch:TP ",this.state);
            // console.log("TrainingCenterSearch: Candidate Vlaues", this.props.allcenters.data);
            // console.log("TrainingCenterSearch:use search  Vlaues", this.state.user);
            //console.log("From Seach",this.props.search);

            /* Search Based Pagination*/

            /* if (this.props.search == 1) {
                //console.log("Here111");
                //console.log("From Seach", this.props.search);
                allTrainingCenterValues = this.props.allcenters;
                //console.log("searched data@@@", allTrainingCenterValues);
            }
            //End of search pagination...
            else {
                //console.log("Here22");
                allTrainingCenterValues = this.props.allcenters.data;
            } */


            //var userInputkeywords = this.state.user;

            /* if (userInputkeywords != undefined) {
                //  var searchString = userInputkeywords.candidate_name.trim().toLowerCase();
                if (userInputkeywords != undefined) {

                  
                    var updatedkeys=clean(userInputkeywords);

                    function clean(obj) {
                        for (var propName in obj) { 
                           // console.log("@@@",obj);
                           // console.log("%%%%%",propName);
                           // console.log("####",obj[propName]);
                          if (obj[propName] === "" || obj[propName] === null || obj[propName] === undefined) {
                            delete obj[propName];
                          }
                        }
                      }                      

                    console.log("User Search Keywords : ", userInputkeywords);

                   // console.log("updated Search Keywords : ", updatedkeys);
                   // console.log("all data : ", allTrainingCenterValues);

                    // var fillStateValue = allTrainingCenterValues;
                    var fillStateValue = allTrainingCenterValues.filter(function (obj) {

                        return Object.keys(userInputkeywords).every(function (c) {
                            
                          //  console.log("SEARCH value",userInputkeywords[c]);
                            if (!isNaN(parseFloat(obj[c])) && isFinite(obj[c])) {
                                return obj[c] == userInputkeywords[c];
                            } else {
                                  var escaped_str = userInputkeywords[c].replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&');
                                  return new RegExp("" + escaped_str + "", "i").test(obj[c]);
                                //return new RegExp("" + userInputkeywords[c] + "", "i").test(obj[c]);
                            }
                        });
                    });

                    console.log("Below Seacrh data", fillStateValue);

                    this.state.results = fillStateValue;
                }
                else {
                    this.state.results = "";
                    //this.setState({results: ""});
                }
            } */
        }


        if (!!this.props.districtlist) {
            var allCities = this.props.districtlist.data;
            var final_allCities = [];
            //State values..
            allCities.map((value, index) => {
                return final_allCities.push(<option key={index} value={value.cityid}>{value.cityName}</option>);
            })
        }

        if (!!this.props.searchformdata) {

            //State values..
            var allStates = this.props.searchformdata.data.state[0];

            var final_allStates = [];
            allStates.map((value, index) => {
                return final_allStates.push(<option key={index} value={value.stateid}>{value.stateName}</option>);
            })


            //Training Center values..
            var final_allTrainingCenters = [];
            if (this.props.searchformdata.data.training_centre !== undefined && this.props.searchformdata.data.training_centre.length > 0) {
                var allTrainingCenters = this.props.searchformdata.data.training_centre[0];
                allTrainingCenters.map((value, index) => {
                    return final_allTrainingCenters.push(<option key={index} value={value.trainingcenterid}>{value.trainingcenter_name}</option>);
                })
            }

            //Training Status list..
            var alltrainingstatus = this.props.searchformdata.data.training_status[0];
            var final_trainingstatus = [];
            alltrainingstatus.map((value, index) => {
                return final_trainingstatus.push(<option value={value.optionValue} key={index} >{value.optionKey}</option>);
            })
        }
        return (
            <Card className="z-depth-1 no-hover">
                <CardBody>
                    <div className="search-bar mb-0">
                        <Row className="align-items-center">
                            <Col className="mb-0 d-none d-md-flex align-items-center">
                                <form name="SearchTrainingCenterform" id="SearchTrainingCenterform" onSubmit={this.handleSubmit} className="w-100">
                                    <div className={'row align-items-center' + (this.state.collapse ? ' d-none' : '')} >

                                        <div className="col-12 col-md-4 pr-0">
                                            <FormGroup className="form-icon">
                                                <Label for="search">Name</Label>
                                                <i className="material-icons">search</i>

                                                <Input type="text" id="trainingcenter_name" name="trainingcenter_name" placeholder="Enter Training Center Name" onChange={this.handleChange}/>

                                            </FormGroup>
                                        </div>

                                        <div className="col-12 col-md-3 pr-0">
                                            <Field name="stateid" component={renderColorSelector} dynamic_values={final_allStates} onChange={this.handleChange} label="State" />
                                        </div>
                                        <div className="col-12 col-md-3 ">
                                            <FormGroup>
                                                <Field name="cityid" component={renderColorSelector} dynamic_values={final_allCities} onChange={this.handleChange} label="District" />
                                            </FormGroup>
                                        </div>

                                        <Button color="default">Go</Button>
                                        {!!this.state.submitted && <Button className="ml-2" color="default" onClick={this.handleClear}>Clear</Button>}
                                    </div>


                                </form>
                            </Col>
                        </Row>
                    </div>

                    <div className={loaderClass} style={styles}></div>
                </CardBody>
            </Card>
        );
    }
}

TrainingCenterSearch = reduxForm({
    form: 'TrainingCenterSearch',
    renableReinitialize: true,
    asyncBlurFields: [], //to remove validation, we must need to remove it from Blur fields...
})(TrainingCenterSearch);

function mapStateToProps(state, ownProps) {
    // console.log("My statesProps",ownProps);
    const { user } = state.authentication;
    const { allcenters, loading } = state.trainingpartners; //From Reducer - trainingpartners function ...
    const { searchformdata } = state.searchform;
    const { jobroles } = state.jobdata; //From Reducer - JobROleReducer function ...
    const { districtlist } = state.generaldata; //From Reducer - CommonReducer function ...
   
    //console.log("HisMap",history.location);
    /*
    console.log("in MapTOState",ownProps.mySearchedData);
    var candidateName = "";
    var sectorName= "";
    var jobroleName="";

   if('mySearchedData' in ownProps)
               {
                    console.log("success here");
                    candidateName = ownProps.mySearchedData.candidate_name
                    sectorName = ownProps.mySearchedData.sectorid
                    jobroleName =  ownProps.mySearchedData.jobroleid
                }
                    
               else
               {
                console.log("failure here");
                candidateName = "";  
                sectorName="";
                jobroleName =  "";
               } 
               */

    return {

        user, searchformdata, jobroles, allcenters, districtlist, loading
        /* initialValues: {
             candidate_name: candidateName,
             sectorid:sectorName,
             jobroleid:jobroleName  
         }*/

    };
}
const connectedTrainingCenterSearch = connect(mapStateToProps)(TrainingCenterSearch);
export { connectedTrainingCenterSearch as TrainingCenterSearch };