import React from 'react';
import { connect } from 'react-redux';
import { userActions } from '../_actions';
import { Link } from 'react-router-dom';
import { TrainingCenterSearch } from '../_components/TrainingCenterSearch';
import trainingCentreSVG from '../assets/images/training_center_grey.svg';
//import { history } from '../_helpers';

import {
    Row, Col, Card, CardHeader, CardSubtitle, CardTitle, CardLink, ListGroup, ListGroupItem, Button, Modal, ModalBody, ModalHeader, ModalFooter } from 'reactstrap';
class GetCenters extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpen: false,
            CardValue: undefined,
            delCardValue: undefined,
            isDeleted: 0,
            page: 1,
            delCardName: undefined,
            searchparams: undefined,
            fromsearch: 0
        };
        this.toggleModal = this.toggleModal.bind(this);
        this.toggleModalforDelete = this.toggleModalforDelete.bind(this);
        this.closeModalBox = this.closeModalBox.bind(this);
        this.deleteTrainingCenter = this.deleteTrainingCenter.bind(this);
    }
    closeModalBox() {
        this.setState({ isOpen: !this.state.isOpen });
    }
    handleChild = (langValue) => {
        //this.setState({ mydata: langValue });
        this.state.page = 1;
        this.state.fromsearch = 1;
        this.setState({ searchparams: langValue, isOpen: false, delCardValue: undefined, CardValue: undefined });
    }
    toggleModal = (e) => {
        this.setState({ isOpen: !this.state.isOpen });
        this.setState({ isOpen: !this.state.isOpen, delCardValue: undefined });
        this.setState({ CardValue: e });
    }
    toggleModalforDelete = (e,Traning_Center_Name) => {
        //console.log("delete clicked with event",e);
        this.setState({ hideModalBox: true });
        this.setState({ isOpen: !this.state.isOpen, CardValue: undefined });
        this.setState({ delCardValue: e, delCardName: Traning_Center_Name });
    }
    componentDidMount() {
        this.loadData();
    }
    deleteTrainingCenter(e) {
        const { page } = this.state
        //console.log("Deleting Training Center!!!",e);
        this.props.dispatch(userActions.deleteTrainingCenter(e, 'trainingcenter', page));
        this.setState({ isOpen: !this.state.isOpen, isDeleted: 1 });
        // console.log("Now going to get new canddiates");
        // this.loadData();
    }
    loadData() {
        const { page } = this.state
        if (localStorage.getItem('user') !== null) {
            var userdata = JSON.parse(localStorage.getItem('user'));
        }
        var training_partner_id = userdata.data[0].trainingpartnerid;
        if (this.state.searchparams !== undefined) {
            var trainingpartnerid = training_partner_id;
            var userFinal = { trainingpartnerid, ...this.state.searchparams };
            //console.log("NEXT PAGE NO", page);
            this.props.dispatch(userActions.getTrainingPartnerSearchCenter(userFinal, page, 1));
        } else {
            this.props.dispatch(userActions.getCenters(training_partner_id, page));
        }
    }
    loadMore = () => {
        //First update the state and then use callback method to call next page call...
        this.setState(prevState => ({
            page: prevState.page + 1
        }), this.loadData)
    }
    render() {
        var loaderClass = '';
        //Show loader..
        //console.log('#this.props.loading',this.props.loading);
        if (this.props.loading === true) {
            loaderClass = 'loading';
        }
        const styles = {
            top: '115px',
            left: '60px'
        };
        var loadMore = "";

        var centers = "";
        var recordFound = '';
        if (!!this.props.allcenters || !!this.props.searchcenters) {
            if (this.props.searchcenters !== undefined) {
                centers = this.props.searchcenters;
            } else {
                centers = this.props.allcenters;
            }
            if (!!centers && centers.paginationOptions !== undefined) {
                var totalpages = centers.paginationOptions.totalPages;
                var currentpage = centers.paginationOptions.curPage;
                if (currentpage < totalpages) {
                    loadMore = (<div className="text-center"><a title="Load More" onClick={this.loadMore} className="btn btn-outline-secondary card-link">Load More</a></div>);
                }
            }
            if (centers.data.length === 0) {
                recordFound = 0;
            } else {
                recordFound = 1;
            }
            var allcenters = centers.data;
            var final_allCandiates = [];
            //console.log('I am 1', allcenters);

            //DeleteModal Box
            if (!!centers && this.state.delCardValue !== undefined) {
                var delmymodaval = this.state.delCardValue;
                var TC_Name = this.state.delCardName;
                var deleteTrainingCenterModal = (
                    <Modal backdrop={false} centered isOpen={this.state.isOpen} className="alert-modal">
                        <ModalHeader toggle={() => this.toggleModalforDelete(delmymodaval)} className="d-block section-title text-center border-bottom-0 no-border">
                            Are you sure you want to remove "{TC_Name}"?
                        </ModalHeader>
                        <ModalBody>
                            <div className="d-flex justify-content-center">
                                <Button color="primary" className="btn-raised mr-3" onClick={() => this.deleteTrainingCenter(delmymodaval)}>Yes</Button>
                                <Button ouline="true" color="default" onClick={this.closeModalBox}>No</Button>
                            </div>
                        </ModalBody>
                    </Modal>
                );
            }
            //End of Delete Modal box
            //console.log('#ARCHT',history.location.state);
            //console.log('#DESIRED OUTPUT',this.state.mydata);
            /*
            if (!!history.location.state) {
            
                console.log('#GETTING DATA FROM URL');

                // loadMore="";//If search data then remove Load More option...

                if(typeof this.state.delCardValue == 'number' && this.state.delCardValue > 0 && this.state.isDeleted == 1){
                    history.location.state = history.location.state.filter(user => user.trainingcenterid !== this.state.delCardValue);
                }
                allcenters = history.location.state;
            }*/

            if (!!this.state.mydata) {
                // loadMore="";//If search data then remove Load More option...
                if (typeof this.state.delCardValue === 'number' && this.state.delCardValue > 0 && this.state.isDeleted === 1) {

                    this.state.mydata = this.state.mydata.filter(user => user.trainingcenterid !== this.state.delCardValue);
                }
                allcenters = this.state.mydata;
            }
            else if ('data' in centers) {
                // console.log("2");
                allcenters = centers.data;
            }
            else {
                // console.log("3");
                allcenters = centers;
            }

            if (!!centers && this.state.CardValue !== undefined) {

                var mymodaval = this.state.CardValue;
                var tel = allcenters[this.state.CardValue].contact_mobile ? "tel:" + allcenters[this.state.CardValue].contact_mobile : ''
                var mailTo = allcenters[this.state.CardValue].contact_email_address ? "mailto:" + allcenters[this.state.CardValue].contact_email_address : ''
                var allcentersModalDetails = (

                    <Modal backdrop={false} centered size="lg" isOpen={this.state.isOpen} toggle={() => this.toggleModal(mymodaval)}>
                        <ModalHeader toggle={() => this.toggleModal(mymodaval)} className="border-bottom-0 modal-header-shadow align-items-center">
                            <div className="section-title">
                                <h5 className="title modal-title">Training Center Details</h5>
                            </div>
                        </ModalHeader>
                        <ModalBody className="details-view">
                            <Row>
                                <Col xs="12">
                                    <h5 className="title modal-view-title">{allcenters[this.state.CardValue].trainingcenter_name}'s Details</h5>
                                </Col>
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">Training Center Name</label>
                                    <p className="title">{allcenters[this.state.CardValue].trainingcenter_name ? allcenters[this.state.CardValue].trainingcenter_name : 'NA'}</p>
                                </Col>
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">SDMS/Training Center ID</label>
                                    <p className="title">{allcenters[this.state.CardValue].sdms_trainingcenter_id ? allcenters[this.state.CardValue].sdms_trainingcenter_id : 'NA'}</p>
                                </Col>
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">Training Center Address</label>
                                    <p className="title">{allcenters[this.state.CardValue].address ? allcenters[this.state.CardValue].address : 'NA'}</p>
                                </Col>
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">State</label>
                                    <p className="title">{allcenters[this.state.CardValue].stateName ? allcenters[this.state.CardValue].stateName : 'NA'}</p>
                                </Col>
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">City</label>
                                    <p className="title">{allcenters[this.state.CardValue].cityName ? allcenters[this.state.CardValue].cityName : 'NA'}</p>
                                </Col>
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">Pincode</label>
                                    <p className="title">{allcenters[this.state.CardValue].pincode ? allcenters[this.state.CardValue].pincode : 'NA'}</p>
                                </Col>
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">Courses/Job Role offered</label>
                                    <p className="title">{allcenters[this.state.CardValue].jobroleName ? allcenters[this.state.CardValue].jobroleName : 'NA'}</p>
                                </Col>
                            </Row>
                            <hr className="modal-data-seperator" />
                            <Row>

                                <Col xs="12">
                                    <h5 className="title modal-view-title">CONTACT PERSON DETAILS</h5>
                                </Col>
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">Name</label>
                                    <p className="title">{allcenters[this.state.CardValue].contact_person_name ? allcenters[this.state.CardValue].contact_person_name : 'NA'}</p>
                                </Col>
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">Email Address</label>
                                    <p className="title">
                                        {mailTo === '' ? 'NA' :
                                            <a href={mailTo} title={allcenters[this.state.CardValue].contact_email_address}>{allcenters[this.state.CardValue].contact_email_address}</a>
                                        }</p>
                                </Col>
                                <Col xs="12" md="6" xl="4">
                                    <label className="subtitle">Mobile Number</label>
                                    <p className="title">
                                        {tel === '' ? 'NA' :
                                            <a href={tel} title={allcenters[this.state.CardValue].contact_mobile}>{allcenters[this.state.CardValue].contact_mobile}</a>
                                        }</p>
                                </Col>

                            </Row>
                        </ModalBody>
                        <ModalFooter>
                            <Button onClick={() => this.toggleModal(mymodaval)}>Close</Button>
                        </ModalFooter>
                    </Modal>
                );
            }
            //Card Details
            allcenters.map((value, index) => {
                var tel = allcenters[index].contact_mobile ? "tel:" + allcenters[index].contact_mobile : ''
                var mailTo = allcenters[index].contact_email_address ? "mailto:" + allcenters[index].contact_email_address : ''
                return final_allCandiates.push(
                    <Col xs="12" md="6" xl="4" key={index}>
                        <Card className="card z-depth-1">
                            <CardHeader className="d-flex justify-content-between bg-transparent align-items-center border-bottom-0 card-header-shadow">
                                <div className="section-title">
                                    <CardTitle className="title card-title">{allcenters[index].trainingcenter_name}</CardTitle>
                                    <CardSubtitle className="subtitle">{allcenters[index].cityName}</CardSubtitle>
                                </div>
                                <CardLink title="Delete" onClick={() => this.toggleModalforDelete(allcenters[index].trainingcenterid,allcenters[index].trainingcenter_name)}  className="btn-fab btn-fab-danger btn"><i className="material-icons">delete_outline</i></CardLink>
                            </CardHeader>
                            <ListGroup className="no-border">
                                <ListGroupItem>
                                    <label className="subtitle">SDMS/Training Center ID</label>
                                    <p className="title mb-0">{allcenters[index].sdms_trainingcenter_id}</p>
                                </ListGroupItem>
                                <ListGroupItem>
                                    <label className="subtitle">Address</label>
                                    <p className="title mb-0">{allcenters[index].address}</p>
                                </ListGroupItem>

                                <ListGroupItem>
                                    <label className="subtitle">Contact Person</label>
                                    <p className="title mb-0">{allcenters[index].contact_person_name}</p>
                                </ListGroupItem>
                                <ListGroupItem>
                                    <label className="subtitle">Mobile</label>
                                    <p className="title mb-0">
                                        {tel === '' ? 'NA' :
                                            <a href={tel} title={allcenters[index].contact_mobile}>{allcenters[index].contact_mobile}</a>
                                        }
                                    </p>
                                </ListGroupItem>

                                <ListGroupItem>
                                    <label className="subtitle">Email Address</label>
                                    <p className="">
                                        {mailTo === '' ? 'NA' :
                                            <a href={mailTo} title={allcenters[index].contact_email_address}>{allcenters[index].contact_email_address}</a>
                                        }
                                    </p>
                                </ListGroupItem>

                                <ListGroupItem className="d-flex">
                                    <CardLink title="View More" key={index} onClick={() => this.toggleModal(index)} className="btn btn-primary btn-raised mr-3" >View More</CardLink>
                                    <Link title="Edit" to={`/add-edit-center/#id:${allcenters[index].trainingcenterid}`} className="btn btn-outline">Edit</Link>
                                    {/* <Link title="Edit" to={`/edit-center/#id:${allcenters[index].trainingcenterid}`} className="btn btn-outline">Edit</Link> */}
                                </ListGroupItem>
                            </ListGroup>
                        </Card>
                    </Col>

                );

            })
        }

        return (<div>
            {(recordFound === 1 || this.state.fromsearch === 1) &&
                <TrainingCenterSearch onSelectedChange={this.handleChild} />
            }
            <div className={loaderClass} style={styles}></div>
            {recordFound === 1 && (<h3 className="page-title mb-3">
                {!!this.props.allcenters ? " Total Number Of Centers are : " + this.props.allcenters.paginationOptions.totalcount.toLocaleString('en-IN') : ''} 
                {(!!this.props.searchcenters && !!this.props.searchcenters.paginationOptions) ? this.props.searchcenters.paginationOptions.totalcount.toLocaleString('en-IN') + ' Center(s) Found ' : ''}  
            </h3>)}
            {recordFound === 1 &&
                <Row className="details-view">
                    {final_allCandiates}
                </Row>
            }
            {recordFound === 0 &&
                <div className="d-flex justify-content-center align-items-center error-page">
                    <div className="text-center">
                        <div className="img-wrapper mb-4">
                            <div className="circle-xl user-avatar">
                                <img src={trainingCentreSVG} alt="Center" />
                            </div>
                        </div>
                        {this.state.fromsearch === 0 ?
                            <h1 className="h2 fw-regular mb-0">No Center(s) Added Yet!</h1>
                            :
                            <h1 className="h2 fw-regular mb-0">No Center(s) Found!</h1>
                        }
                    </div>
                </div>
            }
            {allcentersModalDetails}
            {deleteTrainingCenterModal}
            {loadMore}
        </div>
        );
    }
}
function mapStateToProps(state) {

    const { allcenters, searchcenters, loading } = state.trainingpartners; //From Reducer - trainingpartners function ...
    return {
        allcenters, searchcenters, loading
    };
}
const connectedGetCenters = connect(mapStateToProps)(GetCenters);
export { connectedGetCenters as GetCenters };