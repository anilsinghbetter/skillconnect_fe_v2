import React from 'react';
import { connect } from 'react-redux';
import { userActions } from '../_actions';
import { reduxForm, Field } from 'redux-form';
import { Header } from '../_components/Header';
import { Footer } from '../_components/Footer';
import { history } from '../_helpers';
import { Sidebar } from '../_components/Sidebar';
import { FormGroup, Breadcrumb, BreadcrumbItem } from 'reactstrap';
// import trainingCentreSVG from '../assets/images/training_center_grey.svg';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import 'react-day-picker/lib/style.css';
import { submitFailure } from '../../src/utils/submitFailure';
import { constants } from '../constants';
import { userService } from '../_services';
import { post } from 'axios';
import { alertActions } from '../_actions';
import { config } from '../config';
import {formatDate,parseDate,} from 'react-day-picker/moment';
import Select from 'react-select';

const currentYear = new Date().getFullYear();
const currentMonth = new Date().getMonth();
const fromMonth = new Date(currentYear - 40, currentMonth);
const toMonth = new Date(currentYear, currentMonth);
var regpan = /^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/;
// var regMobile = /^([0-9]){10}?$/;
// var regDrivingLicense = /^([a-zA-Z]){2}([0-9]){13}?$/;

const afterLoginHeaders = {
    "Content-Type": "application/json",
    "api-key": config.APIKEY,
    "UDID": userService.getUDID(),
    "device-type": userService.getDeviceType(),
    "Authorization": userService.getAccessTokenFromLocalStorage()
};

function YearMonthForm({ date, localeUtils, onChange }) {
    const months = localeUtils.getMonths();

    const years = [];
    for (let i = fromMonth.getFullYear(); i <= toMonth.getFullYear(); i += 1) {
        years.push(i);
    }

    const handleChange = function handleChange(e) {
        const { year, month } = e.target.form;
        onChange(new Date(year.value, month.value));
    };

    return (
        <div className="DayPicker-Caption">
            <select name="month" onChange={handleChange} value={date.getMonth()}>
                {months.map((month, i) => (
                    <option key={month} value={i}>
                        {month}
                    </option>
                ))}
            </select>
            <select name="year" onChange={handleChange} value={date.getFullYear()}>
                {years.map(year => (
                    <option key={year} value={year}>
                        {year}
                    </option>
                ))}
            </select>
        </div>
    );
}

const renderTextAreaField = ({ input, label, type, sameValue, disabled,  placeholder, textarea, meta: { touched, error, warning, invalid } }) => {
    // console.log(error, "Meta");
    // console.log(sameValue, "value of the passed state");
    // (sameValue && sameValue !== '') ? input = (input) => {  } : input.value = input.value;
    (sameValue && sameValue !== '') ? input.value = sameValue : input.value = input.value;
    const textareaType = <textarea {...input} disabled={disabled}  placeholder={label} type={type} className={`form-control ${touched && invalid ? 'has-danger' : ''}`} />;

    return <div className=''>
        <div className={'form-group' + (error && touched ? ' has-error' : '')}>
            <label>{label}</label>
            <div> {textarea ? textareaType : ""}
                {touched && ((error && <div className="help-block">{error}</div>) || (warning && <span>{warning}</span>))}
            </div>

        </div>
    </div>
};
const renderField = ({ input, label, type, sameValue, disabled, isChanged, placeholder, textarea, meta: { touched, error, warning, invalid } }) => {
    (sameValue) ? input.value = sameValue : input.value = input.value;
    const textareaType = <textarea {...input} placeholder={label} type={type} className={`form-control ${touched && invalid ? 'has-danger' : ''}`} />;
    return <div className={((label === "SDMS/NSDC Enrollment No" || label === "Attendance Percent" || label === "Pincode" || label === "Experience In Years" || label === "Assessment Score" || label === "Max Assessment Score" || label === "Batch ID" || label === "First Name" || label === "Middle Name" || label === "Last Name") ? 'col-md-4 col-12' : 'col-md-6 col-12')}>
        <div className={'form-group' + (error && touched ? ' has-error' : '')}>
            <label>{label}</label>
            <div> {textarea ? textareaType : <input {...input} disabled={disabled} type={type} placeholder={placeholder} className="form-control" />}
                {touched && ((error && <div className="help-block">{error}</div>) || (warning && <span>{warning}</span>))}
            </div>

        </div>
    </div>
};
const renderError = ({ meta: { touched, error } }) =>

    touched && error ? <div className="help-block">{error}</div> : false

const validate = (values, myst, myst2) => {
    const errors = {}

    if (!values.name) {
        errors.name = 'Please enter name of candidate'
    } /*else if (values.username.length > 15) {
                errors.username = 'Must be 15 characters or less'
                }  */
    if (values.aadhar_card_no && values.aadhar_card_no.length !== 12) {
            errors.aadhar_card_no = 'Aadhaar Card Number should be 12 digits';
    } 
    if (values.alternate_id_type && !values.altername_id_no) {
        errors.altername_id_no = 'Please enter Alternate ID No.';
    }
    if (!values.alternate_id_type && values.altername_id_no) {
        errors.alternate_id_type = 'Please select Alternate ID type';
    }
    if (!!values.alternate_id_type && !!values.altername_id_no) {
        if( values.alternate_id_type === 'pan'){
            if(!regpan.test(values.altername_id_no)){
                errors.altername_id_no = 'Please enter valid PAN number';
             }
        }
        // errors.altername_id_no = 'Please enter Alternate ID No.';
    } 
    /* 
    if (!values.candidate_id) {
        errors.candidate_id = 'Please enter candidate id'
    } 
    if (!values.contact_email_address) {
        errors.contact_email_address = 'Please enter an Email Address'
    }*/
    /* if (!values.age) {
        errors.age = 'Please enter age'
    }
    if (values.age && values.age > 100) {
        errors.age = 'Please enter valid age'
    } */
    if (!values.phone) {
        errors.phone = 'Please enter mobile number'
    }
    if (values.phone && values.phone.length !== 10) {
        errors.phone = 'Please enter 10 digits mobile number'
    }
    // console.log(regMobile.test(values.phone), "see");
    // if (!values.pincode) {
    //     errors.pincode = 'Please enter Pincode'
    // }
    if (!values.permanent_pincode) {
        errors.permanent_pincode = 'Please enter Pincode'
        // if (!values.permanent_statename) {
        //     errors.permanent_statename = 'Please select a state'
        // }
        // if (!values.permanent_districtname) {
        //     errors.permanent_districtname = 'Please select a district'
        // }
    }
    // else if(!values.is_permanent_from_pincode){
        if (!values.permanent_statename) {
            errors.permanent_statename = 'Please select a state'
        }
        if (!values.permanent_districtname) {
            errors.permanent_districtname = 'Please select a district'
        }
    // }
    // if (values.permanent_pincode && values.permanent_pincode.length !== 6) {
    //     errors.permanent_pincode = 'Please enter valid Pincode'
    // }
   
    if (!values.sex) {
        errors.sex = 'Please select gender'
    }
    /*  if (!values.is_willing_to_relocate) {
         errors.is_willing_to_relocate = 'Please select willing to relocate'
     } */
    if (!values.permanent_address) {
        errors.permanent_address = 'Please enter Permanent Address'
    }
   
    if (!values.nsdc_enrollment_no) {
        errors.nsdc_enrollment_no = 'Please enter SDMS/NSDC Enrollment No'
    }
    // if (!values.statename) {
    //     errors.statename = 'Please select a state'
    // }
    // if (!values.districtname) {
    //     errors.districtname = 'Please select a district'
    // }
    
    if(!values.same_as_permanent){
        if (!values.present_address) {
            errors.present_address = 'Please enter Present Address'
        }
        if (!values.present_statename) {
            errors.present_statename = 'Please select a state'
        }
        if (!values.present_districtname) {
            errors.present_districtname = 'Please select a district'
        }
        if (!values.present_pincode) {
            errors.present_pincode = 'Please enter Pincode'
        }
    }
    if (!values.training_type) {
        errors.training_type = "Please select a training type";
    }
    if (!values.training_status) {
        errors.training_status = "Please select training status";
    }
    /* if (!values.placement_status) {
        errors.placement_status = "Please select placement status";
    }
    if (!values.employment_type) {
        errors.employment_type = "Please select employment status";
    } */
    if (!values.sectorname) {
        errors.sectorname = "Please select sector name";
    }
    // if (!values.jobrole) {
    //     errors.jobrole = "Please select job role";
    // }
    if (!values.batch_id) {
        errors.batch_id = "Please select batch Id";
    }
    if (!values.schemename) {
        errors.schemename = "Please select a scheme";
    }
    if (values.schemename === '5') {
        if (!values.state_scheme_name) {
            errors.state_scheme_name = "Please select state scheme";
        }
    }
    // if (!values.skills) {
    //     errors.skills = "Please select Skill Information";
    // }
    if (!values.firstName) {
        errors.firstName = "Please enter first name of candidate";
    }
    // if (!values.middleName) {
    //     errors.middleName = "Please enter middle name of candidate";
    // }
    // if (!values.lastName) {
    //     errors.lastName = "Please enter last name of candidate";
    // }
    if (!values.training_center) {
        errors.training_center = "Please select Training Center";
    }
    if (!values.placement_status) {
        errors.placement_status = "Please select placement status";
    }
    if(values.placement_status && values.placement_status === 'selected'){
        if(!values.employer_name){
            errors.employer_name = "Please select employer name";
        }
    }
    
    return errors;
}

class AddEditCandidate extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            submitted: false,
            combovalue: '',
            startDate: "",
            selectedDOB: 0,
            is_dob_changed: false,
            selectedDay: undefined,
            selectedAssessmentDate: undefined,
            selectedBatchStartDate: undefined,
            selectedBatchEndDate: undefined,
            selectedPassOutDate: undefined,
            show_pass_out_date: false,
            showState: false,
            female: "",
            male: "",
            yes: "",
            no: "",
            registerClick: 0,
            error_flag: 0,
            error_jobrole: 0,
            error_employer: 0,
            error_dist_flag: 0,
            candidate_id: "",
            onchangedistrict: "",
            candidate_details_edit: "",
            isCMState: 0,
            hideState: "",
            showStates: 0,
            month: fromMonth,
            mode: '',
            address_check: 0,
            edu_details : [],
            show_employer_selection: false,
            show_appointment_letter_upload: false,
            initial_edu_details: [],
            training_status_new_flag: 0,
            placement_status_new_flag: 0,
            filedata: null,
            fileresponse: "",
            edu_details_edited: false,
            error_flag_permanent:0,
            error_flag_present:0,
            // skills_for_form: undefined,
            skill_first_time: true,
            initial_salary_first_month: false,
            initial_salary_second_month: false,
            initial_salary_third_month: false,
            initial_appointment_letter: false,
            salary_initial_value_set: false,
            delFile: false,
            sameAddress:false,
            addressAvailable:false,
            permanent_statename_state: '',
            permanent_districtname_state: '',
            present_address_state: '',
            present_statename_state: '',
            present_districtname_state: '',
            present_pincode_state: '',
            enableSameAs: false,
            same_as_permanent_checked: false,
            PreferredCityValue: '',
            displayPrefferedCity: false,
            isMulti:true,
            is_preferred_city_set: false,
            is_first_load: true,
            isLoading:false,
            is_permanent_from_pincode: false,
            is_present_from_pincode: false,
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        //Date Related Pure Functions..
        this.handleDayClick = this.handleDayClick.bind(this);
        this.handleAssessmentClick = this.handleAssessmentClick.bind(this);
        this.handleBatchStartDateClick = this.handleBatchStartDateClick.bind(this);
        this.handleBatchEndDateClick = this.handleBatchEndDateClick.bind(this);
        this.handlePassOutDateClick = this.handlePassOutDateClick.bind(this);
        this.checkValidation = this.checkValidation.bind(this);
        this.handleYearMonthChange = this.handleYearMonthChange.bind(this);
        // this.handleCheckboxClick = this.handleCheckboxClick.bind(this);
        this.handleEduClick = this.handleEduClick.bind(this);
        this.handleFileUpload = this.handleFileUpload.bind(this);
        this.handleChangeUploadButtonFirst = this.handleChangeUploadButtonFirst.bind(this);
        this.handleChangeUploadButtonSecond = this.handleChangeUploadButtonSecond.bind(this);
        this.handleChangeUploadButtonThird = this.handleChangeUploadButtonThird.bind(this);
        this.handleChangeUploadAppointmentLetter = this.handleChangeUploadAppointmentLetter.bind(this);
        this.handleSameAsPermanent = this.handleSameAsPermanent.bind(this);
        this.handleSelectChange = this.handleSelectChange.bind(this);
        this.getStateCityFromPincode = this.getStateCityFromPincode.bind(this);
        this.handleSkillChange = this.handleSkillChange.bind(this);
    }
    handleYearMonthChange(month) {
        this.setState({ month });
    }
    componentDidMount() {
        // const { dispatch, candidate_updated_details } = this.props;
        // console.log("Edit Id",this.props.location.hash);
        // console.log("PROPS",this.props);
        // console.log("STATE",this.state);
        let candidate_idvalue = this.props.location.hash.split(":");
        if (candidate_idvalue[0] === '#id' && candidate_idvalue[1] !== "") {
            this.setState({
                "candidate_id": candidate_idvalue[1],
                mode: 'Edit'
            });
        } else {
            this.setState({
                mode: 'Add'
            })
        }
        this.props.dispatch(userActions.getCandidateFormData(this.props.user.data[0].trainingpartnerid));
        this.props.dispatch(userActions.getAllPrefferedCity('all'));

    }
    
    handleEduClick(event){
        var { edu_details } = this.state;
        var order = ["primary", "secondary", "higherSecondary", "grad", "postGrad"];
        var indexOfCheck;
        // if(edu_details.length === 0 && initial_edu_details.length > 0){
        //     edu_details = initial_edu_details;
        // }
        // console.log(indexOfCheck," indexOfCheck");
        edu_details = [];
        
        indexOfCheck = order.indexOf(event.target.name);

        for(let i = 0; i < indexOfCheck ; i++){
                edu_details.push(order[i]);
        }
        edu_details.push(event.target.name);
        if(!(event.target.checked)){
            let index = edu_details.indexOf(event.target.name);
            if(index > -1){
                edu_details.splice(index, 1);
            }
        }
        
        /* else{
            
            edu_details.splice(indexOfCheck, edu_details.length+1);

        } */
        // this.state.initial_edu_details.splice(0,this.state.initial_edu_details.length);
        
        this.setState({
            edu_details,
            edu_details_edited: true
        });
    }
    handleRedirectionToAddMultipleCandidate(e) {
        history.push('/add-multiple-candidates');
    }
    handleRedirectionToAddCenter(e) {
        history.push('/add-edit-center');
    }
    fileUpload(salary_first, salary_second, salary_third, appointment_letter,candidate_id) {
        // console.log(this.state.mode,"MODE");
        // console.log(this.props.initialValues,"FORM");
        // var sdms_id ;
        // if(this.state.mode === 'Edit'){
        //     if(!!this.state.user){
        //         if(this.state.user.nsdc_enrollment_no){
        //             sdms_id = this.state.user.nsdc_enrollment_no;                
        //         }
        //     }else{
        //         sdms_id = this.props.initialValues.nsdc_enrollment_no;
        //     }
        // }else{
        //     sdms_id = this.state.user.nsdc_enrollment_no;
        // }
        // console.log(file_to_delete,"file_to_delete");
        // console.log(file_to_delete.salary_first,"file_to_delete.salary_first");
        var response = userService.uploadCandidateSalarySlipsData(salary_first, salary_second, salary_third, appointment_letter, candidate_id);
        return post(response.url, response.data, response.headers);
    }
    handleFileUpload = (event) => {
        const { name, files } = event.target;
        this.setState({
            [name]: files[0]
        });
    }
    
    async handleSubmit(event) {
        // event.preventDefault();

        const { user, edu_details } = this.state;
        this.setState({ submitted: true });

        var candidateid;
        if (this.state.mode === 'Edit') {
            candidateid = this.state.candidate_id;
        } else {
            candidateid = '-1';
        }
        // console.log(this.state,"this.state");
        // var candidateid = '-1';
        
        var trainingpartnerid = this.props.user.data[0].trainingpartnerid;
        var trainingcenterid = this.props.user.data[0].trainingcenterid;
        var userFinal;
        var dob,assessment_date,batch_start_date,batch_end_date,pass_out_date;

        if (this.state.mode === 'Add') {
            if (this.state.selectedDay !== "" && this.state.selectedDay !== undefined) dob = this.state.selectedDay;
            else { this.props.dispatch(alertActions.error("Please enter date of birth")); return; }
            userFinal = { dob };
            if (this.state.selectedAssessmentDate !== "" && this.state.selectedAssessmentDate !== undefined) assessment_date = this.state.selectedAssessmentDate;userFinal = { ...userFinal, assessment_date };
            if (this.state.selectedPassOutDate !== "" && this.state.selectedPassOutDate !== undefined) pass_out_date = this.state.selectedPassOutDate;userFinal = { ...userFinal, pass_out_date };
            if (this.state.selectedBatchStartDate !== "" && this.state.selectedBatchStartDate !== undefined) batch_start_date = this.state.selectedBatchStartDate;userFinal = { ...userFinal, batch_start_date };
            if (this.state.selectedBatchEndDate !== "" && this.state.selectedBatchEndDate !== undefined) batch_end_date = this.state.selectedBatchEndDate;userFinal = { ...userFinal, batch_end_date };
        } else if(this.state.mode === 'Edit'){
            if (this.state.selectedDay !== "" && this.state.selectedDay !== undefined) {
                dob = this.state.selectedDay;
                userFinal = { dob };
            }
            if(this.state.selectedDOB === 1){
                this.props.dispatch(alertActions.error("Please enter date of birth")); 
                return;
                // let dob = '';
                // userFinal = { dob };
            }
            if (this.state.selectedAssessmentDate !== "" && this.state.selectedAssessmentDate !== undefined) {
                assessment_date = this.state.selectedAssessmentDate;
                userFinal = { ...userFinal, assessment_date };
            }
            if (this.state.selectedPassOutDate !== "" && this.state.selectedPassOutDate !== undefined) {
                pass_out_date = this.state.selectedPassOutDate;
                userFinal = { ...userFinal, pass_out_date };
            }
            // else if(this.state.selectedPassOutDate === "" || this.state.selectedPassOutDate === undefined){
            //     return;
            // }
            if (this.state.selectedBatchStartDate !== "" && this.state.selectedBatchStartDate !== undefined) {
                batch_start_date = this.state.selectedBatchStartDate;
                userFinal = { ...userFinal, batch_start_date };
            }
            if (this.state.selectedBatchEndDate !== "" && this.state.selectedBatchEndDate !== undefined) {
                batch_end_date = this.state.selectedBatchEndDate;
                userFinal = { ...userFinal, batch_end_date };
            }
        }
        if(edu_details.length === 0 || this.state.edu_details_edited){
            userFinal = { ...userFinal, edu_details };
        }
        // console.log(this.state.PreferredCityValue,"this.state.PreferredCityValue");
        try{
            if ((user && user.is_willing_to_relocate === 'yes') || this.state.PreferredCityValue){
                // console.log("IN");
                if(!this.state.PreferredCityValue){
                    // console.log("return");
                    this.props.dispatch(alertActions.error("Please select prefered city"));
                    return;
                }else{
                    var preferredCityValue = this.state.PreferredCityValue;
                    userFinal = { ...userFinal, preferredCityValue };
                }
    
            }

        }catch(e){
            console.log(e, "E");
        }
        
        // console.log(userFinal,"user_Fnal");
        // return;
        // if(salary_first){
        //     userFinal = { ...userFinal, salary_first };
        // }
        
        // this.setState({ fileresponse : '' });
        // if(!this.state.salary_first || !this.state.salary_second || !this.state.salary_third){
        //     // console.log("here");
        //     this.setState({
        //         submitted:false
        //     });
        //     // this.props.dispatch(alertActions.error("Please attach all required files"));
        //     // // alert("Please attach all required files");
        //     // return;
        // }else{
    
            // console.log(response_upload,"==RESPONSE==");
        // }
        var userFinalOne = { trainingpartnerid, trainingcenterid, candidateid, ...userFinal, ...user };
        
        if(userFinalOne.placement_status){
            if(this.props.initialValues.placement_status !== "joined"){
                if(userFinalOne.placement_status === 'joined' && !this.state.appointment_letter){
                    this.props.dispatch(alertActions.error("Please attach appointment letter"));
                    // console.log("returned because no appt letter attched");
                    return;
                }
            }
        }
        if(userFinalOne.training_status){
            // console.log(userFinalOne.pass_out_date,"!userFinalOne.pass_out_date");
            if(userFinalOne.training_status === 'ongoing' && !userFinalOne.pass_out_date){
                this.props.dispatch(alertActions.error("Please enter Pass-Out Date")); 
                return;
            }
        }
        /*either age and dob is changed in form or 
        dob is changed and age is already present in form...
        */
        // if((userFinalOne.age && userFinalOne.dob) || (this.state.mode === 'Edit' && userFinalOne.dob && this.props.candidate_updated_details.data[0].age && !userFinalOne.age) ){
        //     let calculatedAgeFromDOB = this.getAge(userFinalOne.dob);
        //     var ageEntered;
        //     // console.log(userFinalOne.age, "userFinal.age");
        //     userFinalOne.age ? ageEntered = userFinalOne.age : ageEntered = this.props.candidate_updated_details.data[0].age;
        //     // console.log(ageEntered, "ageEntered");
        //     // console.log(calculatedAgeFromDOB, "calculatedAgeFromDOB");
        //     if(!isNaN(calculatedAgeFromDOB) && calculatedAgeFromDOB != ageEntered){
        //         // console.log("#here...");
        //         this.props.dispatch(alertActions.error("DOB and Age does not match"));
        //         return;
        //     }else{
        //         // console.log("DOB and Age matches...");
        //     }
            
        // }
        //or age is changed and dob is already present in form..
        if(this.state.mode === 'Edit' && userFinalOne.age && !userFinalOne.dob && this.props.candidate_updated_details.data[0].dob ){
            let existingDOB = new Date(this.props.candidate_updated_details.data[0].dob);
            let ageEntered = userFinalOne.age;
            let calculatedAgeFromDOB = this.getAge(existingDOB);
            if(!isNaN(calculatedAgeFromDOB) && calculatedAgeFromDOB !== ageEntered){
                this.props.dispatch(alertActions.error("DOB and Age does not match"));
                // console.log("#here...2");
                return;
            }else{
                // console.log("DOB and Age matches...");
            }

        }
        // console.log(this.state.same_as_permanent_checked," same_as_permanent_checked ");
        if(this.state.same_as_permanent_checked === false && this.state.enableSameAs){
            userFinalOne = {...userFinalOne, same_as_permanent: 'false'}
        }
        // console.log(userFinalOne," userFinalOne ");
        // return;
        //New Mothod for getting everything in sync
        const addUpdateCandidatesAPI = config.API_HOST_URL + 'api/v1/candidate/addUpdateCandidate/web';

        // async function addUpdateCandidates(userFinalOne) {
            const requestOptions = {
                method: 'POST',
                headers: afterLoginHeaders,
                body: JSON.stringify(userFinalOne),
            };
            var response = "";
            // console.log("BEFORE", addUpdateCandidatesAPI);

            response = await fetch(addUpdateCandidatesAPI,requestOptions).then(userService.handleResponse);
            // console.log(response, "response ...addupdate");
            if(response.status === false){
                this.props.dispatch(alertActions.error(response.error.message));
                return;
            }
            // this.props.dispatch(userActions.addUpdateCandidates(response));
            var updatedID = '';
            if(this.state.mode === "Add"){
                updatedID = response.data.updatedID;
            }else{
                updatedID = userFinalOne.candidateid;
            }
            // console.log("AFTER", response);
            // console.log("AFTER.data", response.data, updatedID , "updatedID");
            // return response;
        // }
        // return;
        // New Method finishes...
        
        var response_upload = await this.fileUpload(this.state.salary_first, this.state.salary_second, this.state.salary_third, this.state.appointment_letter, updatedID);
        // console.log(response_upload,"response_upload");
        if(response_upload.data.status === true && response.status === true){
            userFinalOne = { ...userFinalOne, ...response_upload.data.data };
            this.props.dispatch(userActions.addUpdateCandidates(response));
        }else{
            // console.error("response false");
            this.props.dispatch(alertActions.error(response_upload.data.error.message));
            return;
        }
        // if (userFinal.aadhar_card_no && userFinal.name && userFinal.dob) {
        
        
        
        // console.log(userFinalOne,"user_Fnal");
        // return;
        // this.props.dispatch(userActions.addUpdateCandidates(userFinalOne));
        // }
    }
    
    checkValidation(event) {
        //console.log("Register clicked.");
        this.setState({ registerClick: 1 });
    }
    truncateFileName(nameToTrunc){
        if(nameToTrunc.length < 21){
            return nameToTrunc;
        }else{
            return nameToTrunc.slice(0,21) + '...';
        }
    }
    getAge(dateString) {
        var today = new Date();
        var birthDate = new Date(dateString);
        var age = today.getFullYear() - birthDate.getFullYear();
        var m = today.getMonth() - birthDate.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
            age--;
        }
        return age;
    }
    componentDidUpdate(){
        // if(!!this.props && this.props.initialValues && this.props.initialValues.hasOwnProperty('skills') && this.state.skill_first_time){
        //     // console.log(this.props.initialValues," USER IN UPDATE");
        //     this.setState({
        //         skills_for_form: this.props.initialValues.skills,
        //         skill_first_time: false
        //     });
        // }
        // if(this.state.user && this.state.user.skills && this.state.skills_for_form !== this.state.user.skills){
        //     this.setState({
        //         skills_for_form: this.state.user.skills
        //     });
        // }
        if(!this.state.salary_initial_value_set){
            if(!this.state.initial_salary_first_month && this.props.initialValues.salary_first ){
                this.setState({
                    initial_salary_first_month: true,
                    salary_initial_value_set: true
                });
            }
            if(!this.state.initial_salary_second_month && this.props.initialValues.salary_second ){
                this.setState({
                    initial_salary_second_month: true,
                    salary_initial_value_set: true
                });
            }
            if(!this.state.initial_salary_third_month && this.props.initialValues.salary_third ){
                this.setState({
                    initial_salary_third_month: true,
                    salary_initial_value_set: true
                });
            }
            if(!this.state.initial_appointment_letter && this.props.initialValues.appointment_letter ){
                this.setState({
                    initial_appointment_letter: true,
                    salary_initial_value_set: true
                });
            }
        }
    }
    
    
    handleChangeUploadButtonFirst(e) {
        // console.log(e.target.id,"HERE WE ARE");
        let confirmation = window.confirm("Are you sure you want to delete File: \n"+ this.props.initialValues.salary_first );
        if(confirmation){
            if(this.state.initial_salary_first_month){
                this.props.dispatch(userActions.deleteFile(this.state.candidate_id,e.target.id));
                this.setState({
                    initial_salary_first_month: false,
                });
            }
        }
        
    }
    async handleChangeUploadButtonSecond(e){
        let confirmation = window.confirm("Are you sure you want to delete File: \n"+ this.props.initialValues.salary_second );
        if(confirmation){
            if(this.state.initial_salary_second_month){
                this.props.dispatch(userActions.deleteFile(this.state.candidate_id,e.target.id));
                this.setState({
                    initial_salary_second_month: false,
                });
            }
        }
    }
    async handleChangeUploadButtonThird(e){
        let confirmation = window.confirm("Are you sure you want to delete File: \n"+ this.props.initialValues.salary_third );
        if(confirmation){
            this.props.dispatch(userActions.deleteFile(this.state.candidate_id,e.target.id));
            if(this.state.initial_salary_third_month){
                this.setState({
                    initial_salary_third_month: false,
                });
            }
        }
    }
    async handleChangeUploadAppointmentLetter(e){
        let confirmation = window.confirm("Are you sure you want to delete File: \n"+ this.props.initialValues.appointment_letter );
        if(confirmation){
            this.props.dispatch(userActions.deleteFile(this.state.candidate_id,e.target.id));
            if(this.state.initial_appointment_letter){
                this.setState({
                    initial_appointment_letter: false,
                });
            }
        }
    }
   
    handleSelectChange(value) {
        // console.log('You\'ve selected:', value);
        value = value.toString();
        if(value.indexOf('all') > -1){
            this.setState({ 
                PreferredCityValue: 'all',
                isMulti:false
            }); 
        }else{
            this.setState({ 
                PreferredCityValue: value,
                isMulti:true
            }); 
        }
     
    }
     
    async handleChange(event) {

        const { name, value } = event.target;
        const { user } = this.state;
        const re = /^[0-9\b]+$/;
        if (name === 'phone' && value !== '') {
            if(!re.test(value) ){
                event.preventDefault();
            }
        }
        if ((name === 'permanent_pincode' || name === 'present_pincode') && value.length > 6) {
            event.preventDefault();
        }

        if(name === 'is_willing_to_relocate' && value === 'yes'){
            this.setState({
                displayPrefferedCity: true
            });
            
        }else if(name === 'is_willing_to_relocate' && value === 'no'){
            this.setState({
                displayPrefferedCity: false,
                is_first_load: false
            });
        }
        
        // console.log(this.props.user.data[0].trainingpartnerid);
        let tcid = this.props.user.data[0].trainingcenterid;
        const permanent_properties = ["permanent_address","permanent_statename","permanent_districtname","permanent_pincode"];
        const present_properties = ["present_address","present_statename","present_districtname","present_pincode"];
        // const { dispatch } = this.props;
        // this.setState({ value: event.target.value });
        // console.log(this.state,"state");
        // console.log(this.props,"props");

        // permanent_pincode
        var response;
        if(event.target.name === "permanent_pincode" && event.target.value.length === 6 /* && this.state.mode === 'Add' */){
            this.setState({isLoading: true});
            response = await this.getStateCityFromPincode(value);
            // console.log(response, "THIS IS RESPONSE OF PERMANENT");
            if(response.status === true){
                if(user){
                    delete this.state.user.present_statename;
                    delete this.state.user.present_districtname;
                }
                this.setState({
                    isLoading: false,
                    error_flag_permanent: 0,
                    error_flag_permanent_state: 0,
                    sameAddress:false,
                    present_statename_state: null,
                    present_districtname_state: null,
                    user:{
                        ...user,
                        "same_as_permanent": false
                    },
                    //till here setting state to remove value from present state city
                    permanent_statename_state:response.data[0].stateID,
                    permanent_districtname_state:response.data[0].cityID,
                    is_permanent_from_pincode:true,
                },() => {
                    this.props.change("permanent_statename", response.data[0].stateID);
                    this.props.change("permanent_districtname", response.data[0].cityID);    
                });
            }
            else{
                this.setState({
                    isLoading: false,
                    sameAddress:false,
                    present_statename_state: null,
                    present_districtname_state: null,
                    user:{
                        ...user,
                        "same_as_permanent": false
                    },
                    //till here setting state to remove value from present state city
                    permanent_statename_state:'',
                    permanent_districtname_state:'',
                    error_flag_permanent: 1,
                    error_flag_permanent_state: 1,
                    is_permanent_from_pincode:false,
                },() => {
                    this.props.change("permanent_statename", '');
                    this.props.change("permanent_districtname", '');
                });
            }
        }
        if(event.target.name === "present_pincode" && event.target.value.length === 6 /* && this.state.mode === 'Add' */){
            this.setState({isLoading: true});
            response = await this.getStateCityFromPincode(value);
            // console.log(response, "THIS IS RESPONSE OF PRESENT");
            if(response.status === true){
                this.setState({
                    isLoading: false,
                    present_statename_state:response.data[0].stateID,
                    present_districtname_state:response.data[0].cityID,
                    error_flag_present: 0,
                    error_flag_present_state: 0,
                },() => {
                    this.props.change("present_statename", response.data[0].stateID);
                    this.props.change("present_districtname", response.data[0].cityID);
                });
            }else{
                this.setState({
                    isLoading: false,
                    present_statename_state:'',
                    present_districtname_state:'',
                    error_flag_present: 1,
                    error_flag_present_state: 1,
                },() => {
                    this.props.change("present_statename", '');
                    this.props.change("present_districtname", '');
                });
            }
        }
        if(event.target.name === "permanent_statename" || event.target.name === "permanent_districtname"){
            this.setState({
                permanent_statename_state:'',
                permanent_districtname_state:'',
                
            });
        }
        if(event.target.name === "present_statename" || event.target.name === "present_districtname"){
            this.setState({
                present_statename_state:'',
                present_districtname_state:'',
                
            });
        }

        this.setState({
            user: {
                ...user,
                [name]: value
            }
        }, () => {
            if(this.state.mode === 'Edit' && (this.state.user.permanent_address || this.state.user.permanent_statename || this.state.user.permanent_districtname || this.state.user.permanent_pincode) && (permanent_properties.includes(name)) && !this.state.enableSameAs){
                this.setState({
                    enableSameAs:true,
                    same_as_permanent_checked: false
                });
            }else if(this.state.mode === 'Edit' && !(this.state.user.permanent_address || this.state.user.permanent_statename || this.state.user.permanent_districtname || this.state.user.permanent_pincode) && (present_properties.includes(name)) && !this.state.enableSameAs){
                this.setState({
                    enableSameAs:true,
                    // same_as_permanent_checked: false
                });
            }else if(this.state.mode === 'Add' && (this.state.user.permanent_address && this.state.user.permanent_pincode) && !this.state.enableSameAs){
                this.setState({
                    enableSameAs:true,
                    same_as_permanent_checked: false
                });
            }
            //Set Fetched State CIty from state to user object
            if(this.state.permanent_statename_state && this.state.permanent_districtname_state){
                this.setState({
                    user:{
                        ...this.state.user,
                        ['permanent_statename']:this.state.permanent_statename_state,
                        ['permanent_districtname']:this.state.permanent_districtname_state,
                    }
                });
            }
            if(this.state.present_statename_state && this.state.present_districtname_state){
                this.setState({
                    user:{
                        ...this.state.user,
                        ['present_statename']:this.state.present_statename_state,
                        ['present_districtname']:this.state.present_districtname_state,
                    }
                });
            }
            // else{
            //     this.setState({
            //         enableSameAs:false
            //     });
            // }
        });
        
        // console.log(name," Name");
        // console.log(value," value");
        
        
        // if (event.target.name === 'districtname') { this.setState({ error_flag: 0 }) }
        // if (event.target.name === 'statename') {
        //     if (event.target.name !== 'districtname') {
        //         this.setState({ error_flag: 1 });
        //         this.props.dispatch(userActions.getDistrict(value));
        //     }
        // }
         

        if (event.target.name === 'permanent_districtname') { this.setState({ error_flag_permanent: 0 }) }
        if (event.target.name === 'permanent_statename') {
            this.setState({ is_permanent_from_pincode:false, });
            if (event.target.name !== 'permanent_districtname') {
                this.setState({ error_flag_permanent: 1, error_flag_permanent_state: 0 });
                this.props.dispatch(userActions.getDistrict(value));
            }
        }
        if (event.target.name === 'present_districtname') { this.setState({ error_flag_present: 0 }) }
        if (event.target.name === 'present_statename') {
            if (event.target.name !== 'present_districtname') {
                this.setState({ error_flag_present: 1 , error_flag_present_state: 0});
                this.props.dispatch(userActions.getDistrictPresent(value));
            }
        }
        if (event.target.name === 'jobrole') { this.setState({ error_jobrole: 0 }) }
        if (event.target.name === 'sectorname') {
            if (event.target.name !== 'jobrole') {
                this.setState({ error_jobrole: 1 });
                this.props.dispatch(userActions.getJobRoles(value));
            }
        }
        if (event.target.name === 'schemename' && event.target.value === '5') { this.setState({ showState: true, hideState: 0 }); }
        if (event.target.name === 'schemename' && event.target.value !== '5') {

            this.setState({ showState: false, isCMState: 0, hideState: 1 });

            this.props.dispatch(userActions.getSchemeCentralMinistries(value));
        }
        
        if(event.target.name === 'training_status' && event.target.value === 'ongoing') { 
            this.setState({
                show_pass_out_date : true,
            });
            
        }
        if(event.target.name === 'training_status' && event.target.value !== 'ongoing'){
            this.setState({
                show_pass_out_date : false,
            });
            // delete this.state.user['pass_out_date'];
        }
        if(event.target.name === 'placement_status' && event.target.value === 'selected') {
            this.setState({
                show_employer_selection: true
            });
            
            this.props.dispatch(userActions.getEmployersForTC(tcid));
        }
        if(event.target.name === 'placement_status' && event.target.value !== 'selected'){
            this.setState({
                show_employer_selection : false
            });
        }
        if(event.target.name === 'placement_status' && event.target.value === 'joined') {
            this.setState({
                show_appointment_letter_upload: true
            });
        }
        if(event.target.name === 'placement_status' && event.target.value !== 'joined') {
            this.setState({
                show_appointment_letter_upload: false
            });
        }
        // console.log("event data change", event.target.name, event.target.value);
        // console.log(this.state.user,"user");
    }

    async getStateCityFromPincode(value){
        var fetchStateCityAPI = config.API_HOST_URL + 'api/v1/other/getStateCityFromPincode/' + value;
            
        const requestOptions = {
            method: 'GET',
            headers: afterLoginHeaders
        };
        // return fetch(getDistrictAPI, requestOptions).then(handleResponse);
        var response = await fetch(fetchStateCityAPI, requestOptions); 
        response = await response.json();
        return response;
    }

    handleSkillChange(e){
        // console.log("a");
        // console.log(e.target, "TARGET");
    }

    handleSameAsPermanent(event){
        // console.log(this.state.user, "this is user...");
        const { user } = this.state;
        if(event.target.checked === true && user ){
            // console.log("Now Present Address is same as Permanent..");
            this.setState({
                            user: {
                                ...user,
                                "present_address": user.permanent_address ? user.permanent_address : this.props.initialValues.permanent_address,
                                "present_statename": user.permanent_statename ? user.permanent_statename : this.props.initialValues.permanent_statename,
                                "present_districtname": user.permanent_districtname ? user.permanent_districtname : this.props.initialValues.permanent_districtname,
                                "present_pincode": user.permanent_pincode ? user.permanent_pincode : this.props.initialValues.permanent_pincode,
                                "same_as_permanent":true
                            },
                            sameAddress:true,
                            present_address_state: user.permanent_address ? user.permanent_address : this.props.initialValues.permanent_address,
                            present_statename_state: user.permanent_statename ? user.permanent_statename : this.props.initialValues.permanent_statename,
                            present_districtname_state: user.permanent_districtname ? user.permanent_districtname : this.props.initialValues.permanent_districtname,
                            present_pincode_state: user.permanent_pincode ? user.permanent_pincode : this.props.initialValues.permanent_pincode,
                        },/* () => console.log(this.state.user, "This is Updated User") */);
        }else{
            if(user){
                delete this.state.user.present_address;
                delete this.state.user.present_statename;
                delete this.state.user.present_districtname;
                delete this.state.user.present_pincode;
            }
            this.setState({
                sameAddress:false,
                present_address_state: null,
                present_statename_state: null,
                present_districtname_state: null,
                present_pincode_state: null,
                user:{
                    ...user,
                    "same_as_permanent": false
                }
            })
            // console.log("Either not checked or No user object present...");
        }
        this.setState({
            same_as_permanent_checked: !this.state.same_as_permanent_checked,
        });
    }
    handleDayClick(day) {
        // console.log(day, "day");
        this.setState({ 
            selectedDay: day,
            calculatedAgeFromDOB: this.getAge(day),
            is_dob_changed: true,
        }, () => {
            this.setState({
                user:{
                    ...this.state.user,
                    ['age']:this.state.calculatedAgeFromDOB
                }
            })
        });
        if (typeof day === 'undefined') {
            this.setState({ selectedDOB: 1 });
        }
    }
    handleAssessmentClick(day) {
        this.setState({ selectedAssessmentDate: day });
    }
    handlePassOutDateClick(day) {
        // console.log(day);
        this.setState({ selectedPassOutDate: day });
    }
    handleBatchStartDateClick(day) {
        this.setState({ selectedBatchStartDate: day });
    }
    handleBatchEndDateClick(day) {
        this.setState({ selectedBatchEndDate: day });
    }
    render() {

        var newFlag = 0;
        const { handleSubmit } = this.props;
        const { female, male, yes, no, registerClick, show_pass_out_date, show_employer_selection, training_status_new_flag, placement_status_new_flag, submitted } = this.state;
        var loaderClass = '';
        const URLforFirstMonthSalary = config.API_HOST_URL + 'candidate_uploads/'+this.state.candidate_id +'/1/';
        const URLforSecondMonthSalary = config.API_HOST_URL + 'candidate_uploads/'+this.state.candidate_id +'/2/';
        const URLforThirdMonthSalary = config.API_HOST_URL + 'candidate_uploads/'+this.state.candidate_id +'/3/';
        const URLforAppointmentLetter = config.API_HOST_URL + 'candidate_uploads/'+this.state.candidate_id +'/appointment_letter/';
        //Show loader..
        //if (isSubmit == true && loader == true) {
        if (submitted === true) {
            loaderClass = 'loading';
            this.turnOffRedTimeout = setTimeout(() => {
                this.setState(() => ({ submitted: false, showErrorSuccess: true }))
            }, 1000); //1 secs
        }
        const styles = {
            top: '65px',
            left: '270px'
        };
        if(this.state.isLoading){
            loaderClass = 'loading';
        }
        const dayPickerProps = {
            month: this.state.month,
            fromMonth: fromMonth,
            toMonth: toMonth,
            captionElement: ({ date, localeUtils }) => (
                <YearMonthForm
                    date={date}
                    localeUtils={localeUtils}
                    onChange={this.handleYearMonthChange}
                />
            )
        };

        //console.log("EDIT CANDIDAT ID", this.state.candidate_id);
        // console.log("Candidate Details", this.props.candidate_updated_details);
        //  console.log("Candidate Form Details", this.props.candidateformdata);
        //console.log("disctrict value", districtlist);
        //console.log("Job value", jobroles);
        //  console.log("get cnetral minstiry data",this.props);  

        /****For on seletion change data events like , State-District, Sector - Job...*/

        var central_minstry_name = "";
        var allStates;
        var final_allStates;
        var allPreferredCities;
        // if (!!this.props.districtlist) {
        //     var allCities = this.props.districtlist.data;
        //     var final_allCities = [];
        //     //State values..
        //     allCities.map((value, index) => {
        //         return final_allCities.push(<option key={index} value={value.cityid}>{value.cityName}</option>);
        //     })
        // }
        // console.log(this.state.state_change_permanent," this.state.state_change_permanent");
        if (!!this.props.districtlistpreferred && this.props.districtlistpreferred.status === true) {
            // console.log(this.props.districtlistpreferred, " districtlistpreferred ");
            allPreferredCities = this.props.districtlistpreferred.data;
            // console.log(allPreferredCities, " districtlistpreferred ");
            var final_allPreferredCities = [{ value: "all", label: "Anywhere" }];
            allPreferredCities.map((value, index) => {
                return final_allPreferredCities.push({ value: value.cityid, label: value.cityName });
            })
        }
        var allCities;
        if (!!this.props.districtlist) {
            allCities = this.props.districtlist.data;
            var final_allCities_permanent = [];
            //State values..
            allCities.map((value, index) => {
                return final_allCities_permanent.push(<option key={index} value={value.cityid}>{value.cityName}</option>);
            });
        }
        // console.log(this.state.state_change_present," this.state.state_change_present");
        if (!!this.props.districtlistpresent) {
            allCities = this.props.districtlistpresent.data;
            var final_allCities_present = [];
            //State values..
            allCities.map((value, index) => {
                return final_allCities_present.push(<option key={index} value={value.cityid}>{value.cityName}</option>);
            });
        }
        if (!!this.props.jobroles) {
            var allJobroles = this.props.jobroles.data;
            var final_allJobs = [];
            allJobroles.map((value, index) => {
                return final_allJobs.push(<option key={index} value={value.jobroleid}>{value.jobroleName}</option>);
            })
        }
        if (!!this.props.centralministriesdata) {
            //  console.log("CM data",this.props.centralministriesdata);
            if (this.props.centralministriesdata.data.length > 0) {
                central_minstry_name = this.props.centralministriesdata.data[0].centralministryname;

            }
        }
        if (!!this.props.candidate_updated_details) {
            if (this.props.candidate_updated_details.data[0].schemeid === 5) {
                if (!!this.props.candidateformdata) {
                    this.state.isCMState = 1;
                    newFlag = 1;

                    //State values..
                    allStates = this.props.candidateformdata.data.state[0];
                    //  console.log("CMSTATES",allStates);
                    final_allStates = [];
                    allStates.map((value, index) => {
                        return final_allStates.push(<option key={index} value={value.stateid}>{value.stateName}</option>);
                    })
                }
            }
        }

        if (!!this.props.candidate_updated_details && training_status_new_flag === 0) {
            if (this.props.candidate_updated_details.data[0].training_status === 'ongoing') {
                this.state.show_pass_out_date =  true;
                // this.state.selectedPassOutDate =  this.props.candidate_updated_details.data[0].pass_out_date;
                this.state.training_status_new_flag = 1;
            }
        }
        // if(this.props.initialValues.training_status === 'ongoing') { 
        //     this.state.show_pass_out_date =  true;
        // }
        // if(this.props.initialValues.training_status !== 'ongoing' ){
        //     this.state.show_pass_out_date = false;
        // }

        if (!!this.props.candidate_updated_details && placement_status_new_flag === 0) {
            if (this.props.candidate_updated_details.data[0].placement_status === 'selected') {
                // this.setState({
                //     show_employer_selection:true,
                //     placement_status_new_flag: 1
                // });
                this.state.show_employer_selection =  true;
                this.state.placement_status_new_flag = 1;
            }
            if (this.props.candidate_updated_details.data[0].placement_status === 'joined') {
                // this.setState({
                //     show_employer_selection:true,
                //     placement_status_new_flag: 1
                // });
                this.state.show_appointment_letter_upload =  true;
                this.state.placement_status_new_flag = 1;
            }
        }
        // if(this.props.initialValues.placement_status === 'selected'){
        //     this.state.show_employer_selection =  true; 
        // }
        //     else if(this.props.initialValues.training_status !== 'selected') { this.state.show_employer_selection =  false; }
        


        if(!!this.props.employerName){
            var allemployers,final_allEmployers;
            // if (this.props.candidate_updated_details.data[0].schemeid === 5) {
                // if (!!this.props.employerName) {
                    // this.state.isCMState = 1;
                    // newFlag = 1;
                    // console.log("CMSTATES",this.props.employerName);
                    //State values..
                    allemployers = this.props.employerName.data;
                    
                    final_allEmployers = [];
                    allemployers.map((value, index) => {
                        return final_allEmployers.push(<option key={index} value={value.ipid}>{value.ip_name}</option>);
                    })
                // }
            // }
        }
        /****End of getting data on change selection criteria...*/

        if (!!this.props.candidateformdata) {

            // var isTrainingCenter = 0;
            var allTrainingCenterList = this.props.candidateformdata.data.training_centre[0];
            var final_TrainingCenter = [];
            if (allTrainingCenterList !== undefined) {
                // isTrainingCenter = 1;
                allTrainingCenterList.map((value, index) => {
                    return final_TrainingCenter.push(<option key={index} value={value.trainingcenterid}>{value.trainingcenter_name}</option>);

                })
            }
            else {
                // isTrainingCenter = 0;
                //console.log('candidate tc',allTrainingCenterList);
                //this.props.dispatch(alertActions.error("Please add Training Center"));
            }

            //State values..
            allStates = this.props.candidateformdata.data.state[0];
            final_allStates = [];
            allStates.map((value, index) => {
                return final_allStates.push(<option key={index} value={value.stateid}>{value.stateName}</option>);
            })


            //Alternative IDs..
            var allAlternativeIds = this.props.candidateformdata.data.alternateidtype[0];
            var final_alternative_ids = [];
            allAlternativeIds.map((value, index) => {
                return final_alternative_ids.push(<option key={index} value={value.optionValue}>{value.optionKey}</option>);
            })

            //Employment Status values..
            var allEmploymentStatus = this.props.candidateformdata.data.employment_status[0];
            var final_employementstatus = [];
            allEmploymentStatus.map((value, index) => {
                return final_employementstatus.push(<option key={index} value={value.optionValue}>{value.optionKey}</option>);
            })
            //PlacementStatus list..
            var allplacementlist = this.props.candidateformdata.data.placement_status[0];
            var final_placementstatus = [];
            allplacementlist.map((value, index) => {
                return final_placementstatus.push(<option key={index} value={value.optionValue}>{value.optionKey}</option>);

            })
            // //Training Center list..
            // var allTrainingCenterList = this.props.candidateformdata.data.training_centre[0];
            // var final_TrainingCenter = [];
            // allTrainingCenterList.map((value, index) => {
            //     return final_TrainingCenter.push(<option key={index} value={value.trainingcenterid}>{value.trainingcenter_name}</option>);

            // })
            //Gender Type list..
            var allgendertype = this.props.candidateformdata.data.gender[0];
            var final_genderlist = [];
            allgendertype.map((value, index) => {
                return final_genderlist.push(<option key={index} value={value.optionValue}>{value.optionKey}</option>);
            })

            //Scheme list..
            var allschemes = this.props.candidateformdata.data.scheme[0];
            var final_schemes = [];
            allschemes.map((value, index) => {
                return final_schemes.push(<option key={index} value={value.pk_schemeID}>{value.schemeType}</option>);

            })
            //Sector list..
            var allsectors = this.props.candidateformdata.data.sector[0];
            var final_sectors = [];
            allsectors.map((value, index) => {
                return final_sectors.push(<option key={index} value={value.sectorid}>{value.sectorname}</option>);

            })
            //Training Status list..
            var alltrainingstatus = this.props.candidateformdata.data.training_status[0];
            var final_trainingstatus = [];
            alltrainingstatus.map((value, index) => {
                return final_trainingstatus.push(<option key={index} value={value.optionValue}>{value.optionKey}</option>);

            })
            //Training Type list..
            var alltrainingtype = this.props.candidateformdata.data.training_type[0];
            var final_trainingtype = [];
            alltrainingtype.map((value, index) => {
                return final_trainingtype.push(<option key={index} value={value.optionValue}>{value.optionKey}</option>);

            })
            //console.log("fina array", final_allSectors);

            // console.log("fina array", this.props.candidateformdata);
        }
        const renderColorSelector = ({ input, label, sameValueState, sameValueStatePermanent, disabled, sameValueDistrict, sameValueDistrictPermanent, error_state, error_jobroleerror, error_employer, dynamic_values, meta: { touched, error } }) => (
            <React.Fragment>
                {/* {this.state.permanent_statename_state && (input.name === 'permanent_statename' || input.name === 'permanent_districtname')  ? touched = false : ''}
                {this.state.present_statename_state && (input.name === 'present_statename' || input.name === 'present_districtname')  ? touched = false : ''} */}
                <div className={'form-group' + ((error && touched) || (error_state === 1) || (error_jobroleerror === 1) ? ' has-error' : '')}>
                {/* {console.group(input.name,"GRP START")}
                {console.log(error , touched, "error && touched")}
                {console.log(error_state, "error_state")}
                {console.log(error_jobroleerror, "error_jobroleerror")}
                {console.groupEnd(input.name,"GRP END")} */}
                    <div className="hidden">
                        {(sameValueState && sameValueState !== '') ?  input.value = sameValueState : ''}
                        {(sameValueDistrict && sameValueDistrict !== '') ?  input.value = sameValueDistrict : ''}
                        {(sameValueStatePermanent && sameValueStatePermanent !== '') ?  input.value = sameValueStatePermanent : ''}
                        {(sameValueDistrictPermanent && sameValueDistrictPermanent !== '') ?  input.value = sameValueDistrictPermanent : ''}
                        {(sameValueState && sameValueState !== '' ) ? this.props.dispatch(userActions.getDistrictPresent(sameValueState)): ''}
                        {(sameValueStatePermanent && sameValueStatePermanent !== '' ) ? this.props.dispatch(userActions.getDistrict(sameValueStatePermanent)): ''}
                    </div>
                    <label>{label}</label>
                    <select {...input} disabled={disabled} className="form-control">
                        <option value="">Select</option>
                        {dynamic_values}
                        ))}
                </select>
                    {error_state === 1 && <div className="help-block">{error}</div>}
                    {error_jobroleerror === 1 && <div className="help-block">{error}</div>}
                    {error_employer === 1 && <div className="help-block">{error}</div>}
                    
                    {(touched && error) && <div className="help-block">{error}</div>}
                </div>
            </React.Fragment>

        )
        if(this.state.edu_details_edited === false && this.state.edu_details.length === 0 && this.props.initialValues.edu_details){
            this.state.initial_edu_details = this.props.initialValues.edu_details.split(',');
            this.state.edu_details = this.props.initialValues.edu_details.split(',');
        }
        if(!!this.props.initialValues && this.state.is_preferred_city_set === false)  {
            // console.log(this.props.initialValues, "INI");
            var preferred_city = this.props.initialValues.preferred_city;
            // console.log(jobrolevalue,"jobrolevalue");
            if(!!preferred_city){
                preferred_city = preferred_city.split(',');
                this.state.PreferredCityValue = preferred_city;
                this.state.is_preferred_city_set = true;
            }
        }
        
        return (

            <div className="content-wrapper user-flow TP-flow">
        
                <div id="content" className="main-content">
                    <Sidebar {...this.props} />
                    <Header />

                    {/* {isTrainingCenter === 0 ?
                        <div>
                            <div className="d-block d-md-flex align-items-center section-header">
                                <div>
                                    <h1 className="page-title">Add Training Center</h1>
                                    <Breadcrumb>
                                        <BreadcrumbItem><a href="/TrainingPartner-dashboard">Dashboard</a></BreadcrumbItem>
                                        <BreadcrumbItem><a href="/training-centers">Training Centers</a></BreadcrumbItem>
                                        <BreadcrumbItem active>Add Center</BreadcrumbItem>
                                    </Breadcrumb>
                                </div>
                            </div>

                            <div className="d-flex justify-content-center align-items-center error-page">
                                <div className="text-center">
                                    <div className="img-wrapper mb-4">
                                        <div className="circle-xl user-avatar">
                                            <img src={trainingCentreSVG} alt="trainingCenter" />
                                        </div>
                                    </div>
                                    <h1 className="h2 fw-regular mb-0">Add Training Center!</h1>
                                    <span className="h6 ff-roboto d-block mt-0">
                                        To add candidate, you will first have to add training center
                            </span>
                                    <Button color="primary" className="btn-raised" title="Add Training Center" onClick={this.handleRedirectionToAddCenter}>
                                        <i className="material-icons">add</i>
                                        Add Training Center
                            </Button>
                                </div>
                            </div>
                        </div>
                        : */}
                        <div>
                            <div className="d-block d-md-flex align-items-center section-header">

                                <div>
                                    <h1 className="page-title">{this.state.mode} Candidate</h1>
                                    <Breadcrumb>
                                        {/* <BreadcrumbItem><a href="/TrainingPartner-dashboard">Dashboard</a></BreadcrumbItem> */}
                                        <BreadcrumbItem><a href="/candidates">Candidates</a></BreadcrumbItem>
                                        <BreadcrumbItem active>{this.state.mode} Candidate</BreadcrumbItem>
                                    </Breadcrumb>
                                </div>

                            </div>
                            <form id="frm-add-candidate" className="form frm-edit-candidaten" onSubmit={handleSubmit(this.handleSubmit)}>
                            <div className="card z-depth-1">
                                <div className="card-body pb-0">
                                    <div className={'form-group' + (!male || !female ? ' has-error' : '')}>
                                        <label htmlFor="skillInfo" className="d-block">Skill Info</label>
                                        <Field name="skills" component="input" type="radio" value="skilled" className="ml-0" checked /* onChange={this.handleSkillChange} */ /> Skilled
                                        {/* <Field name="skills" component="input" type="radio" value="unskilled" onChange={this.handleSkillChange} /> Unskilled */}
                                        <Field name="skills" component={renderError} />
                                    </div>
                                </div>
                            </div>
                            {/* {this.state.skills_for_form} */}
                            {/* {this.state.skills_for_form && */}
                                <div className="card z-depth-1">
                                    <div className="card-header bg-transparent border-bottom-0 section-title no-border form-title">
                                        <h5 className="title card-title">Personal Details</h5>
                                    </div>
                                    <div className="card-body pb-0">
                                        
                                        {/* <div className={'form-group' + (!male || !female ? ' has-error' : '')}>
                                            <label htmlFor="skillInfo" className="d-block">Skill Info</label>
                                            <Field name="skills" component="input" type="radio" value="skilled" className="ml-0" onChange={this.handleChange} /> Skilled
                                            <Field name="skills" component="input" type="radio" value="unskilled" onChange={this.handleChange} /> Unskilled
                                            <Field name="skills" component={renderError} />
                                        </div> */}
                                        
                                            <div className="row">
                                                {/* <label htmlFor="name" className="d-block"> Name of Candidate </label> */}
                                                
                                                <Field name="firstName" type="text" label="First Name" component={renderField} placeholder="First Name" onChange={this.handleChange} />
                                                <Field name="middleName" type="text" label="Middle Name" component={renderField} placeholder="Middle Name" onChange={this.handleChange} />
                                                <Field name="lastName" type="text" label="Last Name" component={renderField} placeholder="Last Name" onChange={this.handleChange} />
                                                
                                            {/* </div>
                                            <div className="row"> */}
                                                {/* <Field name="aadhar_card_no" type="number" component={renderField} label="Aadhaar No" placeholder="Aadhaar No" onChange={this.handleChange} /> */}
                                            {/* </div>
                                            <div className="row"> */}
                                                
                                                {/* {this.state.skills_for_form === 'skilled' && */}
                                                    <Field name="candidate_id" type="text" component={renderField} label="Candidate ID" placeholder="Candidate ID" onChange={this.handleChange} />
                                                {/* } */}
                                            {/* </div>
                                            <div className="row"> */}
                                                {/* <Field name="aadhaar_enrollment_id" type="text" component={renderField} label="Aadhaar Enrollment ID" placeholder="Aadhaar Enrollment ID" onChange={this.handleChange} /> */}
                                                <div className="col-12 col-md-6">
                                                    <FormGroup>

                                                        <div className='form-group'>
                                                            <Field name="alternate_id_type" component={renderColorSelector} dynamic_values={final_alternative_ids} onChange={this.handleChange} label="Alternate ID Type" />
                                                        </div>
                                                    </FormGroup>
                                                </div>
                                            {/* </div>
                                            <div className="row"> */}
                                                <Field name="altername_id_no" type="text" component={renderField} label="Alternate ID No" placeholder="Alternate ID No" onChange={this.handleChange} />
                                                <Field name="phone" type="text" component={renderField} label="Mobile No" placeholder="Mobile No" onChange={this.handleChange} />
                                            {/* </div>
                                            <div className="row"> */}
                                                <Field name="email_address" type="email" component={renderField} label="Email Address" placeholder="Email Address" onChange={this.handleChange} />

                                                <div className="col-12 col-md-6">
                                                    <div className={'form-group' + (!this.state.selectedPassOutDate ? ' has-error' : '')}>
                                                        <label htmlFor="date_of_birth">Date Of Birth</label>
                                                        <div className="input-group date date-picker" data-provide="datepicker">
                                                            <DayPickerInput
                                                                format={"D-M-YYYY"}
                                                                formatDate={formatDate}
                                                                parseDate= {parseDate}
                                                                placeholder={"DD-MM-YYYY"}
                                                                // placeholder={!!this.props.candidate_updated_details && this.props.candidate_updated_details.data[0].dob !== '' && this.props.candidate_updated_details.data[0].dob !== null ? this.props.candidate_updated_details.data[0].dob : 'Date of Birth'}
                                                                value={!!this.props.candidate_updated_details && this.props.candidate_updated_details.data[0].dob !== '' && this.props.candidate_updated_details.data[0].dob !== null ? this.props.candidate_updated_details.data[0].dob : ''}
                                                                onDayChange={this.handleDayClick}
                                                                dayPickerProps={dayPickerProps}
                                                                inputProps={{ readOnly: true }}
                                                            />
                                                            <div className="input-group-addon">
                                                                <i className="material-icons">date_range</i>
                                                            </div>

                                                        </div>
                                                        <div className="help-block has-error">{(!this.state.selectedDay && !!this.props.candidate_updated_details && !this.props.candidate_updated_details.data[0].dob) && registerClick === 1 ? 'Please enter Date of Birth' : ''}</div>
                                                    </div>
                                                </div>
                                                {this.state.is_dob_changed
                                                 ?
                                                <Field name="age" type="number" component={renderField} label="Age" disabled={true} sameValue={this.state.calculatedAgeFromDOB} placeholder="Age" onChange={this.handleChange} />
                                                 :
                                                <Field name="age" type="number" component={renderField} label="Age" disabled={true}  placeholder="Age" onChange={this.handleChange} />
                                                }
                                                
                                            </div>
                                            <div className={'form-group' + (!male || !female ? ' has-error' : '')}>
                                                <label htmlFor="gender" className="d-block">Gender</label>
                                                <Field name="sex" component="input" type="radio" value="male" className="ml-0" onChange={this.handleChange} /> Male
                                                <Field name="sex" component="input" type="radio" value="female" onChange={this.handleChange} /> Female
                                                <Field name="sex" component="input" type="radio" value="other" onChange={this.handleChange} /> Other
                                                <Field name="sex" component={renderError} />
                                            </div>

                                            {/* {this.state.skills_for_form === 'skilled' && */}
                                                <div className={'form-group' + (!yes || !no ? ' has-error' : '')}>
                                                    <label htmlFor="relocate" className="d-block">Willing To Relocate</label>
                                                    <Field name="is_willing_to_relocate" component="input" type="radio" value="yes" className="ml-0" onChange={this.handleChange} /> Yes
                                                    <Field name="is_willing_to_relocate" component="input" type="radio" value="no" onChange={this.handleChange} /> No
                                                    {/* <Field name="is_willing_to_relocate" component={renderError} /> */}
                                                </div>
                                            {/* } */}
                                            {/* <Col md="4"> */}
                                            {(this.state.displayPrefferedCity || (this.props.initialValues.is_willing_to_relocate === 'yes' && this.state.is_first_load )) &&
                                            <FormGroup>
                                            <span>Select Preferred Cities</span>
                                            <div className={'form-group' + (!this.state.selectedRequirementByDate ? ' has-error' : '')}>
                                            <Select
                                                className="basic-single"
                                                classNamePrefix="select" 
                                                // defaultValue={colourOptions[0]}
                                                isDisabled={false}
                                                isLoading={false}
                                                isClearable={true}
                                                isRtl={false}
                                                isSearchable={true}
                                                multi={this.state.isMulti}
                                                simpleValue
                                                placeholder ="Select Preferred Cities"
                                                // component={renderColorSelector}
                                                // dynamic_values={final_allJobs}
                                                name="preferred_city"
                                                value={this.state.PreferredCityValue}
                                                onChange={this.handleSelectChange}
                                                options={final_allPreferredCities}
                                            />
                                                <div className="help-block">{!this.state.PreferredCityValue && registerClick === 1 ? 'Please select preferred city' : ''}</div>
                                            </div>
                                            </FormGroup>
                                            }
                                            {/* </Col> */}
                                    </div>
                                    <hr className="inner-form-seperator" />
                                    <div className="card-header bg-transparent border-bottom-0 section-title no-border form-title">
                                        <h5 className="title card-title">Permanent Address Details</h5>
                                    </div>
                                    <div className="card-body pb-0">
                                        <div className="">
                                            <Field name="permanent_address" component={renderTextAreaField} type="textarea" label="Permanent Address" textarea={true} onChange={this.handleChange} />
                                            {/* 
                                                <Field name="adress_check" component="input" type="checkbox" value="yes" className="ml-0" onChange={this.handleCheckboxClick} /> Same as Permanent Address
                                            */}
                                        </div>
                                        <div className="row">
                                            <Field name="permanent_pincode" type="text" component={renderField} label="Pincode" placeholder="Pincode" onChange={this.handleChange} />

                                            <div className="col-12 col-md-4">
                                                <div className='form-group'>
                                                    <Field name="permanent_statename" component={renderColorSelector}  error_state={this.state.error_flag_permanent_state} dynamic_values={final_allStates} onChange={this.handleChange} sameValueStatePermanent={this.state.permanent_statename_state} label="State" />
                                                </div>
                                            </div>

                                            <div className="col-12 col-md-4">
                                                <div className='form-group'>
                                                    <Field name="permanent_districtname" component={renderColorSelector} error_state={this.state.error_flag_permanent} dynamic_values={final_allCities_permanent} onChange={this.handleChange} sameValueDistrictPermanent={this.state.permanent_districtname_state} label="District" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr className="inner-form-seperator" />
                                    <div className="card-header bg-transparent border-bottom-0 section-title no-border form-title">
                                        <h5 className="title card-title">Present Address Details</h5>
                                    </div>
                                    <div className="card-body pb-0">
                                    {this.state.enableSameAs ?
                                        <div>
                                            <Field name="same_as_permanent" component="input" value={this.state.same_as_permanent_checked} checked={this.state.same_as_permanent_checked} type="checkbox" className="ml-0" onChange={this.handleSameAsPermanent} /> Same as Permanent
                                        </div>
                                        : 
                                        <div>
                                            <Field name="same_as_permanent_initial" component="input" disabled type="checkbox" className="ml-0" /> Same as Permanent
                                        </div> 
                                    } 
                                    
                                    {/* {!this.state.sameAddress && */}
                                        <div>
                                            {/* {console.log(this.sameAsPermanent.getValue())} */}
                                           
                                            <div className="sameAddress">
                                                <Field name="present_address" component={renderTextAreaField} disabled={this.state.same_as_permanent_checked} sameValue={this.state.present_address_state} type="textarea" label="Present Address" textarea={true} onChange={this.handleChange} />
                                            </div>
                                            <div className="row">
                                                <Field name="present_pincode" type="text" disabled={this.state.same_as_permanent_checked} component={renderField} sameValue={this.state.present_pincode_state} label="Pincode" placeholder="Pincode" onChange={this.handleChange} />
                                                <div className="col-12 col-md-4">
                                                    <div className='form-group'>
                                                        <Field name="present_statename" disabled={this.state.same_as_permanent_checked} component={renderColorSelector} sameValueState={this.state.present_statename_state}  error_state={this.state.error_flag_present_state} dynamic_values={final_allStates} value='12' onChange={this.handleChange} label="State" />
                                                    </div>
                                                </div>

                                                <div className="col-12 col-md-4">
                                                    <div className='form-group'>
                                                        <Field name="present_districtname" disabled={this.state.same_as_permanent_checked} component={renderColorSelector} sameValueDistrict={this.state.present_districtname_state} error_state={this.state.error_flag_present} dynamic_values={final_allCities_present} onChange={this.handleChange} label="District" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    {/* } */}
                                    </div>

                                        {/* <div className="">
                                            <div className={'form-group' + (!yes || !no ? ' has-error' : '')}>
                                                <Field name="8" component="input" type="checkbox" value="yes" className="ml-2" onChange={this.handleCheckboxClick} /> 8<sup>th</sup>
                                                <Field name="10" component="input" type="checkbox" value="yes" className="ml-2" onChange={this.handleCheckboxClick} /> 10<sup>th</sup>
                                                <Field name="12" component="input" type="checkbox" value="yes" className="ml-2" onChange={this.handleCheckboxClick} /> 12<sup>th</sup>
                                                <Field name="graduate" component="input" type="checkbox" value="yes" className="ml-2" onChange={this.handleCheckboxClick} /> Graduate
                                                <Field name="post_graduate" component="input" type="checkbox" value="yes" className="ml-2" onChange={this.handleCheckboxClick} /> Post Graduate
                                            </div>
                                        </div> */}

                                        {/* <div className="row">
                                            <div className="col-12 col-md-4">
                                                <div className='form-group'>
                                                    <Field name="statename" component={renderColorSelector} dynamic_values={final_allStates} onChange={this.handleChange} label="State" />
                                                </div>
                                            </div>

                                            <div className="col-12 col-md-4">
                                                <div className='form-group'>
                                                    <Field name="districtname" component={renderColorSelector} error_state={this.state.error_flag} dynamic_values={final_allCities} onChange={this.handleChange} label="District" />
                                                </div>
                                            </div>
                                            <Field name="pincode" type="text" component={renderField} label="Pincode" placeholder="Pincode" onChange={this.handleChange} />

                                        </div> */}
                                        

                                    <hr className="inner-form-seperator" />

                                    <div className="card-header bg-transparent border-bottom-0 section-title no-border form-title">
                                        <h5 className="title card-title">Education Details</h5>
                                    </div>
                                    <div className="card-body">
                                    
                                        <div className={'form-group' + (!yes || !no ? ' has-error' : '')}>
                                            <Field name="primary" component="input" type="checkbox" value="yes" className="ml-0" checked={this.state.edu_details.indexOf('primary') > -1} onChange={this.handleEduClick} /> 8<sup>th</sup> 
                                            <Field name="secondary" component="input" type="checkbox" value="yes" className="ml-2" checked={this.state.edu_details.indexOf('secondary') > -1} onChange={this.handleEduClick} /> 10<sup>th</sup> 
                                            <Field name="higherSecondary" component="input" type="checkbox" value="yes" className="ml-2" checked={this.state.edu_details.indexOf('higherSecondary') > -1} onChange={this.handleEduClick} /> 12<sup>th</sup> 
                                            <Field name="grad" component="input" type="checkbox" value="yes" className="ml-2" checked={this.state.edu_details.indexOf('grad') > -1} onChange={this.handleEduClick} /> Graduation 
                                            <Field name="postGrad" component="input" type="checkbox" value="yes" className="ml-2" checked={this.state.edu_details.indexOf('postGrad') > -1} onChange={this.handleEduClick} /> Post-Graduation
                                        </div>
                                    
                                        
                                    </div>
    
                                    <hr className="inner-form-seperator" />
                                    <div className="card-header bg-transparent border-bottom-0 section-title no-border form-title">
                                        <h5 className="title card-title">Professional Details</h5>
                                    </div>
                                    <div className="card-body">
                                        <div className="row">
                                            <Field name="exp_in_years" type="number" component={renderField} label="Experience In Years" placeholder="Experience In Years" onChange={this.handleChange} />
                                            
                                            <div className="col-12 col-md-4">
                                                <div className="form-group">
                                                    <FormGroup>
                                                        <Field name="sectorname" component={renderColorSelector} dynamic_values={final_sectors} onChange={this.handleChange} label="Sector" />
                                                    </FormGroup>
                                                </div>
                                            </div>

                                            <div className="col-12 col-md-4">
                                                <div className="form-group">
                                                    <FormGroup>
                                                        <Field name="jobrole" component={renderColorSelector} error_jobroleerror={this.state.error_jobrole} dynamic_values={final_allJobs} onChange={this.handleChange} label="Job Role" />
                                                    </FormGroup>
                                                </div>
                                            </div>
                                        </div>
                                        {/* {this.state.skills_for_form === 'skilled' && */}
                                            <div className="row">
                                                <div className="col-12 col-md-4">
                                                    <FormGroup>
                                                        <Field name="training_type" component={renderColorSelector} dynamic_values={final_trainingtype} onChange={this.handleChange} label="Training Type" />
                                                    </FormGroup>
                                                </div>
                                                <Field name="nsdc_enrollment_no" type="text" component={renderField} label="SDMS/NSDC Enrollment No" placeholder="SDMS/NSDC Enrollment No" onChange={this.handleChange} />
                                            {/* </div> */}

                                            {/* <div className="row"> */}
                                                <div className="col-12 col-md-4">
                                                    <FormGroup>
                                                        <Field name="training_status" component={renderColorSelector} dynamic_values={final_trainingstatus} onChange={this.handleChange} label="Training Status" />
                                                    </FormGroup>
                                                </div>
                                                {/* {console.log(training_status.value,"value")} */}
                                                
                                            {show_pass_out_date &&
                                            <div className="col-12 col-md-4">
                                                    <div className={'form-group' + (!this.state.selectedPassOutDate ? ' has-error' : '')}>
                                                        <label htmlFor="pass_out_date">Pass Out Date</label>
                                                        <div className="input-group date date-picker" data-provide="datepicker">
                                                        {this.state.mode === 'Add' ?
                                                            <DayPickerInput
                                                            format={"D-M-YYYY"}
                                                            formatDate={formatDate}
                                                            parseDate= {parseDate}
                                                            placeholder={"DD-MM-YYYY"}
                                                            onDayChange={this.handlePassOutDateClick}
                                                            inputProps={{ readOnly: true }}
                                                            />
                                                            :
                                                            <DayPickerInput
                                                                // placeholder={(!!this.props.candidate_updated_details) ? this.props.candidate_updated_details.data[0].pass_out_date : ""}
                                                                format={"D-M-YYYY"}
                                                                formatDate={formatDate}
                                                                parseDate= {parseDate}
                                                                placeholder={"DD-MM-YYYY"}
                                                                value={(!!this.props.candidate_updated_details ) ? this.props.candidate_updated_details.data[0].pass_out_date : ""}
                                                                onDayChange={this.handlePassOutDateClick}
                                                                inputProps={{ readOnly: true }}
                                                                />
                                                        }
                                                            <div className="input-group-addon">
                                                                <i className="material-icons">date_range</i>
                                                            </div>
                                                        </div>
                                                        {/* {this.state.mode === 'Add' && */}
                                                            <div className="help-block">{!this.state.selectedPassOutDate && !!this.props.candidate_updated_details && !this.props.candidate_updated_details.data[0].pass_out_date && registerClick === 1 ? 'Please enter Pass-Out Date' : ''}</div>
                                                        {/* } */}
                                                    </div>
                                                </div>
                                            }
                                                <Field name="attendance_percent" type="number" component={renderField} label="Attendance Percent" placeholder="Attendance Percent" onChange={this.handleChange} />
                                                <div className="col-12 col-md-4">
                                                    <FormGroup>
                                                        <Field name="placement_status" component={renderColorSelector} dynamic_values={final_placementstatus} onChange={this.handleChange} label="Placement Status" />
                                                    </FormGroup>
                                                </div>
                                                {show_employer_selection &&
                                                <div className="col-12 col-md-4">
                                                    <div className="form-group">
                                                        <FormGroup>
                                                            <Field name="employer_name" component={renderColorSelector} error_employererror={this.state.error_employer} dynamic_values={final_allEmployers}  onChange={this.handleChange} label="Employer Name" />
                                                        </FormGroup>
                                                    </div>
                                                </div>
                                                }
                                            
                                            {/* </div> */}

                                            {/* <div className="row"> */}
                                                <div className="col-12 col-md-4">
                                                    <FormGroup>
                                                        <Field name="employment_type" component={renderColorSelector} dynamic_values={final_employementstatus} onChange={this.handleChange} label="Employment Type" />
                                                    </FormGroup>
                                                </div>
                                                <Field name="assessment_score" type="number" component={renderField} label="Assessment Score" placeholder="Assessment Score" onChange={this.handleChange} />
                                                <Field name="max_assessment_score" type="number" component={renderField} label="Max Assessment Score" placeholder="Max Assessment Score" onChange={this.handleChange} />
                                            {/* </div> */}

                                            {/* <div className="row"> */}
                                                {/* <div className="col-12 col-md-4">
                                                    <div className={'form-group' + (!this.state.selectedAssessmentDate ? ' has-error' : '')}>
                                                        <label htmlFor="assessment_date">Assessment Date</label>
                                                        <div className="input-group date date-picker" data-provide="datepicker">
                                                        {this.state.mode === 'Add' ?
                                                            <DayPickerInput onDayChange={this.handleAssessmentClick} />
                                                            :
                                                            <DayPickerInput
                                                                placeholder={(!!this.props.candidate_updated_details) ? this.props.candidate_updated_details.data[0].assessment_date : ""}
                                                                value={(!!this.props.candidate_updated_details) ? this.props.candidate_updated_details.data[0].assessment_date : ""}
                                                                onDayChange={this.handleAssessmentClick} />
                                                        }
                                                            <div className="input-group-addon">
                                                                <i className="material-icons">date_range</i>
                                                            </div>
                                                        </div>
                                                        {this.state.mode === 'Add' &&
                                                            <div className="help-block">{!this.state.selectedAssessmentDate && registerClick === 1 ? 'Please enter Assessment Date' : ''}</div>
                                                        }
                                                    </div>
                                                </div> */}

                                                {/* <div className="col-12 col-md-4">
                                                    <div className={'form-group' + (!this.state.selectedBatchStartDate ? ' has-error' : '')}>
                                                        <label htmlFor="Batch Start Date">Batch Start Date</label>
                                                        <div className="input-group date date-picker" data-provide="datepicker">
                                                        {this.state.mode === 'Add' ?
                                                            <DayPickerInput onDayChange={this.handleBatchStartDateClick} />
                                                            :
                                                            <DayPickerInput
                                                                placeholder={(!!this.props.candidate_updated_details) ? this.props.candidate_updated_details.data[0].batch_start_date : ""}
                                                                value={(!!this.props.candidate_updated_details) ? this.props.candidate_updated_details.data[0].batch_start_date : ""}
                                                                onDayChange={this.handleBatchStartDateClick} />
                                                        }
                                                            <div className="input-group-addon">
                                                                <i className="material-icons">date_range</i>
                                                            </div>
                                                        </div>
                                                        {this.state.mode === 'Add' &&
                                                            <div className="help-block">{!this.state.selectedBatchStartDate && registerClick === 1 ? 'Please enter Batch Start Date' : ''}</div>
                                                        }
                                                    </div>
                                                </div> */}
                                                {/* <div className="col-12 col-md-4">
                                                    <div className={'form-group' + (!this.state.selectedBatchEndDate ? ' has-error' : '')}>
                                                        <label htmlFor="Batch End Date">Batch End Date</label>
                                                        <div className="input-group date date-picker" data-provide="datepicker">
                                                        {this.state.mode === 'Add' ? <DayPickerInput onDayChange={this.handleBatchEndDateClick} /> :
                                                            <DayPickerInput
                                                                placeholder={(!!this.props.candidate_updated_details) ? this.props.candidate_updated_details.data[0].batch_end_date : ""}
                                                                value={(!!this.props.candidate_updated_details) ? this.props.candidate_updated_details.data[0].batch_end_date : ""}
                                                                onDayChange={this.handleBatchEndDateClick} />
                                                                }
                                                            <div className="input-group-addon">
                                                                <i className="material-icons">date_range</i>
                                                            </div>
                                                        </div>
                                                        {this.state.mode === 'Add' &&
                                                            <div className="help-block">{!this.state.selectedBatchEndDate && registerClick === 1 ? 'Please enter Batch End Date' : ''}</div>
                                                        }
                                                    </div>
                                                </div> */}


                                            {/* </div> */}
                                            {/* <div className="row"> */}
                                                

                                                {/* <div className="col-12 col-md-4">
                                                    <div className="form-group">
                                                        <FormGroup>
                                                            <Field name="training_center" component={renderColorSelector} dynamic_values={final_TrainingCenter} onChange={this.handleChange} label="Training Center" />
                                                        </FormGroup>
                                                    </div>
                                                </div> */}

                                                <Field name="batch_id" type="text" component={renderField} label="Batch ID" placeholder="Batch ID" onChange={this.handleChange} />
                                            {/* </div> */}

                                            {/* <div className="row"> */}

                                                

                                                <div className="col-12 col-md-4">
                                                    <div className="form-group">
                                                        <FormGroup>
                                                            <Field name="schemename" component={renderColorSelector} dynamic_values={final_schemes} onChange={this.handleChange} label="Scheme" />
                                                        </FormGroup>
                                                    </div>
                                                </div>

                                                {this.state.mode === 'Add' ?
                                                    this.state.showState &&
                                                    <div className="col-12 col-md-4">
                                                        <FormGroup>
                                                            <Field name="state_scheme_name" component={renderColorSelector} dynamic_values={final_allStates} onChange={this.handleChange} label="State Scheme" />
                                                        </FormGroup>
                                                    </div>
                                                    :
                                                    <div className={'col-12 col-md-4' + ((newFlag === 1 && (this.state.hideState === '' || this.state.hideState === 0)) || (newFlag === 0 && this.state.hideState === 0) ? '' : ' d-none')} >
                                                        <FormGroup>
                                                            <Field name="state_scheme_name" component={renderColorSelector} dynamic_values={final_allStates} onChange={this.handleChange} label="State Scheme" />
                                                        </FormGroup>
                                                    </div>
                                                }



                                                {!this.state.showState && central_minstry_name &&
                                                    <div className="form-group col-12 col-md-4">
                                                        <label>Ministry Name</label>
                                                        <div>
                                                            <input type="text" value={central_minstry_name} title={central_minstry_name} className="form-control" readOnly />
                                                        </div>
                                                    </div>
                                                }
                                            </div>
                                        {/* } */}
                                    </div>
                                    {/* {console.log(this.props," this.props ")}
                                                                {console.log(this.state," this.state ")} */}
                                    <hr className="inner-form-seperator" />
                                    <div className="card-header bg-transparent border-bottom-0 section-title no-border form-title">
                                        <h5 className="title card-title">Upload Documents</h5>
                                    </div>
                                    <div className="card-body ">
                                        <div className="action-btn-block container row">
                                            {/* <FileUploadCandidate uploadName="First" /> */}
                                            {/* <FileUploadCandidate uploadName="Second" className="ml-2"/> */}
                                            <div className="upload-btn-wrapper col-12 mt-2">
                                                 
                                                {this.state.initial_salary_first_month ?
                                                    <div>
                                                    <span > 1<sup>st</sup> Month Payslip </span>
                                                        <div className="btn col-4 btn-light btn-lg btn-raised" title="Download File">
                                                                <i className="material-icons" id="first_month_payslip" name="first" title="Remove File" onClick={this.handleChangeUploadButtonFirst}>close</i>
                                                                <a  href={URLforFirstMonthSalary+this.props.initialValues.salary_first}   >
                                                                    {this.props.initialValues.salary_first}
                                                                    
                                                                </a>
                                                        </div>
                                                    </div>
                                                    :
                                                    <div>
                                                        <button className="btn col-4 btn-secondary btn-lg btn-raised">
                                                            <i className="material-icons">arrow_upward</i>
                                                           1<sup>st</sup> Month Payslip
                                                        </button> 
                                                        <input type="file" className="col-4" name="salary_first" onChange={this.handleFileUpload} />
                                                    </div>
                                                }
                                                
                                                
                                                <div className={loaderClass} style={styles}></div>
                                                {!!this.state.salary_first &&
                                                    <p className="pmd-display-0 mt-2">Filename:<strong> {this.truncateFileName(this.state.salary_first.name)}  </strong></p>
                                                }
                                            </div>
                                            <div className="upload-btn-wrapper col-12 mt-2">
                                                {this.state.initial_salary_second_month ?
                                                    <div>
                                                        <span > 2<sup>nd</sup> Month Payslip </span> 
                                                        <div className="btn col-4 btn-light btn-lg btn-raised" title="Download File">
                                                                <i className="material-icons" id="second_month_payslip" name="second" title="Remove File" onClick={this.handleChangeUploadButtonSecond}>close</i>
                                                                <a  href={URLforSecondMonthSalary+this.props.initialValues.salary_second}   >
                                                                    {this.props.initialValues.salary_second}
                                                                </a>
                                                        </div>
                                                    </div>
                                                    :
                                                    <div>
                                                        <button className="btn col-4 btn-secondary btn-lg btn-raised">
                                                            <i className="material-icons">arrow_upward</i>
                                                            2<sup>nd</sup> Month Payslip
                                                            {/* <span>*</span> */}
                                                        </button>
                                                        <input type="file" className="col-4" name="salary_second" onChange={this.handleFileUpload} />
                                                    </div>
                                                }
                                                
                                                {/* <div className={loaderClass} style={styles}></div> */}
                                                {!!this.state.salary_second &&
                                                    <p className="pmd-display-0 mt-2">Filename:<strong> {this.truncateFileName(this.state.salary_second.name)} </strong></p>
                                                }
                                            </div>
                                            <div className="upload-btn-wrapper col-12 mt-2">
                                                {this.state.initial_salary_third_month ?
                                                    <div>
                                                        <span > 3<sup>rd</sup> Month Payslip </span> 
                                                        <div className="btn col-4 btn-light btn-lg btn-raised" title="Download File">
                                                            
                                                            <i className="material-icons" id="third_month_payslip" title="Remove File" onClick={this.handleChangeUploadButtonThird}>close</i>
                                                            <a  href={URLforThirdMonthSalary+this.props.initialValues.salary_third}   >
                                                                {this.props.initialValues.salary_third}
                                                            </a>
                                                        </div>
                                                    </div>
                                                    :
                                                    <div>
                                                        <button className="btn col-4 btn-secondary btn-lg btn-raised">
                                                            <i className="material-icons">arrow_upward</i>
                                                            3<sup>rd</sup> Month Payslip
                                                            {/* <span>*</span> */}
                                                        </button>
                                                        <input type="file" className="col-4" name="salary_third" onChange={this.handleFileUpload} />
                                                    </div>
                                                }
                                                

                                                {/* <div className={loaderClass} style={styles}></div> */}
                                                {!!this.state.salary_third &&
                                                    <p className="pmd-display-0 mt-2">Filename:<strong> {this.truncateFileName(this.state.salary_third.name)} </strong></p>
                                                }
                                            </div>
                                            {/* {this.state.show_appointment_letter_upload && */}
                                            <div className="upload-btn-wrapper col-12 mt-2">
                                                {this.state.initial_appointment_letter ?
                                                    <div>
                                                        <span> Appointment Letter </span> 
                                                        <div className="btn col-4 btn-light btn-lg btn-raised" title="Download File">
                                                            <i className="material-icons" id="appointment_letter" title="Remove File" onClick={this.handleChangeUploadAppointmentLetter}>close</i>
                                                            <a  href={URLforAppointmentLetter+this.props.initialValues.appointment_letter}>
                                                                {this.props.initialValues.appointment_letter}
                                                            </a>
                                                        </div>
                                                    </div>
                                                    :
                                                    <div>
                                                        <button className="btn col-4 btn-secondary btn-lg btn-raised">
                                                            <i className="material-icons">arrow_upward</i>
                                                            Appointment Letter
                                                            {/* <span>*</span> */}
                                                        </button>
                                                        <input className="col-4" type="file" name="appointment_letter" onChange={this.handleFileUpload} />
                                                    </div>
                                                }

                                                {/* <div className={loaderClass} style={styles}></div> */}
                                                {!!this.state.appointment_letter &&
                                                    <p className="pmd-display-0 mt-2">Filename:<strong> {this.truncateFileName(this.state.appointment_letter.name)} </strong></p>
                                                }
                                            </div>
                                            {/* } */}
                                        </div>
                                        <div className="row mb-2 mt-2">
                                            <div className="col-12 ml-0">Allowed File type .pdf, .doc, .docx</div>
                                        </div>
                                    
                                        <div className="action-btn-block">
                                            {/*    <button disabled={this.state.error || pristine}  className="btn btn-primary btn-raised mr-3" onClick={this.checkValidation} title="Save">Save</button> */}

                                            {(this.state.error_flag === 1 || this.state.error_jobrole === 1) ?
                                                <button disabled="disabled" className="btn btn-primary btn-raised mr-3" onClick={this.checkValidation} title="Save">Save</button>
                                                :
                                                <button className="btn btn-primary btn-raised mr-3" onClick={this.checkValidation} title="Save">Save</button>
                                            }
                                            <a title="Cancel" className="btn btn-outline" href="/candidates">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            {/* } */}
                            </form>
                        </div>
                    {/* } */}
                    {/*
                    <addCandidateForm 
                    {...this.props}
                    //initialValues={fields}
                    onSubmit={handleSubmit(this.handleSubmit)}
                    mode = 'add'
                  />*/
                    }
                    <Footer />
                </div>
            </div>
        );
    }
}
const fieldList = [
    // 'name',
    'firstName',
    'age',
    'alternate_id_type',
    'altername_id_no',
    'phone',
    'sex',
    // 'skills',
    // 'address',
    'preferred_city',
    'permanent_address',
    'permanent_statename',
    'permanent_districtname',
    'permanent_pincode',
    'present_address',
    'present_statename',
    'present_districtname',
    'present_pincode',
    'statename',
    'districtname',
    // 'aadhar_card_no',
    
    // 'pincode',
    // 'training_type',
    // 'nsdc_enrollment_no',
    // 'training_status',
    'sectorname',
    'jobrole',
    // 'training_center',
    // 'batch_id',
    // 'schemename'
]
AddEditCandidate = reduxForm(
    {
        form: 'AddEditCandidate',
        validate,
        enableReinitialize: true,
        onSubmitFail: submitFailure(fieldList)
    })(AddEditCandidate);

function mapStateToProps(state, props) {
    const { user } = state.authentication;
    const { candidateformdata } = state.trainingpartners; //From Reducer - trainingpartners function ...
    const { employerName } = state.employerForTC;
    const { districtlist } = state.generaldata; //From Reducer - CommonReducer function ...
    const { districtlistpresent } = state.presentdistrictdata; //From Reducer - CommonReducer function ...
    const { districtlistpreferred } = state.allpreffereddistrictdata; //From Reducer - CommonReducer function ...
    const { jobroles } = state.jobdata; //From Reducer - JobROleReducer function ...
    const { centralministriesdata } = state.centralministrydata; //From Reducer - CentralMinistry function ...
    const { candidate_updated_details } = state.getcandidatedetails; //From Reducer - 
    const { type, message } = state.alert; //From Alert reducer
    // const { message } = state;
    // console.log(employerName,"employer Names");
    // console.log(districtlistpreferred," districtlistpreferred");
    var formated_object = {};
    //console.log("in MapTOState", state);
    if (!!state.getcandidatedetails.candidate_updated_details) {
        var EditCandidateData = state.getcandidatedetails.candidate_updated_details.data[0];
        // console.log("Edit Details", EditCandidateData);

        if(EditCandidateData.experience_in_years > 0){
            EditCandidateData.experience_in_years = EditCandidateData.experience_in_years
        }else{
            EditCandidateData.experience_in_years = ''
        }
    //    (EditCandidateData.experience_in_years > 0) ? EditCandidateData.experience_in_years : "";
        //  if(EditCandidateData.schemeid==5)  this.props.showStates  = 1; 
        

        formated_object = {
            aadhar_card_no: EditCandidateData.aadhar_card_no,
            aadhaar_enrollment_id: EditCandidateData.aadhaar_enrollment_id,
            age: EditCandidateData.age,
            firstName: EditCandidateData.first_name,
            middleName: EditCandidateData.middle_name,
            lastName: EditCandidateData.last_name,
            skills: EditCandidateData.is_skilled,
            permanent_address: EditCandidateData.permanent_address,
            permanent_statename: EditCandidateData.permanent_stateID,
            permanent_districtname: EditCandidateData.permanent_cityID,
            permanent_pincode: EditCandidateData.permanent_pincode,
            same_as_permanent_initial: EditCandidateData.same_as_permanent,
            present_address: EditCandidateData.present_address,
            present_statename: EditCandidateData.present_stateID,
            present_districtname: EditCandidateData.present_cityID,
            present_pincode: EditCandidateData.present_pincode,
            edu_details: EditCandidateData.education_details,
            pass_out_date: EditCandidateData.pass_out_date,
            candidate_id: EditCandidateData.candidate_id,
            name: EditCandidateData.candidate_name,
            alternate_id_type: EditCandidateData.alternateidtype,
            altername_id_no: EditCandidateData.altername_id_no,
            phone: EditCandidateData.phone,
            email_address: EditCandidateData.email_address,
            sex: EditCandidateData.gender,
            is_willing_to_relocate: EditCandidateData.willing_to_relocate,
            preferred_city: EditCandidateData.preferred_city_value,
            address: EditCandidateData.address,
            statename: (state.alert.type !== undefined && (state.alert.message === constants.CANDIDATE_STATE_CITY_SELECT_VALIDATION || state.alert.message === constants.CANDIDATE_COMBINATION_STATECITY_SELECT_VALIDATION) ? '' : EditCandidateData.stateid),
            districtname: (state.alert.type !== undefined && (state.alert.message === constants.CANDIDATE_STATE_CITY_SELECT_VALIDATION || state.alert.message === constants.CANDIDATE_COMBINATION_STATECITY_SELECT_VALIDATION) ? '' : EditCandidateData.cityid),
            pincode: EditCandidateData.pincode,
            nsdc_enrollment_no: EditCandidateData.sdmsnsdc_enrollment_number,
            training_status: EditCandidateData.training_status,
            training_type: EditCandidateData.training_type,
            attendance_percent: EditCandidateData.attendence_percentage,
            placement_status: EditCandidateData.placement_status,
            employer_name: EditCandidateData.employer_id,
            employment_type: EditCandidateData.employment_type,
            assessment_score: EditCandidateData.assessment_score,
            max_assessment_score: EditCandidateData.max_assessment_score,
            sectorname: (state.alert.type !== undefined && (state.alert.message === constants.CANDIDATE_SECTOR_JOBROLE_SELECT_VALIDATION || state.alert.message === constants.CANDIDATE_COMBINATION_SECTORJOBROLE_SELECT_VALIDATION) ? '' : EditCandidateData.sectorid),
            jobrole: (state.alert.type !== undefined && (state.alert.message === constants.CANDIDATE_SECTOR_JOBROLE_SELECT_VALIDATION || state.alert.message === constants.CANDIDATE_COMBINATION_SECTORJOBROLE_SELECT_VALIDATION) ? '' : EditCandidateData.jobroleid),
            training_center: EditCandidateData.trainingcenter_id,
            batch_id: EditCandidateData.batch_id,
            schemename: EditCandidateData.schemeid,
            state_scheme_name: EditCandidateData.stateministryid,
            exp_in_years: EditCandidateData.experience_in_years,
            salary_first: EditCandidateData.salary_first,
            salary_second: EditCandidateData.salary_second,
            salary_third: EditCandidateData.salary_third,
            appointment_letter: EditCandidateData.appointment_letter,
        }
        // console.log(EditCandidateData.education_details," EditCandidateData.education_details ");
        // if(EditCandidateData.education_details){
        //     let education_details = EditCandidateData.education_details.split(',');
        //     education_details.forEach(element => {
        //         if(element === 'primary') formated_object = {...formated_object, primary:element };
        //         if(element === 'secondary') formated_object = {...formated_object, secondary:element };
        //         if(element === 'higherSecondary') formated_object = {...formated_object, higherSecondary:element };
        //         if(element === 'grad') formated_object = {...formated_object, grad:element };
        //         if(element === 'postGrad') formated_object = {...formated_object, postGrad:element };
                
        //     });
        // }

    }
    return {
        user, districtlist, districtlistpresent, jobroles, centralministriesdata, candidateformdata, employerName, candidate_updated_details, type, message,
        initialValues: formated_object,
        districtlistpreferred
    };
}
function mapDispatchToProps(dispatch, ownProps) {
    let candidate_idvalue = window.location.hash.split(":");
    //Verify candidate id and it that is true then only below step should proceed...
    var getData = "";
    if (localStorage.getItem('user') !== null) {
        if(JSON.parse(localStorage.getItem('user')).user_type !== 'TC'){
            history.push('/login');
            return ('');
        }
        if (candidate_idvalue[0] === '#id' && candidate_idvalue[1] !== "") {
            getData = dispatch(userActions.getCandidateDetails(candidate_idvalue[1]));
            return { getData }
        } else {
            return {}
        }
    }

    else {
        history.push('/login');
        return ('');
    }
}

const connectedAddEditCandidate = connect(mapStateToProps, mapDispatchToProps)(AddEditCandidate);
export { connectedAddEditCandidate as AddEditCandidate };